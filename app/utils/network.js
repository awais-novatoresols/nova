import {baseUrl} from "./constants";

const extractResult = async response => {
  // response.headers.forEach(console.log);
  // for(let entry of response.headers.entries()) {
  //   console.log(entry);
  // }
  // console.log("Returning success......", response);
  // console.log("response.headers......", response.headers);
  // console.log("response.headers......", response.headers.values());
  //console.log("Not 200", res);
  // if (res.status === 204) {
  //   return Promise.resolve("");
  // }
  // if (res.status !== 200) {
  //   console.log("Not 200", res);
  // }
  let resp = await response.json();
  // console.log("resp..............", resp);
  if (response.status >= 200 && response.status < 300 && resp.login) {
    // let resHeader = null;
    localStorage.setItem("clientId", response.headers.get("client"));
    localStorage.setItem("accessToken", response.headers.get("access-token"));
    localStorage.setItem("uid", response.headers.get("uid"));
    // resHeader = {
    //   client: response.headers.get("client"),
    //   "access-token": response.headers.get("access-token"),
    //   uid: response.headers.get("uid")
    // };
    // console.log("resHeader from network", resHeader);
  }
  if (resp.login === "true") {
    // let resHeader = null;
    localStorage.setItem("userName", resp.user.user_name);
    localStorage.setItem("userId", resp.user.id);
    localStorage.setItem("profileId", resp.user.profile.id);
    localStorage.setItem("clientId", response.headers.get("client"));
    localStorage.setItem("accessToken", response.headers.get("access-token"));
    localStorage.setItem("uid", response.headers.get("uid"));
  }
  return Promise.resolve(resp);
};
const extractError = err => {
  return Promise.reject(err);
};
const getAuthHeadersObj = (accessToken, clientId, uid, json) => {
  // console.log("token, uid, clientId", json, accessToken, uid, clientId);
  if (json !== undefined && accessToken) return {
    headers: {
      "access-token": accessToken,
      "client": clientId,
      "uid": uid,
      "Content-Type": "application/json",
      "mode": "cors"
    }
  };
  else if (accessToken) return {
    headers: {
      "access-token": accessToken,
      "client": clientId,
      "uid": uid,
      "mode": "cors"
    }
  };
  else return {
      headers: {
        "mode": "cors"
      }
    }
};
export const signUpApi = (url, user, accessToken, clientId, uid) => {
    for (let pair of user.entries()) {
      console.log(pair[0] + ", " + pair[1]);
    }
    let requestData = {
      method: "POST",
      body: user
    };
    if (accessToken) {
      requestData.headers = getAuthHeadersObj(accessToken, clientId, uid).headers;
    }
    const requestUrl = `${baseUrl}${url}`;
    // console.log("Request url:..............", requestUrl, requestData);
    return fetch(requestUrl, requestData)
      .then(extractResult)
      .catch(extractError);
  }
;
export const apiFetch = (url, accessToken, clientId, uid) => {
  let requestData = {};
  const requestUrl = `${baseUrl}${url}`;
  requestData.headers = getAuthHeadersObj(accessToken, clientId, uid).headers;
  // console.log("Request url:....", requestUrl, requestData);
  return fetch(requestUrl, requestData)
    .then(extractResult)
    .catch(extractError);
};
export const apiFetchPost = (url, postData, accessToken, clientId, uid, json) => {
  // console.log(postData, url, accessToken, clientId, uid, json, "JSON.stringify(body) and url from apipost....");
  let requestData;
  if (postData !== null) {
    requestData = {
      method: "POST",
      body: postData
    };
    if (json) requestData.body = JSON.stringify(postData);
  } else {
    requestData = {
      method: "POST",
    };
  }
  requestData.headers = getAuthHeadersObj(accessToken, clientId, uid, json).headers;
  const requestUrl = `${baseUrl}${url}`;
  return fetch(requestUrl, requestData)
    .then(extractResult)
    .catch(extractError);
};
export const apiFetchPatch = (url, body, token) => {
  let requestData = {
    method: "PATCH",
    body: JSON.stringify(body)
  };
  requestData.headers = getAuthHeadersObj(token).headers;
  const requestUrl = `${baseUrl}${url}`;
  // console.log("Request url:", requestUrl);
  return fetch(requestUrl, requestData)
    .then(extractResult)
    .catch(extractError);
};
export const apiFetchDelete = async (url, body, accessToken, clientId, uid) => {
  let requestData = {
    method: "DELETE",
    body
  };
  // console.log("Tokens ....", accessToken, clientId, uid);
  requestData.headers = getAuthHeadersObj(accessToken, clientId, uid).headers;
  const requestUrl = `${baseUrl}${url}`;
  return await fetch(requestUrl, requestData)
    .then(extractResult)
    .catch(extractError => alert("error", extractError));
};
export const apiFetchPut = (url, body, accessToken, clientId, uid, json) => {
  let requestData = {
    method: "PUT",
    body: body
  };
  if (json) requestData.body = JSON.stringify(body);
  requestData.headers = getAuthHeadersObj(accessToken, clientId, uid, json).headers;
  const requestUrl = `${baseUrl}${url}`;
  // console.log(requestUrl, body);
  return fetch(requestUrl, requestData)
    .then(extractResult)
    .catch(extractError);
};
