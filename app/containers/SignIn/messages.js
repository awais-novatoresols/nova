import { defineMessages } from 'react-intl';

export default defineMessages({
  signInTitle: {
    id: 'sign-in-title',
    defaultMessage: 'Sign In',
  },
  signInBody: {
    id: 'sign-in-text',
    defaultMessage: 'Welcome to ubwab'
  },
  signInEnd: {
    id: 'sign-in-end',
    defaultMessage: 'A Social DoorWay Platform'
  },
  enterEmailToSignIn: {
    id: 'enter-email-to-signIn',
    defaultMessage: 'Enter your email to Sign in'
  },
  signInInputEmail: {
    id: 'sign-in-input-email',
    defaultMessage: 'Email'
  },
  signInInputPassword: {
    id: 'sign-in-input-password',
    defaultMessage: 'Password'
  },
  signInForgotPassword: {
    id: 'sign-in-forgot-password',
    defaultMessage: 'Forget Password?'
  },
  signInButtonText: {
    id: 'sign-in-button-text',
    defaultMessage: 'Sign In'
  },
  signInWith: {
    id: 'sign-in-with',
    defaultMessage: 'Sign in with'
  },
  signInRegisterButton: {
    id: 'sign-in-register-button',
    defaultMessage: 'Register'
  },
  headerlinkNewsFeeds: {
    id: 'header-link-news-feed',
    defaultMessage: 'Communities',
  },
  headerlinkProfile: {
    id: 'header-link-profile',
    defaultMessage: 'Profile',
  },
  headerlinkCommunities: {
    id: 'header-link-communities',
    defaultMessage: 'Communities',
  },
});