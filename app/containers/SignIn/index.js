import React, {memo, useEffect} from "react";
import SideImage from "../../components/SideImage";
import InputForm from "./InputForm";
import "./style.css";
import "w3-css/4/w3pro.css";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import {connect} from "react-redux";
import {compose} from "redux";
import {loginUser} from "./actions";
import {createStructuredSelector} from "reselect";
import {makeSelectLoading, makeSelectUserData} from "./selectors";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import reducer from "./reducer";
import saga from "./Saga";
import PropTypes from "prop-types";
import {createUser} from "../SignUp/actions";
import {toast} from "react-toastify";
import {FormattedMessage} from 'react-intl';
import messages from './messages';
import ActionCableProvider from "react-actioncable-provider";

import ActionCable from "actioncable";

let userId = localStorage.getItem("userId");
const cable = ActionCable.createConsumer(`http://3.15.160.19/cable?id=${userId}`);


const key = "signIn";

export function SignIn(props) {
  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});

  let goProceed = (body) => {
    // console.log("login body", body);
    props.loginUser(body);
  };
  useEffect(() => {
    let accessToken = localStorage.getItem("accessToken");
    if (accessToken) {
      props.history.push("/homePage");
    } else {
      if (props.data.user.success) {
        localStorage.setItem("isLogin", "true");
        localStorage.setItem("userId", props.data.user.user.id);
        localStorage.setItem("userName", props.data.user.user.user_name);
        localStorage.setItem("profileId", props.data.user.user.profile.id);
        localStorage.setItem("profile_image", props.data.user.user.profile.profile_image);
        // alert(props.data.user.message);
        props.history.push("/homePage");
      } else if (props.data.user.message !== undefined && !props.data.user.success) {
        alert(props.data.user.message);
      } else if (props.data.user) {
        toast.success("Account created! You can now login!")
      }
    }
  }, [props.data.user]);

  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  
  function onFBLogin(res) {
    // console.log("From onFBLogin", res);
    let formData = new FormData();
    formData.append("user[email]", res.email);
    formData.append("user[user_name]", res.name);
    formData.append("user[provider]", "facebook");
    formData.append("user[facebook_id]", res.id);

    props.createUser(formData);
  }

  function onInstaLogin(res) {
    //  console.log("From onInstaLogin", res);
    let formData = new FormData();
    formData.append("user[email]", `${res.user.username}@instagram.com`);
    formData.append("user[user_name]", res.user.full_name);
    formData.append("user[provider]", "instagram");
    formData.append("user[facebook_id]", res.user.id);

    props.createUser(formData);
  }

  function onConnected() {
    console.log("onConnected from signIn")
  }

  return (

      <div>
        <SideImage
          title={<FormattedMessage {...messages.signInTitle} />}
          body={<FormattedMessage {...messages.signInBody} />}
          end={<FormattedMessage {...messages.signInEnd} />}
        />
        <InputForm goProceed={goProceed} onFBLogin={onFBLogin} onInstaLogin={onInstaLogin}/>
      </div>
  );
}

SignIn.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  history: PropTypes.object,
  loginUser: PropTypes.func,
  createUser: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  data: makeSelectUserData(),
  loading: makeSelectLoading()
});

export function mapDispatchToProps(dispatch) {
  return {
    loginUser: (body) => dispatch(loginUser(body)),
    createUser: body => dispatch(createUser(body))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(SignIn);
