import { LOAD_SUCCESS, LOG_IN } from "./constants";


export function loginUser(body) {
  return {
    type: LOG_IN,
    body
  };
}

export function dataLoaded(user) {
  return {
    type: LOAD_SUCCESS,
    user
  };
}
