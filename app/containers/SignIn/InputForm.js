import "bootstrap/dist/css/bootstrap.css";
import PropTypes from "prop-types";
import React from "react";
import FacebookLogin from 'react-facebook-login';
import InstagramLogin from 'react-instagram-login';
import { Link } from "react-router-dom";
import useInputForm from "../../components/useInputForm";
import email from "../../images/Email.png";
import logo from "../../images/logo.png";
import password from "../../images/Password.png";
import { toast } from "react-toastify";
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import "./style.css";

export default function InputForm(props) {
  let localLanguage = localStorage.getItem('language');
  const { inputs, handleInputChange, handleSubmit } = useInputForm({
    email: "",
    password: ""
  });

  const responseFacebook = res => {
   // console.log("Facebook Response is", res);
    if (res && res.id) {
      props.onFBLogin(res);
    } else toast.error("Unsuccessful Signin!");
  }

  const instaSuccess = res => {
   // console.log("Instagram Response", res);
    if(res && res.access_token){
      props.onInstaLogin(res);
    }else toast.error("Unsuccessful Signin!");
  }

  const instaError = error => {
    console.log("Error while insta Login", error);
  }

  let submitForm = () => {
    let formData = new FormData();
    formData.append("user[email]", inputs.email);
    formData.append("user[password]", inputs.password);
    formData.append("user[provider]", "email");
    // let reg = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    let reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (inputs.email && inputs.password) {
      if (reg.test(inputs.email.trim())) {
        if (inputs.password.length > 5) {
          props.goProceed(formData);
        } else {
          alert("Password must be at least 6 characters & digit ");
        }
      } else {
        alert("Your email is not correct");
      }
    } else {
      alert("Please fill all field");
    }
  };

  return (
    <div className="col-sm-6 login-form-2">
      <div className="sidenav1">
        <div className="text-center logo_img">
          <img src={logo} alt="true" />
        </div>
        <h3><FormattedMessage {...messages.enterEmailToSignIn} /></h3>
        <form onSubmit={handleSubmit} autoComplete="off">
          <div className="form-group">
            <div className="lang d-flex iconImage">
              <FormattedMessage {...messages.signInInputEmail}>
                {
                  placeholder =>
                    <>
                      <input type="text" className="input" placeholder={placeholder} name="email" onChange={handleInputChange} value={inputs.email} required />
                      <img src={email} alt="true" />
                    </>
                }
              </FormattedMessage>
            </div>
            <div className="line-box">
              <div className="line" />
            </div>
          </div>
          <div className="form-group">
            <div className="lang d-flex iconImage">
              <FormattedMessage {...messages.signInInputPassword}>
                {
                  placeholder =>
                    <>
                      <input type="Password" className="input" placeholder={placeholder} name="password" onChange={handleInputChange} value={inputs.password} required />
                      <img src={password} alt="true" />
                    </>
                }
              </FormattedMessage>
            </div>
            <div className="line-box">
              <div className="line" />
            </div>
          </div>
          <Link to="/ForgotPassword">
            <div className="form-group">
              <h6 className={`${localLanguage == 'ar' ? 'justify-content-start' : 'justify-content-end'} ForgetPwd d-flex`}>
                <FormattedMessage {...messages.signInForgotPassword} />
              </h6>
            </div>
          </Link>
          <div className="form-group d-flex justify-content-center">
            <button onClick={submitForm} type="submit" className="btnSubmit w-75 mt-2">
              <FormattedMessage {...messages.signInButtonText} />
            </button>
          </div>
          <h5><FormattedMessage {...messages.signInWith} /></h5>
          <div className="text-center mt-4 d-block">
            <div className="icon-circle">
              <FacebookLogin
                appId="549267839141720"
                callback={responseFacebook}
                cssClass="ifacebook border-0 bg-lightgray"
                icon="fa fa-facebook"
                textButton={null}
              />
              <InstagramLogin
                clientId="5fd2f11482844c5eba963747a5f34556"
                buttonText="Login"
                onSuccess={instaSuccess}
                onFailure={instaError}
                cssClass="iinstagram border-0 bg-lightgray ml-4"
              >
                <i className="fa fa-instagram"></i>
              </InstagramLogin>
              {/* <a  className="ifacebook" title="Facebook"><i className="fa fa-facebook"></i></a>
              <a  className="iinstagram" title="instagram"><i className="fa fa-instagram ml-4"></i></a> */}
            </div>
          </div>
          <Link className="remove_a" to="/SignUp"               >
            <div className="d-flex justify-content-center">
              <button type="button" className="btn btn-default w-75 p-2 mt-4 mb-3">
                <FormattedMessage {...messages.signInRegisterButton} />
              </button>
            </div>
          </Link>
        </form>
      </div>
    </div>
  );
}

InputForm.propTypes = {
  goProceed: PropTypes.func,
  onFBLogin: PropTypes.func,
  onInstaLogin: PropTypes.func
};
