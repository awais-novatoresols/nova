import { put, takeLatest } from "redux-saga/effects";
import { signUpApi } from "../../utils/network";
import { dataLoaded } from "./actions";
import { LOG_IN } from "./constants";

export function* loginUser(body) {

  try {
    const json = yield signUpApi("users/sign_in", body.body);
    // console.log("signUpApi.......",json);
    yield put(dataLoaded(json));
  } catch (error) {
    // console.log(error);
  }
}

export default function* userData() {
  yield takeLatest(LOG_IN, loginUser);
}
