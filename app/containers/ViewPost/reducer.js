import produce from "immer";
import {
  ADD_LIKE_COMMENT,
  CREATE_COMMENT,
  CREATE_COMMENT_SUCCESSFULLY,
  DELETE_CHILD_COMMENTS,
  DELETE_COMMENTS,
  GET_COMMENT_DETAIL,
  LIKE_POST,
  LOAD_COMMENT_SUCCESSFULLY,
  LOAD_ERROR,
  LOAD_POST_SUCCESSFULLY,
  PUSH_COMMENT,
  VIEW_COMMENT_DETAIL,
  VIEW_POST_DETAIL
} from "./constants";

const pushComment = (value, comment, childId, levelId) => {
//  console.log(value, comment, childId, levelId, 'value, comment, childId, levelId....? ');

  let parentComments = [...value];
  if (value.length === 0 && childId === undefined && levelId === undefined) {
    Array.prototype.push.apply(value, comment);
    return [...value, ...comment];
  } else {
    if (childId === "" && levelId === 3) {

      return [...comment, ...value];
    }
    const rootComment = parentComments.find(parentcom => {
      return parentcom.id === comment[0].root_id;
    });
    const newComment = {...rootComment};
    let findComment = false;
    // console.log("rootComment...", newComment);

    if (childId === null) {
      //  console.log("if childId", childId);
      findComment = true;
      return [...comment, ...value];
    }

    if (newComment.id === childId) {
      findComment = true;
      if (newComment.child_comment) {
        newComment.child_comment = [...comment, ...newComment.child_comment];
      } else {
        newComment.child_comment = [...comment];
      }
    }

    if (!findComment) {
      rootComment && rootComment.child_comment.map((child, index) => {
        if (child.id === childId) {
          findComment = true;
          if (newComment.child_comment[index].thirdLevel) {
            newComment.child_comment[index].thirdLevel = [...comment, ...newComment.child_comment[index].thirdLevel];
          } else {
            newComment.child_comment[index].thirdLevel = [...comment];
          }

        }
      });
    }
    if (!findComment) {
      rootComment && rootComment.child_comment.map((child, index) => {
        child.thirdLevel && child.thirdLevel.map((subChild, i) => {
          if (subChild.id === childId) {
            findComment = true;
            if (newComment.child_comment[index].thirdLevel[i].forthLevel) {
              newComment.child_comment[index].thirdLevel[i].forthLevel = [...comment, ...newComment.child_comment[index].thirdLevel[i].forthLevel];
            } else {
              newComment.child_comment[index].thirdLevel[i].forthLevel = [...comment];
            }

          }
        })
      })
    }
    if (!findComment) {
      rootComment && rootComment.child_comment.map((child, index) => {
        child.thirdLevel && child.thirdLevel.map((subChild, i) => {
          subChild.forthLevel && subChild.forthLevel.map((sub, ind) => {
            if (sub.id === childId) {
              findComment = true;
              if (newComment.child_comment[index].thirdLevel[i].forthLevel[ind].fifthLevel) {
                newComment.child_comment[index].thirdLevel[i].forthLevel[ind].fifthLevel = [...comment, ...newComment.child_comment[index].thirdLevel[i].forthLevel[ind].fifthLevel];
              } else {
                newComment.child_comment[index].thirdLevel[i].forthLevel[ind].fifthLevel = [...comment];
              }

            }
          })
        })
      })
    }
    const toReturn = parentComments.map(parentComment => {
      return parentComment.id === comment[0].root_id ? newComment : parentComment;
    });
    return toReturn;
  }
};

const addComment = (value, comment, childId, levelId, showMore) => {
 // console.log(value, comment, childId, levelId, value.length, 'value, comment, childId, levelId....? ');
  console.log("Show more", showMore);
  if (childId === undefined && levelId === undefined && !showMore) {
    let resultArray = _.orderBy(comment, ['created_at'], ['desc']);
    return [...resultArray];
  }
  if(showMore){
    let resultArray = _.orderBy(comment, ['created_at'], ['desc']);
    return [...value, ...resultArray]
  }
  // console.log(value, comment, childId, levelId, 'value, comment, childId, levelId) ');
  let parentComments = [...value];
  if (value.length === 0 && childId === undefined && levelId === undefined) {
    Array.prototype.push.apply(value, comment);
    return value;
  } else {
    const rootComment = parentComments.find(parentcom => {
      return parentcom.id === comment[0].root_id;
    });
    const newComment = {...rootComment};
    let findComment = false;

    rootComment && rootComment.child_comment.map((child, index) => {
      if (child.id === childId) {
        findComment = true;
        newComment.child_comment[index].thirdLevel = [...comment];
      }
    });
    if (!findComment) {
      rootComment && rootComment.child_comment.map((child, index) => {
        child.thirdLevel && child.thirdLevel.map((subChild, i) => {
          if (subChild.id === childId) {
            findComment = true;
            newComment.child_comment[index].thirdLevel[i].forthLevel = [...comment];
          }
        })
      })
    }
    if (!findComment) {
      rootComment && rootComment.child_comment.map((child, index) => {
        child.thirdLevel && child.thirdLevel.map((subChild, i) => {
          subChild.forthLevel && subChild.forthLevel.map((sub, ind) => {
            if (sub.id === childId) {
              findComment = true;
              newComment.child_comment[index].thirdLevel[i].forthLevel[ind].fifthLevel = [...comment];
            }

          })
        })
      })
    }
    const toReturn = parentComments.map(parentComment => {
      return parentComment.id === comment[0].root_id ? newComment : parentComment;
    });
    return toReturn;
  }
};

function deleteComment(value) {
  return value.slice(0, 5);
}

function deleteChildComment(value, childId, levelId) {
  let arrayCopy = JSON.parse(JSON.stringify(value));
  if (levelId === 2 && value !== undefined) {
    //  console.log(" deleteChildComment levelId.............", childId, levelId);
    value && value.map((child, index) => {
      child && child.child_comment && child.child_comment.map((subChild, i) => {
        if (subChild.id === childId) {
          // console.log("subChild............", childId, subChild.id, arrayCopy[index].child_comment[i].thirdLevel.slice(0, 3));
          arrayCopy[index].child_comment[i].thirdLevel = [];
        }
      })
    });
    return arrayCopy;
  } else if (levelId === 3 && value !== undefined) {
    let arrayCopy = JSON.parse(JSON.stringify(value));
    // console.log(" deleteChildComment levelId.............", childId, levelId);
    value && value.map((child, index) => {
      child && child.child_comment && child.child_comment.map((subChild, i) => {
        subChild && subChild.thirdLevel && subChild.thirdLevel.map((thirdChild, ind) => {
          if (thirdChild.id === childId) {
            //      console.log("subChild............3", childId, thirdChild.id);
            arrayCopy[index].child_comment[i].thirdLevel[ind].forthLevel = [];
          }
        })
      })
    });
    return arrayCopy;
  } else if (levelId === 4 && value !== undefined) {
    let arrayCopy = JSON.parse(JSON.stringify(value));
    //  console.log(" deleteChildComment levelId.............", childId, levelId);
    value && value.map((child, index) => {
      child && child.child_comment && child.child_comment.map((subChild, i) => {
        subChild && subChild.thirdLevel && subChild.thirdLevel.map((thirdChild, ind) => {
          thirdChild && thirdChild.forthLevel && thirdChild.forthLevel.map((sub, index1) => {
            if (sub.id === childId) {
              //           console.log("subChild............4", index, i, ind, index1);
              arrayCopy[index].child_comment[i].thirdLevel[ind].forthLevel[index1].fifthLevel = [];
            }
          })
        })
      })
    });
    return arrayCopy;
  } else {
    //  console.log("delete child comment...")
  }
}

function addLikes(comment, commentId, currentStatus, previousStatus) {
 // console.log("Comment, id, currentStatus, previousStatus ", comment, commentId, currentStatus, previousStatus);
  let ourStatus = currentStatus;
  let apiStatus = previousStatus === undefined ? 0 : previousStatus;
  if (previousStatus === currentStatus) ourStatus = 0;
  if (comment.id === commentId) {
    comment.vote_status = ourStatus;
    if (ourStatus === 1 && apiStatus === 0) {
      comment.count = comment.count + 1;
    } else if (ourStatus === 0 && apiStatus === 1) {
      comment.count = comment.count - 1;
    } else if (ourStatus === 2 && apiStatus === 0) {
      comment.count = comment.count - 1;
    } else if (ourStatus === 0 && apiStatus === 2) {
      comment.count = comment.count + 1;
    } else if (ourStatus === 2 && apiStatus === 1) {
      comment.count = comment.count - 2;
    } else if (ourStatus === 1 && apiStatus === 2) {
      comment.count = comment.count + 2;
    }
  }
  return comment;
}

function likeComment(value, data) {
  const {commentId, currentStatus, previousStatus, levelId} = data;
  //console.log("data from like comment...", value, commentId, currentStatus, previousStatus, levelId);
  let arrayCopy = JSON.parse(JSON.stringify(value));
  if (levelId === 1 && value !== undefined) {
   // console.log(" likeComment levelId.............", levelId);
    value && value.map((mainChild, index) => {
      if (mainChild.id === commentId) {
     //   console.log(" nested block.............", levelId);
        let result = addLikes(arrayCopy[index], mainChild.id, currentStatus, previousStatus);
      //  console.log("subChild............", result, levelId);
        arrayCopy[index] = result
      }
    });
    return arrayCopy;
  } else if (levelId === 2 && value !== undefined) {

    value && value.map((mainChild, index) => {
    //  console.log(" likeComment levelId.............", levelId);
      mainChild && mainChild.child_comment && mainChild.child_comment.map((subChild, i) => {
        if (subChild.id === commentId) {
          let result = addLikes(arrayCopy[index].child_comment[i], subChild.id, currentStatus, previousStatus);
      //    console.log("subChild............", result, levelId);
          arrayCopy[index].child_comment[i] = result
        }
      })
    });
    return arrayCopy;
  } else if (levelId === 3 && value !== undefined) {
    let arrayCopy = JSON.parse(JSON.stringify(value));
   // console.log(" likeComment levelId.............", levelId);
    value && value.map((child, index) => {
      child && child.child_comment && child.child_comment.map((subChild, i) => {
        subChild && subChild.thirdLevel && subChild.thirdLevel.map((thirdChild, ind) => {
          if (thirdChild.id === commentId) {
            let result = addLikes(arrayCopy[index].child_comment[i].thirdLevel[ind], thirdChild.id, currentStatus, previousStatus);
      //      console.log("subChild............", result, levelId);
            arrayCopy[index].child_comment[i].thirdLevel[ind] = result;
          }
        })
      })
    });
    return arrayCopy;
  } else if (levelId === 4 && value !== undefined) {
    let arrayCopy = JSON.parse(JSON.stringify(value));
  //  console.log(" likeComment levelId.............", levelId);
    value && value.map((child, index) => {
      child && child.child_comment && child.child_comment.map((subChild, i) => {
        subChild && subChild.thirdLevel && subChild.thirdLevel.map((thirdChild, ind) => {
          thirdChild && thirdChild.forthLevel && thirdChild.forthLevel.map((sub, index1) => {
            if (sub.id === commentId) {
              let result = addLikes(arrayCopy[index].child_comment[i].thirdLevel[ind].forthLevel[index1], sub.id, currentStatus, previousStatus);
              // console.log("subChild............", result, levelId);
              arrayCopy[index].child_comment[i].thirdLevel[ind].forthLevel[index1] = result;
            }
          })
        })
      })
    });
    return arrayCopy;
  } else if (levelId === 5 && value !== undefined) {
    let arrayCopy = JSON.parse(JSON.stringify(value));
    // console.log(" likeComment levelId.............", levelId);
    // console.log(" likeComment levelId.............", levelId);
    value && value.map((child, index) => {
      child && child.child_comment && child.child_comment.map((subChild, i) => {
        subChild && subChild.thirdLevel && subChild.thirdLevel.map((thirdChild, ind) => {
          thirdChild && thirdChild.forthLevel && thirdChild.forthLevel.map((fifthChild, index1) => {
            fifthChild && fifthChild.fifthLevel && fifthChild.fifthLevel.map((child, index12) => {
              if (child.id === commentId) {
                let result = addLikes(arrayCopy[index].child_comment[i].thirdLevel[ind].forthLevel[index1].fifthLevel[index12], child.id, currentStatus, previousStatus);
              //  console.log("subChild............", result, levelId);
                arrayCopy[index].child_comment[i].thirdLevel[ind].forthLevel[index1].fifthLevel[index12] = result;

              }
            })
          })
        })
      })
    });
    return arrayCopy;
  } else {
    //  console.log("delete child comment...")
  }
}

export const initialState = {
  loading: true,
  error: false,
  commentPages: 0,
  like: false,
  postDetail: {
    post: false
  },
  commentData: {
    comment: []
  },
  createComment: {
    comment: []
  },
  childId: "",
};
/* eslint-disable default-case, no-param-reassign */
const getPostDetailReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case VIEW_POST_DETAIL:
        draft.postDetail.post = true;
        draft.loading = true;
        draft.error = false;
        break;
      case LOAD_POST_SUCCESSFULLY:
        draft.postDetail.post = action;
        draft.loading = false;
        draft.error = false;
        break;
      case GET_COMMENT_DETAIL:
        draft.commentData.comment = [];
        break;
      case DELETE_COMMENTS:
        draft.commentData.comment = deleteComment(state.commentData.comment);
        break;
      case PUSH_COMMENT:
        draft.commentData.comment = pushComment(draft.commentData.comment, action.comment.comments, action.comment.childId, action.comment.levelId);
        draft.commentPages = action.comment.total_pages;
        draft.loading = false;
        draft.error = false;
        break;
      case ADD_LIKE_COMMENT:
        draft.commentData.comment = likeComment(draft.commentData.comment, action);
        draft.loading = false;
        draft.error = false;
        break;
      case DELETE_CHILD_COMMENTS:
        draft.commentData.comment = deleteChildComment(draft.commentData.comment, action.arrId, action.levelId);
        break;
      case VIEW_COMMENT_DETAIL:
        draft.commentData.comment = draft.commentData.comment.length === 0 ? [] : draft.commentData.comment;
        draft.loading = true;
        draft.error = false;
        break;
      case LOAD_COMMENT_SUCCESSFULLY:
        draft.commentData.comment = addComment(draft.commentData.comment, action.comment.comments, action.comment.childId, action.comment.levelId, action.showMore);
        draft.commentPages = action.comment.total_pages;
        draft.loading = false;
        draft.error = false;
        break;
      case CREATE_COMMENT:
        draft.createComment.comment = [];
        draft.loading = true;
        draft.error = false;
        break;
      case CREATE_COMMENT_SUCCESSFULLY:
        draft.createComment.comment = [];
        draft.createComment.comment = action.comment;
        draft.loading = false;
        draft.error = false;
        break;
      case LIKE_POST:
        draft.like = true;
        draft.loading = true;
        draft.error = false;
        break;
      case LOAD_ERROR:
        draft.error = action.error;
        draft.loading = false;
        break;
    }
  });
export default getPostDetailReducer;


