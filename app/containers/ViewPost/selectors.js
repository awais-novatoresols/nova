import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.viewPost || initialState;
const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );
const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );
const makePostDetailData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.postDetail
  );
const makePostCommentData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.commentData.comment
  );
const makeNewCommentData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.createComment.comment
  );
const makeCommentPagesData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.commentPages
  );
export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makePostDetailData,
  makePostCommentData,
  makeNewCommentData,
  makeCommentPagesData
};
