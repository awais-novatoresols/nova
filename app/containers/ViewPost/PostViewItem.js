import PropTypes from "prop-types";
import React from "react";
import Comments from "../../components/Comments";
import PostItem from "../../components/PostItem/PostItem";

const PostViewItem = (props) => {
  return (
    <div className="col-sm-7 mt-3">
      {props.data.post.post && props.data.post.post.posts &&
      <PostItem getLatestPost={props.getLatestPost} posts={{post: {posts: [props.data.post.post.posts]}}}
                createComment={props.createComment} {...props} />}
      <Comments
        hideComment={props.hideComment}
        pageNo={props.pageNo}
        {...props}
        getRepliesComment={props.getRepliesComment} createComment={props.createComment}
        comment={props.comment}
        likeAndDislikeComment={props.likeAndDislikeComment}
      />
    </div>
  );
};
PostViewItem.propTypes = {
  pageNo: PropTypes.number,
  data: PropTypes.object,
  comment: PropTypes.array,
  hideComment: PropTypes.func,
  getRepliesComment: PropTypes.func,
  createComment: PropTypes.func
};
export default PostViewItem;
