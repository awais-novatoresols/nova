import {
  ADD_LIKE_COMMENT,
  CREATE_COMMENT,
  CREATE_COMMENT_SUCCESSFULLY,
  DELETE_CHILD_COMMENTS,
  DELETE_COMMENTS,
  GET_COMMENT_DETAIL,
  LIKE_COMMENT,
  LOAD_COMMENT_SUCCESSFULLY,
  LOAD_POST_SUCCESSFULLY,
  PUSH_COMMENT,
  VIEW_COMMENT_DETAIL,
  VIEW_POST_DETAIL
} from "./constants";

export function viewPostData(userId, postId, clientId, accessToken, uid) {
  return {
    type: VIEW_POST_DETAIL,
    userId,
    postId,
    clientId,
    accessToken,
    uid
  };
}

export function viewPostLoaded(post) {
  return {
    type: LOAD_POST_SUCCESSFULLY,
    post
  };
}

export function postCommentData(postId, childId, pageNo, accessToken, clientId, uid, levelId, showMore) {
  console.log("Action show more", showMore)
  return {
    type: VIEW_COMMENT_DETAIL,
    postId,
    childId,
    pageNo,
    accessToken,
    clientId,
    uid,
    levelId,
    showMore
  };
}

export function postCommentLoaded(comment, showMore) {
  return {
    type: LOAD_COMMENT_SUCCESSFULLY,
    comment,
    showMore
  };
}

export function pushComment(comment) {
  return {
    type: PUSH_COMMENT,
    comment
  };
}

export function getCommentLoaded(comment) {
  return {
    type: GET_COMMENT_DETAIL,
    comment
  };
}

export function deleteComments(comment) {
  return {
    type: DELETE_COMMENTS,
    comment
  };
}

export function deleteChildComments(arrId, levelId) {
  return {
    type: DELETE_CHILD_COMMENTS,
    arrId,
    levelId
  };
}

export function createCommentPost(body, itemObj, accessToken, clientId, uid) {
  return {
    type: CREATE_COMMENT,
    body,
    itemObj,
    accessToken,
    clientId,
    uid
  };
}

export function createLikeOnComment(commentId, postData, accessToken, clientId, uid) {
  return {
    type: LIKE_COMMENT,
    commentId,
    postData,
    accessToken,
    clientId,
    uid
  };
}

export function addLikeOnComment(commentId, currentStatus, previousStatus, levelId) {
  return {
    type: ADD_LIKE_COMMENT,
    commentId,
    currentStatus,
    previousStatus,
    levelId
  };
}

export function createCommentSuccessfully(comment) {
  return {
    type: CREATE_COMMENT_SUCCESSFULLY,
    comment
  };
}


