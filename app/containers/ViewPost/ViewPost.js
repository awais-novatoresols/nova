import PropTypes from "prop-types";
import React from "react";
import CommunityCard from "../../components/CommunityCard";
import Header from "../../components/Header";
import PostViewItem from "./PostViewItem";

const ViewPost = (props) => {
  return (
    <div className="container-fluid mh-100 bg-light">
      <Header nav={props.Menu} {...props} />
      <div className="container p-0">
        <div className="row">
          <div className="col-sm-12">
            <div className="row p-3">
              <PostViewItem
                getLatestPost={props.getLatestPost}
                hideComment={props.hideComment}
                {...props}
                pageNo={props.pageNo}
                data={props.data} createComment={props.createComment}
                getRepliesComment={props.getRepliesComment}
                comment={props.comment}
                likeAndDislikeComment={props.likeAndDislikeComment}
              />
              <div className="col-sm-5">
                {/*<CreatePost />*/}
                <CommunityCard/>
                {/*<FooterLink />*/}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
ViewPost.propTypes = {
  Menu: PropTypes.array,
  pageNo: PropTypes.number,
  data: PropTypes.object,
  comment: PropTypes.array,
  getRepliesComment: PropTypes.func,
  getLatestPost: PropTypes.func,
  hideComment: PropTypes.func,
  createComment: PropTypes.func
};
export default ViewPost;
