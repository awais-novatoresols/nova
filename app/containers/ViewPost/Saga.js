import {put, takeLatest} from "redux-saga/effects";
import {apiFetch, apiFetchPost, signUpApi} from "../../utils/network";
import {addLikeOnComment, createCommentSuccessfully, postCommentLoaded, pushComment, viewPostLoaded} from "./actions";
import {ADD_LIKE_COMMENT, CREATE_COMMENT, LIKE_COMMENT, VIEW_COMMENT_DETAIL, VIEW_POST_DETAIL} from "./constants";
import * as _ from 'lodash';

export function* getPostDetail(userId) {
  try {
    const json = yield apiFetch(`users/${userId.userId}/posts/${userId.postId}`, userId.accessToken, userId.clientId, userId.uid);
    yield put(viewPostLoaded(json));
  } catch (error) {
    console.log(error);
  }
}

export function* getPostComments(data) {

  try {
    let json = yield apiFetch(`comments/${data.postId}?page_no=${data.pageNo}&parent_id=${data.childId}`, data.accessToken, data.clientId, data.uid);
    if (data.childId !== "") {
      json.childId = data.childId;
      json.levelId = data.levelId;
      yield put(postCommentLoaded(json, data.showMore));
    }
    if (data.childId === "") {
      yield put(postCommentLoaded(json, data.showMore));
    }
  } catch (error) {
    console.log(error);
  }
}

export function* createCommentOnPost(data) {
  let {itemObj} = data;
  const commentsData = [];
  commentsData.push(itemObj);

  const response = {
    comments: commentsData,
    current_page: "1",
    message: "success",
    success: true,
    total_pages: 1,
    childId: itemObj.parent_id,
    levelId: 3
  };
  let formData = true;
  //yield put(pushComment(response));
  try {
    const json = yield apiFetchPost(`comments`, data.body, data.accessToken, data.clientId, data.uid, formData);
    //   if(json.success){
    //     const getComments = yield apiFetch(`comments/${data.body.comment.post_id}?page_no=${data.pageNo}&parent_id=${''}`, data.accessToken, data.clientId, data.uid);
    //   if (data.body.childId && data.body.childId !== "") {
    //     getComments.childId = data.body.childId;
    //     getComments.levelId = data.body.levelId;
    //     yield put(postCommentLoaded(getComments));
    //   }
    //   if (data.body.childId === "" || data.body.childId === undefined) {
    //     yield put(postCommentLoaded(getComments));
    //   }
    // }
  } catch (error) {
    console.log(error);
  }
}

export function* voteForComment(data) {
  try {
    const json = yield signUpApi(`comments/${data.commentId}/vote_for_comment`, data.postData, data.accessToken, data.clientId, data.uid);
  } catch (error) {
    console.log(error);
  }
}

// export function* likeOnComment(data) {
//   console.log("data.....", data);
//   try {
//     // yield put(addLikeOnComment(data));
//   } catch (error) {
//     console.log(error);
//   }
// }

export default function* postDetail() {
  yield takeLatest(VIEW_POST_DETAIL, getPostDetail);
  yield takeLatest(VIEW_COMMENT_DETAIL, getPostComments);
  yield takeLatest(CREATE_COMMENT, createCommentOnPost);
  yield takeLatest(LIKE_COMMENT, voteForComment);
  // yield takeLatest(ADD_LIKE_COMMENT, likeOnComment);
}
