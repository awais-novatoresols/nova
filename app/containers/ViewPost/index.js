'use strict';
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import PropTypes from "prop-types";
import React, {memo, useEffect, useState} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import {createStructuredSelector} from "reselect";
import "w3-css/4/w3pro.css";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import {
  addLikeOnComment,
  createCommentPost,
  createLikeOnComment,
  deleteChildComments,
  deleteComments,
  postCommentData,
  viewPostData
} from "./actions";
import reducer from "./reducer";
import saga from "./Saga";
import {
  makeCommentPagesData,
  makeNewCommentData,
  makePostCommentData,
  makePostDetailData,
  makeSelectLoading
} from "./selectors";
import ViewPost from "./ViewPost";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const key = "viewPost";
const Menu = [
  {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
  {id: 3, name: <FormattedMessage {...messages.headerlinkGeneral}/>, path: "/general"},
  {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
];

export function ViewPostContainer(props) {
  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});

  const [pageNo, setPageNo] = useState(1);
  const [childId, setChildId] = useState("");
  let postId = props.match.params.id;
  const [levelId, setlevelId] = useState(1);
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let userName = localStorage.getItem("userName");
  let profileImage = localStorage.getItem("profile_image");
  let uid = localStorage.getItem("uid");
  let date = new Date().toISOString();
  let itemData = {
    "id": 0,
    "user_id": 8,
    "post_id": 77,
    "parent_id": "",
    "root_id": 0,
    "comment_text": "p6",
    "vote_status": 0,
    "child_comments": 10,
    "direct_indirect_comments": 20,
    "count": 0,
    "ancestry": null,
    "created_at": date,
    "comment_tags": [],
    "user": {
      "id": userId,
      "email": uid,
      "user_name": userName,
      "confirmed_at": "2018-06-10T07:07:00.000Z",
      "profile": {
        "id": userId,
        "name": userName,
        "country": "",
        "about": "hey Bro",
        "account_type": "private",
        "reputation": 4.63,
        "fcm": null,
        "created_at": "2019-10-09T08:29:01.548Z",
        "profile_image": profileImage,
        "background_image": "https://ubwab-heroku-stagging.s3.us-east-2.amazonaws.com/bgd1anyG8pWJUmp35NkyadJ7?response-content-disposition=inline%3B%20filename%3D%22photo-1541233349642-6e425fe6190e.jpeg%22%3B%20filename%2A%3DUTF-8%27%27photo-1541233349642-6e425fe6190e.jpeg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJWGQ5HMSTL7IPY7Q%2F20191029%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20191029T095541Z&X-Amz-Expires=300&X-Amz-SignedHeaders=host&X-Amz-Signature=7f4a5d5f27fdf97d5c5aee03590473a93184a1ad986c4840e2f93dc540a23b46"
      }
    },
  };

  useEffect(() => {
    props.viewPostData(userId, postId, clientId, accessToken, uid);
    props.postCommentData(postId, childId, pageNo, accessToken, clientId, uid, levelId);
  }, []);


  function createComment(childId, commentText, item) {
    let data;
    if (item.parent_id !== undefined) {
      let itemObj = {...item};
      // console.log("...itemData.user.user_name", itemObj);
      data = {
        ...itemObj,
        user: {
          ...itemObj.user,
          user_name: userName,
          profile: {
            ...itemObj.user.profile,
            profile_image: profileImage
          }
        },
        "created_at": date,
        parent_id: itemObj.id,
        count: 0,
        id: props.newComment.message && props.newComment.message.id,
        comment_text: commentText
      };
    } else {
      let itemObj = {...item};
      data = {
        ...itemData,
        "created_at": date,
        id: props.newComment.message && props.newComment.message.id,
        comment_text: commentText,
        count: 0,
        root_id: itemData.id + 10000000,
      };
    }

    let body = {
      "comment": {
        "post_id": postId,
        "community_id": " ",
        "parent_id": childId,
        "comment_text": commentText
      }
    };
    props.createCommentPost(body, data, accessToken, clientId, uid, pageNo);
    if (childId !== "") {
      props.viewPostData(userId, postId, clientId, accessToken, uid);
      getRepliesComment();
    } else {
      props.viewPostData(userId, postId, clientId, accessToken, uid);
    }
  }

  useEffect(() => {
    if (pageNo >= 1) props.postCommentData(postId, childId, pageNo, accessToken, clientId, uid, levelId, true);
  }, [pageNo, childId]);

  function getRepliesComment(c_dId, levelId) {
    // console.log("levelId...parentId", c_dId, levelId);
    if (levelId === 0) {
      setPageNo(pageNo + 1);
      c_dId = "";
      setChildId(c_dId);
      setlevelId(levelId);
    } else {
      setChildId(c_dId);
      setlevelId(levelId);
    }
  }

  function hideComment(arrId, levelId) {
    // console.log("hideComment...", arrId, levelId);
    // setPageNo(pageNo - 1);
    if (arrId !== undefined) {
      props.deleteChildComments(arrId, levelId);
    } else {
      props.deleteComments();
    }
  }

  function likeAndDislikeComment(commentId, currentStatus, previousStatus, e, levelId) {
    e.preventDefault();
    // console.log("Comment id, currentStatus, previousStatus levelId", commentId, currentStatus, previousStatus, levelId);
    let ourStatus = currentStatus;
    let apiStatus = previousStatus === undefined ? 0 : previousStatus;
    if (previousStatus === currentStatus) ourStatus = 0;
    let formData = new FormData();
    formData.append("current_status", ourStatus);
    formData.append("previous_status", apiStatus);
    //  console.log("commentId from index.....", commentId);
    props.createLikeOnComment(commentId, formData, accessToken, clientId, uid);
    props.addLikeOnComment(commentId, currentStatus, previousStatus, levelId);
  }
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);

  function getLatestPost() {
    // console.log("getLatestPost....");
    // let posts = props.posts.post;
    // if (posts && posts.current_page) {
    //   if (parseInt(posts.current_page) < parseInt(posts.total_pages)) {
    //     props.getUserPost(userId, clientId, accessToken, uid);
    //   }
    // }

  }

  return <ViewPost
    comment={props.comments && props.comments}
    getLatestPost={getLatestPost}
    hideComment={hideComment}
    getRepliesComment={getRepliesComment}
    createComment={createComment}
    likeAndDislikeComment={likeAndDislikeComment}
    data={props.data.post && props.data.post.message && props.data.post.post} Menu={Menu} pageNo={pageNo} {...props} />;
}

ViewPostContainer.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  comments: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  location: PropTypes.object,
  viewPostData: PropTypes.func,
  createComment: PropTypes.func,
  postCommentData: PropTypes.func,
  getRepliesComment: PropTypes.func,
  deleteComments: PropTypes.func,
  deleteChildComments: PropTypes.func,
  hideComment: PropTypes.func,
  createLikeOnComment: PropTypes.func,
  createCommentPost: PropTypes.func,
  addLikeOnComment: PropTypes.func,
  commentPages: PropTypes.number,
  match: PropTypes.object
};
const mapStateToProps = createStructuredSelector({
  data: makePostDetailData(),
  comments: makePostCommentData(),
  commentPages: makeCommentPagesData(),
  loading: makeSelectLoading(),
  newComment: makeNewCommentData()
});

export function mapDispatchToProps(dispatch) {
  return {
    viewPostData: (userId, postId, clientId, accessToken, uid) => dispatch(viewPostData(userId, postId, clientId, accessToken, uid)),
    postCommentData: (postId, c_dId, pageNo, accessToken, clientId, uid, levelId, showMore) => dispatch(postCommentData(postId, c_dId, pageNo, accessToken, clientId, uid, levelId, showMore)),
    createCommentPost: (body, itemObj, accessToken, clientId, uid) => dispatch(createCommentPost(body, itemObj, accessToken, clientId, uid)),
    deleteComments: () => dispatch(deleteComments()),
    deleteChildComments: (arrId, levelId) => dispatch(deleteChildComments(arrId, levelId)),
    createLikeOnComment: (commentId, postData, accessToken, clientId, uid) => dispatch(createLikeOnComment(commentId, postData, accessToken, clientId, uid)),
    addLikeOnComment: (commentId, currentStatus, previousStatus, levelId) => dispatch(addLikeOnComment(commentId, currentStatus, previousStatus, levelId))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(ViewPostContainer);
