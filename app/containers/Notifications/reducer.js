import produce from "immer";
import { GET_NOTIFICATIONS_SUCCESS } from "./constants";
// The initial state of the App
export const initialState = {
  success: false,
  notifications: {}
};
/* eslint-disable default-case, no-param-reassign */
const notificationReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
    case GET_NOTIFICATIONS_SUCCESS:
      draft.success = true;
      draft.notifications = action.data;
      break;
    default:
      draft.success = false;
    }
  });

export default notificationReducer;


