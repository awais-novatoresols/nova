import { put, takeLatest } from "redux-saga/effects";
import { apiFetch } from "../../utils/network";
import { getNotificationSuccess } from "./actions";
import { GET_NOTIFICATIONS } from "./constants";

export function* getNotifications(userId) {
  try {
    let json = yield apiFetch(`users/${userId.userId}/notifications`, userId.accessToken, userId.clientId, userId.uid);
    //console.log("Notifications Saga", json.notifications);
    yield put(getNotificationSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export default function* getNotificationsData() {
  yield takeLatest(GET_NOTIFICATIONS, getNotifications);
}
