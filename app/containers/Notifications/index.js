import PropTypes from "prop-types";
import React, {memo, useEffect} from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {compose} from "redux";
import {createStructuredSelector} from "reselect";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import {getUserNotifications} from "./actions";
import reducer from "./reducer";
import saga from "./Saga";
import {makeSelectNotifications, makeSelectNotificationsSuccess} from "./selectors";
import Header from "../../components/Header";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const Menu = [
  {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
  {id: 3, name: <FormattedMessage {...messages.headerlinkGeneral}/>, path: "/general"},
  {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
];

const key = "notifications";

export function Notifications(props) {
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});

  useEffect(() => {
    props.getNotifications(userId, accessToken, clientId, uid);
  }, []);


  function openChat(id) {
    id = (id === null || id === undefined) ? 1 : id;
    props.history.push('./chat')
  }

  return <>
    <div className="container-fluid mh-100 bg-light">
      <Header nav={Menu} {...props} />
      <div className="container mt-5">
        <div className="row pb-5">
          <div className="col-sm-12">
            <div className="row pt-md-5">
              <div className="col-sm-12 col-12">
                <h6 className="c_h pt-2">Notifications</h6>
              </div>
            </div>
            {props.notifications && props.notifications.length > 0 ? props.notifications.map(notify => {
                return <div className="d-block bg-white p-3 mb-3" key={notify.id}>
                  {
                    notify.notification_type === "like_dislike_post" &&
                    <Link to={`/post/${notify.post_id}`}><p>{notify.message}</p></Link>
                  }
                  {
                    notify.notification_type !== "like_dislike_post" &&
                    <Link to={`/post/${notify.post_id}`}><p>{notify.message}</p></Link>
                  }
                </div>
              })
              : <div>
                <h2>Nothing Yet</h2>
              </div>}
          </div>
        </div>
      </div>
    </div>
  </>;
}

Notifications.propTypes = {
  success: PropTypes.bool,
  following_user_id: PropTypes.number,
  notifications: PropTypes.array,
  getNotifications: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  notifications: makeSelectNotifications(),
  success: makeSelectNotificationsSuccess(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getNotifications: (userId, accessToken, clientId, uid) => dispatch(getUserNotifications(userId, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(Notifications);
