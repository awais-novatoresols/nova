import { GET_NOTIFICATIONS, GET_NOTIFICATIONS_SUCCESS } from "./constants";

export function getUserNotifications(userId, accessToken, clientId, uid) {
  return {
    type: GET_NOTIFICATIONS,
    userId,
    accessToken,
    clientId,
    uid
  };
}

export function getNotificationSuccess(data) {
  return {
    type: GET_NOTIFICATIONS_SUCCESS,
    data
  };
}