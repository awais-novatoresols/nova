import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.notifications || initialState;

const makeSelectNotificationsSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectNotifications = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.notifications.notifications
  );

export { selectGlobal, makeSelectNotificationsSuccess, makeSelectNotifications };