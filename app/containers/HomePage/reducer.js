import produce from "immer";
import {GET_FILTER_POSTS, GET_USER_POSTS, LOAD_ERROR, POST_LOAD_SUCCESS, REMOVE_POST} from "./constants";

// The initial state of the App
export const initialState = {
  loading: true,
  error: false,
  userPost: {
    post: false
  }

};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_USER_POSTS:
        draft.userPost.post = true;
        draft.loading = true;
        draft.error = false;
        break;
      case GET_FILTER_POSTS:
        draft.userPost.post = true;
        draft.loading = true;
        draft.error = false;
        break;
      case POST_LOAD_SUCCESS:
        draft.userPost.post = action.post;
        // console.log("Inside Reducer ..........", initialState.userPost.post, action.post.posts);
        // if(state.userPost.post && state.userPost.post.posts){
        //   console.log("Inside If Reducer ..........", state.userPost.post.posts, action.post.posts);
        //   draft.userPost.post = [...state.userPost.post.posts, ...action.post.posts];
        //   draft.userPost.post.current_page = action.post.current_page;
        // }else draft.userPost.post = action.post;
        draft.loading = false;
        draft.error = false;
        break;
      case REMOVE_POST:
        draft.userPost.post = false;
        draft.loading = false;
        break;
      case LOAD_ERROR:
        draft.error = action.error;
        draft.loading = false;
        break;
    }
    // console.log(' state.userPost.post.......', JSON.stringify( state.userPost.post));
  });

export default homeReducer;


