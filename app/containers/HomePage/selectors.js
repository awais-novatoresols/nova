import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.homepage || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectUserData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userData
  );
const makeSelectUserPost = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userPost
  );
export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectUserData,
  makeSelectUserPost
};
