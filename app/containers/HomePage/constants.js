export const GET_USER_PROFILE = "ubwab/home/GET_USER_PROFILE";
export const LOAD_SUCCESS = "ubwab/home/LOAD_SUCCESS";
export const GET_USER_POSTS = "ubwab/home/GET_USER_POSTS";
export const GET_FILTER_POSTS = "ubwab/home/GET_FILTER_POSTS";
export const POST_LOAD_SUCCESS = "ubwab/home/POST_LOAD_SUCCESS";
export const REMOVE_POST = "ubwab/home/REMOVE_POST";
export const LOAD_ERROR = "ubwab/home/LOAD_ERROR";
