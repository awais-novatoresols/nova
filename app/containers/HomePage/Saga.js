import { put, takeLatest } from "redux-saga/effects";
import { apiFetch } from "../../utils/network";
import { postLoaded } from "./actions";
import { GET_FILTER_POSTS, GET_USER_POSTS } from "./constants";

export function* getHomeData(userId) {
  try {
    const json = yield apiFetch(`users/${userId.userId}/posts/news_feed?page_no=${userId.pageNo}`, userId.accessToken, userId.clientId, userId.uid);
      // console.log("getHomeData...",json);
    yield put(postLoaded(json));
  } catch (error) {
    console.log(error);
  }
}

export function* getFilterData(userId) {

  try {
    const json = yield apiFetch(`users/${userId.userId}/posts/news_feed?${userId.filterType}=${userId.filter}`, userId.accessToken, userId.clientId, userId.uid);
    // console.log("getFilterData...",json);
    yield put(postLoaded(json));
  } catch (error) {
    console.log(error);
  }
}

export default function* userTimelineData() {
  yield takeLatest(GET_USER_POSTS, getHomeData);
  yield takeLatest(GET_FILTER_POSTS, getFilterData);
}
