import React, { memo, useEffect, useRef, useState } from "react";
import "./style.css";
import "../HomePage/style.css";
import "../../containers/Profile/globalStyle.css";
import "w3-css/4/w3pro.css";
import "bootstrap/dist/css/bootstrap.css";
import CreatePost from "../../components/CreatePost";
import "font-awesome/css/font-awesome.min.css";
import Header from "../../components/Header";
import PostItem from "../../components/PostItem/PostItem";
import CreatePostForm from "../../components/CreatePostForm";
import CommunityCard from "../../components/CommunityCard";
import FooterLink from "../../components/FooterLink";
import SearchDataItem from "../../components/SearchDataItem/SearchDataItem";
import SortBy from "../../components/SortBy";
import PropTypes from "prop-types";
import { createStructuredSelector } from "reselect";
import { makeSelectLoading, makeSelectUserPost } from "./selectors";
import { makeSelectAllUserCommunities } from "../../components/CommunityCard/selectors";
import { makeSelecSearchtData } from "../../components/Search/selector";
import { getFilterPost, getUserPost, postRemoved } from "./actions";
import { connect } from "react-redux";
import { compose } from "redux";
import { useInjectReducer } from "../../utils/injectReducer";
import reducer from "./reducer";
import { useInjectSaga } from "../../utils/injectSaga";
import saga from "./Saga";
import Spinner from "../../components/Spinner";
import { useBottomScrollListener } from "react-bottom-scroll-listener";
import { getUserProfile } from "../Profile/actions";
import { FormattedMessage } from "react-intl";
import messages from "./messages";

const key = "homepage";
let Menu = [
  {
    id: 3,
    name: <FormattedMessage {...messages.headerlinkGeneral} />,
    path: "/general"
  },
  {
    id: 2,
    name: <FormattedMessage {...messages.headerlinkCommunities} />,
    path: "/communities"
  },
];

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
}

export function HomePage(props) {
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  let profileId = localStorage.getItem("profileId");
  const [showModel, setShowModel] = useState(false);
  const [showSearchData, setSearchData] = useState(false);
  const [posts, setPosts] = useState([]);
  const [isSearchTab, setIsSearchTab] = useState(false);
  const [filterValue, setFilterValue] = useState("best");
  const [bottomLoading, setBottomLoading] = useState(false);
  const [searchFinished, setSearchFinished] = useState(true);
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  useBottomScrollListener(onBottomHit);
  const ref = useRef();
  let priFilterValue = "";
  const prevFilterValue = usePrevious(filterValue);

  function displayModel() {
    setShowModel(true);
  }

  function hideModel() {
    setShowModel(false);
  }

  function filterData(e) {
    setFilterValue(e.target.id);
    props.getFilterPost(e.target.id, true, userId, clientId, accessToken, uid);
  }

  function getLatestPost(pageNo = 1) {
    props.getUserPost(userId, pageNo, clientId, accessToken, uid);
  }
  function onBottomHit() {
    if (!showSearchData && searchFinished) {
      setBottomLoading(true);
      let posts = props.posts.post;
      if (posts && posts.current_page) {
        if (parseInt(posts.current_page) < parseInt(posts.total_pages)) {
          let pageNo = parseInt(posts.current_page) + 1;
          props.getUserPost(userId, pageNo, clientId, accessToken, uid);
        }
      }
    }
  }

  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  useEffect(() => {
    setIsSearchTab(false);
    if (filterValue !== "new") {
      if (!props.loading && !props.postSearch.success) {
        if (props.posts && props.posts.post && props.posts.post.posts) {
          let newObj = props.posts;
          if (posts.post && posts.post.posts) {
            let newPosts = [...posts.post.posts, ...newObj.post.posts];
            newObj.post.posts = newPosts;
          }
          setPosts(newObj);
          setBottomLoading(false);
        }
      }
    }
  }, [props.loading]);

  function getProfile() {
    props.getUserProfile(userId, profileId, accessToken, clientId, uid);
  }

  useEffect(() => {
    setIsSearchTab(false);

    props.getUserPost(userId, 1, clientId, accessToken, uid);
    if (Menu[0].id === 1) {
      Menu[0] = {
        id: 1,
        name: <FormattedMessage {...messages.headerlinkNewsFeeds} />,
        action: onNewsFeedNavClick
      };
    } else
      Menu.unshift({
        id: 1,
        name: <FormattedMessage {...messages.headerlinkNewsFeeds} />,
        action: onNewsFeedNavClick
      });
    getProfile();
  }, []);

  function openSideBar() {
    props.postRemoved();
    props.history.push("/profile");
  }

  function showSearchResult() {
    setSearchData(!showSearchData);
  }
  useEffect(() => {
    if (props.postSearch.success) {
      showSearchResult();
      setIsSearchTab(true);
    } else setIsSearchTab(false);
    () => {
      setSearchData(!showSearchData);
      setIsSearchTab(false);
    };
  }, [props.postSearch]);

  function onNewsFeedNavClick() {
    if (
      props.location.pathname === "/" ||
      props.location.pathname.toLowerCase() === "/homepage"
    ) {
      window.location.reload();
    } else {
      setIsSearchTab(false);
      props.getUserPost(userId, 1, clientId, accessToken, uid);
    }
  }

  return (
    <>
      <div className="container-fluid bg-light">
        <Header
          postRemoved={props.postRemoved}
          openSideBar={openSideBar}
          getLatestPost={getLatestPost}
          isSearch={true}
          nav={Menu}
          {...props}
          showSearchResult={showSearchResult}
          setSearchFinished={setSearchFinished}
          setSearchData={setSearchData}
        />
        <div className="container mb-4">
          <div className="row">
            <div className=" col-sm-7">
              {props.postSearch && isSearchTab && props.postSearch.success && (
                <SearchDataItem
                  data={props.postSearch}
                  getLatestPost={getLatestPost}
                />
              )}
              {!isSearchTab && posts && (
                <>
                  <SortBy filterData={filterData} />
                  <PostItem
                    getLatestPost={getLatestPost}
                    posts={props.posts && props.posts}
                    {...props}
                  />
                </>
              )}
            </div>
            <div className="col-sm-5 sdie_right">
              {props.communities.post !== false && (
                <CreatePostForm
                  fetchPost={getLatestPost}
                  communities={props.communities}
                  showModel={showModel}
                  displayModel={displayModel}
                  hideModel={hideModel}
                />
              )}
              {props.communities.post !== false && (
                <CreatePost
                  showModel={showModel}
                  displayModel={displayModel}
                  hideModel={hideModel}
                />
              )}
              <CommunityCard />
              <FooterLink isShow={true} />
            </div>
          </div>
        </div>
      </div>
      {bottomLoading && <Spinner />}
    </>
  );
}

HomePage.propTypes = {
  history: PropTypes.object,
  posts: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  postSearch: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  communities: PropTypes.object,
  showSearchResult: PropTypes.func,
  getFilterPost: PropTypes.func,
  getLatestPost: PropTypes.func,
  getUserPost: PropTypes.func,
  loading: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  posts: makeSelectUserPost(),
  postSearch: makeSelecSearchtData(),
  communities: makeSelectAllUserCommunities(),
  loading: makeSelectLoading()
});

export function mapDispatchToProps(dispatch) {
  return {
    getUserProfile: (userId, profileId, accessToken, clientId, uid) =>
      dispatch(getUserProfile(userId, profileId, accessToken, clientId, uid)),
    getUserPost: (userId, pageNo, clientId, accessToken, uid) =>
      dispatch(getUserPost(userId, pageNo, clientId, accessToken, uid)),
    getFilterPost: (filterType, filter, userId, clientId, accessToken, uid) =>
      dispatch(
        getFilterPost(filterType, filter, userId, clientId, accessToken, uid)
      ),
    postRemoved: () => dispatch(postRemoved())
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(HomePage);
