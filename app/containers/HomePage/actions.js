import {GET_FILTER_POSTS, GET_USER_POSTS, POST_LOAD_SUCCESS, REMOVE_POST} from "./constants";


export function getUserPost(userId, pageNo, clientId, accessToken, uid) {
  return {
    type: GET_USER_POSTS,
    userId,
    pageNo,
    clientId,
    accessToken,
    uid
  };
}

export function getFilterPost(filterType, filter, userId, clientId, accessToken, uid) {
  return {
    type: GET_FILTER_POSTS,
    filterType,
    filter,
    userId,
    clientId,
    accessToken,
    uid
  };
}

export function postLoaded(post) {
  return {
    type: POST_LOAD_SUCCESS,
    post
  };
}

export function postRemoved() {
  return {
    type: REMOVE_POST
  };
}
