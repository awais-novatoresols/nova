/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.HomePage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Welcome Ubwab',
  },
  headerlinkNewsFeeds: {
    id: 'header-link-news-feed',
    defaultMessage: 'Home',
  },
  headerlinkProfile: {
    id: 'header-link-profile',
    defaultMessage: 'Profile',
  },
  headerlinkGeneral: {
    id: 'header-link-general',
    defaultMessage: 'General',
  },
  headerlinkCommunities: {
    id: 'header-link-communities',
    defaultMessage: 'Categories',
  },
});
