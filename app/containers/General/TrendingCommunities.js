import React, { memo, useEffect } from "react";
import "w3-css/4/w3pro.css";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import { connect } from "react-redux";
import { compose } from "redux";
import {
  getAllCommunities,
  getTrendingCommunities
} from "../../components/CommunityCard/actions";
import { createStructuredSelector } from "reselect";
import {
  makeSelectAllUserCommunities,
  makeSelectCommunities,
  makeSelectLoading
} from "../../components/CommunityCard/selectors";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import reducer from "../../components/CommunityCard/reducer";
import Slider from "./Slider";
import saga from "../../components/CommunityCard/Saga";
import PropTypes from "prop-types";
import messages from "../../components/CommunityCard/messages";
import { FormattedMessage } from "react-intl";

const key = "communityCard";

export function TrendingCommunities(props) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  useEffect(() => {
    if (!props.data.post.success)
      props.getTrendingCommunities(userId, clientId, accessToken, uid);

    if (props.data.post.success) {
      props.getAllCommunities(userId, clientId, accessToken, uid);
    }
  }, [props.data.post.success]);

  return (
    <div className="container mt-4">
      {" "}
      {props.data && (
        <>
          <div className="lang header p-0 pl-5 pr-5">
            <h2 className="p-3">
              <FormattedMessage {...messages.communityCardHeaderTextTrending} />
            </h2>
          </div>
          <Slider
            categories={
              props.data && props.data.post && props.data.post.communities
            }
          />
        </>
      )}
    </div>
  );
}

TrendingCommunities.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  history: PropTypes.object,
  getTrendingCommunities: PropTypes.func,
  getAllCommunities: PropTypes.func,
  allUserCommunities: PropTypes.object,
  createUserPost: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  data: makeSelectCommunities(),
  loading: makeSelectLoading(),
  allUserCommunities: makeSelectAllUserCommunities()
});

export function mapDispatchToProps(dispatch) {
  return {
    getTrendingCommunities: (userId, clientId, accessToken, uid) =>
      dispatch(getTrendingCommunities(userId, clientId, accessToken, uid)),
    getAllCommunities: (userId, clientId, accessToken, uid) =>
      dispatch(getAllCommunities(userId, clientId, accessToken, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(TrendingCommunities);
