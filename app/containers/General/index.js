import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import "w3-css/4/w3pro.css";
import { makeSelectAllUserCommunities } from "../../components/CommunityCard/selectors";
import Header from "../../components/Header";
import { makeSelecSearchtData } from "../../components/Search/selector";
import "../../containers/Profile/globalStyle.css";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { getFilterPost, getUserPost, postRemoved } from "../HomePage/actions";
import messages from "../HomePage/messages";
import reducer from "../HomePage/reducer";
import saga from "../HomePage/Saga";
import { makeSelectLoading, makeSelectUserPost } from "../HomePage/selectors";
import "../HomePage/style.css";
import { getUserProfile } from "../Profile/actions";
import Home from "./Home";
import TrendingCommunities from "./TrendingCommunities";

const key = "homepage";
function Index(props) {
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  let profileId = localStorage.getItem("profileId");
  const [showSearchData, setSearchData] = useState(false);
  const [searchFinished, setSearchFinished] = useState(true);
  const [isSearchTab, setIsSearchTab] = useState(false);
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  let Menu = [
    {
      id: 1,
      name: <FormattedMessage {...messages.headerlinkNewsFeeds} />,
      path: "/homePage"
    },
    {
      id: 3,
      name: <FormattedMessage {...messages.headerlinkGeneral} />,
      action: onNewsFeedNavClick
    },
    {
      id: 2,
      name: <FormattedMessage {...messages.headerlinkCommunities} />,
      path: "/communities"
    }
  ];
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName("lang");
      for (let i = 0; i < totalElements.length; i++) {
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  function getLatestPost(pageNo = 1) {
    props.getUserPost(userId, pageNo, clientId, accessToken, uid);
  }
  function getProfile() {
    props.getUserProfile(userId, profileId, accessToken, clientId, uid);
  }
  useEffect(() => {
    setIsSearchTab(false);
    props.getUserPost(userId, 1, clientId, accessToken, uid);
    getProfile();
  }, []);
  function openSideBar() {
    props.postRemoved();
    props.history.push("/profile");
  }
  function showSearchResult() {
    setSearchData(!showSearchData);
  }
  useEffect(() => {
    if (props.postSearch.success) {
      showSearchResult();
      setIsSearchTab(true);
    } else setIsSearchTab(false);
    () => {
      setSearchData(!showSearchData);
      setIsSearchTab(false);
    };
  }, [props.postSearch]);

  function onNewsFeedNavClick() {
    if (
      props.location.pathname.toLowerCase() === "/general"
    ) {
      window.location.reload();
    } else {
      setIsSearchTab(false);
      props.getUserPost(userId, 1, clientId, accessToken, uid);
    }
  }
  return (
    <>
      <Header
        postRemoved={props.postRemoved}
        openSideBar={openSideBar}
        getLatestPost={getLatestPost}
        isSearch={true}
        nav={Menu}
        {...props}
        showSearchResult={showSearchResult}
        setSearchFinished={setSearchFinished}
        setSearchData={setSearchData}
      />
      <TrendingCommunities />
      <Home
        searchFinished={searchFinished}
        isSearchTab={isSearchTab}
        setIsSearchTab={setIsSearchTab}
        showSearchData={showSearchData}
      />
    </>
  );
}

Index.propTypes = {
  history: PropTypes.object,
  posts: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  postSearch: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  communities: PropTypes.object,
  showSearchResult: PropTypes.func,
  getFilterPost: PropTypes.func,
  getLatestPost: PropTypes.func,
  getUserPost: PropTypes.func,
  loading: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  posts: makeSelectUserPost(),
  postSearch: makeSelecSearchtData(),
  communities: makeSelectAllUserCommunities(),
  loading: makeSelectLoading()
});

export function mapDispatchToProps(dispatch) {
  return {
    getUserProfile: (userId, profileId, accessToken, clientId, uid) =>
      dispatch(getUserProfile(userId, profileId, accessToken, clientId, uid)),
    getUserPost: (userId, pageNo, clientId, accessToken, uid) =>
      dispatch(getUserPost(userId, pageNo, clientId, accessToken, uid)),
    getFilterPost: (filterType, filter, userId, clientId, accessToken, uid) =>
      dispatch(
        getFilterPost(filterType, filter, userId, clientId, accessToken, uid)
      ),
    postRemoved: () => dispatch(postRemoved())
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(Index);
