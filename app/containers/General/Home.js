import React, { memo, useEffect, useRef, useState } from "react";
import "../HomePage/style.css";
import "../../containers/Profile/globalStyle.css";
import "w3-css/4/w3pro.css";
import "bootstrap/dist/css/bootstrap.css";
import CreatePost from "../../components/CreatePost";
import "font-awesome/css/font-awesome.min.css";
import Header from "../../components/Header";
import PostItem from "../../components/PostItem/PostItem";
import CreatePostForm from "../../components/CreatePostForm";
import CommunityCard from "../../components/CommunityCard";
import FooterLink from "../../components/FooterLink";
import SearchDataItem from "../../components/SearchDataItem/SearchDataItem";
import SortBy from "../../components/SortBy";
import PropTypes from "prop-types";
import { createStructuredSelector } from "reselect";
import { makeSelectLoading, makeSelectUserPost } from "../HomePage/selectors";
import { makeSelectAllUserCommunities } from "../../components/CommunityCard/selectors";
import { makeSelecSearchtData } from "../../components/Search/selector";
import { getFilterPost, getUserPost, postRemoved } from "../HomePage/actions";
import { connect } from "react-redux";
import { compose } from "redux";
import { useInjectReducer } from "../../utils/injectReducer";
import reducer from "../HomePage/reducer";
import { useInjectSaga } from "../../utils/injectSaga";
import saga from "../HomePage/Saga";
import Spinner from "../../components/Spinner";
import { useBottomScrollListener } from "react-bottom-scroll-listener";
import { getUserProfile } from "../Profile/actions";
import { FormattedMessage } from "react-intl";
import messages from "../HomePage/messages";

const key = "homepage";

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
}

export function HomePage(props) {
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  const [showModel, setShowModel] = useState(false);
  const [posts, setPosts] = useState([]);
  const [filterValue, setFilterValue] = useState("best");
  const [bottomLoading, setBottomLoading] = useState(false);
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  useBottomScrollListener(onBottomHit);

  function displayModel() {
    setShowModel(true);
  }

  function hideModel() {
    setShowModel(false);
  }

  function filterData(e) {
    setFilterValue(e.target.id);
    props.getFilterPost(e.target.id, true, userId, clientId, accessToken, uid);
  }

  function getLatestPost(pageNo = 1) {
    props.getUserPost(userId, pageNo, clientId, accessToken, uid);
  }
  function onBottomHit() {
    if (!props.showSearchData && props.searchFinished) {
      setBottomLoading(true);
      let posts = props.posts.post;
      if (posts && posts.current_page) {
        if (parseInt(posts.current_page) < parseInt(posts.total_pages)) {
          let pageNo = parseInt(posts.current_page) + 1;
          props.getUserPost(userId, pageNo, clientId, accessToken, uid);
        }
      }
    }
  }
  useEffect(() => {
    getLatestPost();
  }, [])
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  useEffect(() => {
    props.setIsSearchTab(false);
    if (filterValue !== "new") {
      if (!props.loading && !props.postSearch.success) {
        if (props.posts && props.posts.post && props.posts.post.posts) {
          let newObj = props.posts;
          if (posts.post && posts.post.posts) {
            let newPosts = [...posts.post.posts, ...newObj.post.posts];
            newObj.post.posts = newPosts;
          }
          setPosts(newObj);
          setBottomLoading(false);
        }
      }
    }
  }, [props.loading]);

  return (
    <>
      <div className="container-fluid bg-light">
        <div className="container mb-4">
          <div className="row">
            <div className=" col-sm-7">
              {props.postSearch && props.isSearchTab && props.postSearch.success && (
                <SearchDataItem
                  data={props.postSearch}
                  getLatestPost={getLatestPost}
                />
              )}
              {!props.isSearchTab && posts && (
                <>
                  <SortBy filterData={filterData} />
                  <PostItem
                    getLatestPost={getLatestPost}
                    posts={props.posts && props.posts}
                    {...props}
                  />
                </>
              )}
            </div>
            <div className="col-sm-5 sdie_right">
              {props.communities.post !== false && (
                <CreatePostForm
                  fetchPost={getLatestPost}
                  communities={props.communities}
                  showModel={showModel}
                  displayModel={displayModel}
                  hideModel={hideModel}
                />
              )}
              {props.communities.post !== false && (
                <CreatePost
                  showModel={showModel}
                  displayModel={displayModel}
                  hideModel={hideModel}
                />
              )}
              <CommunityCard />
              <FooterLink isShow={true} />
            </div>
          </div>
        </div>
      </div>
      {bottomLoading && <Spinner />}
    </>
  );
}

HomePage.propTypes = {
  history: PropTypes.object,
  posts: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  postSearch: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  communities: PropTypes.object,
  showSearchResult: PropTypes.func,
  getFilterPost: PropTypes.func,
  getLatestPost: PropTypes.func,
  getUserPost: PropTypes.func,
  loading: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  posts: makeSelectUserPost(),
  postSearch: makeSelecSearchtData(),
  communities: makeSelectAllUserCommunities(),
  loading: makeSelectLoading()
});

export function mapDispatchToProps(dispatch) {
  return {
    getUserProfile: (userId, profileId, accessToken, clientId, uid) =>
      dispatch(getUserProfile(userId, profileId, accessToken, clientId, uid)),
    getUserPost: (userId, pageNo, clientId, accessToken, uid) =>
      dispatch(getUserPost(userId, pageNo, clientId, accessToken, uid)),
    getFilterPost: (filterType, filter, userId, clientId, accessToken, uid) =>
      dispatch(
        getFilterPost(filterType, filter, userId, clientId, accessToken, uid)
      ),
    postRemoved: () => dispatch(postRemoved())
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(HomePage);