import PropTypes from "prop-types";
import React from "react";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const UpdateCommunityForm = props => {
  // console.log("Props", props);
  return (
    <>
      <div className="lang card mt-5 pb-4 custom_c">
        <form>
          <label className="w-100 pl-4 pr-3 pt-4">
            <p className="label-txt pl-2"><FormattedMessage {...messages.createCommunityName} /></p>
            <textarea className="form-control t_a" name="name" onChange={e => props.onInputChange(e, "name")} value={props.name ? props.name : ''}/>
          </label><hr />
          <label className="w-100 pl-4 pr-3">
            <p className="label-txt pl-2"><FormattedMessage {...messages.createCommunityDiscription} /></p>
            <textarea className="form-control t_a" name="description" onChange={e => props.onInputChange(e, "description")} value={props.description ? props.description : ''}/>
          </label><hr />
          <label className="w-100 pl-4 pr-3">
            <p className="label-txt pl-2"><FormattedMessage {...messages.createCommunityRule} /></p>
            <textarea className="form-control t_a" onChange={e => props.onInputChange(e, "rule")} value={props.rules ? props.rules : ''}/>
          </label>
          <hr />
          <label className="w-100 pl-4 pr-3">
            <p className="label-txt"><FormattedMessage {...messages.createCommunityCategory} /></p>
            {props.categories && props.categories.map(category => {
              return (<div className="form-check" key={category.id}>
                <label className="form-check-label">
                  <input type="radio" className="form-check-input" id="radio1" onChange={props.onCategoryChange} name="optradio" value={category.id} checked={props.category === category.id} />
                  <span className="pr-4">{category.name}</span>
                </label>
              </div>);
            })}
          </label>
        </form>
      </div>
      <button type="submit" className="btnsubmit p-2 float-right mt-3 mb-5 mt-lg-n4" onClick={props.onSubmit}>
      <FormattedMessage {...messages.createCommunityButtonTitleUpdate} /></button>
    </>
  )
}

UpdateCommunityForm.propTypes = {
  categories: PropTypes.array,
  onInputChange: PropTypes.func,
  onCategoryChange: PropTypes.func,
  onSubmit: PropTypes.func,
  name: PropTypes.string,
  description: PropTypes.string,
  rules: PropTypes.string,
  category: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
}

export default UpdateCommunityForm;
