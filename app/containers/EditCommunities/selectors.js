import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.updateCommunity || initialState;

const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectCategories = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.categories
  );

const makeSelectCommunity = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.community.community
  );

const makeSelectCommunitySuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.getCommunity
  );

export { selectGlobal, makeSelectCategories, makeSelectSuccess, makeSelectCommunity, makeSelectCommunitySuccess };

