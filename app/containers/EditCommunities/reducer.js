import produce from "immer";
import { UPDATE_COMMUNITY_SUCCESS, GET_CATEGORIES_SUCCESS, UPDATE_COMMUNITY, GET_COMMUNITY_SUCCESS } from "./constants";

// The initial state of the App
export const initialState = {
  success: false,
  loading: false,
  categories: [],
  community: {},
  getCommunity: false
};

/* eslint-disable default-case, no-param-reassign */
const updateCommunityReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_CATEGORIES_SUCCESS:
        draft.categories = action.data;
        break;
      case GET_COMMUNITY_SUCCESS:
        draft.community = action.data;
        draft.getCommunity = true;
        break;
      case UPDATE_COMMUNITY:
        draft.loading = true;
        break;
      case UPDATE_COMMUNITY_SUCCESS:
        draft.success = true;
        draft.loading = false;
        break;
      default:
        draft.success = false;
        draft.loading = false;
    }
  });

export default updateCommunityReducer;


