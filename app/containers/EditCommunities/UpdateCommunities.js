import PropTypes from "prop-types";
import React from "react";
import Cover from "../../components/Cover";
import CreatePost from "../../components/CreatePost";
import FooterLink from "../../components/FooterLink";
import Header from "../../components/Header";
import UpdateCommunityForm from "./UpdateCommunityForm";
import "./style.css";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const Menu = [
  { id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage" },
  { id: 3, name: <FormattedMessage {...messages.headerlinkGeneral}/>, path: "/general" },
  { id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities" }
];
const UpdateCommunities = props => {
  return (
    <div className="container-fluid mh-100 bg-light">
      <Header nav={Menu} {...props} />
      <div className="container p-0">
        <div className="col-sm-12">
          <h5 className="pt-4 text_1">Create New Community</h5>
        </div>
        <Cover
          cover={props.cover ? props.cover : require("../../images/placeholder2.jpg")}
          profilePic={props.profilePic ? props.profilePic : require("../../images/user.png")}
          onCoverImageChange={props.onCoverImageChange} onProfileImageChange={props.onProfileImageChange}
          editMode />
        <div className="row">
          <div className="col-sm-7 mt-4">
            <UpdateCommunityForm
              categories={props.categories} onInputChange={(e, type) => props.onInputChange(e, type)}
              onCategoryChange={props.onCategoryChange} onSubmit={props.onSubmit}
              name={props.name} description={props.description} rules={props.rules} category={props.category} />
          </div>
          <div className="col-sm-5 mt-5">
            <CreatePost />
            {/*<FooterLink />*/}
          </div>
        </div>
      </div>
    </div>
  );
};
UpdateCommunities.propTypes = {
  categories: PropTypes.array,
  onInputChange: PropTypes.func,
  onCategoryChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCoverImageChange: PropTypes.func,
  onProfileImageChange: PropTypes.func,
  cover: PropTypes.string,
  profilePic: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string,
  rules: PropTypes.string,
  category: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
};
export default UpdateCommunities;
