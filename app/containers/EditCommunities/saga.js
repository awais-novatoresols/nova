import { put, takeLatest } from "redux-saga/effects";
import { apiFetch, signUpApi, apiFetchPut } from "../../utils/network";
import { updateCommunitySuccess, getCategoriesSuccess, getCommunitySuccess } from "./actions";
import { UPDATE_COMMUNITY, GET_CATEGORIES, GET_COMMUNITY } from "./constants";

function* getCategories(user) {
  try {
    const json = yield apiFetch(`categories`, user.accessToken, user.clientId, user.uid);
    // console.log("creteCommunities getCategories Saga...", json.categories);
    yield put(getCategoriesSuccess(json.categories));
  } catch (error) {
    // console.log(error);
  }
}

function* updateCommunity(user) {
  try {
    const json = yield apiFetchPut(`users/${user.userId}/communities/${user.communityId}`, user.formData, user.accessToken, user.clientId, user.uid);
    // console.log("creteCommunities Saga...", json);
    yield put(updateCommunitySuccess(json));
  } catch (error) {
    // console.log(error);
  }
}

function* getCommunity(user) {
  try {
    const json = yield apiFetch(`users/${user.userId}/communities/${user.communityId}`, user.accessToken, user.clientId, user.uid);
    //   console.log("viewCommunities Saga...", json);
    yield put(getCommunitySuccess(json));
  } catch (error) {
    // console.log(error);
  }
}

export default function* createCommunities() {
  yield takeLatest(GET_CATEGORIES, getCategories);
  yield takeLatest(UPDATE_COMMUNITY, updateCommunity);
  yield takeLatest(GET_COMMUNITY, getCommunity);
}
