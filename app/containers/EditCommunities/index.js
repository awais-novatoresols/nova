import PropTypes from "prop-types";
import React, {memo, useEffect, useState} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import {createStructuredSelector} from "reselect";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import {getCategories, getCommunity, updateCommunity} from "./actions";
import UpdateCommunities from "./UpdateCommunities";
import reducer from "./reducer";
import saga from "./saga";
import {makeSelectCategories, makeSelectCommunity, makeSelectCommunitySuccess, makeSelectSuccess} from "./selectors";
import {toast} from "react-toastify";

const key = "updateCommunity";
const UpdateCommunitiesContainer = props => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [rules, setRules] = useState("");
  const [category, setCategory] = useState("");
  const [cover, setCover] = useState();
  const [profilePic, setProfilePic] = useState();
  const [coverFile, setCoverFile] = useState();
  const [profilePicFile, setProfilePicFile] = useState();

  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});

  useEffect(() => {
    //console.log("header from profile", userId, clientId, accessToken, uid);
    props.getCategories(userId, clientId, accessToken, uid);
    props.getCommunity(userId, props.match.params.id, clientId, accessToken, uid);
  }, []);
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  useEffect(() => {
    if (props.getCommunitySuccess) {
      // console.log("Props", props.community.category);
      setCover(props.community.background_image);
      setProfilePic(props.community.profile_image);
      setName(props.community.name);
      setDescription(props.community.description);
      setRules(props.community.rules);
      setCategory(props.community.category);
    }
  }, [props.getCommunitySuccess]);

  useEffect(() => {
    if (props.success) {
      toast.success("Community Updated Successfully!");
      props.history.push("/communities");
    }
  }, [props.success]);

  const onInputChange = (e, type) => {
    //console.log(type, e.target.value);
    if (type === "name") setName(e.target.value);
    if (type === "description") setDescription(e.target.value);
    if (type === "rule") setRules(e.target.value);
  }

  const onCategoryChange = e => {
    //console.log("Selected ID", e.target.value);
    setCategory(e.target.value);
  }

  const onCoverImageChange = e => {
    setCover(URL.createObjectURL(e.target.files[0]));
    setCoverFile(e.target.files[0]);
  }

  const onProfileImageChange = e => {
    setProfilePic(URL.createObjectURL(e.target.files[0]));
    setProfilePicFile(e.target.files[0]);
  }

  const onSubmit = () => {
    //console.log("Values", name, description, rules, category);
    if (name && description && rules && category) {
      //console.log("Cover File", coverFile);
      //console.log("Profile File", profilePicFile);
      let formData = new FormData();
      formData.append('community[name]', name);
      formData.append('community[description]', description);
      formData.append('community[rules]', rules);
      formData.append('community[category_id]', category);
      if (profilePicFile) formData.append('community[profile_image]', profilePicFile);
      if (coverFile) formData.append('community[background_image]', coverFile);
      // formData.append('community[longitude]', "12.12");
      // formData.append('community[latitude]', "12.12");
      // formData.append('community[location]', "Qatar");

      props.updateCommunity(formData, userId, props.match.params.id, clientId, accessToken, uid);
      //console.log("Done");
    } else alert("All fields are required!!");
  }

  return <UpdateCommunities
    {...props} onInputChange={(e, type) => onInputChange(e, type)}
    onCategoryChange={onCategoryChange} onSubmit={onSubmit}
    onCoverImageChange={onCoverImageChange} onProfileImageChange={onProfileImageChange}
    cover={cover} profilePic={profilePic} name={name} description={description} rules={rules}
    category={category}/>;
}

UpdateCommunitiesContainer.propTypes = {
  categories: PropTypes.array,
  getCategories: PropTypes.func,
  createCommunity: PropTypes.func,
  success: PropTypes.bool,
  community: PropTypes.object,
  getCommunitySuccess: PropTypes.bool
};

const mapStateToProps = createStructuredSelector({
  categories: makeSelectCategories(),
  success: makeSelectSuccess(),
  community: makeSelectCommunity(),
  getCommunitySuccess: makeSelectCommunitySuccess()
});

export function mapDispatchToProps(dispatch) {
  return {
    getCommunity: (userId, communityId, clientId, accessToken, uid) => dispatch(getCommunity(userId, communityId, clientId, accessToken, uid)),
    getCategories: (userId, clientId, accessToken, uid) => dispatch(getCategories(userId, clientId, accessToken, uid)),
    updateCommunity: (formData, userId, communityId, clientId, accessToken, uid) => dispatch(updateCommunity(formData, userId, communityId, clientId, accessToken, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(UpdateCommunitiesContainer);
