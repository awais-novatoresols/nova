import { UPDATE_COMMUNITY, UPDATE_COMMUNITY_SUCCESS, GET_CATEGORIES, GET_CATEGORIES_SUCCESS, GET_COMMUNITY, GET_COMMUNITY_SUCCESS } from "./constants";

export function getCategories(userId, clientId, accessToken, uid) {
  return {
    type: GET_CATEGORIES,
    userId,
    clientId,
    accessToken,
    uid
  };
}

export function updateCommunity(formData, userId, communityId, clientId, accessToken, uid) {
  return {
    type: UPDATE_COMMUNITY,
    formData,
    userId,
    communityId,
    clientId,
    accessToken,
    uid
  };
}

export function getCategoriesSuccess(data) {
  return {
    type: GET_CATEGORIES_SUCCESS,
    data
  };
}

export function updateCommunitySuccess(data) {
  return {
    type: UPDATE_COMMUNITY_SUCCESS,
    data
  };
}

export function getCommunity(userId, communityId, clientId, accessToken, uid) {
  return {
    type: GET_COMMUNITY,
    userId,
    communityId,
    clientId,
    accessToken,
    uid
  };
}

export function getCommunitySuccess(data) {
  return {
    type: GET_COMMUNITY_SUCCESS,
    data
  };
}