export const GET_CATEGORIES = "ubwab/communities/edit/GET_CATEGORIES";
export const GET_CATEGORIES_SUCCESS = "ubwab/communities/edit/GET_CATEGORIES_SUCCESS";
export const UPDATE_COMMUNITY = "ubwab/communities/edit/UPDATE_COMMUNITY";
export const UPDATE_COMMUNITY_SUCCESS = "ubwab/communities/edit/UPDATE_COMMUNITY_SUCCESS";
export const GET_COMMUNITY = "ubwab/communities/edit/GET_COMMUNITY";
export const GET_COMMUNITY_SUCCESS = "ubwab/communities/edit/GET_COMMUNITY_SUCCESS";