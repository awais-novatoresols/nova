import React from "react";
import "w3-css/4/w3pro.css";
import "./style.css";
import PropTypes from "prop-types";
import "bootstrap/dist/css/bootstrap.css";
import logo from "../../images/logo.png";
// eslint-disable-next-line no-unused-vars

export default function AddCode(props) {

  function handleProceed(e) {
    e.preventDefault();
    props.handleSaveChanges();
  }

  return <div className="col-sm-6 login-form-2 , sidenav2">
    <div className="img-logo text-center">
      <img src={logo} alt="true"/>
    </div>
    <h3 className="p-4">we have sent you a email<br/>please confirm your email</h3>
    <form className="login-2">
      <div className="form-group d-flex justify-content-center mt-5">
        <button type="submit" className="btnSubmit mt-3" onClick={(e) => handleProceed(e)}>Proceed</button>
      </div>
      {/* <Link to="/HomePage">
        <div className="form-group-1 d-flex justify-content-center">
          <h6>Skip for now</h6>
        </div>
      </Link> */}
    </form>
  </div>;
}

AddCode.propTypes = {
  code: PropTypes.string,
  handleChange: PropTypes.func,
  handleSaveChanges: PropTypes.func,
};
