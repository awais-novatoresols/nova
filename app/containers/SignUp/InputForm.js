// eslint-disable-next-line no-unused-vars
import "bootstrap/dist/css/bootstrap.css";
import PropTypes from "prop-types";
import React, {useState} from "react";
import "w3-css/4/w3pro.css";
import useInputForm from "../../components/useInputForm";
import emailImage from "../../images/Email.png";
import logo from "../../images/logo.png";
import passwordImage from "../../images/Password.png";
import user from "../../images/user.png";
import {FormattedMessage} from 'react-intl';
import messages from './messages';
import "./style.css";

let openModal = () => document.getElementById("id01").style.display = "block";

export default function InputForm(props) {
  const [t_condition, getT_condition] = useState(false);
  const {inputs, handleInputChange, handleSubmit} = useInputForm({
    username: "",
    email: "",
    password: "",
    c_password: "",
    t_condition: ""
  });

  let submitForm = () => {
    let formData = new FormData();
    formData.append("user[email]", inputs.email);
    formData.append("user[user_name]", inputs.username);
    formData.append("user[password]", inputs.password);
    formData.append("user[password_confirmation]", inputs.c_password);
    formData.append("user[provider]", "email");
    // let reg = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    let reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (inputs.username && inputs.email && inputs.password && inputs.c_password && t_condition) {
      if (reg.test(inputs.email.trim())) {
        if (inputs.password.length > 5 && inputs.c_password.length > 5) {
          if (inputs.password === inputs.c_password) {
            props.goProceed(formData);
          } else {
            alert("Password must be same");
          }
        } else {
          alert("Password must be at least 6 characters & digit ");
        }
      } else {
        alert("Your email is not correct");
      }
    } else {
      alert("Please fill all field");
    }
  };
  let handleCheckboxChange = function (event) {
    getT_condition(event.target.checked);
  };
  return <div className="col-sm-6 login-form-2">
    <div className="sidenav2">
      <div className="text-center">
        <img src={logo} alt="true"/>
      </div>
      <h3 className=""><FormattedMessage {...messages.signUpTitle} /></h3>
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className="form-group mt-5">
          <div className="lang d-flex iconImage">
            <FormattedMessage {...messages.signUpInputUsername}>
              {
                placeholder => <>
                  <input className="input" type="text" placeholder={placeholder} name="username"
                         onChange={handleInputChange} value={inputs.username} required/>
                  <img src={user} alt="true"/>
                </>
              }
            </FormattedMessage>
          </div>
          <div className="line-box">
            <div className="line"/>
          </div>
        </div>
        <div className="form-group">
          <div className="lang d-flex iconImage">
            <FormattedMessage {...messages.signUpInputEmail}>
              {
                placeholder => <input type="text" className="input" placeholder={placeholder} name="email"
                                      onChange={handleInputChange} value={inputs.email} required/>
              }
            </FormattedMessage>
            <img src={emailImage} alt="true"/>
          </div>
          <div className="line-box">
            <div className="line"/>
          </div>
        </div>
        <div className="form-group">
          <div className="lang d-flex iconImage">
            <FormattedMessage {...messages.signUpInputPassword}>
              {
                placeholder => <input type="Password" className="input" placeholder={placeholder} name="password"
                                      onChange={handleInputChange} value={inputs.password} required/>
              }
            </FormattedMessage>
            <img src={passwordImage} alt="true"/>
          </div>

          <div className="line-box">
            <div className="line"/>
          </div>
        </div>
        <div className="form-group">
          <div className="lang d-flex iconImage">
            <FormattedMessage {...messages.signUpInputConfirmPassword}>
              {
                placeholder => <input type="Password" className="input" placeholder={placeholder} name="c_password"
                                      onChange={handleInputChange} value={inputs.c_password} required/>
              }
            </FormattedMessage>
            <img src={passwordImage} alt="true"/>
          </div>
          <div className="line-box">
            <div className="line"/>
          </div>
        </div>
        <div className="lang checkbox d-flex justify-content-center">
          <label>
            <input type="checkbox" value={t_condition} onChange={handleCheckboxChange} required/>&nbsp;
            <FormattedMessage {...messages.signUpIAccept} /> &nbsp;
            <a onClick={openModal} className="ahover">
              <FormattedMessage {...messages.signUpPrivacyPolicy} />
            </a>
          </label>
        </div>
        <div className="form-group d-flex justify-content-center">
          <button type="submit" onClick={submitForm} className="btnSubmit mt-3 w-75">
            <FormattedMessage {...messages.signUpButtonText} />
          </button>
        </div>
      </form>
    </div>
  </div>;
}
InputForm.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  goProceed: PropTypes.func
};
