import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import PropTypes from "prop-types";
import React, {memo, useEffect, useState} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import {createStructuredSelector} from "reselect";
import "w3-css/4/w3pro.css";
import SideImage from "../../components/SideImage";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import {createUser, editProfileData} from "./actions";
import AddCode from "./AddCode";
import InputForm from "./InputForm";
import reducer from "./reducer";
import saga from "./Saga";
import {makeSelectLoading, makeSelectUserData} from "./selectors";
import "./style.css";
import TermAndCondition from "./TermAndCondition";
import {FormattedMessage} from 'react-intl';
import messages from './messages';
import AddInformation from "./AddInformation";
import {toast} from "react-toastify";

const key = "signUp";

export function SignUp(props) {

  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});

  let userId = localStorage.getItem("userId");
  let profileId = localStorage.getItem("profileId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  // const [myValues, setMyValues] = useState(props.data);
  const [username, setUsername] = useState("");
  const [about, setAbout] = useState("");
  const [cover, setCover] = useState('');
  const [profile, setProfile] = useState('');
  const [isProceed, pressProceed] = useState(true);
  const [code, submitCode] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [country, setCountry] = useState('+974');
  const [isPrivate, setIsPrivate] = useState(true);
  // console.log("data from reducers...", props.data);

  let goProceed = (body) => {
    props.onCreateUsername(body);
  };

  useEffect(() => {
    // console.log("useEffect");
    if (props.data.user.success) {
      pressProceed(false);
    } else if (props.data.user.message !== undefined && !props.data.user.success) {
      alert(props.data.user.message);
    }
  }, [props.data.user]);

  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);

  function reqApi(formData, userId, profileId) {
    props.editProfileData(formData, userId, profileId, clientId, accessToken, uid);
    props.history.push("/");
  }

  function onProfileSubmit() {
    let formData = new FormData();
    let accountType = isPrivate ? 'private' : 'public';
    formData.append("profile[country]", country);
    formData.append("profile[account_type]", accountType);
    if (username !== "") formData.append("profile[name]", username);
    if (about !== "") formData.append("profile[about]", about);
    if (cover) formData.append("profile[background_image]", cover);
    if (profile) {
      formData.append("profile[profile_image]", profile);
      let url = URL.createObjectURL(profile);
      localStorage.setItem("profile_image", url);
    }
    if (username !== '' && cover !== '' && profile !== '' && country !== '') {
      props.editProfileData(formData, userId, profileId, clientId, accessToken, uid);
      props.history.push("/homePage");
    } else if(cover === '') {
      toast.error("Please Select Cover Image");
    } else if(profile === '') {
      toast.error("Please Select Profile Image");
    } else if(username === '') {
      toast.error("Please Enter Name");
    }
  };

  let handleChange = (event) => {
    submitCode(event.target.value);
  };

  function handleSaveChanges() {
    setShowModal(true);
  }

  function handleModalChange() {
    setShowModal(false);
  }

  function onCountrySelect(e) {
    setCountry(e.target.value);
  }

  function onTypeCheck(e) {
    setIsPrivate(!isPrivate);
  }

  const onInputChange = (name, e) => {
    if (name === "name") {
      setUsername(e.target.value);
    }
    if (name === "about") {
      setAbout(e.target.value);
    }
    if (name === "cover") {
      setCover(e.target.files[0]);
    }
    if (name === "profile") {
      setProfile(e.target.files[0]);
    }
  };
  return <>
    <SideImage
      title={<FormattedMessage {...messages.signUpTitle} />}
      body={<FormattedMessage {...messages.signUpBody} />}
      end={<FormattedMessage {...messages.signUpEnd} />}
    />

    {props.data.user && props.data.user.success && !isProceed ? <AddCode
      // onSubmitForm={onSubmitForm}
      code={code}
      handleChange={handleChange} handleSaveChanges={handleSaveChanges}/> : <InputForm goProceed={goProceed}/>
    }
    <AddInformation onChange={onInputChange} onSubmit={onProfileSubmit} showModal={showModal}
                    hideModal={handleModalChange} onCountrySelect={onCountrySelect} country={country}
                    onTypeCheck={onTypeCheck} name={username} about={about}/>
    <TermAndCondition/>
  </>;

}

SignUp.propTypes = {
  loading: PropTypes.bool,
  // error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  // data.user.message: PropTypes.string,
  onCreateUsername: PropTypes.func,
  editProfileData: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  data: makeSelectUserData(),
  loading: makeSelectLoading()
});


export function mapDispatchToProps(dispatch) {
  return {
    onCreateUsername: (body) => dispatch(createUser(body)),
    editProfileData: (formData, userId, profileId, clientId, accessToken, uid) => dispatch(editProfileData(formData, userId, profileId, clientId, accessToken, uid)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withConnect,
  memo
)(SignUp);
