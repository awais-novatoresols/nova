import React from 'react';
import {Link} from 'react-router-dom';
import {Modal} from 'react-bootstrap';
import CountriesList from "../../components/CountriesList";
import InputField from "../../components/InputField";

function AddInformation(props) {
  return (
    <Modal
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={props.showModal} onHide={props.hideModal}>
      <Modal.Body>
        <div className="w3-center"><br/>
          <span onClick={props.hideModel} className="w3-button w3-xlarge w3-hover-red w3-display-topright"
                title="Close Modal">&times;</span>
        </div>
        <div className="container-fluid">
          <div className="row mt-2">
            <div className="col-sm-8 mr-auto ml-auto mb-3">
              <h3 className="text-center">Edit Profile</h3>
              <div>
                <label className="">Upload Background image</label>
                <input type="file" className="form-control" onChange={e => props.onChange("cover", e)}/>
              </div>
              <div>
                <label className="mt-3">Upload profile image</label>
                <input type="file" className="form-control" onChange={e => props.onChange("profile", e)}/>
              </div>
              <form className="form-horizontal" role="form">
                <InputField title="Name " name="name" placeholder="Enter your name" type="text"
                            onChange={e => props.onChange("name", e)} value={props.name}/>
                <div className="form-group">
                  <label className="control-label">About:</label>
                  <textarea rows="5" className="form-control" onChange={e => props.onChange("about", e)}
                  value={props.about} placeholder="Optional"/>
                </div>
                <div>
                  <label className="mt-3">Country</label>
                  <CountriesList onCountrySelect={props.onCountrySelect}
                                 selected={props.country !== "undefined" ? props.country : false}/>
                </div>
                <div className="checkbox d-flex mt-3">
                  <input type="checkbox" className="mt-2" data-toggle="toggle" data-onstyle="success"
                         data-offstyle="info" onChange={props.onTypeCheck}/>
                  <label className="ml-3">Public profile Decide whether to show the communities your profile or not.By default your profile is private </label>
                </div>
                <div className="row mt-3">
                  <div className="col-md-6">
                    <div className="form-group">
                        <button type="button" className="btn btn-info btn_res" onClick={props.onSubmit}>Save
                          Changes
                        </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default AddInformation;
