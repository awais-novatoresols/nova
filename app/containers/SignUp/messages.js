import { defineMessages } from 'react-intl';

export default defineMessages({
  signUpTitle: {
    id: 'sign-Up-title',
    defaultMessage: 'Sign Up',
  },
  signUpBody: {
    id: 'sign-up-text',
    defaultMessage: 'Welcome to ubwab'
  },
  signUpEnd: {
    id: 'sign-up-end',
    defaultMessage: 'A Social DoorWay Platform'
  },
  signUpInputUsername: {
    id: 'sign-up-input-username',
    defaultMessage: 'Username'
  },
  signUpInputEmail: {
    id: 'sign-up-input-email',
    defaultMessage: 'Email'
  },
  signUpInputPassword: {
    id: 'sign-up-input-password',
    defaultMessage: 'Password'
  },
  signUpInputConfirmPassword: {
    id: 'sign-up-input-confirm-password',
    defaultMessage: 'Confirm Password'
  },
  signUpIAccept: {
    id: 'sign-up-i-accept',
    defaultMessage: 'I Accept'
  },
  signUpPrivacyPolicy: {
    id: 'sign-up-input-privacy-policy',
    defaultMessage: 'Privacy Terms and Conditions'
  },
  signUpButtonText: {
    id: 'sign-up-button-text',
    defaultMessage: 'Proceed'
  }
});
