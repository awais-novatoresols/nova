export const SIGN_UP = "ubwab/Sign_up/SIGN_UP";
export const LOAD_SUCCESS = "ubwab/Sign_up/LOAD_SUCCESS";
export const SIGNUP_EDIT_USER = "ubwab/Sign_up/SIGNUP_EDIT_USER";
export const LOAD_ERROR = "ubwab/Sign_up/LOAD_ERROR";
