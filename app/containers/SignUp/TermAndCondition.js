import React from "react";
import "w3-css/4/w3pro.css";
import "./style.css";
import "bootstrap/dist/css/bootstrap.css";

let closeModal = () => document.getElementById("id01").style.display = "none";


export default function TermAndCondition() {
  return <div className="w3-container">
    <div id="id01" className="w3-modal">
      <div id="custom_mc" className="w3-modal-content w3-card-4 w3-animate-zoom p-4">
        <div className="w3-center"><br/>
          <span
            onClick={closeModal}
            className="w3-button w3-xlarge w3-hover-red w3-display-topright"
            title="Close Modal">&times;</span>
        </div>
        <h3>Term and conditions</h3>
        <p>CPT Products as contained in the Programs and Services are provided “as is” without any
          liability to CureMD or the
          AMA, including without limitation, no liability for consequential or special damages, or lost
          profits for sequence,
          accuracy, or completeness of data, or that it will meet the Licensee’s requirements, and that
          the AMA’s sole
          responsibility is to make available to CureMD replacement copies of the CPT Products if the data
          is not intact; and
          that the AMA disclaims any liability for any consequences due to use, misuse, or interpretation
          of information
          contained or not contained in CPT Products.
          CPT Products if the data is not intact; and
          that the<br/><br/> AMA disclaims any liability for any consequences due to use, misuse, or
          interpretation of information
          contained or not contained in CPT Products.<br/>
          CPT Products if the data is not intact; and
          that the AMA disclaims any liability for any consequences due to use, misuse, or interpretation
          of information
          contained or not contained in CPT Products.
        </p>
      </div>
    </div>
  </div>;

}
