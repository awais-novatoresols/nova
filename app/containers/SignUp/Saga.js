import { put, takeLatest } from "redux-saga/effects";
import {apiFetchPut, signUpApi} from "../../utils/network";
import { SIGN_UP,SIGNUP_EDIT_USER } from "../SignUp/constants";
import { dataLoaded } from "./actions";
import {editProfileSuccess} from "../Profile/actions";

export function* createUser(body) {

  try {
    let formData1 = true;
    const json = yield signUpApi("users", body.body, formData1);
    yield put(dataLoaded(json));
  } catch (error) {
    console.log(error);
  }
}



function* editProfile(user) {
  try {
    const json = yield apiFetchPut(`users/${user.userId}/profiles/${user.profileId}`, user.data, user.accessToken, user.clientId, user.uid);
    yield put(editProfileSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export default function* userData() {
  yield takeLatest(SIGN_UP, createUser);
  yield takeLatest(SIGNUP_EDIT_USER, editProfile);
}
