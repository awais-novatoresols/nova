import { LOAD_SUCCESS, SIGN_UP,SIGNUP_EDIT_USER } from "./constants";


export function createUser(body) {
  return {
    type: SIGN_UP,
    body
  };
}

export function dataLoaded(user) {
  return {
    type: LOAD_SUCCESS,
    user
  };
}

export function editProfileData(data, userId, profileId, clientId, accessToken, uid) {
  return {
    type: SIGNUP_EDIT_USER,
    data,
    userId,
    profileId,
    clientId,
    accessToken,
    uid
  };
}
