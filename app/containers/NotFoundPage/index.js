/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React from 'react';
import Header from "../../components/Header";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Menu = [
  {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
  {id: 2, name: "MY COMMUNITIES", path: "/communities"},
];

export default function NotFound(props) {
  return (
    <>
      <Header nav={Menu} {...props} />
      <div className="row">
        <div className="col-md-12 text-center">
          <span className="d-block display-2 mt-5">Nothing yet</span>
          {/*<p className="lead mt-3">Something went wrong!</p>*/}
        </div>
      </div>
    </>
  );
}
