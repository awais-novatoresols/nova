/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage container.
 */
import { defineMessages } from "react-intl";

export const scope = "app.containers.NotFoundPage";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "Under Development!"
  },
  headerlinkNewsFeeds: {
    id: 'header-link-news-feed',
    defaultMessage: 'Communities',
  },
  headerlinkProfile: {
    id: 'header-link-profile',
    defaultMessage: 'Profile',
  },
  headerlinkCommunities: {
    id: 'header-link-communities',
    defaultMessage: 'Communities',
  },
});
