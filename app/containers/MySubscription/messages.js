import { defineMessages } from 'react-intl';

export default defineMessages({
    footerLinkAbout: {
        id: 'footer-link-about',
        defaultMessage: 'About',
    },
    footerLinkPrivacyPolicy: {
        id: 'footer-link-privacy-policy',
        defaultMessage: 'Privacy policy',
    },
    footerLinkFAQs: {
        id: 'footer-link-faqs',
        defaultMessage: 'FAQs',
    },
    footerLinkContentPolicy: {
        id: 'footer-link-content-policy',
        defaultMessage: 'Content Policy',
    },
    footerLinkApp: {
        id: 'footer-link-app',
        defaultMessage: 'App',
    },
    footerUserAgreement: {
        id: 'footer-user-agreement',
        defaultMessage: 'User Agreement',
    },


    headerLinkNewsFeed: {
        id: 'header-link-news-feed',
        defaultMessage: 'News Feed',
    },
    headerLinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Communities',
    },
    headerLinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
    },
    headerLanguageDropDownEn: {
        id: 'header-language-dropdown-en',
        defaultMessage: 'EN',
    },
    headerLanguageDropDownAr: {
        id: 'header-language-dropdown-ar',
        defaultMessage: 'AR',
    },
    creatPostCardHeading:{
        id:"creat-post-card-text-heading",
        defaultMessage:"ubwab"
    },
    creatPostCardDiscription:{
        id:"creat-post-card-text-discription",
        defaultMessage:"Ubwab is a open door to all social life style, you can get connected with your best line communities people."
    },
    creatPostCardButtonTitle:{
        id:"creat-post-card-button-title",
        defaultMessage:"Creat new post"
    },

    imageDetailPostHeaderLinkCommunities:{
        id:"image-detail-post-header-link-my-communities",
        defaultMessage:"My Communities"
    },

    profileNavBarProfile:{
        id:"profile-nav-bar-profile",
        defaultMessage:"Profile"
    },
    profileNavBarSaved:{
        id:"profile-nave-bar-saved",
        defaultMessage:"Saved"
    },
    profileNavBarHistory:{
        id:"profile-nave-bar-history",
        defaultMessage:"History"
    },
    profileNavBarInbox:{
        id:"profile-nave-bar-inbox",
        defaultMessage:"Inbox"
    },
    profileNavBarJoinedList:{
        id:"profile-nave-bar-joined-list",
        defaultMessage:"Joined List"
    },
    profileNavBarSettings:{
        id:"profile-nave-bar-settings",
        defaultMessage:"Settings"
    },
    profileNavBarLogOut:{
        id:"profile-nave-bar-logout",
        defaultMessage:"Logout"
    },
    profileTextReputation:{
        id:"profile-text-reputation",
        defaultMessage:"Reputaion"
    },
    profileTextUbwabAge:{
        id:"profile-text-ubwab-age",
        defaultMessage:"ubwab age"
    },
    profileTextFollowers:{
        id:"profile-text-followers",
        defaultMessage:"Followers"
    },
    profileTextFollowings:{
        id:"profile-text-followings",
        defaultMessage:"Followings"
    },
    editProfileModalTitle:{
        id:"edit-profile-model-title",
        defaultMessage:"Edit Profile"
    },
    editProfileModalLableBackgroundImage:{
        id:"edit-profile-model-lable-background-image",
        defaultMessage:"Upload background image"
    },
    editProfileModalInputButtonTitleBackgroundImage:{
        id:"edit-profile-model-input-button-title-background-image",
        defaultMessage:"Choose file"
    },editProfileModalInputPlaceholderBackgroundimage:{
        id:"edit-profile-model-input-placeholder-background-image",
        defaultMessage:"No file choosen"
    },editProfileModalLableProfileImage:{
        id:"edit-profile-model-lable-profile-image",
        defaultMessage:"Upload profile image"
    },editProfileModalInputButtonTitleProfileImage:{
        id:"edit-profile-model-input-button-title-profile-image",
        defaultMessage:"Choose file"
    },editProfileModalInputPlaceHolderprofileImage:{
        id:"edit-profile-model-input-placeholder-profile-image",
        defaultMessage:"No file choosen"
    },editProfileModalLabelName:{
        id:"edit-profile-model-label-name",
        defaultMessage:"Name"
    },editProfileModalInputPlaceHolderName:{
        id:"edit-profile-model-input-placeholder-name",
        defaultMessage:"Enter your name"
    },editProfileModallableAbout:{
        id:"edit-profile-model-label-about",
        defaultMessage:"About"
    },editProfileModalLableCountryDropDown:{
        id:"edit-profile-model-label-country-country-drop-down",
        defaultMessage:"Country"
    },editProfileModalLableCheckBox:{
        id:"edit-profile-model-lable-checkbox",
        defaultMessage:"Public profile Decide whether to show the communities your profile or not.By default your profile is private"
    },
    headerlinkNewsFeeds: {
        id: 'header-link-news-feed',
        defaultMessage: 'News Feed',
      },
      headerlinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
      },
      headerlinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Communities',
      },

    

});