import React from "react";
import "./style.css";
import CreatePost from "../../components/CreatePost";
import FooterLink from "../../components/FooterLink";
import Header from "../../components/Header";
import NavBar from "../../components/NavBar";

const Menu = [
  { id: 1, name: "MY PROFILE", path: "/homepage" },
  { id: 2, name: "MY COMMUNITIES", path: "/communities" },
];

const MySubscription = props => {
  return (
    <div className="container-fluid p-0 bg-light mh-100">
      <Header nav={Menu} {...props} />
      <div className="container-fluid p-0">
        <div className="row">
          <NavBar />
          <div className="col-sm-10">
            <div className="row p-3">
              <div className="col-sm-7">
                <div className="row">
                  <div className="col-sm-4 col-6">
                    <img className="box_img border-0" src={require("../../images/car.jpg")} width="100%" height="100px" />
                    <div className="cot">
                      <h4 className="top-right">Subscribed</h4>
                      <h4 className="bottom-right custom_h4">Car Racing<br />Internal Combustion</h4>
                    </div>
                  </div>
                  <div className="col-sm-4 col-6">
                    <img className="box_img border-0" src={require("../../images/champ.jpg")} width="100%" height="100px" />
                    <div className="cot">
                      <h4 className="top-right">Subscribed</h4>
                      <h4 className="bottom-right custom_h4">Women football Final<br />The First Organized</h4>
                    </div>
                  </div>
                  <div className="col-sm-4 col-6 res_box">
                    <img className="box_img border-0" src={require("../../images/football.jpg")} width="100%" height="100px" />
                    <div className="cot">
                      <h4 className="top-right">Subscribed</h4>
                      <h4 className="bottom-right custom_h4">Champion<br />Paris republication Le</h4>
                    </div>
                  </div>
                  <div className="col-sm-4 col-6 mt-3">
                    <img className="box_img border-0" src={require("../../images/football2.jpg")} width="100%" height="100px" />
                    <div className="cot">
                      <h4 className="top-right">Subscribed</h4>
                      <h4 className="bottom-right custom_h4">Champion<br />Paris republication Le</h4>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-5">
                <CreatePost />
                {/*<FooterLink />*/}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MySubscription;
