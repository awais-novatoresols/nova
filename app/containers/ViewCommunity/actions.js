import {
  BLOCK_COMMUNITY_USER,
  DELETE_COMMUNITY_USER,
  GET_BLOCK_USERS,
  GET_COMMUNITY,
  GET_COMMUNITY_MEMBERS,
  GET_COMMUNITY_MEMBERS_SUCCESS,
  GET_COMMUNITY_POSTS,
  GET_COMMUNITY_POSTS_SUCCESS,
  GET_COMMUNITY_SUCCESS,
  GET_FILTER_POSTS,
  UPDATE_USER_ROLE,
  BLOCK_COMMUNITY_USER_SUCCESS,
  FETCH_DATA,
  UNBLOCK_COMMUNITY_USER
} from "./constants";
import {UNBLOCK} from "../../components/BlockedContent/constants";


export function updateCommunityUserRole(userId, communityId, formData, accessToken, clientId, uid) {
  return {
    type: UPDATE_USER_ROLE,
    userId,
    communityId,
    formData,
    accessToken,
    clientId,
    uid
  };
}


export function removeCommunityUser(userId, formData, accessToken, clientId, uid) {
  return {
    type: DELETE_COMMUNITY_USER,
    userId,
    formData,
    accessToken,
    clientId,
    uid
  };
}


export function getBlockUserList(userId, communityId, accessToken, clientId, uid) {
  return {
    type: GET_BLOCK_USERS,
    userId,
    communityId,
    accessToken,
    clientId,
    uid
  };
}


export function getCommunity(userId, communityId, clientId, accessToken, uid) {
  return {
    type: GET_COMMUNITY,
    userId,
    communityId,
    clientId,
    accessToken,
    uid
  };
}

export function getCommunitySuccess(data) {
  return {
    type: GET_COMMUNITY_SUCCESS,
    data
  }
}

export function getCommunityPosts(userId, communityId, clientId, accessToken, uid) {
  return {
    type: GET_COMMUNITY_POSTS,
    userId,
    communityId,
    clientId,
    accessToken,
    uid
  };
}

export function getCommunityPostsSuccess(data) {
  return {
    type: GET_COMMUNITY_POSTS_SUCCESS,
    data
  }
}

export function getCommunityMembers(userId, communityId, pageNo, clientId, accessToken, uid) {
  return {
    type: GET_COMMUNITY_MEMBERS,
    userId,
    communityId,
    pageNo,
    clientId,
    accessToken,
    uid
  };
}

export function getCommunityMembersSuccess(data) {
  return {
    type: GET_COMMUNITY_MEMBERS_SUCCESS,
    data
  }
}

export function getBlockedCommunityMembersSuccess(data) {
  return {
    type: BLOCK_COMMUNITY_USER_SUCCESS,
    data
  }
}


export function getFilterPost(filterType, filter, userId, communityId, clientId, accessToken, uid) {
  return {
    type: GET_FILTER_POSTS,
    filterType,
    filter,
    userId,
    communityId,
    clientId,
    accessToken,
    uid
  };
}


export function blockCommunityUser(userId, formData, accessToken, clientId, uid) {
  return {
    type: BLOCK_COMMUNITY_USER,
    userId,
    formData,
    accessToken,
    clientId,
    uid
  };
}

export function fetchNewData(data) {
  return {
    type: FETCH_DATA,
    data
  }
}

export function unblockContent(userId, formData, accessToken, clientId, uid) {
  return {
    type: UNBLOCK_COMMUNITY_USER,
    userId,
    formData,
    accessToken,
    clientId,
    uid
  };
}
