import {createSelector} from "reselect";
import {initialState} from "./reducer";

const selectGlobal = state => state.viewCommunity || initialState;

const makeSelectCommunitySuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectCommunity = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.community.community
  );

const makeSelectCommunityPostsSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.postsSuccess
  );

const makeSelectCommunityPosts = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.communityPosts
  );

const makeSelectCommunityPinedPosts = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.pinnedPost
  );


const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectCommunityMembersObject = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.communityMembers
  );

const makeSelectCommunityAdmins = () =>
  createSelector(
    selectGlobal,
    globalState => {
      if (globalState.communityMembers && globalState.communityMembers.users) {
        return globalState.communityMembers.users.filter(user => user.profile.role === "admin" || user.profile.role === "moderator")
      }
      return [];
    }
  );

const makeSelectCommunityMembers = () =>
  createSelector(
    selectGlobal,
    globalState => {
      if (globalState.communityMembers && globalState.communityMembers.users) {
        return globalState.communityMembers.users.filter(user => user.profile.role === "user")
      }
      return [];
    }
  );

const makeSelectIsAdmin = () =>
  createSelector(
    selectGlobal,
    globalState => {
      if (globalState.communityMembers && globalState.communityMembers.users) {
        let admins = globalState.communityMembers.users.filter(user =>user.profile.role === "admin");

        let isAdmin = false;
        let loggedInUser = localStorage.getItem("userId");
        for (let i = 0; i < admins.length; i++) {
          if (admins[i].id == loggedInUser) isAdmin = true;
        }
        return isAdmin;
      }
      return false;
    }
  );


const makeSelectNewData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.isNewData
  );



export {
  selectGlobal,
  makeSelectCommunity,
  makeSelectIsAdmin,
  makeSelectCommunityAdmins,
  makeSelectCommunityMembers,
  makeSelectCommunityMembersObject,
  makeSelectCommunitySuccess,
  makeSelectCommunityPostsSuccess,
  makeSelectCommunityPosts,
  makeSelectLoading,
  makeSelectNewData,
  makeSelectCommunityPinedPosts
};

