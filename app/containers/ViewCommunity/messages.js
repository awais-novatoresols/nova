import { defineMessages } from "react-intl";

export default defineMessages({
  memberListBlockedUser: {
    id: "members-list-blocked-user",
    defaultMessage: "Blocked Users"
  },
  memberListAdmin: {
    id: "members-list-admin",
    defaultMessage: "Admin's"
  },
  memberListNoAdmin: {
    id: "members-list-no-admin",
    defaultMessage: "No Admin's!"
  },
  memberListMember: {
    id: "members-list-member",
    defaultMessage: "Members"
  },
  memberListNoMember: {
    id: "members-list-no-member",
    defaultMessage: "No Member's!"
  },
  viewCommunityMembers: {
    id: "view-community-members",
    defaultMessage: "Members"
  },
  viewCommunityModelRule: {
    id: "view-community-model-rule",
    defaultMessage: "Community Rule"
  },
  viewCommunityModelNoRule: {
    id: "view-community-model-no-rule",
    defaultMessage: "No Rules"
  },
  headerlinkNewsFeeds: {
    id: "header-link-news-feed",
    defaultMessage: "Home"
  },
  headerlinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  },
  headerlinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Categories"
  },
  headerlinkGeneral: {
    id: "header-link-general",
    defaultMessage: "General"
  }
});
