import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import messages from "./messages";

const CommunityDescription = props => {
  function viewMembers() {
    props.history.push(`${props.history.location.pathname}/members`);
  }

  return (
    <div className="lang card custom_cv mt-3">
      <div className="card-header d-flex custom_ch mt-4">
        <p className="custom_ps1">{props.name}</p>
        <span className="ml-auto mr-3 cursor_p" onClick={viewMembers}>
          {props.members}
          <FormattedMessage {...messages.viewCommunityMembers} />
        </span>
        <a
          className="cursor_p mt-3"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          <img className="c_img1" src={require("../../images/file.png")} />
        </a>
        {props.role === "admin" && (
          <Link
            to={`/communities/edit/${props.communityId}`}
            className="btn bg-ubwab mt-1"
            style={{ marginTop: "-10px", height: "38px" }}
          >
            <span className="fa fa-edit text-white" />
          </Link>
        )}
      </div>
      <div className="card-body p-0">
        <h5 className="p-3 alpha_h">{props.description}</h5>
      </div>
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <div className="row w-100 mr-2">
                <div className="col-md-11">
                  <h5 className="modal-title mt-1" id="exampleModalLabel">
                    <FormattedMessage {...messages.viewCommunityModelRule} />
                  </h5>
                </div>
                <div className="col-md-1">
                  <button
                    type="button"
                    className="close float-left"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
            </div>
            <div className="modal-body">
              <p>
                {props.rules === "" ? (
                  <FormattedMessage {...messages.viewCommunityModelNoRule} />
                ) : (
                  props.rules
                )}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

CommunityDescription.propTypes = {
  name: PropTypes.string,
  description: PropTypes.string,
  rules: PropTypes.string,
  history: PropTypes.object,
  role: PropTypes.string
};

export default CommunityDescription;
