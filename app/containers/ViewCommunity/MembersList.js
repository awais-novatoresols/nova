import PropTypes from "prop-types";
import React, {useEffect, useState} from "react";
import Header from "../../components/Header";
import Spinner from "../../components/Spinner";
import UserBar from "../../components/UserBar";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const Menu = [
  {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
  {id: 3, name: <FormattedMessage {...messages.headerlinkGeneral}/>, path: "/general"},
  {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
];
const MembersList = props => {
  let userId = localStorage.getItem("userId");
  const [isAdmin, SetIsAdmin] = useState([]);
  let localLanguage = localStorage.getItem('language');
  // console.log("props.members....",props.members);
  useEffect(() => {
    let isAdmin = props.admins !== undefined && props.admins.length > 0 && props.admins.filter(item => {
      return item.id == userId
    });
    SetIsAdmin(isAdmin);
  }, []);
  return (<>
    <div className="container-fluid p-0 mh-100 bg-light">
      <Header nav={Menu} {...props} />
      <div className="lang container mt-3">
        <div className="row">
          <div className="col-sm-12">
            {!props.isBLock && <>
              <div className="row">
                <div className="col-md-8">
                  <h5 className="pt-4 text1">{props.community ? `${props.community.name}` : ''}</h5>
                </div>
                {props.isAdmin && <div className="col-md-4">
                  <button onClick={props.getBlockList} type="button" className={localLanguage === 'ar' ? "btn bg-ubwab text-white mt-4 float-left" :"btn bg-ubwab text-white mt-4 float-right"}>
                  <FormattedMessage {...messages.memberListBlockedUser}/>
                  </button>
                </div>}
              </div>
              <p className="lead mt-3"><FormattedMessage {...messages.memberListAdmin}/></p>
              {
                props.admins.length > 0 ?
                  <div className="row">
                    {
                      props.admins.map(admin => {
                        return <UserBar admin={props.admins} key={admin.id} image={admin.profile.profile_image}
                                        admins={props.admins}
                                        name={admin.profile.name}
                                        id={admin.id}
                                        updateRole={props.updateRole}
                                        role={admin.profile.role}
                                        link={`/profile/${admin.id}/${admin.profile.id}`}/>
                      })
                    }
                  </div> : <span className="d-block mb-5"><FormattedMessage {...messages.memberListNoAdmin}/></span>
              }
            </>
            }

            {!props.isBLock ? <p className="lead"><FormattedMessage {...messages.memberListMember}/></p> : <p className="lead">Block Users</p>}
            {
              props.members.length > 0 ?
                <div className="row">
                  {
                    props.members.map(member => {
                      return <UserBar
                        admins={props.admins}
                        unBLockUser={props.unBLockUser}
                        isBLock={props.isBLock}
                        id={member.id}
                        blockThisUser={props.blockThisUser} deleteUser={props.deleteUser} updateRole={props.updateRole}
                        key={member.id} image={member.profile.profile_image} name={member.profile.name}
                        link={`/profile/${member.id}/${member.profile.id}`}/>
                    })
                  }
                </div> : <span className="d-block mb-5"><FormattedMessage {...messages.memberListNoMember}/></span>
            }
          </div>
        </div>
      </div>
    </div>
    {/* {props.loading && <Spinner/>} */}
  </>);
};

MembersList.propTypes = {
  community: PropTypes.object,
  isBLock: PropTypes.bool,
  loading: PropTypes.bool,
  location: PropTypes.object,
  admins: PropTypes.array,
  members: PropTypes.array,
  isAdmin: PropTypes.bool,
  unBLockUser: PropTypes.func,
  getBlockList: PropTypes.func,
  deleteUser: PropTypes.func,
  updateRole: PropTypes.func,
  blockThisUser: PropTypes.func,
}

export default MembersList;
