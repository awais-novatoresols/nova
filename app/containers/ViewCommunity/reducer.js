import produce from "immer";
import {
  BLOCK_COMMUNITY_USER_SUCCESS,
  FETCH_DATA,
  GET_COMMUNITY_MEMBERS_SUCCESS,
  GET_COMMUNITY_POSTS,
  GET_COMMUNITY_POSTS_SUCCESS,
  GET_COMMUNITY_SUCCESS
} from "./constants";

// The initial state of the App
export const initialState = {
  success: false,
  loading: false,
  community: {},
  communityPosts: {},
  pinnedPost: {},
  postsSuccess: false,
  communityMembers: {},
  isNewData: false
};

function appendMembers(prevMembers, newMembers) {
  if (prevMembers && prevMembers.success) {
    if (prevMembers.current_page >= newMembers.current_page) return newMembers;
    let newObject = {...prevMembers};
    newObject.users = [...prevMembers.users, ...newMembers.users];
    newObject.current_page = newMembers.current_page;
    return newObject;
  } else return newMembers;
}

/* eslint-disable default-case, no-param-reassign */
const communityReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_COMMUNITY_POSTS:
        draft.loading = true;
        break;
      case FETCH_DATA:
        draft.isNewData = action.data.success;
        break;
      case GET_COMMUNITY_SUCCESS:
        draft.success = true;
        draft.community = action.data;
        break;
      case GET_COMMUNITY_POSTS_SUCCESS:
        draft.postsSuccess = true;
        draft.communityPosts = action.data;
        draft.pinnedPost = action.data.pinned_post;
        draft.loading = false;
        break;
      // case GET_COMMUNITY_MEMBERS:
      //   draft.loading = true;
      //   break;
      case GET_COMMUNITY_MEMBERS_SUCCESS:
        draft.communityMembers = appendMembers(state.communityMembers, action.data);
        draft.loading = false;
        break;
      case BLOCK_COMMUNITY_USER_SUCCESS:
        draft.communityMembers = appendMembers(state.communityMembers, action.data);
        draft.loading = false;
        break;
      default:
        draft.success = false;
        draft.postsSuccess = false;
        draft.loading = false;
    }
  });

export default communityReducer;


