import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { toast } from "react-toastify";
import {
  blockCommunityUser,
  getBlockUserList,
  getCommunity,
  getCommunityMembers,
  removeCommunityUser,
  unblockContent,
  updateCommunityUserRole
} from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import {
  makeSelectCommunity,
  makeSelectCommunityAdmins,
  makeSelectCommunityMembers,
  makeSelectCommunityMembersObject,
  makeSelectCommunitySuccess,
  makeSelectIsAdmin,
  makeSelectLoading,
  makeSelectNewData
} from "./selectors";
import MembersList from "./MembersList";
import { useBottomScrollListener } from "react-bottom-scroll-listener";

const key = "viewCommunity";
const CommunityMembersList = props => {
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  const [isBLock, setBLock] = useState(false);
  const [isRole, setRole] = useState(false);
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  useBottomScrollListener(onBottomHit);

  useEffect(() => {
    props.getCommunity(
      userId,
      props.match.params.id,
      clientId,
      accessToken,
      uid
    );
    props.getCommunityMembers(
      userId,
      props.match.params.id,
      1,
      clientId,
      accessToken,
      uid
    );
  }, []);

  useEffect(() => {
    props.getCommunity(
      userId,
      props.match.params.id,
      clientId,
      accessToken,
      uid
    );
    props.getCommunityMembers(
      userId,
      props.match.params.id,
      1,
      clientId,
      accessToken,
      uid
    );
  }, [props.isNewData]);

  function onBottomHit() {
    //  console.log("Bottom Hit!!!");
    let page = props.communityMembersObject;
    if (page && page.current_page) {
      if (parseInt(page.current_page) < parseInt(page.total_pages)) {
        let pageNo = parseInt(page.current_page) + 1;
        props.getCommunityMembers(
          userId,
          props.match.params.id,
          pageNo,
          clientId,
          accessToken,
          uid
        );
      }
    }
  }

  function updateRole(id, role) {
    let formData = new FormData();
    formData.append("id", id);
    formData.append("role", role);
    props.updateCommunityUserRole(
      userId,
      props.match.params.id,
      formData,
      accessToken,
      clientId,
      uid
    );
    setRole(!isRole);
    toast.success("Make Moderator user successfully");
  }

  function deleteUser(u_id) {
    let formData = new FormData();
    formData.append("community_id", props.match.params.id);
    formData.append("community_user_id", u_id);
    props.removeCommunityUser(userId, formData, accessToken, clientId, uid);
    setRole(!isRole);
    toast.success("Delete user successfully");
  }

  function getBlockList() {
    setBLock(!isBLock);
    props.getBlockUserList(
      userId,
      props.match.params.id,
      accessToken,
      clientId,
      uid
    );
  }

  function blockThisUser(id) {
    let formData = new FormData();
    formData.append("community_id", props.match.params.id);
    formData.append("block_user_id", id);
    props.blockCommunityUser(userId, formData, accessToken, clientId, uid);
    setRole(!isRole);
    toast.success("Block  user successfully");
  }

  function unBLockUser(id) {
    let formData = new FormData();
    formData.append("community_id", props.match.params.id);
    formData.append("block_user_id", id);
    props.unblockContent(userId, formData, accessToken, clientId, uid);
    toast.success("Block  user successfully");
    // props.getBlockUserList(userId, props.match.params.id, accessToken, clientId, uid);
  }

  // console.log("props.members", props.members, props.communityMembersObject, props.isNewData);

  return (
    <div>
      <MembersList
        unBLockUser={unBLockUser}
        blockThisUser={blockThisUser}
        getBlockList={getBlockList}
        deleteUser={deleteUser}
        updateRole={updateRole}
        {...props}
        isAdmin={props.isAdmin}
        loading={props.loading}
        community={props.community}
        location={props.history.location}
        admins={props.admins}
        isBLock={isBLock}
        members={!isBLock ? props.members : props.communityMembersObject.users}
      />
    </div>
  );
};

CommunityMembersList.propTypes = {
  community: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  getCommunity: PropTypes.func,
  getCommunityMembers: PropTypes.func,
  success: PropTypes.bool,
  loading: PropTypes.bool,
  match: PropTypes.object,
  communityMembersObject: PropTypes.object,
  isAdmin: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  community: makeSelectCommunity(),
  success: makeSelectCommunitySuccess(),
  loading: makeSelectLoading(),
  members: makeSelectCommunityMembers(),
  admins: makeSelectCommunityAdmins(),
  communityMembersObject: makeSelectCommunityMembersObject(),
  isAdmin: makeSelectIsAdmin(),
  isNewData: makeSelectNewData()
});

export function mapDispatchToProps(dispatch) {
  return {
    getCommunity: (userId, communityId, clientId, accessToken, uid) =>
      dispatch(getCommunity(userId, communityId, clientId, accessToken, uid)),
    getCommunityMembers: (
      userId,
      communityId,
      pageNo,
      clientId,
      accessToken,
      uid
    ) =>
      dispatch(
        getCommunityMembers(
          userId,
          communityId,
          pageNo,
          clientId,
          accessToken,
          uid
        )
      ),
    updateCommunityUserRole: (
      userId,
      communityId,
      formData,
      accessToken,
      clientId,
      uid
    ) =>
      dispatch(
        updateCommunityUserRole(
          userId,
          communityId,
          formData,
          accessToken,
          clientId,
          uid
        )
      ),
    removeCommunityUser: (userId, formData, accessToken, clientId, uid) =>
      dispatch(
        removeCommunityUser(userId, formData, accessToken, clientId, uid)
      ),
    getBlockUserList: (userId, communityId, accessToken, clientId, uid) =>
      dispatch(
        getBlockUserList(userId, communityId, accessToken, clientId, uid)
      ),
    blockCommunityUser: (userId, formData, accessToken, clientId, uid) =>
      dispatch(
        blockCommunityUser(userId, formData, accessToken, clientId, uid)
      ),
    unblockContent: (userId, formData, accessToken, clientId, uid) =>
      dispatch(unblockContent(userId, formData, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(CommunityMembersList);
