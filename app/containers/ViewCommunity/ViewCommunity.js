import PropTypes from "prop-types";
import React, {useEffect, useState} from "react";
import Comments from "../../components/Comments";
import CommunityCard from "../../components/CommunityCard";
import Cover from "../../components/Cover";
import CreatePost from "../../components/CreatePost";
import CreatePostForm from "../../components/CreatePostForm";
import Header from "../../components/Header";
import NoPosts from "../../components/NoPosts";
import PostItem from "../../components/PostItem/PostItem";
import "./comment.css";
import CommunityDescription from "./CommunityDescription";
import Spinner from "../../components/Spinner";
import "./vprofile.css";
import SortBy from "../../components/SortBy";
import { FormattedMessage } from 'react-intl';
import messages from './messages'; 

const Menu = [
  {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
  {id: 3, name: <FormattedMessage {...messages.headerlinkGeneral}/>, path: "/general"},
  {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
];
const ViewCommunity = props => {
  const [showModel, setShowModel] = useState(false);
  const [allPosts, setAllPost] = useState([]);

  function displayModel() {
    setShowModel(true);
  }

  function hideModel() {
    setShowModel(false);
  }


  useEffect(() => {
    let post = props.communityPosts && [...props.communityPosts];
    if (props.pinePost !== undefined)
      post !== undefined && post.unshift(props.pinePost && props.pinePost);
    setAllPost(post);
  }, [props.pinePost]);

  return (<>
    <div className="container-fluid p-0 mh-100 bg-light">
      <Header nav={Menu} {...props} />
      <div className="container mt-3">
        <div className="row">
          <div className="col-sm-12 p-0">
            <h5 className="pt-4 text1 com-left"><FormattedMessage {...messages.headerlinkCommunities}/></h5>
            <Cover
              createMessage={props.createMessage}
              showButton={true}
              community={props.community} onFollow={props.onFollow}
              cover={props.community ? props.community.background_image : require("../../images/placeholder2.jpg")}
              profilePic={props.community ? props.community.profile_image : require("../../images/user.png")}
              buttons/>
                </div>
      </div>
      </div>
      <div className="container">
            <div className="row">
              <div className="col-sm-7">
                <CommunityDescription
                  {...props}
                  history={props.history} name={props.community ? props.community.name : ""}
                  description={props.community ? props.community.description : ""}
                  rules={props.community ? props.community.rules : ""}
                  members={props.community ? props.community.followers : 0}
                  role={props.community ? props.community.role : ""}
                  communityId={props.community ? props.community.id : ""}/>
                {props.communityPosts && !props.communityPosts.length ? <NoPosts/> : <div className="mb-3">
                  <SortBy filterData={props.filterData}/>
                  <PostItem
                    pinImage={true}
                    posts={{post: {posts: allPosts}}} history={props.history}
                    getLatestPost={props.fetchPost}
                    community={props.community}
                  />
                  <Comments/>
                </div>}
              </div>
              <div className="col-sm-5">
                {
                  props.community && props.community.follow && <>
                    <CreatePost showModel={showModel} displayModel={displayModel} hideModel={hideModel}/>
                    <CreatePostForm
                      isCommunity={props.community} showModel={showModel}
                      communities={props.communities}
                      displayModel={displayModel} hideModel={hideModel} fetchPost={props.fetchPost}/>
                  </>
                }
                <CommunityCard/>
                {/*<FooterLink />*/}
              </div>
            </div>
            </div>


    </div>
    {/* {props.loading && <Spinner/>} */}
  </>);
};

ViewCommunity.propTypes = {
  location: PropTypes.object,
  community: PropTypes.object,
  communityPosts: PropTypes.array,
  fetchPost: PropTypes.func,
  onFollow: PropTypes.func,
  history: PropTypes.object,
  loading: PropTypes.bool,
  filterData: PropTypes.func
}

export default ViewCommunity;
