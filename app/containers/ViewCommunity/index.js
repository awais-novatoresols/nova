import PropTypes from "prop-types";
import React, {memo, useEffect} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import {createStructuredSelector} from "reselect";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import {getCommunity, getCommunityPosts, getFilterPost} from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import {
  makeSelectCommunity,
  makeSelectCommunityPinedPosts,
  makeSelectCommunityPosts,
  makeSelectCommunityPostsSuccess,
  makeSelectCommunitySuccess,
  makeSelectLoading
} from "./selectors";
import ViewCommunity from "./ViewCommunity";
import {useBottomScrollListener} from 'react-bottom-scroll-listener';
import {makeSelectAllUserCommunities} from "../../components/CommunityCard/selectors";
import {createNewMessage} from "../Chat/actions";
import {makeSelectPinedPostSuccess, makeSelectUnPinedPostSuccess} from '../../components/PostItem/selectors';

const key = "viewCommunity";
const ViewCommunityContainer = props => {
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  let userName = localStorage.getItem("userName");

  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});
  useBottomScrollListener(onBottomHit);

  function onBottomHit() {
    //  console.log("Bottom Hit!!!", props.communityPosts);
    // let posts = props.posts.post;
    // if (posts && posts.current_page) {
    //   if (parseInt(posts.current_page) < parseInt(posts.total_pages)) {
    //     let pageNo = parseInt(posts.current_page) + 1;
    //     console.log("New Posts Loaded!!!", pageNo);
    //     props.getUserPost(userId, pageNo, clientId, accessToken, uid);
    //   }
    // }
  }
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  useEffect(() => {
    props.getCommunity(userId, props.match.params.id, clientId, accessToken, uid);
    props.getCommunityPosts(userId, props.match.params.id, clientId, accessToken, uid);
  }, [props.match.params.id]);


  useEffect(() => {
    if (props.pinSuccess && props.pinSuccess.success) props.getCommunityPosts(userId, props.match.params.id, clientId, accessToken, uid);
    if (props.unpinSuccess && props.unpinSuccess.success) props.getCommunityPosts(userId, props.match.params.id, clientId, accessToken, uid);
    // console.log("pinePost", props.pinePost);
  }, [props.pinSuccess, props.unpinSuccess.success]);


  function getLatestPost() {
    props.getCommunityPosts(userId, props.match.params.id, clientId, accessToken, uid);
  }

  function onFollowBtnClick() {
    props.getCommunity(userId, props.match.params.id, clientId, accessToken, uid);
  }

  function filterData(e) {
    //  console.log(e.target.id, "filter dataa id...");
    props.getFilterPost(e.target.id, true, userId, props.match.params.id, clientId, accessToken, uid);
  }


  function createMessage(chatRoomId, message, messageType, url, communityChat) {
    // console.log("chatRoomId,message, messageType, url, communityChat",chatRoomId,message, messageType, url, communityChat)
    let formData = new FormData();
    formData.append("message[recipient_id]", chatRoomId);
    formData.append("message[sender_name]", userName);
    formData.append("message[content]", message);
    formData.append("message[message_type]", messageType);
    formData.append("message[url]", url);
    formData.append("message[community_chat]", "true");
    props.createNewMessage(formData, accessToken, clientId, uid)
  }


  return <ViewCommunity
    {...props} filterData={filterData}
    createMessage={createMessage}
    communities={props.communities}
    pinePost={props.pinePost}
    communityPosts={props.communityPosts.posts} fetchPost={getLatestPost} onFollow={onFollowBtnClick}
    loading={props.loading}/>;
};

ViewCommunityContainer.propTypes = {
  community: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  getCommunity: PropTypes.func,
  success: PropTypes.bool,
  match: PropTypes.object,
  location: PropTypes.object,
  createMessage: PropTypes.func,
  getCommunityPosts: PropTypes.func,
  communityPosts: PropTypes.object,
  communityPostsSuccess: PropTypes.bool,
  communities: PropTypes.object,
  loading: PropTypes.bool,
};
const mapStateToProps = createStructuredSelector({
  community: makeSelectCommunity(),
  success: makeSelectCommunitySuccess(),
  communityPosts: makeSelectCommunityPosts(),
  communities: makeSelectAllUserCommunities(),
  communityPostsSuccess: makeSelectCommunityPostsSuccess(),
  loading: makeSelectLoading(),
  pinePost: makeSelectCommunityPinedPosts(),
  pinSuccess: makeSelectPinedPostSuccess(),
  unpinSuccess: makeSelectUnPinedPostSuccess(),

});

export function mapDispatchToProps(dispatch) {
  return {
    getCommunity: (userId, communityId, clientId, accessToken, uid) => dispatch(getCommunity(userId, communityId, clientId, accessToken, uid)),
    createNewMessage: (formData, accessToken, clientId, uid) => dispatch(createNewMessage(formData, accessToken, clientId, uid)),
    getFilterPost: (filterType, filter, userId, communityId, clientId, accessToken, uid) => dispatch(getFilterPost(filterType, filter, userId, communityId, clientId, accessToken, uid)),
    getCommunityPosts: (userId, communityId, clientId, accessToken, uid) => dispatch(getCommunityPosts(userId, communityId, clientId, accessToken, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(ViewCommunityContainer);
