import {put, takeLatest} from "redux-saga/effects";
import {apiFetch, apiFetchDelete, apiFetchPost, apiFetchPut} from "../../utils/network";
import {
  fetchNewData,
  getBlockedCommunityMembersSuccess,
  getCommunityMembersSuccess,
  getCommunityPostsSuccess,
  getCommunitySuccess
} from "./actions";
import {
  BLOCK_COMMUNITY_USER,
  DELETE_COMMUNITY_USER,
  GET_BLOCK_USERS,
  GET_COMMUNITY,
  GET_COMMUNITY_MEMBERS,
  GET_COMMUNITY_POSTS,
  GET_FILTER_POSTS,
  UPDATE_USER_ROLE,
  UNBLOCK_COMMUNITY_USER
} from "./constants";
import {unblockSuccess} from "../../components/BlockedContent/actions";

function* getCommunity(user) {
  try {
    const json = yield apiFetch(`users/${user.userId}/communities/${user.communityId}`, user.accessToken, user.clientId, user.uid);
    //   console.log("viewCommunities Saga...", json);
    yield put(getCommunitySuccess(json));
  } catch (error) {
    // console.log(error);
  }
}

function* getCommunityPosts(user) {
  try {
    const json = yield apiFetch(`users/${user.userId}/communities/${user.communityId}/get_community_posts`, user.accessToken, user.clientId, user.uid);
    //  console.log("viewCommunities Post Saga...", json);
    yield put(getCommunityPostsSuccess(json));
  } catch (error) {
    // console.log(error);
  }
}

function* getCommunityMembers(user) {
  try {
    const json = yield apiFetch(`users/${user.userId}/communities/${user.communityId}/community_followers?page_no=${user.pageNo}`, user.accessToken, user.clientId, user.uid);
    yield put(getCommunityMembersSuccess(json));
  } catch (error) {
    // console.log(error);
  }
}

export function* getFilterData(userId) {
  //  console.log("getFilterData...",userId);
  try {
    const json = yield apiFetch(`users/${userId.userId}/communities/${userId.communityId}/get_community_posts?${userId.filterType}=${userId.filter}`, userId.accessToken, userId.clientId, userId.uid);
    // console.log("getFilterData...", json);
    yield put(getCommunityPostsSuccess(json));
  } catch (error) {
    console.log(error);
  }
}


export function* updateUserRole(user) {
  console.log("updateUserRole...", user);
  try {
    const json = yield apiFetchPut(`users/${user.userId}/communities/${user.communityId}/update_role`, user.formData, user.accessToken, user.clientId, user.uid);
    console.log("updateUserRole...", json);
    yield put(fetchNewData(json));
  } catch (error) {
    console.log(error);
  }
}

export function* deleteCommunityUser(user) {
  console.log("deleteCommunityUser...", user);
  try {
    const json = yield apiFetchDelete(`users/${user.userId}/communities/remove_follower_from_community`, user.formData, user.accessToken, user.clientId, user.uid);
    console.log("deleteCommunityUser...", json);
    yield put(fetchNewData(json));
  } catch (error) {
    console.log(error);
  }
}


export function* getCommunityBlockUser(user) {
  console.log("getCommunityBLockUser...", user);
  try {
    const json = yield apiFetch(`users/${user.userId}/blocked_users?community_id=${user.communityId}`, user.accessToken, user.clientId, user.uid);
    console.log("getCommunityBLockUser...", json);
    yield put(getBlockedCommunityMembersSuccess(json));
  } catch (error) {
    console.log(error);
  }
}


export function* blockCommunityUser(user) {
  console.log("blockCommunityUser ...", user);
  try {
    const json = yield apiFetchPost(`users/${user.userId}/block_user`, user.formData, user.accessToken, user.clientId, user.uid);
    console.log("blockCommunityUser Data ..........", json);
    yield put(fetchNewData(json));
  } catch (error) {
    console.log(error);
  }
}

export function* unblockContent(user) {
  console.log("createVote from json ...", user);
  try {
    const json = yield apiFetchPost(`users/${user.userId}/unblock_user`, user.formData, user.accessToken, user.clientId, user.uid);
    console.log("UnBlocked Data ..........", json);
    yield put(unblockSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export default function* communities() {
  yield takeLatest(GET_COMMUNITY, getCommunity);
  yield takeLatest(GET_COMMUNITY_POSTS, getCommunityPosts);
  yield takeLatest(GET_COMMUNITY_MEMBERS, getCommunityMembers);
  yield takeLatest(GET_FILTER_POSTS, getFilterData);
  yield takeLatest(UPDATE_USER_ROLE, updateUserRole);
  yield takeLatest(DELETE_COMMUNITY_USER, deleteCommunityUser);
  yield takeLatest(GET_BLOCK_USERS, getCommunityBlockUser);
  yield takeLatest(BLOCK_COMMUNITY_USER, blockCommunityUser);
  yield takeLatest(UNBLOCK_COMMUNITY_USER, unblockContent);
}
