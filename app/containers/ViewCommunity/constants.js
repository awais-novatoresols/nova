export const GET_COMMUNITY = "ubwab/communities/view/GET_COMMUNITY";
export const GET_COMMUNITY_SUCCESS = "ubwab/communities/view/GET_COMMUNITY_SUCCESS";
export const GET_COMMUNITY_POSTS = "ubwab/communities/view/GET_COMMUNITY_POSTS";
export const GET_COMMUNITY_POSTS_SUCCESS = "ubwab/communities/view/GET_COMMUNITY_POSTS_SUCCESS";
export const GET_COMMUNITY_MEMBERS = "ubwab/communities/view/GET_COMMUNITY_MEMBERS";
export const GET_COMMUNITY_MEMBERS_SUCCESS = "ubwab/communities/view/GET_COMMUNITY_MEMBERS_SUCCESS";
export const GET_FILTER_POSTS = "ubwab/communities/view/GET_FILTER_POSTS";


export const GET_BLOCK_USERS = "ubwab/communities/view/GET_BLOCK_USERS";

export const UPDATE_USER_ROLE = "ubwab/communities/view/UPDATE_USER_ROLE";

export const DELETE_COMMUNITY_USER = "ubwab/communities/view/DELETE_COMMUNITY_USER";

export const BLOCK_COMMUNITY_USER = "ubwab/communities/view/BLOCK_COMMUNITY_USER";

export const BLOCK_COMMUNITY_USER_SUCCESS = "ubwab/communities/view/BLOCK_COMMUNITY_USER_SUCCESS";

export const FETCH_DATA = "ubwab/communities/view/FETCH_DATA";
export const UNBLOCK_COMMUNITY_USER = "ubwab/communities/view/UNBLOCK_COMMUNITY_USER";
