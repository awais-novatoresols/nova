import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Communities from "../Communities";
import CreateCommunities from "../CreateCommunities";
import ForgotPassword from "../ForgotPassword/Loadable";
import History from "../History";
import HomePage from "../HomePage/Loadable";
import InfoPage from "../InfoPage/InfoPage";
import MySubscription from "../MySubscription";
import NotFoundPage from "../NotFoundPage/Loadable";
import Notifications from "../Notifications";
import Profile from "../Profile/Loadable";
import SignIn from "../SignIn/Loadable";
import SignUp from "../SignUp/Loadable";
import ViewCommunity from "../ViewCommunity";
import ViewPost from "../ViewPost";
import CommunityMembersList from "../ViewCommunity/CommunityMembersList";
import ChangePassword from "../ChangePassword";
import UpdateCommunities from "../EditCommunities";
import BlockedContent from "../../components/BlockedContent";
import Chat from "../Chat/Chat";
import General from "../General";
import ActionCableProvider from 'react-actioncable-provider';

import ActionCable from "actioncable";
import PrivacyPolicy from "../../components/PrivacyPolicy";

let userId = localStorage.getItem("userId");
const cable = ActionCable.createConsumer(`http://3.15.160.19/cable?id=${userId}`);

function onConnected() {
  console.log("onConnected")
}

export default function App() {
  return (
    <ActionCableProvider cable={cable}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={SignIn}/>
          <Route path="/signUp" component={SignUp}/>
          <Route path="/chat" component={Chat}/>
          <Route path="/forgotPassword" component={ForgotPassword}/>
          <Route path="/changepassword" component={ChangePassword}/>
          <Route path="/homePage" component={HomePage}/>
          <Route path="/profile/blocked" component={BlockedContent}/>
          <Route path="/profile/followers" component={BlockedContent}/>
          <Route path="/profile/followings" component={BlockedContent}/>
          <Route path="/profile/:userId?/:profileId?" component={Profile} isAuthed={true}/>
          <Route path="/history" component={History}/>
          <Route path="/subscriptions" component={MySubscription}/>
          <Route path="/communities/create" component={CreateCommunities}/>
          <Route path="/communities/edit/:id" component={UpdateCommunities}/>
          <Route path="/communities/view/:id/members" component={CommunityMembersList}/>
          <Route path="/communities/view/:id" component={ViewCommunity}/>
          <Route path="/communities" component={Communities}/>
          <Route path="/post/:id?" component={ViewPost}/>
          <Route path="/infopage" component={InfoPage}/>
          <Route path="/privacy-policy" component={PrivacyPolicy}/>
          <Route path="/notifications" component={Notifications}/>
          <Route path="/general" component={General}/>
          <Route component={NotFoundPage}/>
        </Switch>
      </BrowserRouter>
    </ActionCableProvider>
  );
}
