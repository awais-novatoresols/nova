import React from 'react';
import './style.css';

function CommunityInfo(props) {
  let userId = localStorage.getItem("userId");

  return (
    <div className="d-flex p-3 pl-4 ">
      {/*<img src={require("../../images/arrowleft.png")} height="20px"/>*/}
      <div className="pl-4 pb-4">
        {props.personalChat && props.personalChat.length > 0 &&
        props.personalChat[0].recipient && props.personalChat[0].recipient.recipient_id != userId &&
        <h4
          className="c_clr m-0 font_style">{props.personalChat[0].recipient && props.personalChat[0].recipient.user_name}</h4>}
        {props.personalChat && props.personalChat.length > 0 &&  props.personalChat[0].recipient &&
        props.personalChat[0].recipient.recipient_id == userId &&
        <h4
          className="c_clr m-0 font_style">{props.personalChat[0].sender && props.personalChat[0].sender.user_name}</h4>}
        {/*<p className="c_clr font_style1">2 Members</p>*/}

        {
          props.name  && props.name.length >0 &&  <h4
            className="c_clr m-0 font_style">{props.name && props.name[0].name}</h4>
        }
      </div>
    </div>
  )
}

export default CommunityInfo;
