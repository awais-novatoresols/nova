import React from 'react'
import PropTypes from "prop-types";

function CommunityItem(props) {
  // console.log("id.......", props.id);

  function getId() {
    // console.log("CommunityItem.......", props.id);
    props.getCommunityMessage(props.id && props.id)
  }

  return (
    <div onClick={getId}>
      <ul type="none" className="p-0">
        <li className="cursor_pointer_class">
      <span className="float-left">
        <img
          className="rounded-circle"
          src={props.userImage !== null ? props.userImage : require("../../images/user.png")}
          height="50px"
          width="50px"
        />
      </span>
          <div className="pt-1" style={{paddingLeft: 66}}>
        <span className="h5 c_clr font_style">
          {props.userName}
        </span>
            <p className="c_clr font_style1">{props.groupMembers} </p>
          </div>
          <div>
            <p className="c_clr font_style1">
              {props.messageContent}
            </p>
          </div>
        </li>
      </ul>
      <hr/>
    </div>
  );
}

CommunityItem.propTypes = {
  id: PropTypes.number,
  userName: PropTypes.string,
  messageContent: PropTypes.string,
  userImage: PropTypes.string,
  getCommunityMessage: PropTypes.func
};
export default CommunityItem;
