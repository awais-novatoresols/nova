import { defineMessages } from "react-intl";

export default defineMessages({
  chatNavHeaderPersonal: {
    id: "chat-nav-header-personal",
    defaultMessage: "Messages"
  },
  chatNavHeaderCommunities: {
    id: "chat-nav-header-communities",
    defaultMessage: "Chat Rooms"
  },
  chatNavSearchPlaceHolder: {
    id: "chat-nav-search-place-holder",
    defaultMessage: "Search.."
  },
  headerlinkNewsFeeds: {
    id: "header-link-news-feed",
    defaultMessage: "News Feed"
  },
  headerlinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Communities"
  },
  headerlinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  },
  chatTypeMessagePlaceHolder: {
    id: "chat-type-message-place-holder",
    defaultMessage: "Type a message"
  }
});
