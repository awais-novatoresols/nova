import {createSelector} from "reselect";
import {initialState} from "./reducer";

const selectGlobal = state => state.chat || initialState;

const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );


const makeSelectUserChatRooms = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.chatRooms
  );


const makeSelectUserPersonalMessage = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.personalChat
  );

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectUploadStatus = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.uploadStatus
  );

const makeSelectUploadItemURL = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.uploadItemURL
  );

export {
  selectGlobal,
  makeSelectSuccess,
  makeSelectUserChatRooms,
  makeSelectLoading,
  makeSelectUserPersonalMessage,
  makeSelectUploadStatus,
  makeSelectUploadItemURL
};

