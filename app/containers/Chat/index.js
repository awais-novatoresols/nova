import React, { useState } from "react";
import { useLocation } from 'react-router-dom';
import { ActionCableConsumer } from "react-actioncable-provider";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { appendMessage } from "./actions";
import "./style.css";

const ChatContainer = props => {
  const [newMessage, setNewMessage] = useState(false);
  let location = useLocation();
  let userId = localStorage.getItem('userId');
  function handleReceived(message) {
    props.appendMessage(message);
    if(message.sender_id !== userId && location.pathname.toLowerCase() !== '/chat'){
      setNewMessage(true);
    }
    console.log("message", message.sender_id !== userId);
  }
  function onConnected() {
    console.log("ChatroomChannel onConnected");
  }

  function onDisconnected() {
    // console.log("ChatroomChannel onDisconnected ")
  }

  function onRejected() {
    // console.log("ChatroomChannel onRejected ")
  }

  return (
    <ActionCableConsumer
      channel="ChatroomChannel"
      onConnected={onConnected}
      onReceived={handleReceived}
      onDisconnected={onDisconnected}
      onRejected={onRejected}
    >
      <Link to="/chat">
        <img
          className="top maring-left dropdown-toggle cursor_pointer_class header_icons"
          src={require("../../images/chaticon.png")}
          width="28px"
          height="28px"
        />
        {newMessage && (
          <sup data-toggle="dropdown" className="cart-message mt-1">
            <span className="badge badge-danger badge-pill">
              <span style={{ visibility: "hidden" }}>1</span>
            </span>
          </sup>
        )}
      </Link>
    </ActionCableConsumer>
  );
};

export function mapDispatchToProps(dispatch) {
  return {
    appendMessage: data => dispatch(appendMessage(data))
  };
}

export default connect(
  null,
  mapDispatchToProps
)(ChatContainer);
