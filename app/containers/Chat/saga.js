import {put, takeLatest} from "redux-saga/effects";
import {apiFetch, signUpApi} from "../../utils/network";
import {getUserChatData, getUserPersonalData, uploadContentSuccess} from "./actions";
import {toast} from "react-toastify";
import {
  CREATE_NEW_MESSAGE,
  GET_COMMUNITY_CHAT_HISTORY,
  GET_PERSONAL_MESSAGE,
  GET_USER_CHATROOMS,
  UPLOAD_CONTENT
} from "./constants";

let userId = localStorage.getItem("userId");

function* getUserChatRoomsData(user) {
  // console.log("getUserChatRoomsData from saga ??????", user);
  try {
    const json = yield apiFetch(`chatrooms?inbox=${user.roomType}`, user.accessToken, user.clientId, user.uid);
    console.log("getUserChatRoomsData json Saga...", json);
    if (json.chatrooms[0].messag && json.chatrooms[0].messag.recipient || json.chatrooms[0].messag && json.chatrooms[0].messag.sender) {
      let id = json.chatrooms[0].messag.recipient && json.chatrooms[0].messag.recipient.recipient_id != userId ? json.chatrooms[0].messag.recipient.recipient_id : json.chatrooms[0].messag && json.chatrooms[0].messag.sender.sender_id
      user.recipient_id = id;
      yield getPersonalChat(user);
    }
    yield put(getUserChatData(json));

  } catch (error) {
    console.log(error);
  }
}

function* getPersonalChat(user) {
  // console.log("getPersonalChat from saga ", user);
  try {
    const json = yield apiFetch(`chatrooms/check_chatroom?recipient_id=${user.recipient_id}`, user.accessToken, user.clientId, user.uid);
    yield put(getUserPersonalData(json));
  } catch (error) {
    console.log(error);
  }
}

function* getCommunityChat(user) {
  //  console.log("getCommunityChat from saga ", user);
  try {
    const json = yield apiFetch(`messages/chat_history?chatroom_id=${user.chat_id}`, user.accessToken, user.clientId, user.uid);
    //   console.log("getCommunityChat json Saga...", json);
    yield put(getUserPersonalData(json));
  } catch (error) {
    console.log(error);
  }
}

function* createMessage(user) {

  // for (let pair of user.formData.entries()) {
  //   console.log(pair[0] + ", " + pair[1]);
  // }

  // console.log("createMessage Saga user...", user);
  try {
    const json = yield signUpApi(`messages`, user.formData, user.accessToken, user.clientId, user.uid);
    //  console.log("createMessage Saga json...", json);
    // yield put(createCommunitySuccess(json));
  } catch (error) {
    // console.log(error);
  }
}


export function* uploadContent(body) {
  localStorage.setItem("imageUpload", "");
  // console.log("Upload Content ...", body);
  try {
    //  console.log("Body", body);
    const json = yield signUpApi(`upload_contents`, body.data, body.accessToken, body.clientId, body.uid);
    //const json = yield uploadFile(data);
    console.log("Upload Content ...???", json);
    // console.log("URL", json.content.url);
    localStorage.setItem("imageUpload", json.content.url);
    if(json.success) toast.success(json.message)

     // yield put(uploadContentSuccess(json.content.url));
  } catch (error) {
    console.log("error...", error);
  }
}

export default function* chatMessage() {
  yield takeLatest(GET_USER_CHATROOMS, getUserChatRoomsData);
  yield takeLatest(GET_PERSONAL_MESSAGE, getPersonalChat);
  yield takeLatest(GET_COMMUNITY_CHAT_HISTORY, getCommunityChat);
  yield takeLatest(CREATE_NEW_MESSAGE, createMessage);
  yield takeLatest(UPLOAD_CONTENT, uploadContent);
}
