import React from 'react'
import PropTypes from "prop-types";

function UserItem(props) {

  function getId() {
    props.getPersonalMessage(props.id && props.id)
  }

  return (
    <div onClick={getId}>
      <ul type="none" className="p-0 ">
        <li className="cursor_pointer_class">
          <span className="float-left">
            <img
              className="rounded-circle"
              src={props.userImage !== null ? props.userImage : require("../../images/user.png")}
              height="50px"
              width="50px"/>
          </span>
          <div className="pt-1" style={{paddingLeft: 66}}>
            <span className="h5 c_clr font_style"> {props.userName} </span>
            <span className="float-right c_clr font_style1">{props.time}</span>
            <p className="c_clr font_style1">{props.messageContent}</p>
          </div>
        </li>
      </ul>
      <hr/>
    </div>
  );
}

UserItem.propTypes = {
  userName: PropTypes.string,
  messageContent: PropTypes.string,
  userImage: PropTypes.string,
  time: PropTypes.string,
  id: PropTypes.number,
  getPersonalMessage: PropTypes.func
};

export default UserItem;
