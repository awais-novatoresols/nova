import produce from "immer";
import {
  ADD_NEW_MESSAGE,
  CREATE_NEW_MESSAGE,
  FILTER_USER_CHATROOMS,
  GET_PERSONAL_CHAT,
  GET_USER_CHATROOMS,
  GET_USER_CHATROOMS_DATA,
  GET_USER_MESSAGE,
  UPLOAD_CONTENT_SUCCESS
} from "./constants";

// The initial state of the App
export const initialState = {
  loading: true,
  success: false,
  chatMessage: [],
  personalChat: [],
  chatRooms: [],
  uploadStatus: false,
  uploadItemURL: "",
};

function appendMessage(priMessage, newMessage) {
  // console.log("appendMessage...", priMessage.length, newMessage.content);
  let dataMessage = {
    message_id: 339,
    content: newMessage.content,
    message_type: newMessage.message_type && newMessage.message_type ,
    url: newMessage.url !== "" && newMessage.url,
    sender: {
      sender_id: newMessage.sender_id !== null && newMessage.sender_id,
      email: 'aiman@gmail.com',
      user_name: 'aiman',
      confirmed_at: '2019-11-11T13:28:55.403Z',
      profile: {
        profile_id: 15,
        name: 'Aim♥N',
        country: '+92',
        about: 'tester',
        account_type: 'public',
        reputation: 13.94,
        fcm: 'dA22G3OGpdY:APA91bHxwklZ2ObIxao5dXPidHvn8RCNqwU1f-hrQBHpuPQSlmdiRsu8iCEjC576EQYXa0VUVokaL8nU-g8Fh707tTtNm5itlcC01QVl_9Nf1w3WJ3kjKFajWRo-IotDYmq_q12eoN_j',
        created_at: '2019-10-10T12:51:55.491Z',
        profile_image: 'https://ubwab-heroku-stagging.s3.us-east-2.amazonaws.com/ptjFvW6tq2PMxN8FCbsTeh3e?response-content-disposition=inline%3B%20filename%3D%22cropped2044613426.jpg%22%3B%20filename%2A%3DUTF-8%27%27cropped2044613426.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJWGQ5HMSTL7IPY7Q%2F20191118%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20191118T131806Z&X-Amz-Expires=300&X-Amz-SignedHeaders=host&X-Amz-Signature=d039352d7beeca3f600869a8f33ce53302318745883d500bc22265d45cb8215d',
        background_image: 'https://ubwab-heroku-stagging.s3.us-east-2.amazonaws.com/1ds2uKyyVJVF2BdxVRs9a4Do?response-content-disposition=inline%3B%20filename%3D%22cropped1675956752.jpg%22%3B%20filename%2A%3DUTF-8%27%27cropped1675956752.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJWGQ5HMSTL7IPY7Q%2F20191118%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20191118T131806Z&X-Amz-Expires=300&X-Amz-SignedHeaders=host&X-Amz-Signature=972eea4b860e402dca3fab2c95f844da07a1c703a1d07a92efe9561e1498efa0'
      }
    },
    recipient: {
      recipient_id: newMessage.recipient_id !== null && newMessage.recipient_id,
      email: 'fas@gmail.com',
      user_name: 'fasih',
      confirmed_at: null,
      profile: {
        profile_id: 5,
        name: 'hello qwe',
        country: '+1',
        about: '',
        account_type: 'private',
        reputation: 3.32,
        fcm: 'fddzRLmeynI:APA91bHWwmQIDuOTAgmu8N8aqsEzj1QynlqU5rvDEX52U9JhswfEbL9CLBcboGjgWehnmv_4Zch9j3Rc30R06KAKkPjO6e00M8QPEzrG42TEajII_QW5G-sbr047n6hXwI8hgAeDUtcq',
        created_at: '2019-10-09T05:16:21.245Z',
        profile_image: 'https://ubwab-heroku-stagging.s3.us-east-2.amazonaws.com/j9fsvdtJKyFir9aE4LrUkxYq?response-content-disposition=inline%3B%20filename%3D%22file.jpg%22%3B%20filename%2A%3DUTF-8%27%27file.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJWGQ5HMSTL7IPY7Q%2F20191118%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20191118T131806Z&X-Amz-Expires=300&X-Amz-SignedHeaders=host&X-Amz-Signature=5f579faa96ebb462d67a6ac0e2d236f891db8f4c93d816d0a92d24a95cfb8e07',
        background_image: 'https://ubwab-heroku-stagging.s3.us-east-2.amazonaws.com/yWQquKZStybwhxwBbKuSwt4z?response-content-disposition=inline%3B%20filename%3D%22file.jpg%22%3B%20filename%2A%3DUTF-8%27%27file.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJWGQ5HMSTL7IPY7Q%2F20191118%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20191118T131806Z&X-Amz-Expires=300&X-Amz-SignedHeaders=host&X-Amz-Signature=e9ee47f802344dd1d9636a259f5c2c7f8b931c61d6abe1a5995b15d06fad51e7'
      }
    }
  };

  let newData = [...priMessage];

  if ((newData[0].sender && newData[0].sender.user_name === newMessage.sender_name) || (newData[0].recipient && newData[0].recipient.user_name === newMessage.sender_name)) {
    newData.push(dataMessage);
  }else {
    newData.push(dataMessage);
  }

  return newData;
}


function filterChatRooms(chatRooms, filterText) {
  let data = [...chatRooms];
  if (filterText.length > 2) {
    data = data.filter(item => {
      // console.log("item.messag.sender.user_name", item.messag.sender.user_name);
      return  item.messag.sender.user_name.includes(filterText);
    });
  }
  return data;
}

/* eslint-disable default-case, no-param-reassign */
const chatReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_USER_CHATROOMS:
        draft.loading = false;
        break;
      case CREATE_NEW_MESSAGE:
        draft.loading = false;
        break;
      case GET_USER_MESSAGE:
        draft.loading = false;
        break;
      case GET_USER_CHATROOMS_DATA:
        draft.chatRooms = action.data.chatrooms;
        draft.loading = false;
        break;
      case FILTER_USER_CHATROOMS:
        draft.chatRooms = filterChatRooms(draft.chatRooms, action.data);
        draft.loading = false;
        break;
      case GET_PERSONAL_CHAT:
        draft.personalChat = action.data.chatroom.messages;
        draft.loading = false;
        break;
      case ADD_NEW_MESSAGE:
        //  console.log("ADD_NEW_MESSAGE", action);
        draft.personalChat = appendMessage(draft.personalChat, action.data);
        draft.loading = false;
        break;
      case UPLOAD_CONTENT_SUCCESS:
        draft.uploadStatus = true;
        draft.uploadItemURL = action.url;
        break;
      default:
        draft.uploadStatus = false;
        draft.loading = false;
        draft.success = false;
    }
  });

export default chatReducer;


