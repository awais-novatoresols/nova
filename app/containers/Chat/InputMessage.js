import React, {useState} from 'react';
import './style.css';
import PropTypes from "prop-types";
import ReactPlayer from "react-player";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

function InputMessage(props) {

  const [image, setImage] = useState();
  const [video, setVideo] = useState();

  function getInoutMessage(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      props.createNewMessage(e);
      props.setMessage('');
    } else{
      props.getMessage(e.target.value, image);
    }
  }

  function handleImageChange(e) {
    props.getImage(e.target.files[0]);
    setImage(URL.createObjectURL(e.target.files[0]));
  }

  function handleVideoChange(e) {
    props.getImage(e.target.files[0]);
    setVideo(URL.createObjectURL(e.target.files[0]));
  }

  // let imageCheck =image && image.type.includes("image/");

  return (
    <div className="type_msg shadow_card bg-white">
      <div className="input_msg_write">
        <FormattedMessage {...messages.chatTypeMessagePlaceHolder}>
          {placeholder=> 
            <input
              type="text" maxLength={2000}
              className="write_msg pl-5"
              placeholder={placeholder}
              onChange={e => getInoutMessage(e)}
              onKeyDown={e => getInoutMessage(e)}
              value={props.message}
            />
          }
        </FormattedMessage>
        {image !== undefined &&  <img src={image} alt="" className="attach_image"/>}
        {
          video !== undefined && <ReactPlayer url={video} config={{
            youtube: {
              playerVars: {showinfo: 1}
            },
            facebook: {
              appId: '549267839141720'
            }
          }} controls width="25%" height="20%"/>
        }
        <label htmlFor="image">
          <img className="attach_image_btn cursor_pointer_class" src={require("../../images/metro-image.png")}/>
        </label>
        <label htmlFor="video">
          <img className="attach_send_btn cursor_pointer_class" src={require("../../images/ionic-md-videocam.png")}/>
        </label>
        <input
        id="image"
        type="file"
        multiple
        className="uploadImageHide"
        onChange={(e) => handleImageChange(e)}
      />
        <input
          id="video"
          type="file"
          multiple
          className="uploadImageHide"
          onChange={(e) => handleVideoChange(e)}
        />
        <img onClick={e => handleCreateMessage(e)} className="msg_send_btn cursor_pointer_class"
             src={require("../../images/sendbutton.png")}/>
      </div>
    </div>
  );
  function handleCreateMessage(e){
    props.setMessage('');
    props.createNewMessage(e);
  }
}

PropTypes.InputMessage = {
  getImage: PropTypes.func,
  getMessage: PropTypes.func,
  createNewMessage: PropTypes.func
}

export default InputMessage;
