import {
  GET_COMMUNITY_CHAT,
  GET_COMMUNITY_CHAT_HISTORY,
  GET_PERSONAL_CHAT,
  GET_USER_CHATROOMS,
  GET_USER_CHATROOMS_DATA,
  GET_USER_MESSAGE,
  GET_PERSONAL_MESSAGE,
  CREATE_NEW_MESSAGE,
  ADD_NEW_MESSAGE,
  FILTER_USER_CHATROOMS,
  UPLOAD_CONTENT, UPLOAD_CONTENT_SUCCESS
} from "./constants";

export function createNewMessage(formData, accessToken, clientId, uid) {
  return {
    type: CREATE_NEW_MESSAGE,
    formData,
    accessToken,
    clientId,
    uid
  };
}

export function getUserChatRooms(roomType, accessToken, clientId, uid) {
  return {
    type: GET_USER_CHATROOMS,
    roomType,
    accessToken,
    clientId,
    uid
  };
}

export function getUserMessage(recipient_id, accessToken, clientId, uid) {
  return {
    type: GET_PERSONAL_MESSAGE,
    recipient_id,
    accessToken,
    clientId,
    uid
  };
}

export function getUserPersonalData(data) {
  return {
    type: GET_PERSONAL_CHAT,
    data
  };
}
export function filterChatRoomsData(data) {
  return {
    type: FILTER_USER_CHATROOMS,
    data
  };
}



export function getUserCommunityData(data) {
  return {
    type: GET_COMMUNITY_CHAT,
    data
  };
}

export function getCommunityMessage(chat_id, accessToken, clientId, uid) {
  return {
    type: GET_COMMUNITY_CHAT_HISTORY,
    chat_id,
    accessToken,
    clientId,
    uid
  };
}

export function getUserChatData(data) {
  return {
    type: GET_USER_CHATROOMS_DATA,
    data
  };
}


export function appendMessage(data) {
  return {
    type:ADD_NEW_MESSAGE,
    data
  }
}

export function uploadContent(data, accessToken, clientId, uid){
  return {
    type: UPLOAD_CONTENT,
    data,
    clientId,
    accessToken,
    uid
  }
}

export function uploadContentSuccess(url) {
  return {
    type: UPLOAD_CONTENT_SUCCESS,
    url
  };
}
