import React from 'react';
import './style.css';
import ReactPlayer from "react-player";

function RecipientItem(props) {
  return (
    <div className="outgoing_msg mr-5 mt-2 mb-2">
      <div className="sent_msg p-2 rounded text-white">
        {
          props.masgtype !== "pdf" && props.masgtype !== "image" &&
          <p className="pl-3 pr-3 p-2 rounded font_style1 m-0 text-white sent_msg_clr">
            {props.messageContent}
          </p>
        }
        {
          props.masgtype === "image" && <img
            className="float-right"
            src={props.image !== null ? props.image : require("../../images/user.png")}
            height="250px"
            width="250px"
          />
        }
        {
          props.masgtype === "pdf" && <ReactPlayer url={props.video}

                                                   config={{
                                                     youtube: {
                                                       playerVars: {showinfo: 1}
                                                     },
                                                     facebook: {
                                                       appId: '549267839141720'
                                                     }
                                                   }} controls className="float-right" width="40%" height="40%"/>

        }
        <div className="w-100 float-right">
          <span className="time_date time_font_style"> {props.time}</span>
        </div>

      </div>
    </div>
  )
}

export default RecipientItem;
