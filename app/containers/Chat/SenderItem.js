import React from 'react';
import './style.css';
import PropTypes from "prop-types";
import ReactPlayer from "react-player";

function SenderItem(props) {
  function getData() {
    props.getHeaderInfo(props.user_name);
  }


  return (
    <div onClick={props.getUserId(props.id)} className="incoming_msg ml-4 mt-2 mb-2">
      {/*<div className="incoming_msg_img">*/}
      {/*  <img*/}
      {/*    className="rounded-circle"*/}
      {/*    src={props.userImage !== null ? props.userImage : require("../../images/user.png")}*/}
      {/*    height="30px"*/}
      {/*    width="30px"*/}
      {/*  />*/}
      {/*</div>*/}
      <div className="received_msg">
        <div className="rounded p-2 bg-white shadow_card">
          {/*<p className="pl-3 pr-3 font_style1">{props.user_name}</p>*/}
          <p className="pl-3 pr-3 font_style1">{props.messageContent}</p>
          {
            props.masgtype === "pdf" && <ReactPlayer url={props.video} config={{
              youtube: {
                playerVars: {showinfo: 1}
              },
              facebook: {
                appId: '549267839141720'
              }
            }} controls width="100%"/>
          }
          {
            props.masgtype === "image" && props.image && <img
              className="rounded-circle"
              src={props.image !== null ? props.image : require("../../images/user.png")}
              height="50px"
              width="50px"
            />
          }
          <span className="time_date float-right time_font_style"> {props.time}</span>
        </div>
      </div>
    </div>
  )
}

PropTypes.SenderItem = {
  getUserId: PropTypes.func
};

export default SenderItem;
