import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import Header from "../../components/Header";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { appendMessage, createNewMessage, filterChatRoomsData, getCommunityMessage, getUserChatRooms, getUserMessage, uploadContent } from "./actions";
import MessageBox from "./MessageBox";
import reducer from "./reducer";
import saga from "./saga";
import { makeSelectLoading, makeSelectUploadItemURL, makeSelectUploadStatus, makeSelectUserChatRooms, makeSelectUserPersonalMessage } from "./selectors";
import Sidebar from "./Sidebar";
import "./style.css";

const key = "chat";
const Chat = props => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  let userName = localStorage.getItem("userName");
  const [isActiveTab, setTabActive] = useState(true);
  const [communityId, setCommunityId] = useState(0);
  const [isCommunity, setIsCommunity] = useState(false);

  console.log("personalChat", props.personalChat);
  function tabNavigator(id) {
    if (id.target.id === "") {
      props.getUserChatRooms(id.target.id, accessToken, clientId, uid);
      setTabActive(true);
    } else {
      setTabActive(false);
      props.getUserChatRooms(id.target.id, accessToken, clientId, uid);
    }
  }

  useEffect(() => {
    props.getUserChatRooms("", accessToken, clientId, uid);
  }, []);

  function getPersonalMessage(id) {
    setCommunityId(id);
    props.getUserMessage(id, accessToken, clientId, uid);
  }

  function getCommunityMessage(id) {
    setIsCommunity(true);
    setCommunityId(id);
    props.getCommunityMessage(id, accessToken, clientId, uid);
  }

  function handleReceived(message) {
    props.appendMessage(message);
  }

  function createMessage(chatRoomId, message, messageType, url, communityChat) {
    let formData = new FormData();
    if (!isCommunity) formData.append("message[recipient_id]", communityId);
    if (isCommunity) formData.append("message[chatroom_id]", communityId);
    formData.append("message[sender_name]", userName);
    if (messageType === "image" || messageType === "pdf") {
      formData.append("message[content]", "");
    } else {
      formData.append("message[content]", message);
    }
    formData.append("message[message_type]", messageType);
    let imageUpload = localStorage.getItem("imageUpload");
    if (messageType === "image" || messageType === "pdf") {
      formData.append("message[url]", imageUpload);
    }
    formData.append("message[community_chat]", isCommunity);
    if (message !== "" || imageUpload !== "") {
      if (chatRoomId !== 1) {
        props.createNewMessage(formData, accessToken, clientId, uid);
        localStorage.setItem("imageUpload", "");
      } else {
        toast.success("You can't reply admin message");
      }
    } else {
      toast.success("Please write your message");
    }
  }

  return (
    <div className="custom_b">
      <Header {...props} />
      <div className="row">
        <Sidebar
          filterChatRoomsData={props.filterChatRoomsData}
          getPersonalMessage={getPersonalMessage}
          getCommunityMessage={getCommunityMessage}
          tabNavigator={tabNavigator}
          isActiveTab={isActiveTab}
          chatRooms={props.chatRooms && props.chatRooms}
        />
        <MessageBox
          uploadStatus={props.uploadStatus}
          uploadItemURL={props.uploadItemURL}
          uploadContent={props.uploadContent}
          chatRooms={props.chatRooms && props.chatRooms}
          communityId={communityId}
          createMessage={createMessage}
          personalChat={props.personalChat}
        />
      </div>
    </div>
  );
};

Chat.propTypes = {
  getCommunityMessage: PropTypes.func,
  getUserMessage: PropTypes.func,
  getUserChatRooms: PropTypes.func,
  createMessage: PropTypes.func,
  chatRooms: PropTypes.array,
  loading: PropTypes.bool,
  personalChat: PropTypes.array,
  uploadContent: PropTypes.func,
  uploadStatus: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  chatRooms: makeSelectUserChatRooms(),
  loading: makeSelectLoading(),
  personalChat: makeSelectUserPersonalMessage(),
  uploadItemURL: makeSelectUploadItemURL(),
  uploadStatus: makeSelectUploadStatus()
});

export function mapDispatchToProps(dispatch) {
  return {
    getUserChatRooms: (roomType, accessToken, clientId, uid) =>
      dispatch(getUserChatRooms(roomType, accessToken, clientId, uid)),
    getUserMessage: (recipient_id, accessToken, clientId, uid) =>
      dispatch(getUserMessage(recipient_id, accessToken, clientId, uid)),
    getCommunityMessage: (chat_id, accessToken, clientId, uid) =>
      dispatch(getCommunityMessage(chat_id, accessToken, clientId, uid)),
    createNewMessage: (formData, accessToken, clientId, uid) =>
      dispatch(createNewMessage(formData, accessToken, clientId, uid)),
    filterChatRoomsData: data => dispatch(filterChatRoomsData(data)),
    appendMessage: data => dispatch(appendMessage(data)),
    uploadContent: (data, accessToken, clientId, uid) =>
      dispatch(uploadContent(data, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(Chat);
