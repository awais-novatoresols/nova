import React, {useEffect, useRef} from 'react';
import './style.css'
import RecipientItem from './RecipientItem';
import SenderItem from './SenderItem';
import moment from "moment";
import PropTypes from "prop-types";

function Message(props) {
  let userId = localStorage.getItem("userId");

  function getDuration(publishDate) {
    let startDate = new Date();
    let a = moment(startDate);
    let b = moment(publishDate);
    let difference = a.diff(b, "minutes");
    return moment.duration(difference, "minutes").humanize();
  }

  const el = useRef(null);

  useEffect(() => {
    el.current.scrollIntoView({block: 'start', behavior: 'auto'});
  });

  return (
    <div className="msg_history">
      {
        props.personalChat && props.personalChat.length > 0 && props.personalChat.map((item, index) => {
          return <div className="mb-5" key={index}>
            {
              item.sender && item.sender.sender_id == userId && <RecipientItem
                // userImage={item.sender && item.sender.profile.profile_image}
                messageContent={item.content}
                masgtype={item.message_type}
                video={item.url && item.url}
                image={item.url && item.url}
                time={item.recipient && getDuration(item.recipient && item.created_at)}
              />
            }
            {
              item.sender && item.sender.sender_id != userId && <SenderItem
                getHeaderInfo={props.getHeaderInfo}
                masgtype={item.message_type}
                video={item.url && item.url}
                image={item.url && item.url}
                user_name={item.sender && item.sender.user_name}
                getUserId={props.getUserId}
                id={item.sender.sender_id}
                userImage={item.sender && item.sender.profile.profile_image}
                messageContent={item.content}
                time={item.sender && getDuration(item.sender && item.created_at)}
              />
            }
          </div>
        })
      }
      <div id={'el'} ref={el}/>
    </div>
  )
}

Message.propTypes = {
  getHeaderInfo: PropTypes.func,
  getUserId: PropTypes.func,
  personalChat: PropTypes.array,
};
export default Message;
