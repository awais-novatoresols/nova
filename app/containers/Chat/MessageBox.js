import React, {useEffect, useState} from 'react';
import CommunityInfo from './CommunityInfo';
import Message from './Message';
import InputMessage from './InputMessage';
import './style.css';

const MessageBox = React.memo(function MessageBox(props) {

  const [chatRoomId, setChatRoomId] = useState("");
  const [message, setMessage] = useState("");
  const [messageType, setMessageType] = useState("message");
  const [name, setName] = useState("");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  function createNewMessage(e) {
    e.preventDefault();
    props.createMessage(chatRoomId, message, messageType, "", false);
  }

  function getUserId(id) {
    setChatRoomId(id);
  }

  function getMessage(message) {
    //  console.log("getMessage", message);
    setMessage(message);
  }

  function getImage(image) {
    let data = new FormData();
    data.append("upload_content[avatar]", image);
    props.uploadContent(data, accessToken, clientId, uid);
    if (image.type.includes("image/")) {
      setMessageType('image');
      setMessage(image);
    } else {
      setMessageType('pdf');
      setMessage(image);
    }
  }

  // function getImage(image) {
  //   //  console.log("getMessage", message);
  //   setMessageType('pdf');
  //   setMessage(image);
  // }

  function getHeaderInfo() {
    // console.log("props.chatRooms", props.chatRooms);
    // console.log("communityId", props.communityId);
    let name = props.chatRooms.filter((item) => {
      return item.id === props.communityId
    });
    setName(name);
  }

  useEffect(() => {
    getHeaderInfo();
  }, []);

  return (
    <div className="hide_on_450 col-md-7 col-lg-8 col-xl-9 ">
      <CommunityInfo name={name} personalChat={props.personalChat}/>
      <hr className="hr_margin"/>
      <div>
        <Message getHeaderInfo={getHeaderInfo} getUserId={getUserId} personalChat={props.personalChat}/>
      </div>
      <InputMessage uploadStatus={props.uploadStatus}
                    uploadItemURL={props.uploadItemURL} createNewMessage={createNewMessage} getImage={getImage}
                    getMessage={getMessage} setMessage={setMessage} message={message}
                    createMessage={props.createMessage}/>
    </div>
  )
})

export default MessageBox;
