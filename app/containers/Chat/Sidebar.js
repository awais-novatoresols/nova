import React from "react";
import "./style.css";
import Useritem from "./Useritem";
import Communityitem from "./Communityitem";
import moment from "moment";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import messages from "./messages";

function Sidebar(props) {
  let userId = localStorage.getItem("userId");

  function getDuration(publishDate) {
    let startDate = new Date();
    let a = moment(startDate);
    let b = moment(publishDate);
    let difference = a.diff(b, "minutes");
    return moment.duration(difference, "minutes").humanize();
  }

  return (
    <div className="col-md-5 col-lg-4 col-xl-3 sidebar bg-white">
      <section id="tabs">
        <div className="row">
          <div className="col-md-12">
            <nav>
              <div className="nav nav-tabs nav-fill ">
                <h5
                  onClick={props.tabNavigator}
                  className={
                    props.isActiveTab
                      ? "nav-item nav-link active width_tabs cursor_pointer_class"
                      : "nav-item nav-link  width_tabs cursor_pointer_class"
                  }
                >
                  <FormattedMessage {...messages.chatNavHeaderPersonal} />
                </h5>
                <h5
                  id={"community"}
                  onClick={props.tabNavigator}
                  className={
                    !props.isActiveTab
                      ? "nav-item nav-link active width_tabs cursor_pointer_class"
                      : "nav-item nav-link  width_tabs cursor_pointer_class"
                  }
                >
                  <FormattedMessage {...messages.chatNavHeaderCommunities} />
                </h5>
              </div>
            </nav>
            <div className="py-3 px-lg-2 px-sm-0 sidebar_chat">
              <span className="vendor-id-field mb-3">
                <span className="searchicon">
                  <i className="fa fa-search" />
                </span>
                <FormattedMessage {...messages.chatNavSearchPlaceHolder}>
                  {placeholder => (
                    <input
                      type="text"
                      onChange={event =>
                        event.target.value.length > 2 &&
                        props.filterChatRoomsData(event.target.value)
                      }
                      name="vendor-id"
                      placeholder={placeholder}
                    />
                  )}
                </FormattedMessage>
              </span>
              {props.isActiveTab &&
              props.chatRooms &&
              props.chatRooms.length > 0
                ? props.chatRooms.map((item, index) => {
                    return (
                      <Useritem
                        key={index}
                        id={
                          item.messag &&
                          item.messag.recipient &&
                          item.messag.recipient.recipient_id != userId
                            ? item.messag.recipient.recipient_id
                            : item.messag &&
                              item.messag.sender &&
                              item.messag.sender.sender_id
                        }
                        getPersonalMessage={props.getPersonalMessage}
                        userName={
                          item.messag &&
                          item.messag.recipient &&
                          item.messag.recipient.recipient_id != userId
                            ? item.messag.recipient.user_name
                            : item.messag &&
                              item.messag.sender &&
                              item.messag.sender &&
                              item.messag.sender &&
                              item.messag.sender.user_name
                        }
                        userImage={
                          item.messag &&
                          item.messag.recipient &&
                          item.messag.recipient.recipient_id != userId
                            ? item.messag.recipient &&
                              item.messag.recipient.profile &&
                              item.messag.recipient.profile.profile_image
                            : item.messag &&
                              item.messag.sender &&
                              item.messag.sender.profile.profile_image
                        }
                        messageContent={item.messag && item.messag.content}
                        time={
                          item.messag &&
                          item.messag.recipient &&
                          getDuration(
                            item.messag.recipient.recipient_id != userId
                              ? item.messag.recipient
                              : item.messag &&
                                  item.messag.sender &&
                                  item.messag.sender.created_at
                          )
                        }
                      />
                    );
                  })
                : props.chatRooms.map((item, index) => {
                    return (
                      <Communityitem
                        key={index}
                        id={item.id}
                        getCommunityMessage={props.getCommunityMessage}
                        userName={
                          item.community && item.community.chatroom_name
                        }
                        userImage={
                          item.community && item.community.profile_image
                        }
                        messageContent={item.messag && item.messag.content}
                        groupMembers={
                          item.community && item.community.followers
                        }
                      />
                    );
                  })}
            </div>
          </div>
        </div>
      </section>
      {/*<div className="text-center btn_below">*/}
      {/*  <button type="button" className="btn btn_clor text-white w-75 shadow-none">Start New Chat</button>*/}
      {/*</div>*/}
    </div>
  );
}

Sidebar.propTypes = {
  getCommunityMessage: PropTypes.func,
  getMessageType: PropTypes.func,
  getPersonalMessage: PropTypes.func,
  tabNavigator: PropTypes.func,
  getDuration: PropTypes.func,
  chatRooms: PropTypes.array,
  isActiveTab: PropTypes.bool
};

export default Sidebar;
