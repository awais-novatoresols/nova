import { put, takeLatest } from "redux-saga/effects";
import { apiFetchPut } from "../../utils/network";
import { success, error } from "./actions";
import { CHANGE_PASSWORD } from "./constants";

export function* changePassword(user) {
  try {
    const json = yield apiFetchPut(`users/password`, user.data, user.accessToken, user.clientId, user.uid);
   // console.log("Change Password .........", json);
    if (json.success) yield put(success(json));
    else yield put(error(json));
  } catch (err) {
    yield put(error(err));
  }
}

export default function* userData() {
  yield takeLatest(CHANGE_PASSWORD, changePassword);
}
