import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.changePassword || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.error
  );

const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );
export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectSuccess
};
