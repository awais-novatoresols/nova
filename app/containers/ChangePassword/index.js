import React, { memo, useEffect, useState } from "react";
import SideImage from "../../components/SideImage";
import InputForm from "./InputForm";
import "../SignIn/style.css";
import "bootstrap/dist/css/bootstrap.css";
import { connect } from "react-redux";
import { compose } from "redux";
import { changePassword } from "./actions";
import { createStructuredSelector } from "reselect";
import { makeSelectLoading, makeSelectSuccess, makeSelectError } from "./selectors";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import reducer from "./reducer";
import saga from "./Saga";
import PropTypes from "prop-types";
import "w3-css/4/w3pro.css";
import "font-awesome/css/font-awesome.min.css";
import PasswordChanged from "./PasswordChanged";
import { toast } from "react-toastify";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const key = "changePassword";

export function ChangePassword(props) {
  const [isProceed, setIsProceed] = useState(true);
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  let goProceed = (body) => {
   // console.log("Change Password Body ....", body);
    props.changePassword(body, accessToken, clientId, uid);
  };

  useEffect(() => {
    if (props.success) {
      setIsProceed(false);
    }
  }, [props.success]);

  useEffect(() => {
    if (props.error) toast.error(props.error.message);
  }, [props.error]);

  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);

  return (
    <>
      <SideImage
        title= {<FormattedMessage {...messages.changePasswordSideNaveleftMainHeading}/>}
        body={<FormattedMessage {...messages.changePasswordSideNaveleftSecendHeading}/>}
        end={<FormattedMessage {...messages.changePasswordSideNaveleftThirdHeading}/>}
      />
      {isProceed ? <InputForm goProceed={goProceed} /> : <PasswordChanged />}
    </>
  );
}

ChangePassword.propTypes = {
  success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  changePassword: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  success: makeSelectSuccess(),
  error: makeSelectError(),
  loading: makeSelectLoading()
});

export function mapDispatchToProps(dispatch) {
  return {
    changePassword: (formData, accessToken, clientId, uid) => dispatch(changePassword(formData, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(ChangePassword);
