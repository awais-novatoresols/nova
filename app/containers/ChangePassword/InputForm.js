import "bootstrap/dist/css/bootstrap.css";
import PropTypes from "prop-types";
import React from "react";
import useInputForm from "../../components/useInputForm";
import email from "../../images/Email.png";
import logo from "../../images/logo.png";
import "./style.css";
import messages from './messages';
import { FormattedMessage } from 'react-intl';


export default function InputForm(props) {
  // const userAlert = () => console.log(`Email: ${inputs.email}`);
  const { inputs, handleInputChange, handleSubmit } = useInputForm({
    oldPassword: "",
    newPassword: "",
    confirmPassword: ""
  });
  let submitForm = () => {
    let formData = new FormData();
    formData.append("old_password", inputs.oldPassword);
    formData.append("password", inputs.newPassword);
    formData.append("password_confirmation", inputs.confirmPassword);
    if (inputs.newPassword === inputs.confirmPassword && inputs.newPassword.length >= 8) {
      props.goProceed(formData);
    } else {
      alert("New Password should be same and contain atleast 8 characters");
    }
  };
  return (
    <div className="col-sm-6 login-form-2">
      <div className="sidenav2">
        <div className="text-center">
          <img src={logo} alt="true" />
        </div>
        <h3 className=""><FormattedMessage {...messages.changePasswordSideNaveRightMainHeading}/></h3>
        <form onSubmit={handleSubmit} autoComplete="off">
          <div className="form-group">
            <div className="lang d-flex iconImage">
            <FormattedMessage {...messages.changePasswordSideNaveRightHeading}>
              {placeholder=>
              <input
                type="password" className="input" placeholder={placeholder} name="oldPassword"
                onChange={handleInputChange} value={inputs.oldPassword} required />
              }
              </FormattedMessage>
            </div>
            <div className="line-box">
              <div className="line" />
            </div>
          </div>
          <div className="form-group">
            <div className="lang d-flex iconImage">
            <FormattedMessage {...messages.changePasswordSideNaveRightInputplaceholderNewPass}>
              {placeholder=>
              <input
                type="password" className="input" placeholder={placeholder} name="newPassword"
                onChange={handleInputChange} value={inputs.newPassword} required />
              }
              </FormattedMessage>
            </div>
            <div className="line-box">
              <div className="line" />
            </div>
          </div>
          <div className="form-group">
            <div className="lang d-flex iconImage">
            <FormattedMessage {...messages.changePasswordSideNaveRightInputplaceholderConfirmPass}>
              {placeholder=>
              <input
                type="password" className="input" placeholder={placeholder} name="confirmPassword"
                onChange={handleInputChange} value={inputs.confirmPassword} required />
              }
              </FormattedMessage>
            </div>
            <div className="line-box">
              <div className="line" />
            </div>
          </div>
          <div onClick={submitForm} className="form-group d-flex justify-content-center mt-5">
            <button type="submit" className="btnSubmit mt-3 w-75"><FormattedMessage {...messages.changePasswordSideNaveRightButtonTitle}/></button>
          </div>
        </form>
      </div>
    </div>
  );
}
InputForm.propTypes = {
  goProceed: PropTypes.func
};
