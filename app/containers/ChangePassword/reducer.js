import produce from "immer";
import { CHANGE_PASSWORD, LOAD_ERROR, LOAD_SUCCESS } from "./constants";
// The initial state of the App
export const initialState = {
  loading: false,
  error: false,
  success: false
};
/* eslint-disable default-case, no-param-reassign */
const forgotPasswordReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
    case CHANGE_PASSWORD:
      draft.loading = true;
      draft.success = false;
      draft.error = false;
      break;
    case LOAD_SUCCESS:
      draft.success = action.user;
      draft.loading = false;
      draft.error = false;
      break;
    case LOAD_ERROR:
      draft.error = action.user;
      draft.loading = false;
      draft.success = false;
      break;
    }
  });
export default forgotPasswordReducer;


