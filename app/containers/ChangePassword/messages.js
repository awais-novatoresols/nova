import { defineMessages } from 'react-intl';

export default defineMessages({
    changePasswordSideNaveleftMainHeading : {
        id: 'change-password-side-nav-left-main-heading',
        defaultMessage: 'Change Password?',
    },
    changePasswordSideNaveleftSecendHeading : {
        id: 'change-password-side-nav-left-secend-heading',
        defaultMessage: 'Keep your account secure',
    },
    changePasswordSideNaveleftThirdHeading : {
        id: 'change-password-side-nav-left-third-heading',
        defaultMessage: 'Choose a strong password that is harder to guess',
    },
    changePasswordSideNaveRightMainHeading : {
        id: 'change-password-side-nav-right-main-heading',
        defaultMessage: 'Change Password',
    },
    changePasswordSideNaveRightHeading : {
        id: 'change-password-side-nav-right-input-placeholder-old-password',
        defaultMessage: 'Old Password',
    },
    changePasswordSideNaveRightInputplaceholderNewPass : {
        id: 'change-password-side-nav-right-secend-input-placeholder-new-password',
        defaultMessage: 'New Password',
    },
    changePasswordSideNaveRightInputplaceholderConfirmPass : {
        id: 'change-password-side-nav-right-input-placeholder-confirm-password',
        defaultMessage: 'Confirm Password',
    },
    changePasswordSideNaveRightButtonTitle : {
        id: 'change-password-side-nav-right-button-title',
        defaultMessage: 'Submit',
    },
    changePasswordSuccessfully : {
        id: 'change-password-password-change-succesfully',
        defaultMessage: 'Change Password Change Succesfully!',
    },
    changePasswordProceedLogin : {
        id: 'change-password-procced-login',
        defaultMessage: 'You can now proceed Login',
    },
    headerlinkNewsFeeds: {
        id: 'header-link-news-feed',
        defaultMessage: 'News Feed',
      },
      headerlinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Communities',
      },
      headerlinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
      },

      

});