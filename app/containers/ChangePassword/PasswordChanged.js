import React, { useEffect } from "react";
import "../SignIn/style.css";
import "bootstrap/dist/css/bootstrap.css";
import logo from "../../images/logo.png";
import { Link } from "react-router-dom";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default function PasswordChanged() {
  useEffect(() => {
    localStorage.removeItem('profileId');
    localStorage.removeItem('userId');
    localStorage.removeItem('userName');
    localStorage.removeItem('uid');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('clientId');
  });

  return <div className="col-sm-6 login-form-2 sidenav2">
    <div className="text-center">
      <img src={logo} alt="true" />
    </div>
    <h3 className="p-4"><FormattedMessage {...messages.changePasswordSuccessfully}/>
    <br /><FormattedMessage {...messages.changePasswordProceedLogin}/></h3>
    <form className="login-2">
      <Link to="/">
        <div className="form-group d-flex justify-content-center mt-5">
          <button type="submit" className="btnSubmit mt-3">Login</button>
        </div>
      </Link>
    </form>
  </div>;
}
