import { CHANGE_PASSWORD, LOAD_SUCCESS, LOAD_ERROR } from "./constants";

export function changePassword(data, accessToken, clientId, uid) {
  return {
    type: CHANGE_PASSWORD,
    data,
    clientId,
    accessToken,
    uid
  };
}

export function success(user) {
  return {
    type: LOAD_SUCCESS,
    user
  };
}

export function error(user) {
  return {
    type: LOAD_ERROR,
    user
  };
}