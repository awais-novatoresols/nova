import PropTypes from "prop-types";
import React from "react";
import InputField from "../../components/InputField";
import CountriesList from "../../components/CountriesList";
import messages from "./messages";
import { FormattedMessage } from "react-intl";

const EditProfile = props => {
  let localLanguage = localStorage.getItem("language");
  function changePass() {
    props.history.push("/changepassword");
  }

  return (
    <div className="container-fluid">
      <div className="lang row mt-2">
        <div className="col-sm-8 mr-auto ml-auto mb-3">
          <h3 className="text-center">
            {" "}
            <FormattedMessage {...messages.editProfileModalTitle} />
          </h3>
          <div className={localLanguage === "ar" ? "directRtl" : ""}>
            <label className={localLanguage === "ar" ? "float-right" : ""}>
              {" "}
              <FormattedMessage
                {...messages.editProfileModalLableBackgroundImage}
              />
            </label>
            <input
              type="file"
              className="form-control cursor_pointer_class"
              onChange={e => props.onChange("cover", e)}
            />
          </div>
          <div className={localLanguage === "ar" ? "directRtl" : ""}>
            <label
              className={localLanguage === "ar" ? "float-right mt-3" : "mt-3"}
            >
              {" "}
              <FormattedMessage
                {...messages.editProfileModalLableProfileImage}
              />
            </label>
            <input
              type="file"
              className="form-control cursor_pointer_class"
              onChange={e => props.onChange("profile", e)}
            />
          </div>
          <form className="form-horizontal" role="form">
            <label
              className={localLanguage === "ar" ? "float-right mt-2" : "mt-2"}
            >
              <FormattedMessage {...messages.editProfileModalLabelName} />
            </label>
            <FormattedMessage {...messages.editProfileModalLabelName}>
              {placeholder => (
                <input
                  title="Name"
                  name="name"
                  placeholder={placeholder}
                  maxLength={20}
                  type="text"
                  className={
                    localLanguage === "ar"
                      ? "directRtl form-control"
                      : "form-control"
                  }
                  onChange={e => props.onChange("name", e)}
                  value={props.data && props.data.name ? props.data.name : ""}
                />
              )}
            </FormattedMessage>
            <div className="form-group mt-3">
              <label
                className={
                  localLanguage === "ar"
                    ? "float-right control-label"
                    : "control-label"
                }
              >
                <FormattedMessage {...messages.editProfileModalLableBio} />
              </label>
              <FormattedMessage {...messages.editProfileModalOptional}>
                {placeholder => (
                  <textarea
                    maxLength={150}
                    rows="5"
                    className={
                      localLanguage === "ar"
                        ? "directRtl form-control"
                        : "form-control"
                    }
                    onChange={e => props.onChange("about", e)}
                    placeholder={placeholder}
                    value={
                      props.data && props.data.about ? props.data.about : ""
                    }
                  />
                )}
              </FormattedMessage>
            </div>
            <div>
              <label
                className={localLanguage === "ar" ? "float-right mt-2" : "mt-2"}
              >
                <FormattedMessage
                  {...messages.editProfileModalLableCountryDropDown}
                />
              </label>
              <CountriesList
                onCountrySelect={props.onCountrySelect}
                selected={
                  props.data && props.data.country !== "undefined"
                    ? props.data.country
                    : false
                }
              />
            </div>
            <div className="row checkbox mt-3">
              <div className={localLanguage === "ar" ? "col-md-12 text-right" : "col-md-12"}>
                {localLanguage === "ar" && (
                  <label className="ml-3">
                    <FormattedMessage
                      {...messages.editProfileModalLableCheckBox}
                    />{'\u00A0'}
                  </label>
                )}
                <input
                  type="checkbox"
                  className="mt-1 cursor_pointer_class"
                  data-toggle="toggle"
                  data-onstyle="success"
                  data-offstyle="info"
                  onChange={props.onTypeCheck}
                />
                {localLanguage !== "ar" && (
                  <label className="ml-1">
                    <FormattedMessage
                      {...messages.editProfileModalLableCheckBox}
                    />
                  </label>
                )}
              </div>
            </div>
            <div className="row mt-3">
              <div className="col-md-6">
                <div className="form-group">
                  <button
                    type="button"
                    className="btn btn-info btn_res"
                    onClick={props.onSubmit}
                  >
                    <FormattedMessage
                      {...messages.editProfileModalButtonSaveChanges}
                    />
                  </button>
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  <button
                    type="button"
                    className="btn btn-danger float-right"
                    onClick={changePass}
                  >
                    <FormattedMessage
                      {...messages.editProfileModalButtonChangePassword}
                    />
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
EditProfile.propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  showModel: PropTypes.bool,
  hideModel: PropTypes.func,
  data: PropTypes.object,
  history: PropTypes.object,
  onCountrySelect: PropTypes.func
};
export default EditProfile;
