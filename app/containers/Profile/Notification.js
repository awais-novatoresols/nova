import React from "react";

const Notification = () => {
  return (
    <div className="alpharow">
      <div className="d-flex pl-4">
        <div>
          <img src={require("../../images/ic_profile.png")} className="rounded-circle mt-2" width="28px" height="28px" />
        </div>
        <div>
          <p className="m-0 pt-2 pl-2">James Doe</p>
        </div>
        <div className="ml-auto">
          <h6 className="custom_h6 pr-3">6 Hr</h6>
        </div>
      </div>
      <div>
        <p className="ml-3 pl-5">To open the dropdown menu, use a button..</p>
      </div>
      <div className="text-right mb-4">
        <img className="cursor_p mr-2 mb-2" src={require("../../images/tick.png")} width="40px" height="28px" />
        <img className="cursor_p mr-4 mb-2" src={require("../../images/cross.png")} width="40px" height="28px" />
      </div>
    </div>
  );
}

export default Notification;
