import { defineMessages } from "react-intl";

export default defineMessages({
  footerLinkAbout: {
    id: "footer-link-about",
    defaultMessage: "About"
  },
  footerLinkPrivacyPolicy: {
    id: "footer-link-privacy-policy",
    defaultMessage: "Privacy policy"
  },
  footerLinkFAQs: {
    id: "footer-link-faqs",
    defaultMessage: "FAQs"
  },
  footerLinkContentPolicy: {
    id: "footer-link-content-policy",
    defaultMessage: "Content Policy"
  },
  footerLinkApp: {
    id: "footer-link-app",
    defaultMessage: "App"
  },
  footerUserAgreement: {
    id: "footer-user-agreement",
    defaultMessage: "User Agreement"
  },
  headerLinkNewsFeed: {
    id: "header-link-news-feed",
    defaultMessage: "Home"
  },
  headerLinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Categories"
  },
  headerLinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  },
  headerLinkGeneral: {
    id: "header-link-general",
    defaultMessage: "General"
  },
  headerLanguageDropDownEn: {
    id: "header-language-dropdown-en",
    defaultMessage: "EN"
  },
  headerLanguageDropDownAr: {
    id: "header-language-dropdown-ar",
    defaultMessage: "AR"
  },
  creatPostCardHeading: {
    id: "creat-post-card-text-heading",
    defaultMessage: "ubwab"
  },
  creatPostCardDiscription: {
    id: "creat-post-card-text-discription",
    defaultMessage:
      "Ubwab is a open door to all social life style, you can get connected with your best line communities people."
  },
  creatPostCardButtonTitle: {
    id: "creat-post-card-button-title",
    defaultMessage: "Creat new post"
  },

  imageDetailPostHeaderLinkCommunities: {
    id: "image-detail-post-header-link-my-communities",
    defaultMessage: "My Communities"
  },

  profileNavBarProfile: {
    id: "profile-nav-bar-profile",
    defaultMessage: "Profile"
  },
  profileNavBarSaved: {
    id: "profile-nave-bar-saved",
    defaultMessage: "Saved"
  },
  profileNavBarHistory: {
    id: "profile-nave-bar-history",
    defaultMessage: "History"
  },
  profileNavBarInbox: {
    id: "profile-nave-bar-inbox",
    defaultMessage: "Inbox"
  },
  profileNavBarJoinedList: {
    id: "profile-nave-bar-joined-list",
    defaultMessage: "Joined List"
  },
  profileNavBarSettings: {
    id: "profile-nave-bar-settings",
    defaultMessage: "Settings"
  },
  profileNavBarLogOut: {
    id: "profile-nave-bar-logout",
    defaultMessage: "Logout"
  },
  profileTextReputation: {
    id: "profile-text-reputation",
    defaultMessage: "Reputaion"
  },
  profileTextUbwabAge: {
    id: "profile-text-ubwab-age",
    defaultMessage: "ubwab age"
  },
  profileTextFollowers: {
    id: "profile-text-followers",
    defaultMessage: "Followers"
  },
  profileTextFollowings: {
    id: "profile-text-followings",
    defaultMessage: "Followings"
  },
  userProfileFollowersNothingYet: {
    id: "nothing-yet",
    defaultMessage: "Nothing Yet!"
  },
  userProfileFollowingsNothingYet: {
    id: "nothing-yet",
    defaultMessage: "Nothing Yet!"
  },
  editProfileModalTitle: {
    id: "edit-profile-model-title",
    defaultMessage: "Edit Profile"
  },
  editProfileModalLableBackgroundImage: {
    id: "edit-profile-model-lable-background-image",
    defaultMessage: "Upload background image"
  },
  editProfileModalInputButtonTitleBackgroundImage: {
    id: "edit-profile-model-input-button-title-background-image",
    defaultMessage: "Choose file"
  },
  editProfileModalInputPlaceholderBackgroundimage: {
    id: "edit-profile-model-input-placeholder-background-image",
    defaultMessage: "No file choosen"
  },
  editProfileModalLableProfileImage: {
    id: "edit-profile-model-lable-profile-image",
    defaultMessage: "Upload profile image"
  },
  editProfileModalInputButtonTitleProfileImage: {
    id: "edit-profile-model-input-button-title-profile-image",
    defaultMessage: "Choose file"
  },
  editProfileModalInputPlaceHolderprofileImage: {
    id: "edit-profile-model-input-placeholder-profile-image",
    defaultMessage: "No file choosen"
  },
  editProfileModalLabelName: {
    id: "edit-profile-model-label-name",
    defaultMessage: "Name"
  },
  editProfileModalInputPlaceHolderName: {
    id: "edit-profile-model-input-placeholder-name",
    defaultMessage: "Enter your name"
  },
  editProfileModalLableBio: {
    id: "edit-profile-model-label-bio",
    defaultMessage: "Bio"
  },
  editProfileModalOptional: {
    id: "edit-profile-model-optional",
    defaultMessage: "Optional"
  },
  editProfileModalLableCountryDropDown: {
    id: "edit-profile-model-label-country-country-drop-down",
    defaultMessage: "Country"
  },
  editProfileModalLableCheckBox: {
    id: "edit-profile-model-lable-checkbox",
    defaultMessage: "Make my profile private"
  },
  editProfileModalButtonSaveChanges: {
    id: "edit-profile-model-button-save-changes",
    defaultMessage: "Save Changes"
  },
  editProfileModalButtonChangePassword: {
    id: "edit-profile-model-button-change-password",
    defaultMessage: "Change password"
  },

  profileSavedheading: {
    id: "profile-saved-heading-saved-post",
    defaultMessage: "Saved"
  },
  profileSavedNoPost: {
    id: "profile-saved-heading-no-post",
    defaultMessage: "No Posts"
  },
  profileHistoryHeading: {
    id: "profile-history-heading-history-post",
    defaultMessage: "History Post"
  },
  profileHistoryDropDownHeading: {
    id: "profile-history-drop-down-sort-by",
    defaultMessage: "Sort by"
  },
  profileHistoryDropDownFilter: {
    id: "profile-history-drop-down-filter",
    defaultMessage: "Filter"
  },
  profileHistoryDropDownUpVote: {
    id: "profile-history-drop-down-upvote",
    defaultMessage: "Upvote"
  },
  profileHistoryDropDownDownVote: {
    id: "profile-history-drop-down-downvote",
    defaultMessage: "Downvote"
  },
  profileHistoryDropDownHide: {
    id: "profile-history-drop-down-hide",
    defaultMessage: "Hide"
  },
  profileInboxHeading: {
    id: "profile-inbox-heading",
    defaultMessage: "Inbox"
  },
  profileInboxHeaderMessages: {
    id: "profile-inbox-header-messages",
    defaultMessage: "Messages"
  },
  profileInboxHeaderNotifications: {
    id: "profile-inbox-header-notifications",
    defaultMessage: "Notifications"
  },
  profileInboxHeaderModEmail: {
    id: "profile-inbox-header-mod-email",
    defaultMessage: "Mod Email"
  },
  profileInboxItemNewMessage: {
    id: "profile-inbox-item-new-message",
    defaultMessage: "New Message"
  },
  profileInboxItemNewMessageFormHeadingTo: {
    id: "profile-inbox-item-new-message-form-heading-to",
    defaultMessage: "To"
  },
  profileInboxItemNewMessageFormInputPlaceholder: {
    id: "profile-inbox-item-new-message-form-input-placeholder",
    defaultMessage: "Type text here"
  },
  profileInboxItemNewMessageFormButtonTitle: {
    id: "profile-inbox-item-new-message-form-button-title",
    defaultMessage: "Send message"
  },
  settingHeadingGeneralInfo: {
    id: "settings-heading-general-info",
    defaultMessage: "General"
  },
  settingHeadingButtonTitle: {
    id: "settings-heading-button-title",
    defaultMessage: "Add Acount"
  },
  settingContainerHeadingAccount: {
    id: "settings-container-heading-account",
    defaultMessage: "Account"
  },
  settingContainerLinkNotification: {
    id: "settings-container-link-notification",
    defaultMessage: "Notification"
  },
  settingContainerHeadingVirifyEmail: {
    id: "settings-container-heading-verify-email",
    defaultMessage: "Verify Email"
  },
  settingContainerHeadingVirified: {
    id: "settings-container-heading-verified",
    defaultMessage: "Verified"
  },
  settingContainerHeadingNotVirified: {
    id: "settings-container-heading-not-verified",
    defaultMessage: "Not Verified Yet"
  },
  settingContainerLinkBlockedUser: {
    id: "settings-container-link-blocked-user",
    defaultMessage: "Blocked user"
  },
  settingsAddAccountModelHeader: {
    id: "settings-add-account-model-header",
    defaultMessage: "Account"
  },
  headerlinkNewsFeeds: {
    id: "header-link-news-feed",
    defaultMessage: "Home"
  },
  headerlinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  },
  headerlinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Categories"
  }
});
