import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.profile || initialState;
const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading,

  );
const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );
const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );
const makeSelectUserData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userData.user.user
  );
const makeSelectUserPost = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userPost
  );

const makeSelectSavedPost = () => createSelector(
  selectGlobal,
  globalState => globalState.savedPosts
);

const makeSelectHistoryPost = () => createSelector(
  selectGlobal,
  globalState => globalState.historyPosts
);

const makeSelectLogout = () => createSelector(
  selectGlobal,
  globalState => globalState.logout
);

const makeSelectCommunities = () => createSelector(
  selectGlobal,
  globalState => globalState.communities.communities
);

const makeSelectJoinedCommunities = () => createSelector(
  selectGlobal,
  globalState => globalState.joinedCommunities.communities
);

export { selectGlobal, makeSelectCommunities, makeSelectJoinedCommunities, makeSelectLoading, makeSelectError, makeSelectUserData, makeSelectUserPost, makeSelectSuccess, makeSelectSavedPost, makeSelectHistoryPost, makeSelectLogout };