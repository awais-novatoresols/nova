import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import PropTypes from "prop-types";
import React from "react";
import "w3-css/4/w3pro.css";
import "../HomePage/style.css";
import EditProfile from "./EditProfile";
import "./globalStyle.css";
import "./style.css";
import {Modal} from "react-bootstrap";
import {getFlagByCode} from "../../components/CountriesList";
import moment from "moment";
import {Link} from "react-router-dom";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

export default function UserProfile(props) {
  function getDuration(publishDate) {
    let startDate = new Date();
    let a = moment(startDate);
    let b = moment(publishDate);
    let difference = a.diff(b, "minutes");
    let hour = a.diff(b, "hour");
    return `${moment.duration(difference, "minutes").humanize()} ${moment.duration(hour, "minutes").humanize()}`;
  }
  return (
    <>
      <div className="card custom_c mt-3">
        <div className="d-flex mt-4">
          <div>
            <h6 className="font-weight-bold pl-3  custom_ps">{props.data && props.data.profile.name}</h6>
            <span className="pl-3 pb-5">{props.data && props.data.user_name}</span>
          </div>
          {props.data && props.data.email === localStorage.getItem("uid") && <div className="b_model1 ml-auto mr-4">
            <a onClick={props.displayModel} className="b_model cursor_pointer_class">
              <img className="img-responsive img_res1 cursor_pointer_class" height="15px" width="15px"
                   src={require("../../images/pencil2.png")}/>
            </a>
            <div className="container-fluid">
              <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={props.showModel} onHide={props.hideModel}>
                <Modal.Body>
                  <div className="w3-center"><br/>
                    <span onClick={props.hideModel}
                          className="w3-button w3-xlarge w3-hover-red w3-display-topright"
                          title="Close Modal">&times;</span>
                  </div>
                  <EditProfile
                    onChange={(name, e) => props.onChange(name, e)} onSubmit={props.onSubmit}
                    data={props.formValues} onCountrySelect={props.onCountrySelect}
                    onTypeCheck={props.onTypeCheck} history={props.history}/>
                </Modal.Body>
              </Modal>
            </div>
          </div>}
        </div>
        <div className="d-flex">
          <div className="c_text1 pl-3">
            <img className="c_img mr-2"
                 src={require("../../images/heart.png")}/>{props.data && props.data.profile && props.data.profile.reputation ? parseInt(props.data.profile.reputation) : 0} 
                 {'\u00A0'}<FormattedMessage {...messages.profileTextReputation} />
          </div>
          <div className="c_text2 ml-auto mr-4 mt-2">
            <img className="c_img mr-2"
                 src={require("../../images/cake.png")}/> {props.data && props.data.profile && props.data.profile.reputation ? getDuration(props.data.profile.created_at) : 0}
                {'\u00A0'}<FormattedMessage {...messages.profileTextUbwabAge} />
          </div>
        </div>
        <div className="row mt-2">
          <div className="col-md-4 text-center">
            {props.data && getFlagByCode(props.data.profile.country)}
          </div>
          <div className="col-md-4 text-center font-weight-bold">
            <span className="d-block custom_ps">{props.data && props.data.profile.followers}</span>
            <Link to={{
              pathname: "/profile/followers",
              title: "Followers",
              id: props.data && props.data.profile.id
            }}>
              <span className="d-block custom_ps"><FormattedMessage {...messages.profileTextFollowers} /></span>
            </Link>
          </div>
          <div className="col-md-4 text-center font-weight-bold">
            <span className="d-block custom_ps">{props.data && props.data.profile.followed}</span>
            <Link to={{
              pathname: "/profile/followings",
              title: "Followings",
              id: props.data && props.data.profile.id
            }}>
              <span className="d-block custom_ps"><FormattedMessage {...messages.profileTextFollowings} /></span>
            </Link>
          </div>
        </div>
        <div className="pl-4">
          <p className="alpha_h">{props.data && props.data.profile.about}</p>
        </div>
      </div>
    </>
  );
}
UserProfile.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  showModel: PropTypes.bool,
  hideModel: PropTypes.func,
  displayModel: PropTypes.func,
  onCountrySelect: PropTypes.func,
  onTypeCheck: PropTypes.func,
  formValues: PropTypes.object,
  history: PropTypes.object
};
