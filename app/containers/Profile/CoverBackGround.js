import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import PropTypes from "prop-types";
import React from "react";
import "../HomePage/style.css";
import "./globalStyle.css";
import "./style.css";

export default function CoverBackGround(props) {
  let coverImage = props.data && props.data.profile.background_image;
  let profileImage = props.data && props.data.profile.profile_image;
  return (
    <>
      <div className="row">
        <div className="col-sm-12">
          <div className="card mt-4 card_img">
            <img className="card_img" src={coverImage ? coverImage : require("../../images/placeholder2.jpg")}
                 height="250px"/>
            <div className="row">
              <div className="col-sm-8 col-4 bottom-left">
                <img className="rounded-circle thumbnail_1"
                     src={profileImage ? profileImage : require("../../images/user.png")}
                     width="100" height="100"/>
              </div>
              <div className="col-sm-4 col-8 bottom-right_1 text-right">
                <a><img id="t1_img" className="btnSubmt3 mr-3 mb-1" src={require("../../images/bellicon.png")}
                        width="38px"/></a>
                <button type="submit" className="w-50 btnSubmt2 mt-2">Following</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
CoverBackGround.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  cover: PropTypes.string,
  profilePic: PropTypes.string
};
