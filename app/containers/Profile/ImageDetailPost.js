import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import moment from "moment";
import PropTypes from "prop-types";
import React from "react";
import "../HomePage/style.css";
import "./globalStyle.css";
import "./style.css";

export default function ImageDetailPost(props) {
 // console.log(props.posts.post && props.posts.post.posts, ' from Image Detail Component');
  let postArray = props.posts.post && props.posts.post.posts;
  let imagePostArray = postArray && postArray.filter((item) => {
    return item.poll_options.length === 0;
  });

  function getDuration(publishDate) {
    let startDate = new Date();
    let a = moment(startDate);
    let b = moment(publishDate);
    let difference = a.diff(b, "minutes");
    return moment.duration(difference, "minutes").humanize();
  }

  return (
    <>
      {imagePostArray && imagePostArray.map((item, index) => (
        <div className="card custom_c mt-4" key={index}>
          <div className="card-header d-flex custom_ch">
            <img src={item.user.profile && item.user.profile.profile_image !== null ?
              item.user.profile.profile_image : require("../../images/user.png")}
              className="rounded-circle" />
            <p className="custom_p1">{item.user && item.user.user_name}<br />{item.community && item.community.name}</p>
            <h6 className="ml-auto mr-2 mt-2">{getDuration(item.created_at)} ago</h6>
            <div className="dropdown">
              <button className="btn alphab1" id="menu1" data-toggle="dropdown">
                <img className="alphab2" src={require("../../images/list.png")} />
              </button>
              <div className="dropdown-menu dropdown-menu-right">
                <li role="presentation" className="mt-2">
                  <img
                    src={require("../../images/inbox.png")}
                    className="ml-2 p-1" /> Save
                </li>
                <li role="presentation" className="mt-2">
                  <img
                    src={require("../../images/visibility.png")}
                    className="ml-2 p-1" /> Hide
                </li>
                <li role="presentation" className="mt-2">
                  <img
                    src={require("../../images/flag.png")} className="ml-2 p-1" />Report
                </li>
                <li role="presentation" className="mt-2">
                  <img
                    src={require("../../images/block.png")}
                    className="ml-2 p-1" /> Block User
                </li>
              </div>
            </div>
          </div>
          <div className="card-body p-0">
            <h5 className="p-3">{item.title}</h5>
            <img className="custom_i2"
              src={item.post_type === "image" && item.link ? item.link : require("../../images/placeholder2.jpg")} />
          </div>
          <div className="card-footer custom_f1">
            <a  className="card-link"><img
              src={require("../../images/arrowup.png")} width="3%" className="ml-4" /></a>
            <a  className="card-link custom_a1">{item.community && item.community.followers}</a>
            <a  className="card-link"><img
              src={require("../../images/arrowdown.png")} width="3%" /></a>
            <a  className="card-link custom_a2"><img
              src={require("../../images/comment.png")} width="5%" /></a>
            <a  className="card-link ml-auto custom_a1"> {" " + item.comment_count} Comment</a>
            <a  className="card-link custom_a3"><img
              src={require("../../images/share.png")} width="5%" /></a>
            <a  className="card-link ml-auto custom_a1">Share</a>
          </div>
        </div>
      ))}
    </>
  );
}

ImageDetailPost.propTypes = {
  posts: PropTypes.object
};
