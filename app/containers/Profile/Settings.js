import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Settings = props => {
  let userId = localStorage.getItem("userId");
  let localLanguage = localStorage.getItem('language');
  return <>
    <div className="container">
      <div className="row sdie_right2 mb-3">
        <div className="col-md-12 p-0">
          <div className="d-flex">
            <h3 className="m-0"><FormattedMessage {...messages.settingHeadingGeneralInfo} /></h3>
            <button type="button" className="btn_acc text-white btn ml-auto border-0" data-toggle="modal"
                    data-target="#usermodal"><i className="fa fa-plus"></i>&nbsp; <FormattedMessage {...messages.settingHeadingButtonTitle} />
            </button>
          </div>
          <div className="modal fade" id="usermodal">
            <div className="modal-dialog modal-dialog-centered">
              <div className="modal-content">
                <div className="modal-header text-center">
                  <h4 className="modal-title w-100 text-center" id="exampleModalLabel">
                    <FormattedMessage {...messages.settingsAddAccountModelHeader} /></h4>
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                </div>
                <div className="modal-body">
                  <div className="d-flex">
                    <img
                      src={localStorage.getItem("profile_image") !== null ? localStorage.getItem("profile_image") : require("../../images/ic_profile.png")}
                      className="rounded-circle" height="45px"/>
                    <p className="pl-3 pt-2">{props.user}</p>
                    <i className="fa fa-sign-out ml-auto sign_out cursor_pointer_class"></i>
                  </div>
                  <div className="d-flex mt-3 ">
                    <img src={require("../../images/user.png")} className="rounded-circle" height="45px"/>
                    <p className="pl-3 pt-2 cursor_pointer_class"> <FormattedMessage {...messages.settingHeadingButtonTitle} /></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={`${localLanguage === 'ar' ? 'directRtl ' : ' '} col-md-12 bg-white pl-4 pr-4 mt-3 shadow rounded`}>
          <div className="d-flex mt-2">
            <h6><b><FormattedMessage {...messages.settingContainerHeadingAccount} /></b></h6>
            <h5 className={`${localLanguage === 'ar' ? 'mr-auto ' : 'ml-auto '} h5_clr`}>{props.user}</h5>
          </div>
          <hr className="top_m"/>
          <div className="d-flex">
            <Link to="/notifications">
              <h6 className="text-dark"><b><FormattedMessage {...messages.settingContainerLinkNotification} /></b></h6>
            </Link>
          </div>
          <hr className="top_m"/>
          <div className="d-flex">
            <h6><b><FormattedMessage {...messages.settingContainerHeadingVirifyEmail} /></b></h6>
            {
              props.verified ? <h5 className={`${localLanguage === 'ar' ? 'mr-auto ' : 'ml-auto '} h5_clr`}>
                <FormattedMessage {...messages.settingContainerHeadingVirified} />
                </h5> :
                <h5 onClick={props.verifyUser} className={`${localLanguage === 'ar' ? 'mr-auto ' : 'ml-auto '} h5_clr`}>
                  <FormattedMessage {...messages.settingContainerHeadingNotVirified} />
                </h5>
            }
          </div>
          <hr className="top_m"/>
          <div className="d-flex">
            <Link to={{
              pathname: "/profile/blocked",
              title: "Blocked User",
              id: userId
            }}>
              <h6 className="text-dark"><b><FormattedMessage {...messages.settingContainerLinkBlockedUser} /></b></h6>
            </Link>
          </div>
        </div>
      </div>
    </div>
  </>
}

Settings.propTypes = {
  user: PropTypes.string,
  verified: PropTypes.string,
  verifyUser:PropTypes.func
}

export default Settings;
