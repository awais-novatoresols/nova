import React from "react";
import PropTypes from "prop-types";

const InboxItem = props => {
  return (
    <div className="alpharow">
      <div className="d-flex pl-4 pb-2">
        <div>
          <img src={require("../../images/ic_profile.png")} className="rounded-circle mt-2" width="28px" height="28px" />
        </div>
        <div>
          <p className="lead m-0 pt-2 pl-2">James Doe</p>
        </div>
        <div className="ml-auto">
          <h6 className="custom_h6 pr-3 mt-3">6 Hr</h6>
        </div>
      </div>
      <div>
        <p className="ml-3 pl-5">To open the dropdown menu, use a button..</p>
      </div>
      {props.sentMessage && <div className="text-right mb-4" onClick={props.onNewMessage}>
        <h6 className="pb-3 pr-4 text-info">New Messages</h6>
      </div>}
    </div>
  );

  // return (
  //   <div className="d-block bg-white mt-3">
  //     <div className="row pt-2 pb-2">
  //       <div className="col-sm-2">
  //         <img className="rounded-circle float-right" src={require("../../images/avatar.jpg")} alt="item" width="50" height="50" />
  //       </div>
  //       <div className="col-sm-7 ">
  //         <span className="d-block lead">Text</span>
  //         <p>askljasdlkasd  asjfsalkd alskjdasd saldkjasdla asldjalksf safhsalkfdsa dolsjfsa ajsldkas</p>
  //       </div>
  //       <div className="col-sm-3">
  //         <span className="d-inline-block mr-2 p-2 text-white" style={{ background: "#17a2b8"}}><i className="fa fa-check ml-2 mr-2"></i></span>
  //         <span className="d-inline-block p-2 text-white" style={{ background: "#fd7e14"}}><i className="fa fa-times ml-2 mr-2"></i></span>
  //       </div>
  //     </div>
  //   </div>
  // )
}

InboxItem.propTypes = {
  sentMessage: PropTypes.bool,
  onNewMessage: PropTypes.func
}

export default InboxItem;
