import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import "w3-css/4/w3pro.css";
import CommunityBar from "../../components/CommunityBar/CommunityBar";
import Cover from "../../components/Cover";
import CreatePost from "../../components/CreatePost";
import CreatePostForm from "../../components/CreatePostForm";
import FooterLink from "../../components/FooterLink";
import Header from "../../components/Header";
import NavBar from "../../components/NavBar";
import PostItem from "../../components/PostItem/PostItem";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import "../HomePage/style.css";
import {
  editProfile,
  getHistoryPost,
  getJoinedCommunities,
  getSavedPost,
  getUserCommunities,
  getUserPost,
  getUserProfile,
  logout,
  verifyUserProfile
} from "./actions";
import "./globalStyle.css";
import InboxItem from "./InboxItem";
import NewMessage from "./NewMessage";
import Notification from "./Notification";
import reducer from "./reducer";
import saga from "./Saga";
import {
  makeSelectCommunities,
  makeSelectHistoryPost,
  makeSelectJoinedCommunities,
  makeSelectLoading,
  makeSelectLogout,
  makeSelectSavedPost,
  makeSelectSuccess,
  makeSelectUserData,
  makeSelectUserPost
} from "./selectors";
import "./style.css";
import UserProfile from "./UserProfile";
import CommunityCard from "../../components/CommunityCard";
import NoPosts from "../../components/NoPosts";
import { useBottomScrollListener } from "react-bottom-scroll-listener";
import SortHistory from "../../components/SortHistory";
import Settings from "./Settings";
import {makeSelectAllUserCommunities} from "../../components/CommunityCard/selectors";
import {createNewMessage} from "../Chat/actions";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const key = "profile";
let Menu = [
  {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
  {id: 3, name: <FormattedMessage {...messages.headerLinkGeneral}/>, path: "/general"},
  {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
];

export function Profile(props) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  useBottomScrollListener(onBottomHit);
  const [name, setName] = useState("");
  const [about, setAbout] = useState("");
  const [cover, setCover] = useState();
  const [profile, setProfile] = useState();
  const [showModel, setShowModel] = useState(false);
  const [showEditProfile, setEditProfile] = useState(false);
  const [activeView, setActiveView] = useState("My Profile");
  const [inboxTab, setInboxTab] = useState("1");
  const [userProfile, setUserProfile] = useState({});
  const [country, setCountry] = useState();
  const [isPrivate, setIsPrivate] = useState();
  let userName = localStorage.getItem("userName");
  const [editProfileProps, setEditProfileProps] = useState();
  let userId = props.match.params.userId || localStorage.getItem("userId");
  let profileId =
    props.match.params.profileId || localStorage.getItem("profileId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  // console.log("navBarOption...", profileView, savedPostView, historyPost, subscriptionView);
  let data = props.data;
  let posts = props.posts;
  let localLanguage = localStorage.getItem('language');
  // console.log("data....", data);
  // useEffect(() => {
  //   setActiveView("Profile");
  // }, []);

  useEffect(() => {
    if (data && data.profile) {
      setEditProfileProps({
        name: data.profile.name,
        about: data.profile.about,
        country: data.profile.country,
        acountType: data.profile.account_type === "private" ? true : false
      });
    }
  }, [props.data]);

  function displayModel() {
    setShowModel(true);
  }

  function hideModel() {
    setShowModel(false);
  }

  function editProfileModal() {
    setEditProfile(!showEditProfile);
  }

  function setInboxTabIndex(e) {
    setActiveView("Inbox");
    setInboxTab(e.target.id);
    // console.log("Inbox Tab Changed", e.target.id);
  }

  function logout() {
    // console.log("Logging out ...", uid);
    let formData = new FormData();
    formData.append("user[email]", uid);
    props.getLogout(formData, clientId, accessToken, uid);
    localStorage.removeItem('profileId');
    localStorage.removeItem('userId');
    localStorage.removeItem('userName');
    localStorage.removeItem('uid');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('clientId');
    this.props.history.location.push("/");
  }

  function onNewMessage() {
    setActiveView("NewMessage");
  }

  function onBottomHit() {
    console.log("Bottom Hit!!!");
    let posts = props.posts.post;
    if (posts && posts.current_page) {
      if (parseInt(posts.current_page) < parseInt(posts.total_pages)) {
        let pageNo = parseInt(posts.current_page) + 1;
        console.log("New Posts Loaded!!!", pageNo);
        props.getUserPost(userId, pageNo, clientId, accessToken, uid);
      }
    }
  }
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  useEffect(() => {
    if (props.logoutStatus) {
      props.history.push("/");
    }
  }, [props.logoutStatus]);

  useEffect(() => {
    // console.log("Inside Use Effect ....", userId);
    props.getUserPost(userId, 1, clientId, accessToken, uid);
    if (data) setUserProfile(data);
  }, [props.data]);
  useEffect(() => {
    if (props.success) {
      props.getUserProfile(userId, profileId, accessToken, clientId, uid);
      // alert("Profile Edited Successfully!");
    }
  }, [props.success]);
  useEffect(() => {
    // console.log("header from profile", clientId, accessToken, uid);
    props.getUserProfile(userId, profileId, accessToken, clientId, uid);
    // props.getSavedPost(clientId, accessToken, uid);
    props.getHistoryPost("", clientId, accessToken, uid);
    props.getUserCommunities(userId, accessToken, clientId, uid);
    props.getJoinedCommunities(userId, accessToken, clientId, uid);
  }, []);
  const [sideBar, openSidebar] = useState(true);
  let openSideBar = () => openSidebar(true);
  const onInputChange = (name, e) => {
    if (name === "name") {
      setName(e.target.value);
      let newEditProfileProps = { ...editProfileProps };
      newEditProfileProps.name = e.target.value;
      setEditProfileProps(newEditProfileProps);
    }
    if (name === "about") {
      setAbout(e.target.value ? e.target.value : " ");
      let newEditProfileProps = { ...editProfileProps };
      newEditProfileProps.about = e.target.value;
      setEditProfileProps(newEditProfileProps);
    }
    if (name === "cover") setCover(e.target.files[0]);
    if (name === "profile") setProfile(e.target.files[0]);
  };
  const onProfileSubmit = () => {
    let formData = new FormData();
    let accountType = isPrivate ? "private" : "public";
    formData.append("profile[country]", country);
    formData.append("profile[account_type]", accountType);
    if (name !== "") formData.append("profile[name]", name);
    if (about !== "") formData.append("profile[about]", about);
    if (cover) formData.append("profile[background_image]", cover);
    if (profile) {
      formData.append("profile[profile_image]", profile);
      let url = URL.createObjectURL(profile);
      localStorage.setItem("profile_image", url);
    }
    if (!name && !about && !cover && !profile && !country) {
      editProfileModal();
    } else {
      props.editProfile(
        formData,
        userId,
        profileId,
        clientId,
        accessToken,
        uid
      );
      props.getUserProfile(userId, profileId, accessToken, clientId, uid);
      editProfileModal();
    }
  };

  function fetchPost() {
    if (activeView === "My Profile")
      props.getUserPost(userId, 1, clientId, accessToken, uid);
    if (activeView === "Saved") props.getSavedPost(clientId, accessToken, uid);
    if (activeView === "History")
      props.getHistoryPost("", clientId, accessToken, uid);
  }

  function navBarOption(numb) {
    if (numb === 1) {
      Menu = [
        {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
        {id: 3, name: <FormattedMessage {...messages.headerLinkGeneral}/>, path: "/general"},
        {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
      ];
      setActiveView("My Profile");
    } else if (numb === 2) {
      Menu = [
        {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
        {id: 3, name: <FormattedMessage {...messages.headerLinkGeneral}/>, path: "/general"},
        {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"},
      ];
      props.getSavedPost(clientId, accessToken, uid);
      setActiveView("Saved");
    } else if (numb === 3) {
      Menu = [
        {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
        {id: 3, name: <FormattedMessage {...messages.headerLinkGeneral}/>, path: "/general"},
        {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
      ];
      setActiveView("History");
    } else if (numb === 4) {
      Menu = [
        { id: 1, name: "MESSAGES", action: setInboxTabIndex },
        { id: 2, name: "NOTIFICATIONS", action: setInboxTabIndex },
        { id: 3, name: "MOD EMAIL", action: setInboxTabIndex }
      ];
      setActiveView("Inbox");
    } else if (numb === 5) {
      Menu = [
        {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
        {id: 3, name: <FormattedMessage {...messages.headerLinkGeneral}/>, path: "/general"},
        {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
      ];
      setActiveView("Joined List");
    } else if (numb === 6 && props.communities.length > 0) {
      Menu = [
        {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
        {id: 3, name: <FormattedMessage {...messages.headerLinkGeneral}/>, path: "/general"},
        {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
      ];
      setActiveView("Moderation");
    } else if (numb === 7) setActiveView("Settings");
  }

  function onFollow() {
    props.getUserProfile(userId, profileId, accessToken, clientId, uid);
  }

  function onCountrySelect(e) {
    setCountry(e.target.value);
    let newEditProfileProps = { ...editProfileProps };
    newEditProfileProps.country = e.target.value;
    setEditProfileProps(newEditProfileProps);
  }

  function onTypeCheck(e) {
    let newType = !isPrivate;
    let newEditProfileProps = { ...editProfileProps };
    newEditProfileProps.accountType = newType;
    setEditProfileProps(newEditProfileProps);
    setIsPrivate(newType);
  }

  function onHistorySort(e) {
    props.getHistoryPost(e.target.id, clientId, accessToken, uid);
  }

  const verifyUser = () => {
    props.verifyUserProfile(userId, accessToken, clientId, uid);
  };

  function createMessage(chatRoomId, message, messageType, url, communityChat) {
    let formData = new FormData();
    formData.append("message[recipient_id]", chatRoomId);
    formData.append("message[sender_name]", userName);
    formData.append("message[content]", message);
    formData.append("message[message_type]", messageType);
    formData.append("message[url]", url);
    formData.append("message[community_chat]", communityChat);
    props.createNewMessage(formData, accessToken, clientId, uid);
  }

  return (
    <>
      <div className="container-fluid bg-light">
        <Header
          openSideBar={openSideBar}
          nav={Menu}
          {...props}
          selectedTab={inboxTab}
        />
        <div className="row">
          {props.data && props.data.email === localStorage.getItem("uid") ? (
            sideBar && (
              <NavBar
                navBarOption={navBarOption}
                logoutUser={logout}
                activeView={activeView}
                moderation={props.communities}
              />
            )
          ) : (
            <div className="col-lg-1" />
          )}
          <div className="col-sm-12 col-md-12 col-lg-10">
            {activeView === "My Profile" && (
              <Cover
                createMessage={createMessage}
                data={props.data}
                cover={data && data.profile.background_image}
                profilePic={data && data.profile.profile_image}
                buttons={props.match.params.userId ? true : false}
                profile={userProfile.profile}
                onFollow={onFollow}
              />
            )}
            <div className="container">
              <div className="row">
                <div className="col-sm-7">
                  {activeView === "My Profile" && (
                    <UserProfile
                      data={data}
                      onChange={onInputChange}
                      onSubmit={onProfileSubmit}
                      formValues={editProfileProps}
                      showModel={showEditProfile}
                      displayModel={editProfileModal}
                      hideModel={editProfileModal}
                      onCountrySelect={onCountrySelect}
                      onTypeCheck={onTypeCheck}
                      history={props.history}
                    />
                  )}
                  {activeView === "My Profile" && (
                    <div>
                      {posts && props.posts.post.posts && (
                        <PostItem
                          posts={posts}
                          getLatestPost={fetchPost}
                          {...props}
                        />
                      )}
                    </div>
                  )}
                  {activeView === "Saved" && (
                    <div>
                      <h3 className={`${localLanguage === 'ar' ? 'text-right' : ''} mt-2`}><FormattedMessage {...messages.profileSavedheading}/></h3>
                      {props.savedPosts && (
                        <PostItem
                          posts={{ post: { posts: props.savedPosts.data } }}
                          getLatestPost={fetchPost}
                          isSavedTab
                          hideTime
                        />
                      )}
                      {props.savedPosts &&
                        props.savedPosts.data &&
                        props.savedPosts.data.length === 0 && <NoPosts />}
                    </div>
                  )}
                  {activeView === "History" && (
                    <div>
                      {/* <h3 className="mt-2">History Post</h3> */}
                      {props.historyPosts &&
                        props.historyPosts.posts &&
                        props.historyPosts.posts.length > 0 && (
                          <>
                            <SortHistory onHistorySort={onHistorySort} />
                            <PostItem
                              posts={{ post: props.historyPosts }}
                              getLatestPost={fetchPost}
                              hidePostOptions
                            />
                          </>
                        )}
                      {props.historyPosts &&
                        props.historyPosts.posts &&
                        props.historyPosts.posts.length === 0 && (
                          <>
                            <SortHistory onHistorySort={onHistorySort} />
                            <NoPosts />
                          </>
                        )}
                    </div>
                  )}
                  {/* {activeView === "Inbox" && inboxTab === "1" && (
                    <div className="container-fluid p-2">
                      <h3 className="mt-2 mb-4">Inbox</h3>
                      <InboxItem sentMessage onNewMessage={onNewMessage} />
                      <InboxItem sentMessage onNewMessage={onNewMessage} />
                    </div>
                  )}
                  {activeView === "Inbox" && inboxTab === "2" && (
                    <div className="container-fluid p-2">
                      <h3 className="mt-2 mb-4">Notifications</h3>
                      <Notification />
                      <Notification />
                    </div>
                  )}
                  {activeView === "Inbox" && inboxTab === "3" && (
                    <div className="container-fluid p-2">
                      <h3 className="mt-2 mb-4">MOD Email</h3>
                      <InboxItem />
                      <InboxItem />
                    </div>
                  )} */}
                  {activeView === "Moderation" && props.communities.length > 0 && (
                    <div className="pl-3 pr-3 community_joined">
                      {props.communities.map(community => {
                        return (
                          <CommunityBar
                            key={community.id}
                            community={community}
                            isModeration
                          />
                        );
                      })}
                    </div>
                  )}
                  {activeView === "Joined List" &&
                    props.joinedCommunities.length > 0 && (
                      <div className="pl-3 pr-3 community_joined">
                        {props.joinedCommunities.map(community => {
                          return (
                            <CommunityBar
                              key={community.id}
                              community={community}
                              isModeration
                            />
                          );
                        })}
                      </div>
                    )}
                  {activeView === "Settings" && (
                    <div className="pl-3 pr-3">
                      <Settings
                        user={props.data.user_name}
                        verifyUser={verifyUser}
                        verified={props.data.confirmed_at}
                      />
                    </div>
                  )}
                  {/*{activeView === "NewMessage" &&*/}
                  {/*<NewMessage id={props.match.params.userId} createMessage={createMessage}/>}*/}
                </div>
                <div className="lang col-sm-5 mt-5">
                  {props.data &&
                    props.data.email === localStorage.getItem("uid") && (
                      <>
                        <CreatePostForm
                          fetchPost={fetchPost}
                          isCommunity={false}
                          showModel={showModel}
                          communities={props.communitiesData}
                          displayModel={displayModel}
                          hideModel={hideModel}
                        />
                        <CreatePost
                          showModel={showModel}
                          displayModel={displayModel}
                          hideModel={hideModel}
                        />
                      </>
                    )}
                  <CommunityCard />
                  <FooterLink isShow={true} />
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-1" />
        </div>
      </div>
      {/*{props.loading && <Spinner/>}*/}
    </>
  );
}

Profile.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  posts: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  communities: PropTypes.array,
  getUserProfile: PropTypes.func,
  getUserPost: PropTypes.func,
  editProfile: PropTypes.func,
  success: PropTypes.bool,
  getSavedPost: PropTypes.func,
  savedPosts: PropTypes.object,
  getHistoryPost: PropTypes.func,
  historyPosts: PropTypes.object,
  getLogout: PropTypes.func,
  logoutStatus: PropTypes.bool,
  history: PropTypes.object,
  match: PropTypes.object,
  loading: PropTypes.bool,
  verifyUserProfile: PropTypes.func,
  getUserCommunities: PropTypes.func,
  joinedCommunities: PropTypes.array,
  createMessage: PropTypes.func,
  getJoinedCommunities: PropTypes.func,
  communitiesData: PropTypes.object
};
const mapStateToProps = createStructuredSelector({
  posts: makeSelectUserPost(),
  data: makeSelectUserData(),
  loading: makeSelectLoading(),
  communities: makeSelectCommunities(),
  success: makeSelectSuccess(),
  savedPosts: makeSelectSavedPost(),
  historyPosts: makeSelectHistoryPost(),
  logoutStatus: makeSelectLogout(),
  joinedCommunities: makeSelectJoinedCommunities(),
  communitiesData: makeSelectAllUserCommunities()
});

export function mapDispatchToProps(dispatch) {
  return {
    getUserProfile: (userId, profileId, accessToken, clientId, uid) =>
      dispatch(getUserProfile(userId, profileId, accessToken, clientId, uid)),
    getUserPost: (userId, pageNo, clientId, accessToken, uid) =>
      dispatch(getUserPost(userId, pageNo, clientId, accessToken, uid)),
    editProfile: (formData, userId, profileId, clientId, accessToken, uid) =>
      dispatch(
        editProfile(formData, userId, profileId, clientId, accessToken, uid)
      ),
    getSavedPost: (clientId, accessToken, uid) =>
      dispatch(getSavedPost(clientId, accessToken, uid)),
    getHistoryPost: (filterBy, clientId, accessToken, uid) =>
      dispatch(getHistoryPost(filterBy, clientId, accessToken, uid)),
    getLogout: (email, clientId, accessToken, uid) =>
      dispatch(logout(email, clientId, accessToken, uid)),
    getUserCommunities: (userId, accessToken, clientId, uid) =>
      dispatch(getUserCommunities(userId, accessToken, clientId, uid)),
    getJoinedCommunities: (userId, accessToken, clientId, uid) =>
      dispatch(getJoinedCommunities(userId, accessToken, clientId, uid)),
    verifyUserProfile: (userId, accessToken, clientId, uid) =>
      dispatch(verifyUserProfile(userId, accessToken, clientId, uid)),
    createNewMessage: (formData, accessToken, clientId, uid) =>
      dispatch(createNewMessage(formData, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(Profile);
