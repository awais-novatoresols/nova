import {put, takeLatest} from "redux-saga/effects";
import {apiFetch, apiFetchDelete, apiFetchPost, apiFetchPut} from "../../utils/network";
import {
  dataLoaded,
  editProfileSuccess,
  getJoinedCommunitiesSuccess,
  getUserCommunitiesSuccess,
  historyPostLoaded,
  logoutSuccess,
  postLoaded,
  savedPostLoaded
} from "./actions";
import {
  EDIT_PROFILE,
  GET_HISTORY_POSTS,
  GET_SAVED_POSTS,
  GET_USER_POSTS,
  GET_USER_PROFILE,
  JOINED_COMMUNITIES,
  LOGOUT,
  USER_COMMUNITIES,
  VERIFY_USER_PROFILE
} from "./constants";


export function* getProfileData(userId) {
  let userID = localStorage.getItem("userId");
  try {
    const json = yield apiFetch(`users/${userId.userId}/profiles/${userId.profileId}`, userId.accessToken, userId.clientId, userId.uid);
    if (json.user.id == userID) localStorage.setItem("profile_image", json.user.profile.profile_image);


    yield put(dataLoaded(json));
  } catch (error) {
    console.log(error);
  }
}

export function* getSavedPosts(user) {
  try {
    const json = yield apiFetch(`saved_contents?content_type=post`, user.accessToken, user.clientId, user.uid);
    console.log("Saved Post", json);
    yield put(savedPostLoaded(json));
  } catch (error) {
    console.log(error);
  }
}

export function* getHistoryPosts(user) {
  try {
    const json = yield apiFetch(`post_histories?search=${user.filterBy}`, user.accessToken, user.clientId, user.uid);
    // console.log("History Post", json);
    yield put(historyPostLoaded(json));
  } catch (error) {
    console.log(error);
  }
}

export function* gePostsData(userId) {
  // console.log('user post header...', userId);
  try {
    const json = yield apiFetch(`users/${userId.userId}/posts?page_no=${userId.pageNo}`, userId.accessToken, userId.clientId, userId.uid);
    // console.log("gePostsData...", json);
    yield put(postLoaded(json));
  } catch (error) {
    console.log(error);
  }
}

function* editProfile(user) {
  console.log("editProfile Saga...", user);
  try {
    const json = yield apiFetchPut(`users/${user.userId}/profiles/${user.profileId}`, user.data, user.accessToken, user.clientId, user.uid);
    console.log("editProfile Saga...", json);
    yield put(editProfileSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export function* logout(user) {
  // console.log('user post header...', userId);
  try {
    const json = yield apiFetchDelete(`users/sign_out`, user.email, user.accessToken, user.clientId, user.uid);
    if (json && json.success) {
      yield put(logoutSuccess());
    }
  } catch (error) {
    console.log(error);
  }
}

export function* getUserCommunities(userId) {
  let jsonData = true;
  try {
    const json = yield apiFetch(`users/${userId.userId}/communities?admin=true`, userId.accessToken, userId.clientId, userId.uid, jsonData);
    //console.log("get User Communities ...", json);
    yield put(getUserCommunitiesSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export function* getJoinedCommunities(userId) {
  let jsonData = true;
  try {
    const json = yield apiFetch(`users/${userId.userId}/communities`, userId.accessToken, userId.clientId, userId.uid, jsonData);
    //console.log("get Joined Communities ...", json);
    yield put(getJoinedCommunitiesSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export function* verifyUserEmail(userId) {
  // let jsonData = true;
  let data = null;

  try {
    const json = yield apiFetchPost(`users/${userId.userId}/resend_verification_email`, data, userId.accessToken, userId.clientId, userId.uid);
    console.log("verifyUserEmail ...", json);
    alert(json.message);
  } catch (error) {
    console.log(error);
  }
}

export default function* userData() {
  yield takeLatest(GET_USER_POSTS, gePostsData);
  yield takeLatest(GET_USER_PROFILE, getProfileData);
  yield takeLatest(EDIT_PROFILE, editProfile);
  yield takeLatest(GET_SAVED_POSTS, getSavedPosts);
  yield takeLatest(GET_HISTORY_POSTS, getHistoryPosts);
  yield takeLatest(USER_COMMUNITIES, getUserCommunities);
  yield takeLatest(JOINED_COMMUNITIES, getJoinedCommunities);
  yield takeLatest(LOGOUT, logout);
  yield takeLatest(VERIFY_USER_PROFILE, verifyUserEmail)
}
