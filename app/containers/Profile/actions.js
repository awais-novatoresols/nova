import {
  EDIT_PROFILE,
  EDIT_PROFILE_FAILURE,
  EDIT_PROFILE_SUCCESS,
  GET_HISTORY_POSTS,
  GET_HISTORY_POSTS_SUCCESS,
  GET_SAVED_POSTS,
  GET_SAVED_POSTS_SUCCESS,
  GET_USER_POSTS,
  GET_USER_PROFILE,
  JOINED_COMMUNITIES,
  JOINED_COMMUNITIES_SUCCESS,
  LOAD_SUCCESS,
  LOGOUT,
  LOGOUT_SUCCESS,
  POST_LOAD_SUCCESS,
  USER_COMMUNITIES,
  USER_COMMUNITIES_SUCCESS,
  VERIFY_USER_PROFILE
} from "./constants";

export function getUserProfile(userId, profileId, accessToken, clientId, uid) {
  return {
    type: GET_USER_PROFILE,
    userId,
    profileId,
    accessToken,
    clientId,
    uid
  };
}

export function getUserPost(userId, pageNo, clientId, accessToken, uid) {
  return {
    type: GET_USER_POSTS,
    userId,
    pageNo,
    clientId,
    accessToken,
    uid
  };
}

export function getSavedPost(clientId, accessToken, uid) {
  return {
    type: GET_SAVED_POSTS,
    clientId,
    accessToken,
    uid
  };
}

export function getHistoryPost(filterBy, clientId, accessToken, uid) {
  return {
    type: GET_HISTORY_POSTS,
    filterBy,
    clientId,
    accessToken,
    uid
  };
}

export function editProfile(data, userId, profileId, clientId, accessToken, uid) {
  console.log("editProfile from action",data, userId, profileId, clientId, accessToken, uid);
  return {
    type: EDIT_PROFILE,
    data,
    userId,
    profileId,
    clientId,
    accessToken,
    uid
  };
}

export function dataLoaded(user) {
  return {
    type: LOAD_SUCCESS,
    user
  };
}

export function postLoaded(post) {
  return {
    type: POST_LOAD_SUCCESS,
    post
  };
}

export function savedPostLoaded(post) {
  return {
    type: GET_SAVED_POSTS_SUCCESS,
    post
  };
}

export function historyPostLoaded(post) {
  return {
    type: GET_HISTORY_POSTS_SUCCESS,
    post
  };
}

export function editProfileSuccess() {
  return {
    type: EDIT_PROFILE_SUCCESS
  };
}

export function editProfileFailure() {
  return {
    type: EDIT_PROFILE_FAILURE
  };
}

export function logout(email, clientId, accessToken, uid) {
  return {
    type: LOGOUT,
    email,
    clientId,
    accessToken,
    uid
  };
}

export function getUserCommunities(userId, accessToken, clientId, uid) {
  return {
    type: USER_COMMUNITIES,
    userId,
    accessToken,
    clientId,
    uid
  };
}

export function getUserCommunitiesSuccess(data) {
  return {
    type: USER_COMMUNITIES_SUCCESS,
    data
  };
}

export function getJoinedCommunities(userId, accessToken, clientId, uid) {
  return {
    type: JOINED_COMMUNITIES,
    userId,
    accessToken,
    clientId,
    uid
  };
}

export function getJoinedCommunitiesSuccess(data) {
  return {
    type: JOINED_COMMUNITIES_SUCCESS,
    data
  };
}

export function logoutSuccess() {
  return {
    type: LOGOUT_SUCCESS
  };
}

export function verifyUserProfile(userId, accessToken, clientId, uid) {
  return {
    type: VERIFY_USER_PROFILE,
    userId,
    accessToken,
    clientId,
    uid
  };
}
