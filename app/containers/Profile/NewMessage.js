import React, {useState} from "react";
import PropTypes from "prop-types";

const NewMessage = (props) => {

  const [messageText, setMessage] = useState("");

  function newMessage() {
    console.log("newMessage...");
    // chatRoomId, message, messageType, url, communityChat
    props.createMessage(props.id, messageText, "message", "", false);
  }

  function getMessage(event) {
    setMessage(event.target.value);
  }

  return (
    <>
      <div className="card mt-4 custom_c m-0">
        <form>
          <label className="w-100">
            <p className="custom_cn pt-2 pl-2">To:&nbsp;&nbsp; Umar</p>
            <hr className="custom_m"/>
            <textarea rows="6" onChange={event => getMessage(event)} className="form-control t_a"
                      placeholder="Type Text Here"/>
          </label>

        </form>
      </div>
      <button type="submit" onClick={newMessage} className="btnsubmit p-2 float-md-right mt-2">Send Message
      </button>
    </>
  );
};


NewMessage.propTypes = {
  createMessage: PropTypes.func,

};

export default NewMessage;
