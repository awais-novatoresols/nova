import produce from "immer";
import {
  EDIT_PROFILE_SUCCESS,
  GET_USER_POSTS,
  GET_USER_PROFILE,
  LOAD_ERROR,
  LOAD_SUCCESS,
  POST_LOAD_SUCCESS,
  GET_SAVED_POSTS_SUCCESS,
  GET_HISTORY_POSTS_SUCCESS,
  LOGOUT_SUCCESS,
  USER_COMMUNITIES_SUCCESS,
  JOINED_COMMUNITIES_SUCCESS
} from "./constants";
// The initial state of the App
export const initialState = {
  loading: false,
  error: false,
  logout: false,
  userData: {
    user: []
  },
  userPost: {
    post: false
  },
  communities: {},
  success: false,
  savedPosts: {},
  historyPosts: {},
  joinedCommunities: {}
};
/* eslint-disable default-case, no-param-reassign */
const profileReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
    case GET_USER_PROFILE:
      draft.userData.user = false;
      draft.loading = true;
      draft.error = false;
      draft.success = false;
      break;
    case LOAD_SUCCESS:
      draft.userData.user = action.user;
      draft.loading = false;
      draft.error = false;
      draft.success = false;
      break;
    case GET_USER_POSTS:
      draft.loading = true;
      draft.error = false;
      draft.success = false;
      break;
    case POST_LOAD_SUCCESS:
      draft.userPost.post = action.post;
      draft.loading = false;
      draft.error = false;
      draft.success = false;
      break;
    case GET_SAVED_POSTS_SUCCESS:
      draft.savedPosts = action.post;
      draft.loading = false;
      draft.error = false;
      draft.success = false;
      break;
    case GET_HISTORY_POSTS_SUCCESS:
      draft.historyPosts = action.post;
      draft.loading = false;
      draft.error = false;
      draft.success = false;
      break;
    case LOAD_ERROR:
      draft.error = action.error;
      draft.loading = false;
      draft.success = false;
      break;
    case EDIT_PROFILE_SUCCESS:
      draft.error = false;
      draft.loading = false;
      draft.success = true;
      break;
    case LOGOUT_SUCCESS:
      draft.logout = true;
      draft.success = false;
      draft.userData.user = false;
      draft.loading = false;
      draft.error = false;
      draft.success = false;
      draft.userPost.post = false;
      break;
    case USER_COMMUNITIES_SUCCESS:
      draft.communities = action.data;
      break;
    case JOINED_COMMUNITIES_SUCCESS:
      draft.joinedCommunities = action.data;
      break;
    default:
      draft.success = false;
      draft.logout = false;
      draft.loading = false;
    }
    // console.log(' state.userPost.post.......', JSON.stringify( state.userPost.post));
  });
export default profileReducer;


