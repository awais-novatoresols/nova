import { put, takeLatest } from "redux-saga/effects";
import { apiFetch, signUpApi } from "../../utils/network";
import { createCommunitySuccess, getCategoriesSuccess } from "./actions";
import { CREATE_COMMUNITY, GET_CATEGORIES } from "./constants";

function* getCategories(user) {
  try {
    const json = yield apiFetch(`categories`, user.accessToken, user.clientId, user.uid);
    // console.log("creteCommunities getCategories Saga...", json.categories);
    yield put(getCategoriesSuccess(json.categories));
  } catch (error) {
    // console.log(error);
  }
}

function* createCommunity(user) {
  try {
    const json = yield signUpApi(`users/${user.userId}/communities`, user.formData, user.accessToken, user.clientId, user.uid);
    // console.log("creteCommunities Saga...", json);
    yield put(createCommunitySuccess(json));
  } catch (error) {
    // console.log(error);
  }
}

export default function* createCommunities() {
  yield takeLatest(GET_CATEGORIES, getCategories);
  yield takeLatest(CREATE_COMMUNITY, createCommunity);
}
