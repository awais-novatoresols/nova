import { CREATE_COMMUNITY, CREATE_COMMUNITY_SUCCESS, GET_CATEGORIES, GET_CATEGORIES_SUCCESS } from "./constants";

export function getCategories(userId, clientId, accessToken, uid) {
  return {
    type: GET_CATEGORIES,
    userId,
    clientId,
    accessToken,
    uid
  };
}

export function createCommunity(formData, userId, clientId, accessToken, uid) {
  return {
    type: CREATE_COMMUNITY,
    formData,
    userId,
    clientId,
    accessToken,
    uid
  };
}

export function getCategoriesSuccess(data) {
  return {
    type: GET_CATEGORIES_SUCCESS,
    data
  };
}

export function createCommunitySuccess(data) {
  return {
    type: CREATE_COMMUNITY_SUCCESS,
    data
  };
}
