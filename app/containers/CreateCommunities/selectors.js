import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.createCommunity || initialState;

const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectCategories = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.categories
  );

export { selectGlobal, makeSelectCategories, makeSelectSuccess };

