import produce from "immer";
import { CREATE_COMMUNITY_SUCCESS, GET_CATEGORIES_SUCCESS } from "./constants";

// The initial state of the App
export const initialState = {
  success: false,
  categories: []
};

/* eslint-disable default-case, no-param-reassign */
const communityReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_CATEGORIES_SUCCESS:
        draft.categories = action.data;
        break;
      case CREATE_COMMUNITY_SUCCESS:
        draft.success = true;
        break;
      default:
        draft.success = false;
    }
  });

export default communityReducer;


