export const GET_CATEGORIES = "ubwab/communities/create/GET_CATEGORIES";
export const GET_CATEGORIES_SUCCESS = "ubwab/communities/create/GET_CATEGORIES_SUCCESS";
export const CREATE_COMMUNITY = "ubwab/communities/create/CREATE_COMMUNITY";
export const CREATE_COMMUNITY_SUCCESS = "ubwab/communities/create/CREATE_COMMUNITY_SUCCESS";