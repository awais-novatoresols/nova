import { defineMessages } from 'react-intl';

export default defineMessages({
    createCommunityMainHeading: {
        id: 'create-community-main-heading',
        defaultMessage: 'Creat New Community',
    },
    createCommunityName: {
        id: 'create-community-community-name',
        defaultMessage: 'Community Name',
    },
    createCommunityDiscription: {
        id: 'create-community-discription',
        defaultMessage: 'Discription',
    },
    createCommunityRule: {
        id: 'create-community-rule',
        defaultMessage: 'Rules and Regulations',
    },
    createCommunityCategory: {
        id: 'create-community-select-category',
        defaultMessage: 'Select any Category',
    },
    createCommunityButtonTitle: {
        id: 'create-community-button-title',
        defaultMessage: 'Create Community',
    },
    headerlinkNewsFeeds: {
        id: 'header-link-news-feed',
        defaultMessage: 'News Feed',
      },
      headerlinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Communities',
      },
      headerlinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
      },
    

});