import PropTypes from "prop-types";
import React from "react";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const CreateCommunityForm = props => {
  return (
    <>
      <div className="card mt-5 pb-4 custom_c">
        <form>
          <label className="w-100 pl-4 pt-4">
            <p className="label-txt pl-2"> <FormattedMessage {...messages.createCommunityName} /></p>
            <textarea maxLength={30} className="form-control t_a" name="name" onChange={e => props.onInputChange(e, "name")}/>
          </label><hr />
          <label className="w-100 pl-4">
            <p className="label-txt pl-2"><FormattedMessage {...messages.createCommunityDiscription} /></p>
            <textarea maxlength={1000} className="form-control t_a" name="description" onChange={e => props.onInputChange(e, "description")}/>
          </label><hr />
          <label className="w-100 pl-4">
            <p className="label-txt pl-2"><FormattedMessage {...messages.createCommunityRule} /></p>
            <textarea className="form-control t_a" onChange={e => props.onInputChange(e, "rule")}/>
          </label>
          <hr />
          <label className="w-100 pl-4">
            <p className="label-txt pl-2"><FormattedMessage {...messages.createCommunityCategory} /></p>
            {props.categories.map(category => {
              return (<div className="form-check" key={category.id}>
                <label className="form-check-label">
                  <input type="radio" className="form-check-input" id="radio1" onChange={props.onCategoryChange} name="optradio" value={category.id} />
                  <span>{category.name}</span>
                </label>
              </div>);
            })}
          </label>
        </form>
      </div>
      <button type="submit" className="btnsubmit p-2 float-right mt-3 mb-5 mt-lg-n4" onClick={props.onSubmit}>
      <FormattedMessage {...messages.createCommunityButtonTitle} /></button>
    </>
  )
}

CreateCommunityForm.propTypes = {
  categories: PropTypes.array,
  onInputChange: PropTypes.func,
  onCategoryChange: PropTypes.func,
  onSubmit: PropTypes.func
}

export default CreateCommunityForm;
