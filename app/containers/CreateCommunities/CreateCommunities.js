import PropTypes from "prop-types";
import React from "react";
import Cover from "../../components/Cover";
import CreatePost from "../../components/CreatePost";
import FooterLink from "../../components/FooterLink";
import Header from "../../components/Header";
import CreateCommunityForm from "./CreateCommunityForm";
import "./style.css";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Menu = [
  { id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage" },
  { id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities" },
  { id: 3, name: <FormattedMessage {...messages.headerlinkProfile}/>, path: "/profile" }
];
const CreateCommunities = props => {
  return (
    <div className="container-fluid mh-100 bg-light">
      <Header nav={Menu} {...props} />
      <div className="container p-0">
        <div className="col-sm-12">
          <h5 className="pt-4 text_1"><FormattedMessage {...messages.createCommunityMainHeading} /></h5>
        </div>
        <Cover
          cover={props.cover ? props.cover : require("../../images/placeholder2.jpg")}
          profilePic={props.profilePic ? props.profilePic : require("../../images/user.png")}
          onCoverImageChange={props.onCoverImageChange} onProfileImageChange={props.onProfileImageChange}
          editMode />
        <div className="row">
          <div className="col-sm-7 mt-4">
            <CreateCommunityForm
              categories={props.categories} onInputChange={(e, type) => props.onInputChange(e, type)}
              onCategoryChange={props.onCategoryChange} onSubmit={props.onSubmit} />
          </div>
          <div className="col-sm-5 mt-5">
             {/*<CreatePost />*/}
            {/*<FooterLink />*/}
          </div>
        </div>
      </div>
    </div>
  );
};
CreateCommunities.propTypes = {
  categories: PropTypes.array,
  onInputChange: PropTypes.func,
  onCategoryChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCoverImageChange: PropTypes.func,
  onProfileImageChange: PropTypes.func,
  cover: PropTypes.string,
  profilePic: PropTypes.string
};
export default CreateCommunities;
