import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { createCommunity, getCategories } from "./actions";
import CreateCommunities from "./CreateCommunities";
import reducer from "./reducer";
import saga from "./saga";
import { makeSelectCategories, makeSelectSuccess } from "./selectors";
import { toast } from "react-toastify";

const key = "createCommunity";
const CreateCommunitiesContainer = props => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [rules, setRules] = useState("");
  const [category, setCategory] = useState("");
  const [cover, setCover] = useState();
  const [profilePic, setProfilePic] = useState();
  const [coverFile, setCoverFile] = useState();
  const [profilePicFile, setProfilePicFile] = useState();

  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    //console.log("header from profile", userId, clientId, accessToken, uid);
    props.getCategories(userId, clientId, accessToken, uid);
  }, []);

  useEffect(() => {
    if (props.success) {
      toast.success("Community Created Successfully!");
      props.history.push("/communities");
    }
  }, [props.success]);

  const onInputChange = (e, type) => {
    //console.log(type, e.target.value);
    if (type === "name") setName(e.target.value);
    if (type === "description") setDescription(e.target.value);
    if (type === "rule") setRules(e.target.value);
  }

  const onCategoryChange = e => {
    //console.log("Selected ID", e.target.value);
    setCategory(e.target.value);
  }

  const onCoverImageChange = e => {
    setCover(URL.createObjectURL(e.target.files[0]));
    setCoverFile(e.target.files[0]);
  }

  const onProfileImageChange = e => {
    setProfilePic(URL.createObjectURL(e.target.files[0]));
    setProfilePicFile(e.target.files[0]);
  }

  const onSubmit = () => {
    //console.log("Values", name, description, rules, category);
    if (name && description && rules && category) {
      //console.log("Cover File", coverFile);
      //console.log("Profile File", profilePicFile);
      let formData = new FormData();
      formData.append('community[name]', name);
      formData.append('community[description]', description);
      formData.append('community[rules]', rules);
      formData.append('community[category_id]', category);
      formData.append('community[profile_image]', profilePicFile);
      formData.append('community[background_image]', coverFile);
      formData.append('community[longitude]', "12.12");
      formData.append('community[latitude]', "12.12");
      formData.append('community[location]', "Qatar");

      props.createCommunity(formData, userId, clientId, accessToken, uid);
      //console.log("Done");
    } else alert("All fields are required!!");
  }

  return <CreateCommunities {...props} onInputChange={(e, type) => onInputChange(e, type)} onCategoryChange={onCategoryChange} onSubmit={onSubmit} onCoverImageChange={onCoverImageChange} onProfileImageChange={onProfileImageChange} cover={cover} profilePic={profilePic} />;
}

CreateCommunitiesContainer.propTypes = {
  categories: PropTypes.array,
  getCategories: PropTypes.func,
  createCommunity: PropTypes.func,
  success: PropTypes.bool
};

const mapStateToProps = createStructuredSelector({
  categories: makeSelectCategories(),
  success: makeSelectSuccess()
});

export function mapDispatchToProps(dispatch) {
  return {
    getCategories: (userId, clientId, accessToken, uid) => dispatch(getCategories(userId, clientId, accessToken, uid)),
    createCommunity: (formData, userId, clientId, accessToken, uid) => dispatch(createCommunity(formData, userId, clientId, accessToken, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(CreateCommunitiesContainer);