import React from "react";
import "./style.css"
import Header from "../../components/Header";
import NavBar from "../../components/NavBar";
import SortBy from "../../components/SortBy";
import CreatePost from "../../components/CreatePost";
import FooterLink from "../../components/FooterLink";

const Menu = [
  { id: 1, name: "MY PROFILE", path: "/homepage" },
  { id: 2, name: "MY COMMUNITIES", path: "/communities" }
];

const History = props => {
  return (
    <div className="container-fluid p-0 mh-100 bg-light">
      <Header nav={Menu} {...props} />
      <div className="container-fluid p-0">
        <div className="row">
          <NavBar />
          <div className="col-sm-10">
            <div className="row p-3">
              <div className="col-sm-7">
                <SortBy />
                <div className="card custom_c">
                  <div className="card-header d-flex custom_ch">
                    <img src="assets/ic_profile.png" className="rounded-circle" />
                    <p className="custom_p1">Jane Teacher<br />Posted By U/abcd</p>
                    <h6 className="ml-auto mr-2 mt-2">6 Hr</h6>
                  </div>
                  <div className="card-body p-0">
                    <h5 className="p-3">Sunset is beautiful...</h5>
                    <img className="custom_i2" src="assets/nature.jpeg" />
                  </div>
                  <div className="card-footer custom_f1 d-flex justify-content-around">
                    <div className="d-flex justify-content-around">
                      <a className="remove_an custom_cn1" ><img src="assets/arrowup.png"
                        width="14px" />&nbsp;&nbsp; 213</a>
                      <a  className="remove_an cut_a pl-3"><img src="assets/arrowdown.png" width="14px" /></a>
                    </div>
                    <a  className="remove_an custom_cn1 custom_a2"><img src="assets/comment.png"
                      width="20px" /> Comment</a>
                    <a className="remove_an custom_cn1 custom_a3"><img src="assets/share.png" width="18px" /> Share</a>
                  </div>
                </div>

                <div className="card custom_c mt-4">
                  <div className="card-header d-flex custom_ch">
                    <img src="assets/ic_profile.png" className="rounded-circle" />
                    <p className="custom_p1">Jane Teacher<br />Posted By U/abcd</p>
                    <h6 className="ml-auto mr-2 mt-2">6 Hr</h6>
                  </div>
                  <div className="card-body p-0">
                    <h5 className="p-3">Sunset is beautiful...</h5>
                    <img className="custom_i2" src="assets/nature.jpeg" />
                  </div>
                  <div className="card-footer custom_f1 d-flex justify-content-around">
                    <div className="d-flex justify-content-around">
                      <a className="remove_an custom_cn1" ><img src="assets/arrowup.png"
                        width="14px" />&nbsp;&nbsp; 213</a>
                      <a  className="remove_an cut_a pl-3"><img src="assets/arrowdown.png" width="14px" /></a>
                    </div>
                    <a className="remove_an custom_cn1 custom_a2"><img src="assets/comment.png"
                      width="20px" /> Comment</a>
                    <a  className="remove_an custom_cn1 custom_a3"><img src="assets/share.png" width="18px" /> Share</a>
                  </div>
                </div>
              </div>
              <div className="col-sm-5">
                <CreatePost />
                {/*<FooterLink />*/}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default History;
