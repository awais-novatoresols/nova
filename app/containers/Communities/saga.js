import {put, takeLatest} from "redux-saga/effects";
import {apiFetch} from "../../utils/network";
import {getCategoriesSuccess, getCommuntiesSuccess, getJoinedCommunitiesSuccess} from "./actions";
import {GET_CATEGORIES, GET_COMMUNITIES, JOINED_COMMUNITIES} from "./constants";

function* getCommunitiesData(user) {
  try {
    const json = yield apiFetch(`users/${user.userId}/communities/?category_id=${user.communityId}&page_no=${user.pageNo}`, user.accessToken, user.clientId, user.uid);
    yield put(getCommuntiesSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

function* getAllCategories() {
  try {
    const json = yield apiFetch(`categories`);
    //console.log("get Categories Saga...", json.categories);
    yield put(getCategoriesSuccess(json.categories));
  } catch (error) {
    console.log(error);
  }
}

export function* getJoinedCommunities(userId) {
  let jsonData = true;
  try {
    const json = yield apiFetch(`users/${userId.userId}/communities`, userId.accessToken, userId.clientId, userId.uid, jsonData);
    // console.log("get Joined Communities ...", json);
    yield put(getJoinedCommunitiesSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export default function* communities() {
  yield takeLatest(GET_COMMUNITIES, getCommunitiesData);
  yield takeLatest(GET_CATEGORIES, getAllCategories);
  yield takeLatest(JOINED_COMMUNITIES, getJoinedCommunities);
}
