import { GET_CATEGORIES, GET_CATEGORIES_SUCCESS, GET_COMMUNITIES, GET_COMMUNITIES_SUCCESS, JOINED_COMMUNITIES, JOINED_COMMUNITIES_SUCCESS } from "./constants";

export function getCommunities(userId, communityId, pageNo, clientId, accessToken, uid) {
  return {
    type: GET_COMMUNITIES,
    userId,
    communityId,
    pageNo,
    clientId,
    accessToken,
    uid
  };
}

export function getCategories() {
  return {
    type: GET_CATEGORIES
  };
}

export function getCommuntiesSuccess(data) {
  return {
    type: GET_COMMUNITIES_SUCCESS,
    data
  };
}

export function getCategoriesSuccess(data) {
  return {
    type: GET_CATEGORIES_SUCCESS,
    data
  };
}

export function getJoinedCommunities(userId, accessToken, clientId, uid) {
  return {
    type: JOINED_COMMUNITIES,
    userId,
    accessToken,
    clientId,
    uid
  };
}

export function getJoinedCommunitiesSuccess(data) {
  return {
    type: JOINED_COMMUNITIES_SUCCESS,
    data
  };
}
