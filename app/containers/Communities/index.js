import PropTypes from "prop-types";
import React, {memo, useEffect, useState} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import {createStructuredSelector} from "reselect";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import {getCategories, getCommunities, getJoinedCommunities} from "./actions";
import {search} from "../../components/Search/actions";
import Communities from "./Communities";
import reducer from "./reducer";
import saga from "./saga";
import {
  makeSelectActivePage,
  makeSelectCategories,
  makeSelectCommunities,
  makeSelectCommunitiesSuccess,
  makeSelectJoinedCommunities,
  makeSelectLoading,
  makeSelectTotalPages
} from "./selectors";


const key = "communities";
const CommunitiesContainer = props => {
  const [pageNo, setpageNo] = useState(1);
  const [categoryId, setCategoryId] = useState();
  const [selectedCategory, setSelectedCategory] = useState();
  const [activeView, setActiveView] = useState("All");
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});
  // useEffect(() => {
  //   // console.log("header from profile", clientId, accessToken, uid);
  //   props.getCommunities(userId, communityId, pageNo, clientId, accessToken, uid);
  // }, [pageNo]);
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  useEffect(() => {
    props.getCategories();
    props.getJoinedCommunities(userId, accessToken, clientId, uid);
  }, []);
  useEffect(() => {
    if (props.categories && props.categories.length) {
      setCategoryId(props.categories[0].id);
      setSelectedCategory(props.categories[0].name);
    }
  }, [props.success]);
  useEffect(() => {
    props.getCommunities(userId, categoryId, pageNo, clientId, accessToken, uid);
  }, [categoryId, pageNo]);
  const onPageChange = e => {
    setpageNo(e.target.id);
  };
  const onCategorySelect = e => {
    setActiveView("All");
    setpageNo(1);
    setCategoryId(e.target.id);

    let selectedCat = props.categories.filter(cat => cat.id === parseInt(e.target.id));
    setSelectedCategory(selectedCat[0].name);
  };

  function onFollowBtnClick() {
    props.getCommunities(userId, categoryId, pageNo, clientId, accessToken, uid);
  }

  function onJoinedUnfollow() {
    props.getJoinedCommunities(userId, accessToken, clientId, uid);
  }

  function joinedOrAllCommunities() {
    activeView === "All" ? setActiveView("Joined") : setActiveView("All");
  }

  function searchCommunities(searchText, filter,) {
    props.search(searchText, filter, userId, clientId, accessToken, uid);
  }

  return <Communities
    {...props} searchCommunities={searchCommunities} onLeave={onJoinedUnfollow}
    onButtonClick={joinedOrAllCommunities} activeView={activeView} selectedCategory={selectedCategory}
    onPageChange={onPageChange} onCategorySelect={onCategorySelect} onFollow={onFollowBtnClick}
    loading={props.loading} joinedCommunities={props.joinedCommunities}/>;
};
CommunitiesContainer.propTypes = {
  communities: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  getCommunities: PropTypes.func,
  search: PropTypes.func,
  getCategories: PropTypes.func,
  success: PropTypes.bool,
  categories: PropTypes.oneOfType([PropTypes.bool, PropTypes.array]),
  loading: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  communities: makeSelectCommunities(),
  success: makeSelectCommunitiesSuccess(),
  totalPages: makeSelectTotalPages(),
  activePage: makeSelectActivePage(),
  categories: makeSelectCategories(),
  loading: makeSelectLoading(),
  joinedCommunities: makeSelectJoinedCommunities()
});

export function mapDispatchToProps(dispatch) {
  return {
    getCommunities: (userId, communityId, pageNo, clientId, accessToken, uid) => dispatch(getCommunities(userId, communityId, pageNo, clientId, accessToken, uid)),
    getJoinedCommunities: (userId, accessToken, clientId, uid) => dispatch(getJoinedCommunities(userId, accessToken, clientId, uid)),
    getCategories: () => dispatch(getCategories()),
    search: (text, filter, userId, clientId, accessToken, uid) => dispatch(search(text, filter, userId, clientId, accessToken, uid)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(CommunitiesContainer);
