export const GET_COMMUNITIES = "ubwab/communities/GET_COMMUNITIES";
export const GET_COMMUNITIES_SUCCESS = "ubwab/communities/GET_COMMUNITIES_SUCCESS";
export const GET_CATEGORIES = "ubwab/communities/GET_CATEGORIES";
export const GET_CATEGORIES_SUCCESS = "ubwab/communities/GET_CATEGORIES_SUCCESS";
export const JOINED_COMMUNITIES = "ubwab/communities/JOINED_COMMUNITIES";
export const JOINED_COMMUNITIES_SUCCESS = "ubwab/communities/JOINED_COMMUNITIES_SUCCESS";