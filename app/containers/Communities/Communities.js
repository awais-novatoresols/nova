import React from "react";
import {Link} from "react-router-dom";
import CommunityBar from "../../components/CommunityBar/CommunityBar";
import Slider from "./Slider";
import Header from "../../components/Header";
import PaginationContainer from "../../components/Pagination";
import PropTypes from "prop-types";
import Spinner from "../../components/Spinner";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const Menu = [
  {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
  {id: 3, name: <FormattedMessage {...messages.headerlinkGeneral}/>, path: "/general"},
  {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
];
const Communities = props => {
  const communities = props.communities || [];
  const categories = props.categories || [];
  // console.log("From communities", communities);
  //  const communitiesArray = communities.filter((item) => {
  //    return item.role === "admin";
  //  });
  return (
    <>
      <div className="container-fluid mh-100 bg-light">
        <Header nav={Menu} {...props} searchCommunities={props.searchCommunities}/>
        <div className="container mt-5">
          <Slider categories={categories} onCategorySelect={props.onCategorySelect}/>
          <div className="row pb-5">
            <div className="col-sm-12">
              <div className="row pt-md-5">
                <div className="col-sm-6 col-6">
                  <h6
                    className="c_h pt-2">{props.activeView === "All" ? props.selectedCategory : <FormattedMessage {...messages.communitiesListContainerJoined}/>}</h6>
                </div>
                <div className="col-sm-6 col-6">
                  <Link to="/communities/create">
                    <button type="button" className="btnsubmit p-2 float-right">
                      <FormattedMessage {...messages.communitiesListContainerHeaderButttonCreateCommunity}/></button>
                  </Link>
                  <button type="button" className="btnsubmit p-2 float-right mr-2"
                          onClick={props.activeView === "All" && props.onButtonClick}>
                    <FormattedMessage {...messages.communitiesListContainerHeaderButttonJoinedCommunities}/>
                  </button>
                </div>
              </div>
              {props.activeView === "All" && communities.length > 0 && <>{communities.map(community => {
                {
                  if (community.type) {
                    return (<CommunityBar
                      communityImage={community.community && community.community.profile_image}
                      key={community.community.id} community={community.community}
                      onFollow={props.onFollow}/>)
                  }
                  if (!community.type) {
                    return <CommunityBar
                      communityImage={community.profile_image && community.profile_image}
                      key={community.id} community={community} onFollow={props.onFollow}/>
                  }
                }
              })}
                <PaginationContainer
                  activePage={props.activePage}
                  totalPages={props.totalPages}
                  onPageChange={props.onPageChange}
                />
              </>
              }
              {
                props.activeView === "Joined" && props.joinedCommunities.length > 0 && props.joinedCommunities.map(community => {
                  return <CommunityBar
                    communityImage={community.profile_image && community.profile_image}
                    key={community.id} community={community} onFollow={props.onLeave}/>;
                })
              }
            </div>
          </div>
        </div>
      </div>
      {/* {props.loading && <Spinner/>} */}
    </>
  );
};
Communities.propTypes = {
  communities: PropTypes.array,
  totalPages: PropTypes.number,
  activePage: PropTypes.number,
  onPageChange: PropTypes.func,
  categories: PropTypes.array,
  onCategorySelect: PropTypes.func,
  onFollow: PropTypes.func,
  loading: PropTypes.bool,
  selectedCategory: PropTypes.string,
  onButtonClick: PropTypes.func,
  joinedCommunities: PropTypes.array,
  searchCommunities: PropTypes.func,
  onLeave: PropTypes.func,
};
export default Communities;
