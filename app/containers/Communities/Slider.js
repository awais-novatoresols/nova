import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./style.css";
import PropTypes from "prop-types";


const SliderImage = (props) => {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow:   props.categories && props.categories.length >= 4? 4 : props.categories && props.categories.length,
    // autoplay: true,
    slidesToScroll: 1,
    initialSlide: 0,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          initialSlide: 0
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  // console.log("From Slider", props.categories);
  return (
    <Slider {...settings} className="mr-auto ml-auto">
      {
        props.categories.map((item, index) => {
          return (
            <div className="text_centr" key={index} onClick={props.onCategorySelect}>
              <img
                src={item.image ? item.image : require("../../images/placeholder2.jpg")}
                alt="Image" className="sliderImg" />
              <div className="transparentbox" id={item.id} />
              <p className="font-weight-bold centered_text" id={item.id} onClick={props.onCategorySelect}>{item.name}</p>
            </div>
          );
        })
      }
    </Slider>
  );
};
SliderImage.propTypes = {
  categories: PropTypes.array,
  onCategorySelect: PropTypes.func
};
export default SliderImage;
