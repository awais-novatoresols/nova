import {createSelector} from "reselect";
import {initialState} from "./reducer";

const selectGlobal = state => state.communities || initialState;

const makeSelectCommunitiesSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectCommunities = () =>
  createSelector(
    selectGlobal,
    globalState => {
    //  console.log("globalState.communities.communities", globalState.communities);
      if (globalState.communities.communities) {
        return globalState.communities.communities
      } else {
        return globalState.communities.data
      }
    }
  );

const makeSelectTotalPages = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.communities.total_pages
  );

const makeSelectActivePage = () =>
  createSelector(
    selectGlobal,
    globalState => parseInt(globalState.communities.current_page)
  );

const makeSelectCategories = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.categories
  );

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectJoinedCommunities = () => createSelector(
  selectGlobal,
  globalState => globalState.joinedCommunities.communities
);

export {
  selectGlobal,
  makeSelectJoinedCommunities,
  makeSelectCommunitiesSuccess,
  makeSelectCommunities,
  makeSelectTotalPages,
  makeSelectActivePage,
  makeSelectCategories,
  makeSelectLoading
};

