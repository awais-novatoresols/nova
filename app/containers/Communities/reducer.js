import produce from "immer";
import { GET_COMMUNITIES ,GET_COMMUNITIES_SUCCESS, GET_CATEGORIES_SUCCESS, JOINED_COMMUNITIES_SUCCESS } from "./constants";

// The initial state of the App
export const initialState = {
  loading: false,
  success: false,
  communities: {},
  categories: [],
  joinedCommunities: {}
};

/* eslint-disable default-case, no-param-reassign */
const communitiesReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
    case GET_COMMUNITIES:
      draft.loading = true;
      break;
    case GET_COMMUNITIES_SUCCESS:
      draft.communities = action.data;
      draft.loading = false;
      break;
    case GET_CATEGORIES_SUCCESS:
      draft.categories = action.data;
      draft.success = true;
      break;
    case JOINED_COMMUNITIES_SUCCESS:
      draft.joinedCommunities = action.data;
      break;
    default:
      draft.loading = false;
      draft.success = false;
    }
  });

export default communitiesReducer;


