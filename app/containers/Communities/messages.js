import { defineMessages } from 'react-intl';

export default defineMessages({
    communitiesListContainerHeaderButttonJoinedCommunities : {
        id: 'communities-list-container-header-buttton-joined-communities',
        defaultMessage: 'Joined Communities',
    },
    communitiesListContainerHeaderButttonCreateCommunity: {
        id: 'communities-list-container-header-buttton-create-community',
        defaultMessage: 'Create a Community',
    },
    communitiesCommunityRowTextSubscriber: {
        id: 'communities-community-row-text-subscriber',
        defaultMessage: 'Subscriber',
    },
    communitiesCommunityRowButtonleave: {
        id: 'communities-community-row-button-leave',
        defaultMessage: 'Leave',
    },
    communitiesCommunityRowButtonVisitPage: {
        id: 'communities-community-row-button-visit-page',
        defaultMessage: 'Visit Page',
    },
    communitiesCommunityRowDropDownDiscription: {
        id: 'communities-community-row-drop-down-discription',
        defaultMessage: 'Discription',
    },
    headerlinkNewsFeeds: {
        id: 'header-link-news-feed',
        defaultMessage: 'Home',
    },
    headerlinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Categories',
    },
    headerlinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
    },
    headerlinkGeneral: {
        id: 'header-link-general',
        defaultMessage: 'General',
    },
    communitiesListContainerJoined: {
        id: 'communities-list-container-joined',
        defaultMessage: 'Joined',
    }
});