import React,{useEffect, useRef} from "react";
import "./style.css";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import PrivacyPolicy from "../../components/PrivacyPolicy";

const InfoPage = (props) => {
  const el = useRef(null);

  useEffect(() => {
    el.current.scrollIntoView({ block: 'start', behavior: 'smooth' });
  });
  // console.log("props from link ... ", props);
  return (
    <>
      <div className="container-fluid">
        <div id={'el'} ref={el}/>
        <div className="row nav_style_f">
          <div className="col-sm-12">
            <nav className="container navbar navbar-expand-lg navbar-light">
              <Link to={"/homePage"}>
                <img
                  className="mr-3" src={require("../../images/logowhite.png")} width="70px"/></Link>
              <Link to={"/homePage"}>
                <img src={require("../../images/home.png")} width="20px"/></Link>
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <form className="form-inline my-2 my-lg-0 ml-auto float-left">
                </form>
              </div>
            </nav>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <h1 className="text-center mt-5 nav_style1">{props.location.title}</h1>
          </div>
        </div>
      </div>
      <div className="container mb-3">
        <div className="row mt-5">
          <div className="col-sm-12 bg-white custom_radius">
            <p className="p-5 custom_hght"><PrivacyPolicy /></p>
          </div>
        </div>
      </div>
    </>
  );
};
InfoPage.propTypes = {
  location: PropTypes.object
};
export default InfoPage;
