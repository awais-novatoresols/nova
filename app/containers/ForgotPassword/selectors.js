import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.forgotPassword || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectUserData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userData
  );
export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectUserData
};
