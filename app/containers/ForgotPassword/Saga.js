import { put, takeLatest } from "redux-saga/effects";
import { signUpApi } from "../../utils/network";
import { dataLoaded } from "./actions";
import { FORGOT_PASSWORD } from "./constants";

export function* sendEmail(body) {
  try {
    const json = yield signUpApi("users/password", body.body);
    yield put(dataLoaded(json));
  } catch (error) {
    // console.log(error);
  }
}

export default function* userData() {
  yield takeLatest(FORGOT_PASSWORD, sendEmail);
}
