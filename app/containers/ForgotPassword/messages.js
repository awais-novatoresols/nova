import { defineMessages } from 'react-intl';

export default defineMessages({
  forgotPasswordTitle: {
    id: 'forgot-password-title',
    defaultMessage: 'Forgot Password?',
  },
  forgotPasswordBody: {
    id: 'forgot-password-side-text',
    defaultMessage: 'Do not Worry'
  },
  forgotPasswordEnd: {
    id: 'forgot-password-end',
    defaultMessage: 'We will help you recover'
  },
  forgotPasswordText: {
    id: 'forgot-password-text',
    defaultMessage: 'Forgot Password'
  },
  forgotPasswordEmail: {
    id: 'forgot-password-input-email',
    defaultMessage: 'Enter Email'
  },
  forgotPasswordButton: {
    id: 'forgot-password-button',
    defaultMessage: 'Proceed'
  },
  headerlinkNewsFeeds: {
    id: 'header-link-news-feed',
    defaultMessage: 'Communities',
  },
  headerlinkCommunities: {
    id: 'header-link-communities',
    defaultMessage: 'Communities',
  },
  headerlinkProfile: {
    id: 'header-link-profile',
    defaultMessage: 'Profile',
  },
});