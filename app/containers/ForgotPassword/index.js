import React, { memo, useEffect, useState } from "react";
import SideImage from "../../components/SideImage";
import InputForm from "./InputForm";
import AddEmail from "./AddEmail";
import "../SignIn/style.css";
import "bootstrap/dist/css/bootstrap.css";
import { connect } from "react-redux";
import { compose } from "redux";
import { sendEmail } from "./actions";
import { createStructuredSelector } from "reselect";
import { makeSelectLoading, makeSelectUserData } from "./selectors";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import reducer from "./reducer";
import saga from "./Saga";
import PropTypes from "prop-types";
import "w3-css/4/w3pro.css";
import "font-awesome/css/font-awesome.min.css";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const key = "forgotPassword";

export function ForgotPassword(props) {
  const [isProceed, pressProceed] = useState(true);
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  let goProceed = (body) => {
    //  console.log("login body", body);
    props.sendEmail(body);
  };
  useEffect(() => {
    //  console.log("useEffect");
    if (props.data.user.success) {
      alert(props.data.user.message);
      pressProceed(false);
      // props.history.push("/HomePage");
    } else if (props.data.user.message !== undefined && !props.data.user.success) {
      alert(props.data.user.message);
    }
  }, [props.data.user]);
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  return (
    <>
      <SideImage
        title={<FormattedMessage {...messages.forgotPasswordTitle} />}
        body={<FormattedMessage {...messages.forgotPasswordBody} />}
        end={<FormattedMessage {...messages.forgotPasswordEnd} />}
      />
      {isProceed ? <InputForm goProceed={goProceed}/> : <AddEmail/>}
    </>
  );
}

ForgotPassword.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  sendEmail: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  data: makeSelectUserData(),
  loading: makeSelectLoading()
});

export function mapDispatchToProps(dispatch) {
  return {
    sendEmail: (body) => dispatch(sendEmail(body))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(ForgotPassword);
