import produce from "immer";
import { FORGOT_PASSWORD, LOAD_ERROR, LOAD_SUCCESS } from "./constants";
// The initial state of the App
export const initialState = {
  loading: true,
  error: false,
  userData: {
    user: false
  }
};
/* eslint-disable default-case, no-param-reassign */
const forgotPasswordReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
    case FORGOT_PASSWORD:
      draft.userData.user = false;
      draft.loading = true;
      draft.error = false;
      break;
    case LOAD_SUCCESS:
      draft.userData.user = action.user;
      draft.loading = false;
      draft.error = false;
      break;
    case LOAD_ERROR:
      draft.error = action.error;
      draft.loading = false;
      break;
    }
  });
export default forgotPasswordReducer;


