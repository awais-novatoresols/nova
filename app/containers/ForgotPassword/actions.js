import { FORGOT_PASSWORD, LOAD_SUCCESS } from "./constants";


export function sendEmail(body) {
  return {
    type: FORGOT_PASSWORD,
    body
  };
}

export function dataLoaded(user) {
  return {
    type: LOAD_SUCCESS,
    user
  };
}
