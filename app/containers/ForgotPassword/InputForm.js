import "bootstrap/dist/css/bootstrap.css";
import PropTypes from "prop-types";
import React from "react";
import useInputForm from "../../components/useInputForm";
import email from "../../images/Email.png";
import logo from "../../images/logo.png";
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import "./style.css";

export default function InputForm(props) {
  // const userAlert = () => console.log(`Email: ${inputs.email}`);
  const { inputs, handleInputChange, handleSubmit } = useInputForm({
    email: "",
    password: ""
  });
  let submitForm = () => {
    let formData = new FormData();
    formData.append("email", inputs.email);
    formData.append("redirect_url", "http://localhost:3000/ForgotPassword");
    let reg = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    if (inputs.email && reg.test(inputs.email.trim())) {
      props.goProceed(formData);
    } else {
      alert("Please enter your email");
    }
  };
  return (
    <div className="col-sm-6 login-form-2">
      <div className="sidenav2">
        <div className="text-center">
          <img src={logo} alt="true"/>
        </div>
        <h3 className=""><FormattedMessage {...messages.forgotPasswordText} /></h3>
        <form onSubmit={handleSubmit} autoComplete="off">
          <div className="form-group">
            <div className="lang d-flex iconImage">
              <FormattedMessage {...messages.forgotPasswordEmail}>
                  {
                    placeholder => 
                      <>
                        <input type="email" className="input" placeholder={placeholder} name="email" onChange={handleInputChange} value={inputs.email} required/>
                        <img src={email} alt="true"/>
                      </>
                  }
                </FormattedMessage>
            </div>
            <div className="line-box">
              <div className="line"/>
            </div>
          </div>
          <div onClick={submitForm} className="form-group d-flex justify-content-center mt-5">
            <button type="submit" className="btnSubmit mt-3 w-75"><FormattedMessage {...messages.forgotPasswordButton} /></button>
          </div>
        </form>
      </div>
    </div>
  );
}
InputForm.propTypes = {
  goProceed: PropTypes.func
};
