import React from "react";
import "../SignIn/style.css";
import "bootstrap/dist/css/bootstrap.css";
import logo from "../../images/logo.png";
import { Link } from "react-router-dom";

export default function AddEmail() {
  return <div className="col-sm-6 login-form-2 , sidenav2">
    <div className="img-logo">
      <img src={logo} alt="true"/>
    </div>
    <h3 className="p-4">An email has been sent with<br/> containing instructions for<br/> resetting your password.</h3>
    <form className="login-2">
      <Link to="/">
        <div className="form-group d-flex justify-content-center mt-5">
          <button type="submit" className="btnSubmit mt-3">Login</button>
        </div>
      </Link>
      <Link to="/HomePage">
        <div className="form-group-1 d-flex justify-content-center">
          <h6>Skip for now</h6>
        </div>
      </Link>
    </form>
  </div>;
}
