// Load the favicon and the .htaccess file
/* eslint-disable import/no-unresolved, import/extensions */
import "!file-loader?name=[name].[ext]!./images/favicon.ico";
import "@babel/polyfill";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min";
import { ConnectedRouter } from "connected-react-router";
import App from "containers/App";
import LanguageProvider from "containers/LanguageProvider";
import "file-loader?name=.htaccess!./.htaccess";
import "font-awesome/css/font-awesome.min.css";
import "jquery/dist/jquery";
import React from "react";
import ReactDOM from "react-dom";
// import "./assets/css/globalStyle.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import "sanitize.css/sanitize.css";
import history from "utils/history";
import "w3-css/4/w3pro.css";
import configureStore from "./configureStore";
import { translationMessages } from "./i18n";

import ActionCable from "actioncable";
import ActionCableProvider from "react-actioncable-provider";

let userId = localStorage.getItem("userId");
const cable = ActionCable.createConsumer(
  `http://3.15.160.19/cable?id=${userId}`
);
const initialState = {};
const store = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById("app");

const render = messages => {
  ReactDOM.render(
    <Provider store={store}>
      <LanguageProvider messages={messages}>
        <ConnectedRouter history={history}>
          <ActionCableProvider cable={cable}>
            <App />
          </ActionCableProvider>
          <ToastContainer />
        </ConnectedRouter>
      </LanguageProvider>
    </Provider>,
    MOUNT_NODE
  );
};

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(["./i18n", "containers/App"], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render(translationMessages);
  });
}

// Chunked polyfill for browsers without Intl support
if (!window.Intl) {
  new Promise(resolve => {
    resolve(import("intl"));
  })
    .then(() =>
      Promise.all([
        import("intl/locale-data/jsonp/en.js"),
        import("intl/locale-data/jsonp/ar.js")
      ])
    )
    .then(() => render(translationMessages))
    .catch(err => {
      throw err;
    });
} else {
  render(translationMessages);
}

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === "production") {
  require("offline-plugin/runtime").install(); // eslint-disable-line global-require
}
