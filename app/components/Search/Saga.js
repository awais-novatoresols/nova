import {put, takeLatest} from "redux-saga/effects";
import {GET_NOTIFICATIONS, SEARCH} from "./constants";
import {getNotificationSuccess, searchFailure, searchSuccess} from "./actions";
import {apiFetch} from "../../utils/network";
import {getCommuntiesSuccess} from "../../containers/Communities/actions";

function* search(user) {
  // console.log('data from search function..', user);
  try {
    const json = yield apiFetch(`users/${user.userId}/search?${user.filter}=${user.text}`, user.accessToken, user.clientId, user.uid);
    // console.log("Search Saga...", json);
    yield put(getCommuntiesSuccess(json));
    yield put(searchSuccess(json));
  } catch (error) {
    console.log("Error", error);
    yield put(searchFailure());
  }
}


export function* getNotifications(userId) {
  try {
    let json = yield apiFetch(`users/${userId.userId}/notifications`, userId.accessToken, userId.clientId, userId.uid);
    console.log("Notifications Saga", json);
    yield put(getNotificationSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export default function* userData() {
  yield takeLatest(SEARCH, search);
  yield takeLatest(GET_NOTIFICATIONS, getNotifications);

}
