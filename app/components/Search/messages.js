import { defineMessages } from 'react-intl';

export default defineMessages({
  searchBarTextSearch:{
    id: 'search-bar-text-search',
    defaultMessage: 'Search'
  },
  searchBarDropDownAll: {
    id: 'search-bar-dropdown-all',
    defaultMessage: 'All',
  },
  searchBarDropDownPost: {
    id: 'search-bar-dropdown-post',
    defaultMessage: 'Post'
  },
  searchBarDropDownCommunities: {
    id: 'search-bar-dropdown-communities',
    defaultMessage: 'Communities'
  },
  searchBarDropDownUserName: {
    id: 'search-bar-dropdown-username',
    defaultMessage: 'UserName'
  },
  notificationViewAll: {
    id: 'notification-view-all',
    defaultMessage: 'View All'
  },
  notificationNoNotification: {
    id: 'notification-no-notification',
    defaultMessage: 'No Notification Yet'
  },
  headerlinkNewsFeeds: {
    id: 'header-link-news-feed',
    defaultMessage: 'Communities',
  },
  headerlinkCommunities: {
    id: 'header-link-communities',
    defaultMessage: 'Communities',
  },
  headerlinkProfile: {
    id: 'header-link-profile',
    defaultMessage: 'Profile',
  },
  
  
});