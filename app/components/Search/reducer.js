import produce from "immer";
import { SEARCH_SUCCESS, SEARCH_FAILURE, GET_NOTIFICATIONS_SUCCESS } from "./constants";

export const initialState = {
  error: false,
  success: false,
  data: [],
  notifications: {}
};

const searchReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
    case SEARCH_SUCCESS:
      draft.error = false;
      draft.success = true;
      draft.data = action.data;
      break;
    case GET_NOTIFICATIONS_SUCCESS:
      draft.notifications = action.data;
      break;
    case SEARCH_FAILURE:
      draft.error = true;
      draft.success = false;
      break;
    default:
      draft.success = false;
      draft.error = false;
    }
  });

export default searchReducer;