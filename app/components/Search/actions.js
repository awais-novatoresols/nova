import { GET_NOTIFICATIONS, GET_NOTIFICATIONS_SUCCESS, SEARCH, SEARCH_FAILURE, SEARCH_SUCCESS } from "./constants";

export function search(text, filter, userId, clientId, accessToken, uid) {
  return {
    type: SEARCH,
    text,
    filter,
    userId,
    clientId,
    accessToken,
    uid
  };
}

export function searchSuccess(data) {
  return {
    type: SEARCH_SUCCESS,
    data
  };
}

export function searchFailure() {
  return {
    type: SEARCH_FAILURE
  };
}

export function getUserNotifications(userId, accessToken, clientId, uid) {
  return {
    type: GET_NOTIFICATIONS,
    userId,
    accessToken,
    clientId,
    uid
  };
}

export function getNotificationSuccess(data) {
  return {
    type: GET_NOTIFICATIONS_SUCCESS,
    data
  };
}
