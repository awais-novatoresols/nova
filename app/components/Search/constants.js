export const SEARCH = "ubwab/SEARCH";
export const SEARCH_SUCCESS = "ubwab/SEARCH_SUCCESS";
export const SEARCH_FAILURE = "ubwab/SEARCH_FAILURE";
export const GET_NOTIFICATIONS = "ubwab/user/GET_NOTIFICATIONS";
export const GET_NOTIFICATIONS_SUCCESS = "ubwab/user/GET_NOTIFICATIONS_SUCCESS";