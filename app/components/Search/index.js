import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import NotificationImage from "../../components/NotificationImage";
import ChatImage from "../../containers/Chat";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { getUserNotifications, search } from "./actions";
import messages from "./messages";
import reducer from "./reducer";
import saga from "./Saga";
import { makeSelecSearchtData, makeSelectError, makeSelectNotifications, makeSelectSuccess } from "./selector";
import "./style.css";

const key = "search";
const filterArray = [
  {
    id: "search",
    name: <FormattedMessage {...messages.searchBarDropDownAll} />
  },
  {
    id: "post_title",
    name: <FormattedMessage {...messages.searchBarDropDownPost} />
  },
  {
    id: "community_name",
    name: <FormattedMessage {...messages.searchBarDropDownCommunities} />
  },
  {
    id: "user_name",
    name: <FormattedMessage {...messages.searchBarDropDownUserName} />
  }
];
const Search = props => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  // console.log("props from search ....", props);
  let localLanguage = localStorage.getItem('language');
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let profileImage = localStorage.getItem("profile_image");
  let uid = localStorage.getItem("uid");
  const [searchText, setSearchText] = useState("");
  const [filter, setFilter] = useState("search");
  const [showNotificationDropDown, setShowNotificationDropDown] = useState(
    false
  );
  const [showMessageDropDown, setShowMessageDropDown] = useState(false);
  useEffect(() => {
    //console.log("Hitting ..............");
    props.getNotifications(userId, accessToken, clientId, uid);
    if (filter === "community_name") {
      props.search(
        searchText,
        "community_name",
        userId,
        clientId,
        accessToken,
        uid
      );
    }
  }, [filter === "community_name"]);

  const onSearch = () => {
    if (props.location.pathname.toLowerCase() !== "/homepage" && props.location.pathname.toLowerCase() !== "/general")
      return props.searchCommunities(searchText, "community_name");
    props.search(searchText, filter, userId, clientId, accessToken, uid);
  };

  const onSearchCommunities = () => {
    props.search(
      searchText,
      "community_name",
      userId,
      clientId,
      accessToken,
      uid
    );
  };
  const onInput = e => {
    if (e.key === "Enter") {
      e.preventDefault();
      onSearch();
    }
    setSearchText(e.target.value.trim());
    // if (e.target.value.length > 2) {
    //   if (props.location.pathname.toLowerCase() !== "/homepage") setFilter("community_name");
    //   onSearch();
    // }
  };

  function toggleSidebar() {
    let sidebar = document.getElementById("sidebar-toggle");
    if (sidebar) {
      sidebar.classList.toggle("d-lg-block");
      sidebar.classList.toggle("d-md-none");
      sidebar.classList.toggle("d-sm-none");
      sidebar.classList.toggle("d-none");
      sidebar.classList.toggle("active");
      sidebar.classList.toggle("sidebar-offcanvas");
    }
  }

  function onChange(value) {
    setFilter(value.target.value);
  }

  function chatView() {
    props.postRemoved();
    props.history.push("/chat");
  }

  function goToProfile() {
    props.postRemoved();
    props.history.push("/profile");
  }

  function openChat(id) {
    id = id === null || id === undefined ? 1 : id;
    //  console.log("openChat..", id);
    props.history.push("./chat");
  }

  return (
    <>
      {(props.location.pathname === "/" ||
        props.location.pathname.toLowerCase() === "/homepage" ||
        props.location.pathname.toLowerCase() === "/communities" ||  
        props.location.pathname.toLowerCase() === "/general") && (
        <div className="lang searchbar input-group bg-white rounded hide-from-768">
          <FormattedMessage {...messages.searchBarTextSearch}>
            {placeholder => (
              <>
                <input
                  type="text"
                  className="form-control border-0 shadow-none"
                  placeholder={placeholder}
                  onKeyDown={onInput}
                  onChange={onInput}
                />
              </>
            )}
          </FormattedMessage>
          <select
            onChange={value => onChange(value)}
            className={`sel1 bg-white cursor_pointer_class ${localLanguage === 'ar' ? 'text-align-last-left' : 'text-align-last-right'}`}
            name="sellist1"
          >
            {filterArray.map((item, index) => {
              return (
                <option
                  className="cursor_pointer_class"
                  key={index}
                  value={item.id}
                >
                  {item.name.props.defaultMessage}
                </option>
              );
            })}
          </select>
          <button
            type="button"
            className="btn bg-white shadow-none"
            onClick={onSearch}
          >
            <i className="fa fa-search" />
          </button>
        </div>
      )}
      {props.location.pathname === "/profile" ? (
        <>
          <NotificationImage />
          <div className="dropdown-menu dropdown-menu-right w_width p-0">
            <div className="list-group">
              {props.notifications &&
                props.notifications.length > 0 &&
                props.notifications.slice(0, 5).map(notify => {
                  return (
                    <div key={notify.id}>
                      {notify.notification_type === "like_dislike_post" && (
                        <Link
                          className="list-group-item flex-column align-items-start list_style  text-decoration-none"
                          to={`/post/${notify.post_id}`}
                        >
                          <p>{notify.message}</p>
                        </Link>
                      )}
                      {notify.notification_type !== "like_dislike_post" && (
                        <p
                          className="list-group-item flex-column align-items-start list_style  text-decoration-none"
                          onClick={() => openChat(notify.following_user_id)}
                        >
                          {notify.message}
                        </p>
                      )}
                    </div>
                  );
                })}
              {props.notifications && props.notifications.length > 0 && (
                <Link
                  to="/notifications"
                  className="list-group-item flex-column text-center"
                >
                  <FormattedMessage {...messages.notificationViewAll} />
                </Link>
              )}
              {props.notifications && props.notifications.length === 0 && (
                <span className="list-group-item flex-column text-center p-4">
                  <FormattedMessage {...messages.notificationNoNotification} />
                </span>
              )}
            </div>
          </div>
          <ChatImage />
          <button
            className="navbar-toggler text-white show-below-768 ml-auto"
            type="button"
            onClick={toggleSidebar}
          >
            <span className="fa fa-bars" />
          </button>
        </>
      ) : (
        <>
          <NotificationImage />
          <div className="dropdown-menu dropdown-menu-right w_width p-0">
            <div className="list-group">
              {props.notifications &&
                props.notifications.length > 0 &&
                props.notifications.slice(0, 5).map(notify => {
                  return (
                    <div key={notify.id}>
                      {notify.notification_type === "like_dislike_post" && (
                        <Link to={`/post/${notify.post_id}`}>
                          <p className="list-group-item flex-column align-items-start list_style  text-decoration-none">
                            {notify.message}
                          </p>
                        </Link>
                      )}
                      {notify.notification_type !== "like_dislike_post" && (
                        <Link to={`/post/${notify.post_id}`}>
                          <p
                            className="list-group-item flex-column align-items-start list_style  text-decoration-none"
                            // onClick={() => openChat(notify.following_user_id)}
                          >
                            {notify.message}
                          </p>
                        </Link>
                      )}
                    </div>
                  );
                  // <Link key={notify.id} to={`/post/${notify.post_id}`}
                  //              className="list-group-item flex-column align-items-start list_style  text-decoration-none">
                  //   <p className="mb-2">{notify.message}</p>
                  // </Link>
                })}
              {props.notifications && props.notifications.length > 0 && (
                <Link
                  to="/notifications"
                  className="list-group-item flex-column text-center cursor_pointer_class"
                >
                  <FormattedMessage {...messages.notificationViewAll} />
                </Link>
              )}
              {props.notifications && props.notifications.length === 0 && (
                <span className="list-group-item flex-column text-center p-4">
                  <FormattedMessage {...messages.notificationNoNotification} />
                </span>
              )}
            </div>
          </div>
          <ChatImage />
          <img
            onClick={goToProfile}
            className="rounded-circle img1 cursor_pointer_class"
            src={
              profileImage !== "null"
                ? profileImage
                : require("../../images/ic_profile.png")
            }
            width="38px"
            height="38px"
          />
        </>
      )}
    </>
  );
};
Search.propTypes = {
  data: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  openSideBar: PropTypes.func,
  searchCommunities: PropTypes.func,
  search: PropTypes.func,
  postRemoved: PropTypes.func,
  getLatestPost: PropTypes.func,
  showSearchResult: PropTypes.func,
  success: PropTypes.bool,
  isSearch: PropTypes.bool,
  error: PropTypes.bool,
  notificationCount: PropTypes.number,
  location: PropTypes.object,
  notifications: PropTypes.array,
  getNotifications: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  success: makeSelectSuccess(),
  data: makeSelecSearchtData(),
  error: makeSelectError(),
  notifications: makeSelectNotifications()
});

export function mapDispatchToProps(dispatch) {
  return {
    search: (text, filter, userId, clientId, accessToken, uid) =>
      dispatch(search(text, filter, userId, clientId, accessToken, uid)),
    getNotifications: (userId, accessToken, clientId, uid) =>
      dispatch(getUserNotifications(userId, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(Search);
