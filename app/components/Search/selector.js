import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.search || initialState;

const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelecSearchtData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.data
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.error
  );

const makeSelectNotifications = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.notifications.notifications
  );

export { selectGlobal, makeSelectError, makeSelectSuccess, makeSelecSearchtData, makeSelectNotifications };

