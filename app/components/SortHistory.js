import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const SortHistory = (props) => {
  let localLanguage = localStorage.getItem('language');
  return (
    <>
      <div className={localLanguage === 'ar' ? 'directRtl text-right row mt-3' : 'row mt-3'}>
        <div className="col-sm-4">
          <div className="dropdown">
            <button type="button" className="custom_b1 btn dropdown-toggle" data-toggle="dropdown">
            <FormattedMessage {...messages.historyPostDropDownSortBy}/>
            </button>
            <div className={localLanguage === 'ar' ? 'text-right dropdown-menu' : 'dropdown-menu'}>
              <p className="custom_p m-2 pb-1 pl-2"><FormattedMessage {...messages.historyPostDropDownFilter}/></p>
              <a id="upvote" onClick={props.onHistorySort} className={localLanguage === 'ar' ? "dropdown-item mt-3 mr-n3" : "dropdown-item mt-3 pl-3"}>
                <span className="fa fa-arrow-up" />{'\u00A0'}<FormattedMessage {...messages.historyPostDropDownUpVote}/></a>
              <a id="downvote" onClick={props.onHistorySort} className={localLanguage === 'ar' ? "dropdown-item mt-3 mr-n3" : "dropdown-item mt-3 pl-3"}>
                <span className="fa fa-arrow-down" />{'\u00A0'}<FormattedMessage {...messages.historyPostDropDownDownVote}/></a>
              <a id="hidden" onClick={props.onHistorySort} className={localLanguage === 'ar' ? "dropdown-item mt-3 mr-n3" : "dropdown-item mt-3 pl-3"}>
                <span className="fa fa-eye-slash" />{'\u00A0'}<FormattedMessage {...messages.historyPostDropDownHide}/></a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
SortHistory.propTypes = {
  onHistorySort: PropTypes.func
};
export default SortHistory;
