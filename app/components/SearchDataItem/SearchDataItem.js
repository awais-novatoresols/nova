import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import moment from "moment";
import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { Collapse, ProgressBar } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import ReactPlayer from "react-player";
import { connect } from "react-redux";
import { Link, useHistory, useLocation } from "react-router-dom";
import ShowMoreText from "react-show-more-text";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { getCommentLoaded } from "../../containers/ViewPost/actions";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { givePollVote, likePost } from "../PostItem/actions";
import { makeSelectLikeSuccess } from "../PostItem/selectors";
import PostUserAction from "../PostUserAction";
import { makeSelectDeleteSuccess } from "../PostUserAction/selector";
import messages from "./messages";
import reducer from "../PostItem/reducer";
import saga from "../PostItem/Saga";
import "./style.css";

const key = "postItem";

function SearchDataItem(props) {
  const history = useHistory();
  const location = useLocation();

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  const [commentText, setText] = useState("");
  const [open, setOpen] = useState(false);
  let postArray =
    props.data &&
    props.data.data.filter(item => {
      return item.type === 1;
    });
  let communityArray =
    props.data &&
    props.data.data.filter(item => {
      return item.type === 2;
    });
  let userName =
    props.data &&
    props.data.data.filter(item => {
      return item.type === 0;
    });
  // useEffect(() => {
  //   if (props.likeSuccess) {
  //     props.getLatestPost();
  //   }
  // }, [props.likeSuccess]);

  let userId = localStorage.getItem("userId");

  function postLike(postId, currentStatus, previousStatus, e) {
    e.preventDefault();
    let ourStatus = currentStatus;
    let apiStatus = previousStatus === undefined ? 0 : previousStatus;
    if (previousStatus === currentStatus) ourStatus = 0;
    let formData = new FormData();
    formData.append("current_status", ourStatus);
    formData.append("previous_status", apiStatus);
    props.likePost(userId, postId, formData, accessToken, clientId, uid);
    postArray.forEach(post => {
      let newPost = post.post;
      if (newPost.id === postId) {
        newPost.vote_status = ourStatus;
        if (ourStatus === 1 && apiStatus === 0) {
          newPost.count = newPost.count + 1;
        } else if (ourStatus === 0 && apiStatus === 1) {
          newPost.count = newPost.count - 1;
        } else if (ourStatus === 2 && apiStatus === 0) {
          newPost.count = newPost.count - 1;
        } else if (ourStatus === 0 && apiStatus === 2) {
          newPost.count = newPost.count + 1;
        } else if (ourStatus === 2 && apiStatus === 1) {
          newPost.count = newPost.count - 2;
        } else if (ourStatus === 1 && apiStatus === 2) {
          newPost.count = newPost.count + 2;
        }
      }
      return newPost;
    });
  }

  function getCommentValue(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      props.createComment("", commentText, posts[0]);
    }
    setText(e.target.value);
  }

  function changePinStatus(community_id, postId, unpin) {
    let formData = new FormData();
    formData.append("community_id", community_id);
    if (unpin) formData.append("unpin", unpin);
    props.pinPost(userId, postId, formData, accessToken, clientId, uid);
    if (unpin) props.getLatestPost();
  }

  function getPostId(id) {
    console.log("id", id);
    if (
      location.pathname !== undefined &&
      !location.pathname.includes("/post")
    ) {
      let json = [];
      props.getCommentLoaded(json);
      history.push(`/post/${id.target.id}`);
    }
  }

  function commentOpen(postId) {
    setOpen(!open);
    if (
      location.pathname !== undefined &&
      !location.pathname.includes("/post")
    ) {
      let json = [];
      props.getCommentLoaded(json);
      history.push(`/post/${postId}`);
    }
  }

  function findLinkText(link) {
    let isExist = link.includes("you");
    if (isExist) {
      return true;
    } else {
      return false;
    }
  }

  function getDuration(publishDate) {
    let startDate = new Date();
    let a = moment(startDate);
    let b = moment(publishDate);
    let difference = a.diff(b, "minutes");
    return moment.duration(difference, "minutes").humanize();
  }

  return (
    <>
      {postArray.length > 0 && (
        <div className="row">
          <div className="col-sm-12">
            <h5 className="mt-4">Posts</h5>
          </div>
        </div>
      )}
      {postArray.length > 0 &&
        postArray.map((item, index) => {
          console.log("item.post.comment_count", item.post);

          return (
            <div className="container" key={index}>
              <div className="row">
                <div className="card mt-3 post_c w-100">
                  <div className="card-header d-flex post_ch">
                    <Link
                      to={`/profile/${item.post.user.id}/${
                        item.post.user.profile.id
                      }`}
                    >
                      <img
                        src={
                          item.post.user.profile &&
                          item.post.user.profile.profile_image !== null
                            ? item.post.user.profile.profile_image
                            : require("../../images/user.png")
                        }
                        className="rounded-circle"
                      />
                    </Link>
                    <div className="col p-0">
                      <Link
                        to={`/profile/${item.post.user.id}/${
                          item.post.user.profile.id
                        }`}
                      >
                        <span className="post_p1 d-block">
                          {item.post.user && item.post.user.profile.name}
                        </span>
                      </Link>
                      {item.post.community && (
                        <Link
                          to={`/communities/view/${item.post.community.id}`}
                        >
                          <span className="post_p1 d-block ">
                            {item.post.community && item.post.community.name}
                          </span>
                        </Link>
                      )}{" "}
                    </div>
                    {!props.hideTime && (
                      <h6 className="ml-auto mr-2 mt-2 ">
                        {getDuration(item.post.created_at)} ago
                      </h6>
                    )}
                    {props.pinImage && item.post.pinned && (
                      <img
                        onClick={() =>
                          changePinStatus(
                            item.post.community.id,
                            item.post.id,
                            true
                          )
                        }
                        src={require("../../images/redIcon.png")}
                        className="rounded-circle cursor_pointer_class mt-1"
                        style={{ width: "30px", height: "30px" }}
                      />
                    )}
                    {!props.hidePostOptions && (
                      <PostUserAction
                        getLatestPost={props.getLatestPost}
                        item={item.post}
                        isSavedTab={props.isSavedTab ? true : false}
                        community={props.community}
                        changePinStatus={changePinStatus}
                      />
                    )}
                  </div>
                  <div className="pl-4 mt-5 cursor_pointer_class">
                    <h5
                      value={item.post.id}
                      id={item.post.id}
                      onClick={getPostId}
                    >
                      {item.post.title}
                    </h5>
                    {item.post.post_type === "text" && (
                      <>
                        {item.post.link.length < 300 ? (
                          <p>{item.post.link}</p>
                        ) : (
                          <span>
                            <ShowMoreText
                              lines={4}
                              more="Show more"
                              less="Show less"
                              anchorClass=""
                              expanded={false}
                            >
                              {item.post.link}
                            </ShowMoreText>
                          </span>
                        )}
                      </>
                    )}
                  </div>
                  <div>
                    {item.post.post_type === "image" && (
                      <img
                        value={item.post.id}
                        id={item.post.id}
                        onClick={getPostId}
                        className="custom_i2 cursor_pointer_class"
                        style={{ objectFit: "contain" }}
                        src={
                          item.post.post_type === "image" && item.post.link
                            ? item.post.link
                            : require("../../images/placeholder2.jpg")
                        }
                      />
                    )}
                    {item.post.post_type === "video" && (
                      <ReactPlayer
                        url={item.post.link}
                        config={{
                          youtube: {
                            playerVars: { showinfo: 0, rel: 0 }
                          },
                          facebook: {
                            appId: "549267839141720"
                          }
                        }}
                        controls
                        width="100%"
                      />
                    )}
                    {item.post.post_type === "link" && (
                      <>
                        {findLinkText(item.post.link) === true && (
                          <ReactPlayer
                            url={item.post.link}
                            config={{
                              youtube: {
                                playerVars: { showinfo: 1 }
                              },
                              facebook: {
                                appId: "549267839141720"
                              }
                            }}
                            controls={true}
                            playing
                            loop={false}
                            muted
                            controls
                            width="100%"
                          />
                        )}
                      </>
                    )}
                  </div>
                  {item.post.poll_options.length !== 0 && (
                    <div className="pl-4">
                      <div className="mt-4">
                        <div className="row mt-5">
                          <div className="col-sm-12">
                            <div className="d-flex">
                              <img
                                src={require("../../images/poll.png")}
                                width="30px"
                              />
                            </div>
                          </div>
                        </div>
                        {item.post.poll_options.map((data, index) => {
                          return (
                            data.title !== "" && (
                              <div key={index} className="row mt-3">
                                <div className="col-sm-5 col-md-5 col-6">
                                  <div className="custom-control custom-checkbox">
                                    {item.post.poll_option_index === data.id ? (
                                      <div className="row">
                                        <img
                                          className="cursor_pointer_class"
                                          src={require("../../images/success.png")}
                                          height="20px"
                                        />
                                        <label className="pl-3">
                                          {data.title}
                                        </label>
                                      </div>
                                    ) : (
                                      <div
                                        className="row"
                                        id={data.id}
                                        onClick={e =>
                                          getVoteId(data.id, item.post)
                                        }
                                      >
                                        <img
                                          className="cursor_pointer_class"
                                          src={require("../../images/circleoutline.png")}
                                          height="20px"
                                        />
                                        <label className="pl-3">
                                          {data.title}
                                        </label>
                                      </div>
                                    )}
                                  </div>
                                </div>
                                <div className="col-sm-6 col-md-6 col-5 mt-1">
                                  <ProgressBar
                                    variant="success"
                                    now={data.count}
                                  />
                                </div>
                                <div className="col-sm-1 col-md-1 col-1" />
                              </div>
                            )
                          );
                        })}
                      </div>
                    </div>
                  )}
                  <div className="card-footer post_f1 d-flex justify-content-around">
                    <div className="d-flex justify-content-around">
                      <a className="remove_a1">
                        <span
                          id="like"
                          onClick={e =>
                            postLike(item.post.id, 1, item.post.vote_status, e)
                          }
                          className={
                            item.post.vote_status === 1
                              ? "fa fa-arrow-up text-success"
                              : "fa fa-arrow-up cursor_p"
                          }
                        />
                      </a>
                      &nbsp;&nbsp;{item.post.count > 0 ? item.post.count : 0}
                      &nbsp;&nbsp;
                      <a className="remove_a1">
                        <span
                          id="dislike"
                          onClick={e =>
                            postLike(item.post.id, 2, item.post.vote_status, e)
                          }
                          className={
                            item.post.vote_status === 2
                              ? "fa fa-arrow-down text-danger"
                              : "fa fa-arrow-down cursor_p"
                          }
                        />
                      </a>
                    </div>
                    <a
                      onClick={() => commentOpen(item.post.id)}
                      className="cursor_p remove_a1 custom_a2"
                    >
                      <img
                        src={require("../../images/comment.png")}
                        width="20px"
                      />{" "}
                      {" " + item.post.count}{" "}
                      <FormattedMessage {...messages.postItemComment} />
                    </a>
                    <div className="dropdown">
                      <button
                        className="btn alphab1"
                        id="menu1"
                        data-toggle="dropdown"
                      >
                        <img
                          src={require("../../images/share.png")}
                          width="18px"
                          className="mr-3"
                        />
                        <FormattedMessage {...messages.postItemShare} />
                      </button>
                      <div className="dropdown-menu dropdown-menu-right">
                        <a
                          onClick={() => {
                            navigator.clipboard.writeText(
                              `${homeUrl}post/${item.post.id}`
                            );
                            toast.success("Link copied to clipboard!");
                          }}
                          className="remove_a1"
                        >
                          <li
                            role="presentation"
                            className="mt-2 cursor_p"
                            id="Post 1"
                          >
                            <img
                              src={require("../../images/copy.png")}
                              className="ml-2 p-1"
                            />
                            <span>
                              <FormattedMessage
                                {...messages.postItemShareDropDownCopyLink}
                              />
                            </span>
                          </li>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-5" />
            </div>
          );
        })}
      {communityArray.length > 0 && (
        <div className="row">
          <div className="col-sm-12">
            <h5>Communities</h5>
          </div>
        </div>
      )}
      {communityArray.length > 0 &&
        communityArray.map((item, index) => {
          return (
            <div className="row mt-2 m-0 alpharow mb-4" key={index}>
              <div className="col-sm-10 p-3 col-8">
                <div className="d-flex alphaa">
                  <img
                    src={
                      item.community && item.community.profile_image !== null
                        ? item.community && item.community.profile_image
                        : require("../../images/ic_profile.png")
                    }
                    className="rounded-circle"
                    width="28px"
                    height="28px"
                  />
                  <h4 className="text pl-4">
                    {item.community && item.community.name}
                  </h4>
                </div>
              </div>
              <div className="col-sm-2 col-4">
                <Link to={`/communities/view/${item.community.id}`}>
                  <button
                    type="button"
                    className="btnsubmit1 w-100 p-1 mt-3 mb-2"
                  >
                    View Page
                  </button>
                </Link>
              </div>
            </div>
          );
        })}
      {userName.length > 0 && (
        <div className="row">
          <div className="col-sm-12">
            <h5 className="mt-3">Users</h5>
          </div>
        </div>
      )}
      {userName.length > 0 &&
        userName.map((item, index) => {
          return (
            <div className="row mt-2 m-0 alpharow mb-4" key={index}>
              <div className="col-sm-10 p-3 col-8">
                <div className="d-flex alphaa">
                  <img
                    src={
                      item.user &&
                      item.user.profile &&
                      item.user.profile.profile_image !== null
                        ? item.user.profile.profile_image
                        : require("../../images/ic_profile.png")
                    }
                    className="rounded-circle"
                    width="28px"
                    height="28px"
                  />
                  <h4 className="text pl-4">
                    {item.user && item.user.user_name}
                  </h4>
                </div>
              </div>
              <div className="col-sm-2 col-4">
                <Link to={`/profile/${item.user.id}/${item.user.profile.id}`}>
                  <button
                    type="button"
                    className="btnsubmit1 w-100 p-1 mt-3 mb-2"
                  >
                    View Profile
                  </button>
                </Link>
              </div>
            </div>
          );
        })}
      <Collapse in={open}>
        <div className="row ">
          <div className="col-sm-12">
            <div className="card t-4 mt-3">
              <div>
                <FormattedMessage {...messages.commentMainInputPlaceHolder}>
                  {placeholder => (
                    <>
                      <input
                        rows="6"
                        maxLength={100}
                        size={100}
                        className="form-control t_a mt-1 mb-5 p-3"
                        type="text"
                        onChange={getCommentValue}
                        onKeyDown={getCommentValue}
                        placeholder={placeholder}
                      />
                    </>
                  )}
                </FormattedMessage>
                <button
                  type="submit"
                  onClick={() => props.createComment("", commentText, posts[0])}
                  className="btnSubmit1 w-25 mt-2"
                >
                  <FormattedMessage {...messages.commentMainButtonTitle} />
                </button>
              </div>
            </div>
          </div>
        </div>
      </Collapse>
    </>
  );
}
SearchDataItem.propTypes = {
  posts: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  activeTab: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  deletePostSuccess: makeSelectDeleteSuccess(),
  likeSuccess: makeSelectLikeSuccess()
});

export function mapDispatchToProps(dispatch) {
  return {
    givePollVote: (postData, accessToken, clientId, uid) =>
      dispatch(givePollVote(postData, accessToken, clientId, uid)),
    getCommentLoaded: json => dispatch(getCommentLoaded(json)),
    likePost: (userId, postId, formData, accessToken, clientId, uid) =>
      dispatch(likePost(userId, postId, formData, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(SearchDataItem);
