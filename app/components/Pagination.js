import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import Pagination from 'react-bootstrap/Pagination';

const PaginationContainer = props => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    let pageItems = [];
    for (let index = 1; index <= props.totalPages; index++) {
      pageItems.push(index);
    }
    //console.log("Page Items", pageItems, props.totalPages)
    setItems(pageItems);
  }, [props.activePage, props.totalPages]);


  return <div className="mt-3 float-right"><Pagination>{items.length > 1 && items.map(item => {
    return <Pagination.Item key={item} id={item} active={item === props.activePage} onClick={props.onPageChange}>
      {item}
    </Pagination.Item>
  })}</Pagination></div>;
}

PaginationContainer.propTypes = {
  totalPages: PropTypes.number,
  activePage: PropTypes.number,
  onPageChange: PropTypes.func
}

export default PaginationContainer;