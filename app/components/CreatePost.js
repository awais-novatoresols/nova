import React from "react";
import "../containers/Profile/globalStyle.css";
import "../containers/Profile/style.css";
import "../containers/HomePage/style.css";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import "w3-css/4/w3pro.css";
import "bootstrap/dist/js/bootstrap.min";
import PropTypes from "prop-types";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

export default function CreatePost(props) {
  return (
    <>
      <div className="lang card custom_c1 mt-3">
        <div className="card-header custom_ch1 p-0">
          <img className="custom_i3" src={require("../images/nature.png")} />
        </div>
        <div className="card-body custom_ch1">
          <h2> <FormattedMessage {...messages.creatPostCardHeading} /> </h2>
          <p><FormattedMessage {...messages.creatPostCardDiscription} /></p>
        </div>
        <div onClick={props.displayModel} id="custom_cf" className="card-footer text-center pointer">
          <div id="custom_cfa"><FormattedMessage {...messages.creatPostCardButtonTitle} /></div>
        </div>
      </div>
    </>
  );
}

CreatePost.propTypes = {
  displayModel: PropTypes.func
}