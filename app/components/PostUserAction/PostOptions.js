import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useLocation } from 'react-router-dom';
import { FormattedMessage } from "react-intl";
import messages from "./messages";

const PostOptions = props => {
  const [dropDownArray, setDropDownArray] = useState([]);
  const userEmail = localStorage.getItem("uid");
  const location = useLocation();
  let userId = localStorage.getItem("userId");
  let localLanguage = localStorage.getItem('language');
  // console.log("props.item.", props.item);
  useEffect(() => {
    let data = [];
    if (
      (props.item.user && props.item.user.email === userEmail) ||
      (props.item.user && props.item.user.id === userId)
    ) {
      data = [
        {
          id: <FormattedMessage {...messages.postItemDropDownEdit} />,
          name: "edit",
          image: require("../../images/edit.png")
        },
        {
          id: <FormattedMessage {...messages.postItemDropDownDelete} />,
          name: "delete",
          image: require("../../images/delete.png")
        }
      ];
      if (
        props.community &&
        props.community.role === "admin" &&
        props.item.pinned === false
      )
        data.unshift({
          id: <FormattedMessage {...messages.postItemDropDownPin} />,
          image: require("../../images/blackIconsmall.png"),
          name: "pin"
        });
    } else {
      data = [
        {
          id: <FormattedMessage {...messages.postItemDropDownHide} />,
          name: "hide",
          image: require("../../images/visibility1.png")
        },
        {
          id: <FormattedMessage {...messages.postItemDropDownReport} />,
          name: "report",
          image: require("../../images/flag1.png")
        },
        {
          id: <FormattedMessage {...messages.postItemDropDownBlock} />,
          name: "block",
          image: require("../../images/block1.png")
        }
      ];
      if ((props.item.saved_post === undefined || props.item.saved_post === false) && location.pathname !== '/profile')
        data.unshift({
          id: <FormattedMessage {...messages.postItemDropDownSave} />,
          name: "save",
          image: require("../../images/inbox1.png")
        });
      else if (
        props.community &&
        props.community.role === "admin" &&
        props.item.pinned === false
      )
        data.unshift({
          id: <FormattedMessage {...messages.postItemDropDownPin} />,
          image: require("../../images/blackIconsmall.png"),
          name: "pin"
        });
      if (props.isSavedTab)
        data.unshift({
          id: <FormattedMessage {...messages.postItemDropDownUnsave} />,
          image: require("../../images/inbox1.png"),
          name: "unsave"
        });
    }
    setDropDownArray(data);
  }, [props.item]);
  return (
    <div className="dropdown">
      <button className="btn alphab1" id="menu1" data-toggle="dropdown">
        <img className="alphab2" src={require("../../images/list.png")} />
      </button>
      <div className={`${localLanguage === 'ar' ? 'directRtl text-right' : 'dropdown-menu-right'} dropdown-menu`}>
        {dropDownArray.map((dropDownitem, index) => {
          if (dropDownitem.name === "pin") {
            return (
              <li
                key={index}
                id={dropDownitem.name}
                onClick={() =>
                  props.changePinStatus(props.item.community.id, props.item.id)
                }
                role="presentation"
                className="mt-2 cursor_pointer_class"
              >
                <img src={dropDownitem.image} className="ml-2 mr-2" />
                <span className="" id={dropDownitem.name}>
                  {dropDownitem.id}
                </span>
              </li>
            );
          } else {
            return (
              <li
                key={index}
                id={dropDownitem.name}
                onClick={e => props.userAction(e)}
                role="presentation"
                className="mt-2 cursor_pointer_class"
              >
                <img
                  src={dropDownitem.image}
                  className="ml-2 mr-2 p-1"
                  height="15"
                />
                <span className="" id={dropDownitem.name}>
                  {dropDownitem.id}
                </span>
              </li>
            );
          }
        })}
      </div>
    </div>
  );
};
PostOptions.propTypes = {
  userAction: PropTypes.func,
  dropDownArray: PropTypes.array,
  item: PropTypes.object,
  isSavedTab: PropTypes.bool
};
export default PostOptions;
