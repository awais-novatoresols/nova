import { defineMessages } from 'react-intl';

export default defineMessages({
    postItemDropDownSave: {
        id: 'post-item-drop-down-save',
        defaultMessage: 'Save',
    },
    postItemDropDownPin: {
        id: 'post-item-drop-down-pin',
        defaultMessage: 'Pin',
    },
    postItemDropDownUnsave: {
        id: 'post-item-drop-down-unsave',
        defaultMessage: 'Unsave',
    },
    postItemDropDownEdit: {
        id: 'post-item-drop-down-edit',
        defaultMessage: 'Edit',
    },
    postItemDropDownDelete: {
        id: 'post-item-drop-down-delete',
        defaultMessage: 'Delete',
    },
    postItemDropDownHide: {
        id: 'post-item-drop-down-hide',
        defaultMessage: 'Hide',
    },
    postItemDropDownReport: {
        id: 'post-item-drop-down-report',
        defaultMessage: 'Report',
    },
    postItemDropDownBlock: {
        id: 'post-item-drop-down-block',
        defaultMessage: 'Block user',
    },
    headerlinkNewsFeeds: {
        id: 'header-link-news-feed',
        defaultMessage: 'News Feed',
      },
      headerlinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Communities',
      },
      headerlinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
      },

    

});