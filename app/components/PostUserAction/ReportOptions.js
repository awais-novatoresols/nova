import PropTypes from "prop-types";
import React from "react";
import { Button, Form } from "react-bootstrap";

const ReportOption = props => {
  return (
    <div className="container">
      <p className="lead text-center">Rules and Conditions</p>
      <Form.Check
        onClick={props.onSelect}
        type='radio'
        className="mt-2"
        name="reason"
        value="It infrings my copyright"
        label="It infrings my copyright"
      />
      <Form.Check
        onClick={props.onSelect}
        className="mt-2"
        type='radio'
        name="reason"
        value="It doesn't fit this community"
        label="It doesn't fit this community"
      />
      <Form.Check
        onClick={props.onSelect}
        className="mt-2"
        type='radio'
        name="reason"
        value="It's personal information"
        label="It's personal information"
      />
      <Form.Check
        onClick={props.onSelect}
        className="mt-2"
        type='radio'
        name="reason"
        value="It's not morally excepted"
        label="It's not morally excepted"
      />
      <Form.Check
        onClick={props.onSelect}
        className="mt-2"
        type='radio'
        name="reason"
        value="It's harmful content"
        label="It's harmful content"
      />
      <div className="row mt-3">
        <div className="col"><Button variant="info" block onClick={props.report}>Report</Button></div>
        <div className="col"><Button variant="warning" block onClick={props.hide}>Cancel</Button></div>
      </div>
    </div>
  )
}

ReportOption.propTypes = {
  hide: PropTypes.func,
  report: PropTypes.func,
  onSelect: PropTypes.func
}

export default ReportOption;