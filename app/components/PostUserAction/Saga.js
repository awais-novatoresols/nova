import {put, takeLatest} from "redux-saga/effects";
import {actionFailure, actionSuccess, deletePostError, deletePostSuccess, unsaveSuccess} from "./actions";
import {apiFetchDelete, signUpApi} from "../../utils/network";
import {DELETE_POST, POST_UNSAVE, POST_USER_ACTION} from "./constants";

function* userActionOnPost(user) {
   // console.log("userActionOnPost...", user);
  try {
    const json = yield signUpApi(`users/${user.userId}/posts/${user.postId}/perform_action_on_post`, user.body, user.accessToken, user.clientId, user.uid);
    // console.log("userActionOnPost Saga...", json);
    yield put(actionSuccess(json));
  } catch (error) {
    console.log("Error", error);
    yield put(actionFailure());
  }
}

function* unsaveContent(user) {
  try {
    const json = yield apiFetchDelete(`saved_contents/${user.postId}`, user.formData, user.accessToken, user.clientId, user.uid);
  //  console.log("unsaveContent Saga...", json);
    yield put(unsaveSuccess(json));
  } catch (error) {
    console.log("Error", error);
  }
}

function* deleteContent(user) {
  try {
    let json = yield apiFetchDelete(`users/${user.userId}/posts/${user.postId}`, '', user.accessToken, user.clientId, user.uid);
  //  console.log("deleteContent Post", json);
    yield put(deletePostSuccess(json));
  } catch (error) {
    console.log("Error", error);
    yield put(deletePostError());
  }
}

export default function* userData() {
  yield takeLatest(POST_USER_ACTION, userActionOnPost);
  yield takeLatest(POST_UNSAVE, unsaveContent);
  yield takeLatest(DELETE_POST, deleteContent);
}
