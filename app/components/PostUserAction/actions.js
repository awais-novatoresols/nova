import { POST_USER_ACTION, USER_ACTION_FAILURE, USER_ACTION_SUCCESS, POST_UNSAVE, POST_UNSAVE_SUCCESS, DELETE_POST, DELETE_POST_SUCCESS, DELETE_POST_ERROR } from "./constants";

export function takeUserAction(userId, postId, body, accessToken, clientId, uid) {
  return {
    type: POST_USER_ACTION,
    userId,
    postId,
    body,
    clientId,
    accessToken,
    uid
  };
}

export function actionSuccess(data) {
  return {
    type: USER_ACTION_SUCCESS,
    data
  };
}

export function actionFailure() {
  return {
    type: USER_ACTION_FAILURE
  };
}

export function unsavePost(postId, formData, accessToken, clientId, uid) {
  return {
    type: POST_UNSAVE,
    postId,
    formData,
    clientId,
    accessToken,
    uid
  };
}

export function unsaveSuccess() {
  return {
    type: POST_UNSAVE_SUCCESS
  };
}

export function deletePost(userId, postId, accessToken, clientId, uid) {
  return {
    type: DELETE_POST,
    userId,
    postId,
    accessToken,
    clientId,
    uid
  };
}

export function deletePostSuccess(data) {
  return {
    type: DELETE_POST_SUCCESS,
    data
  };
}

export function deletePostError() {
  return {
    type: DELETE_POST_ERROR
  };
}
