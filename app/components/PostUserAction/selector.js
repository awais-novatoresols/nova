import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.postAction || initialState;
const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );
const makeSelectAction = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.data
  );
const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.error
  );
const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectDeleteError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.deleteError
  );
const makeSelectDeleteSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.deleteSuccess
  );

const makeSelectPostActionSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.actionPost
  );


  const makeSelectUnsaveSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.unsaveSucess
  );

export { selectGlobal,makeSelectLoading, makeSelectError,makeSelectPostActionSuccess, makeSelectUnsaveSuccess, makeSelectSuccess, makeSelectAction, makeSelectDeleteSuccess, makeSelectDeleteError };

