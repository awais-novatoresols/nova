export const POST_USER_ACTION = "ubwab/postAction/POST_USER_ACTION";
export const USER_ACTION_SUCCESS = "ubwab/postAction/USER_ACTION_SUCCESS";
export const USER_ACTION_FAILURE = "ubwab/postAction/USER_ACTION_FAILURE";
export const POST_UNSAVE = "ubwab/postAction/POST_UNSAVE";
export const POST_UNSAVE_SUCCESS = "ubwab/postAction/POST_UNSAVE_SUCCESS";
export const DELETE_POST = "ubwab/postAction/DELETE_POST";
export const DELETE_POST_SUCCESS = "ubwab/postAction/DELETE_POST_SUCCESS";
export const DELETE_POST_ERROR = "ubwab/postAction/DELETE_POST_ERROR";