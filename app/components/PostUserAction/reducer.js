import produce from "immer";
import {
  DELETE_POST_ERROR,
  DELETE_POST_SUCCESS,
  POST_UNSAVE_SUCCESS,
  POST_USER_ACTION,
  USER_ACTION_FAILURE
} from "./constants";

export const initialState = {
  error: false,
  success: false,
  data: [],
  unsaveSucess: false,
  deleteSuccess: true,
  deleteError: false,
  loading:true,
  actionPost: {},
};
const userActionReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case POST_USER_ACTION:
        draft.error = false;
        draft.success = true;
        draft.loading=false;
        draft.data = action.data;
        break;
      case USER_ACTION_FAILURE:
        draft.error = true;
        draft.success = false;
        draft.loading=false;
        break;
      case POST_UNSAVE_SUCCESS:
        draft.actionPost = action.data;
        draft.unsaveSucess = true;
        draft.loading=false;
        draft.error = false;
        break;
      case DELETE_POST_SUCCESS:
        draft.actionPost = action.data;
        draft.deleteSuccess = false;
        draft.loading=false;
        draft.deleteError = false;
        break;
      case DELETE_POST_ERROR:
        draft.deleteSuccess = false;
        draft.deleteError = true;
        draft.loading=false;
        break;
      default:
        draft.success = false;
        draft.error = false;
        draft.unsaveSucess = false;
    }
  });
export default userActionReducer;
