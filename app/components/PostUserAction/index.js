import PropTypes from "prop-types";
import React, {memo, useEffect, useState} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import {createStructuredSelector} from "reselect";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import PopupModel from "../PopupModel";
import {deletePost, takeUserAction, unsavePost} from "./actions";
import PostOptions from "./PostOptions";
import reducer from "./reducer";
import ReportOption from "./ReportOptions";
import saga from "./Saga";
import {
  makeSelectAction,
  makeSelectDeleteError,
  makeSelectDeleteSuccess,
  makeSelectError,
  makeSelectLoading,
  makeSelectPostActionSuccess,
  makeSelectSuccess,
  makeSelectUnsaveSuccess
} from "./selector";
import UpdatePostForm from "../UpdatePostForm";
import {toast} from "react-toastify";

const key = "postAction";
const PostAction = (props) => {
  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});
  const [showModel, setShowModel] = useState(false);
  const [reason, setReason] = useState("Invalid Content");
  const [editProfileModel, setEditProfileModel] = useState(false);
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  // useEffect(() => {
  //   console.log("unSaveSuccess",props.unSaveSuccess);
  //    props.getLatestPost();
  // }, [props]);

  // useEffect(() => {
  //   if (props.deleteSuccess) props.getLatestPost();
  // }, [props.deleteSuccess]);

  useEffect(() => {
    // console.log("props.postAction...", props.postAction);
    // if (props.postAction.message === "Post deleted successfully") {
    //   toast.error(props.postAction.message);
    //   props.getLatestPost();
    // }

    if (props.deleteError) toast.error("Invalid error occured!");
  }, [props]);

  // console.log("userAction from index......", props.item);
  function userAction(e) {
    if (e.target.id === "report") {
      displayModel();
    } else if (e.target.id === "unsave") {
      let formData = new FormData();
      formData.append("unsave", true);
      props.unsavePost(props.item.saved_content_id, formData, accessToken, clientId, uid);
      props.getLatestPost();
    } else if (e.target.id === "edit") {
      displayModel("", true);
    } else if (e.target.id === "delete") {
      props.deletePost(props.item.user.id, props.item.id, accessToken, clientId, uid);
      toast.success("Post deleted successfully");
      // props.getLatestPost();
    } else if(e.target.id !== '') {
      let formData = new FormData();
      formData.append("post_id", props.item.id);
      formData.append("save", e.target.id === "save");
      formData.append("hide", e.target.id === "hide");
      if (e.target.id === "block") {
        formData.append("block_user_id", props.item.user.id);
      }
      if(e.target.id === "block") {toast.success(`User ${e.target.id} successfully.`)}
      else {toast.success(`Post ${e.target.id} successfully.`)}
      props.userActionOnPost(userId, props.item.id, formData, accessToken, clientId, uid);
      // props.getLatestPost();
    }
    //
  }

  function displayModel(e, isEditProfile = false) {
    isEditProfile ? setEditProfileModel(true) : setShowModel(true);
  }

  function hideModel(e, isEditProfile = false) {
    isEditProfile ? setEditProfileModel(false) : setShowModel(false);
  }

  function reportPost() {
    //  console.log("Post Reported");
    let formData = new FormData();
    formData.append("post_id", props.item.id);
    formData.append("reason", reason);
    formData.append("report", true);
    if (props.item.community && props.item.community.id) {
      formData.append("community_id", props.item.community.id);
    }
    props.userActionOnPost(userId, props.item.id, formData, accessToken, clientId, uid);
    //props.getLatestPost();
    hideModel();
    toast.success("Post reported successfully!");
  }

  function onSelect(e) {
    //  console.log("Selected", e.target.value);
    setReason(e.target.value);
  }

  return <>
    <PostOptions userAction={userAction} item={props.item} isSavedTab={props.isSavedTab} community={props.community} changePinStatus={props.changePinStatus}/>
    <PopupModel display={showModel} hideModel={hideModel}>
      <ReportOption hide={hideModel} report={reportPost} onSelect={onSelect}/>
    </PopupModel>
    <PopupModel display={editProfileModel} hideModel={(e) => hideModel(e, true)} size="lg">
      <UpdatePostForm hide={(e) => hideModel(e, true)} getLatestPost={props.getLatestPost} post={props.item}/>
    </PopupModel>
    {/*{!props.loading && <Spinner/>}*/}
  </>;
};
PostAction.propTypes = {
  postAction: PropTypes.object,
  loading: PropTypes.bool,
  data: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  userActionOnPost: PropTypes.func,
  item: PropTypes.object,
  getLatestPost: PropTypes.func,
  unsavePost: PropTypes.func,
  isSavedTab: PropTypes.bool,
  deletePost: PropTypes.func,
  deleteSuccess: PropTypes.bool,
  makeSelectUnsaveSuccess: PropTypes.bool,
};
const mapStateToProps = createStructuredSelector({
  success: makeSelectSuccess(),
  data: makeSelectAction(),
  error: makeSelectError(),
  deleteSuccess: makeSelectDeleteSuccess(),
  deleteError: makeSelectDeleteError(),
  unSaveSuccess: makeSelectUnsaveSuccess(),
  postAction: makeSelectPostActionSuccess(),
  loading: makeSelectLoading()
});

export function mapDispatchToProps(dispatch) {
  return {
    userActionOnPost: (userId, postId, body, clientId, accessToken, uid) => dispatch(takeUserAction(userId, postId, body, clientId, accessToken, uid)),
    unsavePost: (postId, formData, clientId, accessToken, uid) => dispatch(unsavePost(postId, formData, clientId, accessToken, uid)),
    deletePost: (userId, postId, clientId, accessToken, uid) => dispatch(deletePost(userId, postId, clientId, accessToken, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(PostAction);
