import React from "react";
import {Link} from "react-router-dom";
import {privacyPolicy} from "./PrivacyPolicy";
import {FormattedMessage} from 'react-intl';
import messages from './messages';

const FooterLink = (props) => {
  return (
    <div className="lang card custom_c1 sticky_mode1 mt-4 p-1 mb-5">
      <div className="container">
        <div className="row p-4 cust_r">
          <div className="col-md-6 pl-5 col-6">
            <Link to={{
              pathname: "./infoPage",
              state: "Umar",
              title: "About Page",
              privacyPolicy: privacyPolicy
            }}>
              <h6 className="ancho1"><FormattedMessage {...messages.footerLinkAbout}/></h6>
            </Link>
          </div>
          <div className="col-md-6 col-6">
            <Link to={{
              pathname: "./infoPage",
              title: "Privacy policy",
              privacyPolicy: privacyPolicy
            }}>
              <h6 className="ancho1"><FormattedMessage {...messages.footerLinkPrivacyPolicy}/></h6>
            </Link>
          </div>
          <div className="col-md-6 pl-5 col-6">
            <Link to={{
              pathname: "./infoPage",
              title: "FAQs",
              privacyPolicy: privacyPolicy
            }}>
              <h6 className="ancho1"><FormattedMessage {...messages.footerLinkFAQs}/></h6>
            </Link>
          </div>
          <div className="col-md-6 col-6">
            <Link to={{
              pathname: "./infoPage",
              title: "Content Policy",
              privacyPolicy: privacyPolicy
            }}>
              <h6 className="ancho1"><FormattedMessage {...messages.footerLinkContentPolicy}/></h6>
            </Link>
          </div>
          <div className="col-md-6 pl-5 col-6">
            <Link to={{
              pathname: "./infoPage",
              title: "App",
              privacyPolicy: privacyPolicy
            }}>
              <h6 className="ancho1"><FormattedMessage {...messages.footerLinkApp}/></h6>
            </Link>
          </div>
          <div className="col-md-6 col-6">
            <Link to={{
              pathname: "./infoPage",
              title: "User Agreement",
              privacyPolicy: privacyPolicy
            }}>
              <h6 className="ancho1"><FormattedMessage {...messages.footerUserAgreement}/></h6>
            </Link>
          </div>
        </div>
      </div>
    </div>)
};

export default FooterLink;
