import PropTypes from "prop-types";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import messages from "./messages";
import { FormattedMessage } from "react-intl";

const MainComment = props => {
  const [commentText, setText] = useState("");

  function getCommentValue(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      props.createComment(props.id, commentText, props.item);
    }
    setText(e.target.value);
  }

  function getComments() {
    props.getRepliesComment("", 0);
  }

  function commentDataPostAndReset(id, commentText, item) {
    setText("");
    props.createComment(id, commentText, item);
  }

  return (
    <div className="row bg-white m-0 rounded_1">
      <div className="col-sm-2 col-3 text-center pt-2">
        <a
          onClick={e =>
            props.likeAndDislikeComment(props.id, 1, props.previousStatus, e, 1)
          }
        >
          <img
            className="img-fluid mt-2"
            src={
              props.previousStatus === 1
                ? require("../../images/greenLike.png")
                : require("../../images/arrowup.png")
            }
            width="24px"
            height="24px"
          />
        </a>
        <p className="comment_p m-0">{props.likes < 0 ? 0 : props.likes}</p>
        <a
          onClick={e =>
            props.likeAndDislikeComment(props.id, 2, props.previousStatus, e, 1)
          }
        >
          <img
            className="img-fluid"
            src={
              props.previousStatus === 2
                ? require("../../images/reddislike.png")
                : require("../../images/arrowdown.png")
            }
            width="24px"
            height="24px"
          />
        </a>
      </div>
      <div className="col-sm-10 p-0 col-9">
        <div className="d-flex pt-2">
          <Link to={`/profile/${props.userId}/${props.profileId}`}>
            <img
              src={
                props.profileImage
                  ? props.profileImage
                  : require("../../images/user.png")
              }
              className="rounded-circle m-1"
              height="30px"
              width="30px"
            />
          </Link>
          <h2 className="comment_h">{props.user}</h2>
          <h6 className="comment_hs ml-5 pt-1">{props.time}</h6>
        </div>
        <div className="d-flex">
          <p className="commint mt-0 ml-0 mb-0 mr-5">{props.comment}</p>
          <div className="ml-5">
            {props.isInArray.includes(props.index) ? (
              <div className="pl-5">
                <img
                  src={require("../../images/Icon-feather-plus.png")}
                  className="ml-5"
                  height="17px"
                  width="20px"
                  onClick={() => props.handleCommentHideShow(props.index)}
                />
              </div>
            ) : (
              <div className="pl-5">
                <img
                  src={require("../../images/Icon-feather-plus-1.png")}
                  className="ml-5"
                  height="5px"
                  width="14px"
                  onClick={() => props.handleCommentHideShow(props.index)}
                />
              </div>
            )}
          </div>
        </div>
        <div className="card-footer custom_f1 d-flex justify-content-start p-0 mb-3">
          <a
            href={`#${`demo${props.id}`}`}
            data-toggle="collapse"
            className="remove_a1 custom_cn1 custom_a2"
          >
            <img src={require("../../images/reply.png")} width="20px" />{" "}
            <FormattedMessage {...messages.commentReplyTitle} />
          </a>
          {props.aarLength.length > 4 &&
            props.aarLength.length - 1 === props.index &&
            props.commentPages !== props.pageNo && (
              <h5
                onClick={getComments}
                className="custom_cn1 custom_a3 pl-3 mt-2 ml-3"
              >
                Views More
              </h5>
            )}
          {props.aarLength.length > 5 &&
            props.aarLength.length - 1 === props.index &&
            props.commentPages === props.pageNo && (
              <a
                onClick={() => props.hideComment()}
                className="custom_cn1 custom_a3 pl-3 mt-2 ml-3"
              >
                Hide comments
              </a>
            )}
        </div>
        <div className="card t-4 mt-3">
          <div id={`demo${props.id}`} className="collapse">
            <FormattedMessage {...messages.commentReplyInputPlaceHolder}>
              {placeholder => (
                <input
                  rows="6"
                  maxLength={100}
                  size={100}
                  className="form-control t_a mt-1 mb-5 p-3"
                  type="text"
                  onChange={getCommentValue}
                  onKeyDown={getCommentValue}
                  value={commentText}
                  placeholder={placeholder}
                />
              )}
            </FormattedMessage>
            <button
              type="submit"
              onClick={() =>
                commentDataPostAndReset(props.id, commentText, props.item)
              }
              className="btnSub mt-2"
              href={`#${`demo${props.id}`}`}
              data-toggle="collapse"
            >
              <FormattedMessage {...messages.commentReplyButtonTitle} />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
MainComment.propTypes = {
  commentPages: PropTypes.number,
  pageNo: PropTypes.number,
  aarLength: PropTypes.array,
  id: PropTypes.number,
  index: PropTypes.number,
  likes: PropTypes.number,
  user: PropTypes.string,
  time: PropTypes.string,
  comment: PropTypes.string,
  getRepliesComment: PropTypes.func,
  hideComment: PropTypes.func,
  createComment: PropTypes.func
};
export default MainComment;
