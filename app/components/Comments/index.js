import moment from "moment";
import PropTypes from "prop-types";
import React, { useState } from "react";
import * as _ from 'lodash';
import ChildComment from "./ChildComment";
import MainComment from "./MainComment";

const Comments = React.memo((props) => {
  function getDuration(publishDate) {
    let startDate = new Date();
    let a = moment(startDate);
    let b = moment(publishDate);
    let difference = a.diff(b, "minutes");
    return moment.duration(difference, "minutes").humanize();
  }
  const [isInArray, setIsInArray] = useState([]);

  function handleCommentHideShow(index){
    if(isInArray.includes(index)){
      let stateData = [...isInArray];
      stateData = _.filter(stateData, x => x !== index);
      setIsInArray(stateData);
    } else {
      let stateData = [...isInArray];
      stateData.push(index);
      setIsInArray(stateData);
    }
  }
  return (
    <>
      <div className="card t-4 mt-3">
        <div id="demo" className="collapse">
          <textarea
            rows="6" maxLength={100} className="form-control t_a"
            placeholder="write your comment here"/>
          <button type="submit" className="btnSubmit1 w-25 mt-2">Comment</button>
        </div>
      </div>
      {props.comment && props.comment.length > 0 && props.comment.map((item, index) => {
        return <div key={index}>
          <MainComment
            item={item}
            hideComment={props.hideComment}
            previousStatus={item.vote_status}
            pageNo={props.pageNo}
            parentId={item.root_id}
            userId={item.user && item.user.id}
            profileId={item.user && item.user.profile && item.user.profile.id}
            profileImage={item.user && item.user.profile && item.user.profile.profile_image}
            {...props}
            childCommentLength={item.child_comment && item.child_comment.length}
            id={item.id} aarLength={props.comment} index={index}
            createComment={props.createComment}
            getRepliesComment={props.getRepliesComment}
            user={item.user && item.user.user_name} likes={item.count}
            time={getDuration(item.created_at)} comment={item.comment_text}
            likeAndDislikeComment={props.likeAndDislikeComment}
            handleCommentHideShow={handleCommentHideShow}
            isInArray={isInArray}
          />
          {item.child_comment && item.child_comment.length > 0 && item.child_comment.map((child, uniqueId) => {
            if(!isInArray.includes(index)){
              return <ChildComment
              item={child}
              previousStatus={child.vote_status}
              userId={child.user && child.user.id}
              profileId={child.user && child.user.profile && child.user.profile.id}
              profileImage={child.user && child.user.profile && child.user.profile.profile_image}
              child_comments={item.child_comments}
              thirdLevel={child.thirdLevel && child.thirdLevel}
              hideComment={props.hideComment}
              aarLength={item.child_comment} id={child.id} index={uniqueId}
              pageNo={props.pageNo}
              parentId={item.root_id}
              childCommentLength={child.child_comments}
              key={uniqueId}
              likes={child.count}
              getRepliesComment={props.getRepliesComment}
              user={child.user && child.user.user_name} createComment={props.createComment}
              comment={child.comment_text}
              time={getDuration(child.created_at)}
              likeAndDislikeComment={props.likeAndDislikeComment}
            />;
            }
          })
          }
        </div>;
      })
      }
    </>
  );
});
Comments.propTypes = {
  pageNo: PropTypes.number,
  comment: PropTypes.array,
  hideComment: PropTypes.func,
  getRepliesComment: PropTypes.func,
  createComment: PropTypes.func,
  getDuration: PropTypes.func
};
Comments.displayName = "Comments";
export default Comments;


//