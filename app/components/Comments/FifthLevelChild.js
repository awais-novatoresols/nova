import PropTypes from "prop-types";
import React, {useState} from "react";
import {Link} from "react-router-dom";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const ForthlevelChild = props => {
  const [commentText, setText] = useState("");
  const [state, setState] = useState(true);
  const getRepliesComment = () => {
    setState(!state);
    props.getRepliesComment(props.id, 2)
  };

  function getCommentValue(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      props.createComment(props.id, commentText, props.item);
    }
    setText(e.target.value);
  }

  function commentDataPostAndReset(id, commentText) {
    setText("");
    props.createComment(id, commentText);
  }

  return (
    <div className="row m-0">
      <div className="col-sm-4 col-2"/>
      <div className="col-sm-2 col-3 custom_b text-center pt-2">
        <a onClick={(e) => props.likeAndDislikeComment(props.id, 1, props.previousStatus, e, 5)}>
          <img className="img-fluid mt-3"
               src={props.previousStatus === 1 ? require("../../images/greenLike.png") : require("../../images/arrowup.png")}
               width="24px"/></a>
        <p className="comment_p">{props.likes < 0 ? 0 : props.likes}</p>
        <a onClick={(e) => props.likeAndDislikeComment(props.id, 2, props.previousStatus, e, 5)}>
          <img className="img-fluid mb-4"
               src={props.previousStatus === 2 ? require("../../images/reddislike.png") : require("../../images/arrowdown.png")}
               width="24px"/></a>
      </div>
      <div className="col-sm-6 custom_b col-10">
        <div className="d-flex pt-2">
          <Link to={`/profile/${props.userId}/${props.profileId}`}><img
            src={props.profileImage ?
              props.profileImage : require("../../images/user.png")}
            className="rounded-circle m-1" height="30px" width="30px"/>
          </Link>
          <h2 className="comment_h">{props.user}</h2>
          <h6 className="comment_hs ml-4 pt-1">{props.time}</h6>
        </div>
        <p className="commint">{props.comment}</p>
        <div className="card-footer custom_f1_f d-flex justify-content-start p-0 mb-3">
        </div>
        <div className="row  custom_b">
          <div className="col-sm-10">
            <div className="card t-4 mt-3">
              <div id={`demo${props.id}`} className="collapse">
              <FormattedMessage {...messages.commentReplyInputPlaceHolder}>
              {placeholder=> <input
                  rows="6" maxLength={100} size={100} className="form-control t_a mt-1 mb-5 p-3" type="text"
                  onChange={getCommentValue} onKeyDown={getCommentValue}
                  placeholder={placeholder}/>
              }
              </FormattedMessage>
                <button
                  type="submit" onClick={() => commentDataPostAndReset(props.id, commentText)}
                  className="btnSubmit1 w-25 mt-2"><FormattedMessage {...messages.commentSendButtonTitle}/>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
ForthlevelChild.propTypes = {
  childCommentLength: PropTypes.number,
  commentPages: PropTypes.number,
  likes: PropTypes.number,
  parentId: PropTypes.number,
  index: PropTypes.number,
  forthLevel: PropTypes.array,
  aarLength: PropTypes.array,
  id: PropTypes.number,
  user: PropTypes.string,
  time: PropTypes.string,
  comment: PropTypes.string,
  hideComment: PropTypes.func,
  getRepliesComment: PropTypes.func,
  createComment: PropTypes.func
};
export default ForthlevelChild;
