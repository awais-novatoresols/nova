import moment from "moment";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import ThirdLevelChild from "./ThirdLevelChild";
import { FormattedMessage } from "react-intl";
import messages from "./messages";

const ChildComment = props => {
  const [commentText, setText] = useState("");
  const [state, setState] = useState(true);
  const getRepliesComment = () => {
    setState(!state);
    props.getRepliesComment(props.id, 2);
  };

  function getCommentValue(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      props.createComment(props.id, commentText, props.item);
    }
    setText(e.target.value);
  }

  function getDuration(publishDate) {
    let startDate = new Date();
    let a = moment(startDate);
    let b = moment(publishDate);
    let difference = a.diff(b, "minutes");
    return moment.duration(difference, "minutes").humanize();
  }

  function commentDataPostAndReset(id, commentText, item) {
    setText("");
    props.createComment(id, commentText, item);
  }

  const hideComments = id => {
    setState(!state);
    props.hideComment(id, 2);
  };
  return (
    <>
      <div className="row m-0">
        <div className="col-sm-1" />
        <div className="col-sm-2 col-2 custom_b text-center" style={{height:'120px'}}>
          <a
            onClick={e =>
              props.likeAndDislikeComment(
                props.id,
                1,
                props.previousStatus,
                e,
                2
              )
            }
          >
            <img
              className="img-fluid mt-3"
              src={
                props.previousStatus === 1
                  ? require("../../images/greenLike.png")
                  : require("../../images/arrowup.png")
              }
              width="24px" height="24px"
            />
          </a>
          <p className="comment_p m-0">{props.likes < 0 ? 0 : props.likes}</p>
          <a
            onClick={e =>
              props.likeAndDislikeComment(
                props.id,
                2,
                props.previousStatus,
                e,
                2
              )
            }
          >
            <img
              className="img-fluid mb-4"
              src={
                props.previousStatus === 2
                  ? require("../../images/reddislike.png")
                  : require("../../images/arrowdown.png")
              }
              width="24px" height="24px"
            />
          </a>
        </div>
        <div className="col-sm-9 custom_b col-10">
          <div className="d-flex pt-2">
            <Link to={`/profile/${props.userId}/${props.profileId}`}>
              <img
                src={
                  props.profileImage
                    ? props.profileImage
                    : require("../../images/user.png")
                }
                className="rounded-circle m-1"
                height="30px"
                width="30px"
              />
            </Link>
            <h2 className="comment_h">{props.user}</h2>
            <h6 className="comment_hs ml-4 pt-1">{props.time}</h6>
          </div>
          <p className="commint m-0">{props.comment}</p>
          {/* <div className="card-footer custom_f1_f d-flex justify-content-start p-0 mb-3">
            <a
              href={`#${`demo${props.id}`}`}
              data-toggle="collapse"
              className="remove_a1 custom_cn1 custom_a2"
            >
              <img src={require("../../images/reply.png")} width="20px" />
              <FormattedMessage {...messages.commentReplyTitle} />
            </a>
            {props.childCommentLength !== 0 && state && (
              <a
                onClick={getRepliesComment}
                className="remove_a1 custom_cn1 custom_a3 pl-3 mt-2 ml-3"
              >
                <FormattedMessage {...messages.commentViewReplies} />
              </a>
            )}
            {props.ChildComment > 4 && state && (
              <a
                onClick={() => props.getRepliesComment(props.parentId, 6)}
                className="remove_a1 custom_cn1 custom_a3 pl-3 mt-2 ml-3"
              >
                <FormattedMessage {...messages.commentViewMore} />
              </a>
            )}
            {props.thirdLevel && props.thirdLevel.length > 0 && !state && (
              <a
                onClick={() => hideComments(props.id)}
                className="custom_cn1 custom_a3 pl-3 mt-2 ml-3"
              >
                <FormattedMessage {...messages.commentHideComments} />
              </a>
            )}
          </div> */}
          {/* <div className="row  custom_b">
            <div className="col-sm-10">
              <div className="card t-4 mt-3">
                <div id={`demo${props.id}`} className="collapse">
                  <input
                    rows="6"
                    maxLength={100}
                    size={100}
                    className="form-control t_a mt-1 mb-5 p-3"
                    type="text"
                    onChange={getCommentValue}
                    onKeyDown={getCommentValue}
                    value={commentText}
                    placeholder="write your comment here"
                  />
                  <button
                    href={`#${`demo${props.id}`}`}
                    data-toggle="collapse"
                    type="submit"
                    onClick={() =>
                      commentDataPostAndReset(props.id, commentText, props.item)
                    }
                    className="btnSubmit1 w-25 mt-2"
                  >
                    <FormattedMessage {...messages.commentMainButtonTitle} />
                  </button>
                </div>
              </div>
            </div>
          </div> */}
        </div>
      </div>
      {/* {props.thirdLevel && props.thirdLevel.length > 0 && props.thirdLevel.map((child, index) => {
        return <ThirdLevelChild
          item={child}
          previousStatus={child.vote_status}
          userId={child.user && child.user.id}
          profileId={child.user && child.user.profile && child.user.profile.id}
          profileImage={child.user && child.user.profile && child.user.profile.profile_image}
          forthLevel={child.forthLevel && child.forthLevel}
          hideComment={props.hideComment}
          // aarLength={item.child_comment}
          id={child.id}
          index={index}
          // pageNo={props.pageNo}
          //  parentId={item.root_id}
          childCommentLength={child.child_comments}
          key={index}
          likes={child.count}
          getRepliesComment={props.getRepliesComment}
          user={child.user && child.user.user_name} createComment={props.createComment}
          comment={child.comment_text}
          time={getDuration(child.created_at)}
          likeAndDislikeComment={props.likeAndDislikeComment}
        />;
      })
      } */}
    </>
  );
};
ChildComment.propTypes = {
  childCommentLength: PropTypes.number,
  commentPages: PropTypes.number,
  likes: PropTypes.number,
  parentId: PropTypes.number,
  index: PropTypes.number,
  thirdLevel: PropTypes.array,
  aarLength: PropTypes.array,
  id: PropTypes.number,
  user: PropTypes.string,
  time: PropTypes.string,
  comment: PropTypes.string,
  hideComment: PropTypes.func,
  getRepliesComment: PropTypes.func,
  createComment: PropTypes.func
};
export default ChildComment;
