import { defineMessages } from 'react-intl';

export default defineMessages({
    
    commentMainButtonTitle: {
        id: 'comment-main-button-title-send',
        defaultMessage: 'Send',
    },
    commentReplyTitle: {
        id: 'comment-reply-title',
        defaultMessage: 'Reply',
    },
    commentReplyInputPlaceHolder: {
        id: 'comment-reply-input-place-holder',
        defaultMessage: 'write tour comment here',
    },
    commentReplyButtonTitle: {
        id: 'comment-reply-comment-button-title',
        defaultMessage: 'Comment',
    },
    commentViewReplies: {
        id: 'comment-view-replies',
        defaultMessage: 'View Replies',
    },
    commentViewMore: {
        id: 'comment-view-more',
        defaultMessage: 'View Replies',
    },
    commentHideComments: {
        id: 'comment-hide-comments',
        defaultMessage: 'Hide Comments',
    },
    headerlinkNewsFeeds: {
        id: 'header-link-news-feed',
        defaultMessage: 'News Feed',
      },
      headerlinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Communities',
      },
      headerlinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
      },
    

});