import React from "react";
import "w3-css/4/w3pro.css";
import "../containers/SignIn/style.css";
import "bootstrap/dist/css/bootstrap.css";
import door from "../images/door.png";

export default function SideImage(props) {
  return <div className="col-sm-6 p-0">
    <div className="sidenav">
      <div className="login-main-text">
        <h3>{props.title}</h3>
        <h5>{props.body}</h5>
        <h6>{props.end}</h6>
        <img src={door} className="mt-4"/>
      </div>
    </div>
  </div>;
}
