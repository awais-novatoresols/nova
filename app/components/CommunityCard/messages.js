import { defineMessages } from "react-intl";

export default defineMessages({
  communityCardHeaderTextTrending: {
    id: "community-card-header-text-trending-communities",
    defaultMessage: "Trending Communities"
  },
  communityCardFooterTextSeeMore: {
    id: "community-card-footer-text-see-more",
    defaultMessage: "See More"
  },
  headerlinkNewsFeeds: {
    id: "header-link-news-feed",
    defaultMessage: "News Feed"
  },
  headerlinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Communities"
  },
  headerlinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  }
});