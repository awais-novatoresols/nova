import React from "react";
import style from "styled-components";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Container = style.div`
    position: relative !important;
    background-color: black !important;
	  border: none !important;
	  border-radius: 12px 12px 12px 12px !important;
    width: 100%;
    height: 120px;
    margin-bottom: 10px;
`;

const ImageContainer = style.img`
  border: none !important;
  border-radius: 12px 12px 12px 12px !important;
  width: 100%;
  height: 100%;
`;

const Holder = style.div`
    position: absolute;
    background-color: #cccccc70;
    border-radius: 0px 0px 12px 12px !important;
    width: 100%;
    height: auto;
    padding-top: 5%;
    bottom: 0px;
    left: 0;
`;

const TextContainer = style.p`
    font-weight: 400;
    font-size: 18px;
    color: white;
    text-align: center;
`;

const CommunityItem = props => {
  return (
    <div className="col-sm-6 col-6 mb-3">
      <Link to={`/communities/view/${props.id}`}>
      <Container>
        <ImageContainer src={props.img} />
        <Holder><TextContainer>{props.text}</TextContainer></Holder>
      </Container>
      </Link>
    </div>
  );
};
CommunityItem.propTypes = {
  id: PropTypes.number,
  img: PropTypes.string,
  text: PropTypes.string
};

export default CommunityItem;
