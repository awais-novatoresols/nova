import React from "react";
import CommunityItem from "./CommunityItem";
import style from "styled-components";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import messages from "./messages";
import { FormattedMessage } from "react-intl";

const TextContainer = style.p`
    font-weight: 500;
    font-size: 14px;
    color: #00A6AA;
    text-align: right;
    margin-right:10px
`;

const CommunityList = props => {
  return (
    <>
      <div className="lang card custom_c1 sticky_mode mt-4 p-1 shadow mt-3 mb-3">
        <div className="header p-0">
          <h2 className="p-3">
            <FormattedMessage {...messages.communityCardHeaderTextTrending} />
          </h2>
        </div>
        <div className="row pl-2 pr-2">
          {props.data &&
            props.data &&
            props.data.length > 0 &&
            props.data.slice(0, 4).map((item, index) => {
              return (
                <CommunityItem
                  getAllCommunities={props.getAllCommunities}
                  key={index}
                  id={item.id}
                  img={item.profile_image}
                  text={item.name}
                />
              );
            })}
        </div>
        <Link to="/communities">
          <TextContainer>
            <FormattedMessage {...messages.communityCardFooterTextSeeMore} />
          </TextContainer>
        </Link>
      </div>
    </>
  );
};

CommunityList.propTypes = {
  data: PropTypes.oneOfType([PropTypes.array, PropTypes.bool])
};
export default CommunityList;
