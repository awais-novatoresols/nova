import produce from "immer";
import {
  GET_ALL_COMMUNITIES_SUCCESS,
  GET_TRENDING_COMMUNITIES,
  LOAD_COMMUNITIES_SUCCESSFULLY,
  LOAD_ERROR
} from "./constants";

// The initial state of the App
export const initialState = {
  loading: true,
  error: false,
  userPostCommunities: {
    post: false
  },
  allCommunities: {}
};

/* eslint-disable default-case, no-param-reassign */

const getCommunityReducer = (state = initialState, action) =>

  produce(state, draft => {
    switch (action.type) {
      case GET_TRENDING_COMMUNITIES:
        state.userPostCommunities.post = true;
        draft.loading = true;
        draft.error = false;
        break;
      case LOAD_COMMUNITIES_SUCCESSFULLY:
        state.userPostCommunities.post = action.post;
        draft.loading = false;
        draft.error = false;
        break;
      case LOAD_ERROR:
        draft.error = action.error;
        draft.loading = false;
        break;
      case GET_ALL_COMMUNITIES_SUCCESS:
        draft.allCommunities = action.post;
        draft.loading = false;
        break;
    }
    // console.log(' state.userPost.post.......',state.userPost.post);
  });

export default getCommunityReducer;


