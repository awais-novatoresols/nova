import { GET_TRENDING_COMMUNITIES, LOAD_COMMUNITIES_SUCCESSFULLY, GET_ALL_COMMUNITIES, GET_ALL_COMMUNITIES_SUCCESS } from "./constants";


export function getTrendingCommunities(userId, clientId, accessToken, uid) {
  return {
    type: GET_TRENDING_COMMUNITIES,
    userId,
    clientId,
    accessToken,
    uid
  };
}

export function getAllCommunities(userId, clientId, accessToken, uid) {
  return {
    type: GET_ALL_COMMUNITIES,
    userId,
    clientId,
    accessToken,
    uid
  };
}


export function communitiesLoaded(post) {
  // console.log('communitiesLoaded.......',post);
  return {
    type: LOAD_COMMUNITIES_SUCCESSFULLY,
    post
  };
}

export function allCommunitiesLoaded(post) {
  // console.log('communitiesLoaded.......',post);
  return {
    type: GET_ALL_COMMUNITIES_SUCCESS,
    post
  };
}
