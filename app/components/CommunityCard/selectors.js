import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.communityCard || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectUserData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userData
  );

const makeSelectCommunities = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userPostCommunities
  );

const makeSelectAllUserCommunities = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.allCommunities
  );
export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectUserData,
  makeSelectCommunities,
  makeSelectAllUserCommunities
};
