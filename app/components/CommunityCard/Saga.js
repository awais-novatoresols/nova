import { put, takeLatest } from "redux-saga/effects";
import { apiFetch } from "../../utils/network";
import { communitiesLoaded, allCommunitiesLoaded } from "./actions";
import { GET_TRENDING_COMMUNITIES, GET_ALL_COMMUNITIES } from "./constants";

export function* getCommunities(userId) {
  let jsonData = true;
  try {
    const json = yield apiFetch(`users/${userId.userId}/communities?trending=true`, userId.accessToken, userId.clientId, userId.uid, jsonData);
    // console.log("getCommunities ...", json);
    yield put(communitiesLoaded(json));
  } catch (error) {
    // console.log(error);
  }
}

export function* getJoinCommunities(userId) {
  let jsonData = true;
  try {
    const json = yield apiFetch(`users/${userId.userId}/communities`, userId.accessToken, userId.clientId, userId.uid, jsonData);
    // console.log("getCommunities ...", json);
    yield put(allCommunitiesLoaded(json));
  } catch (error) {
    // console.log(error);
  }
}

export default function* communitiesData() {
  yield takeLatest(GET_TRENDING_COMMUNITIES, getCommunities);
  yield takeLatest(GET_ALL_COMMUNITIES, getJoinCommunities);
}
