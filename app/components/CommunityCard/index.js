import React, {memo, useEffect, useState} from "react";
import CommunityList from "./CommunityList";
import "w3-css/4/w3pro.css";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import {connect} from "react-redux";
import {compose} from "redux";
import {getAllCommunities, getTrendingCommunities} from "./actions";
import {createStructuredSelector} from "reselect";
import {makeSelectAllUserCommunities, makeSelectCommunities, makeSelectLoading} from "./selectors";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import reducer from "./reducer";
import saga from "./Saga";
import PropTypes from "prop-types";

const key = "communityCard";

export function CommunityCard(props) {
  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});
  const [notificationCount, setNotificationCount] = useState(0);
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  // console.log("props.data...",props.data);
  useEffect(() => {
    //   console.log("Function..............", props.data.post.success);
    if (!props.data.post.success) props.getTrendingCommunities(userId, clientId, accessToken, uid);

    if (props.data.post.success) {
      props.getAllCommunities(userId, clientId, accessToken, uid);
    }
  }, [props.data.post.success]);
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName("lang");
      for (let i = 0; i < totalElements.length; i++) {
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);

  return (
    <> {props.data && <CommunityList data={props.data && props.data.post && props.data.post.communities}/>}</>
  );
}

CommunityCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  history: PropTypes.object,
  getTrendingCommunities: PropTypes.func,
  getAllCommunities: PropTypes.func,
  allUserCommunities: PropTypes.object,
  createUserPost: PropTypes.func,

};
const mapStateToProps = createStructuredSelector({
  data: makeSelectCommunities(),
  loading: makeSelectLoading(),
  allUserCommunities: makeSelectAllUserCommunities()
});

export function mapDispatchToProps(dispatch) {
  return {
    getTrendingCommunities: (userId, clientId, accessToken, uid) => dispatch(getTrendingCommunities(userId, clientId, accessToken, uid)),
    getAllCommunities: (userId, clientId, accessToken, uid) => dispatch(getAllCommunities(userId, clientId, accessToken, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(CommunityCard);
