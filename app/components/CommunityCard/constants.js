export const GET_TRENDING_COMMUNITIES = "ubwab/CommunityCard/GET_TRENDING_COMMUNITIES";
export const LOAD_COMMUNITIES_SUCCESSFULLY = "ubwab/CommunityCard/LOAD_COMMUNITIES_SUCCESSFULLY";
export const LOAD_ERROR = "ubwab/CommunityCard/LOAD_ERROR";
export const GET_ALL_COMMUNITIES = "ubwab/CommunityCard/GET_ALL_COMMUNITIES";
export const GET_ALL_COMMUNITIES_SUCCESS = "ubwab/CommunityCard/GET_ALL_COMMUNITIES_SUCCESS";
