import React from "react";
import Loader from "react-loader-spinner";
import { Modal } from "react-bootstrap";

const Spinner = (props) => {
  return (
    <Modal
      size='sm'
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={true}
      onHide={() => { }}>
      <Loader
        type="Puff"
        color="#00A6AA"
        height={100}
        width={100}
        visible={true}
        className="m-3 mr-auto ml-auto"
      />
      <p className="lead text-center">{props.uploading ? 'Uploading ...' : 'Processing ...'}</p>
    </Modal>
  );
}

export default Spinner;