import React, {memo, useState, useEffect} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import { getUserNotifications } from "./Search/actions";
import {ActionCableConsumer} from "react-actioncable-provider";
import { apiFetch } from "../utils/network";

/**
 * @return {boolean}
 */
function NotificationImage(props) {
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  const [notificationCount, SetNotificationCount] = useState(0);
  useEffect(() => {
    console.log(notificationCount);
  }, [notificationCount])
  function handleReceived(count) {
    props.getNotifications(userId, accessToken, clientId, uid);
    SetNotificationCount(count.data.unread_notifcations);
  }

  function onConnected() {
    console.log("NotificationChannel onConnected")
  }

  function onDisconnected() {
    console.log("NotificationChannel onDisconnected ")
  }

  function onRejected() {
    console.log(" NotificationChannelonRejected ")
  }

  async function ResetCount () {
    SetNotificationCount(0);
    try{
      await apiFetch(`users/${userId}/notifications`, accessToken, clientId, uid);
    } catch (err) {
      console.log("Error")
    }
  }

  return (
    <ActionCableConsumer
      channel="NotificationChannel"
      onConnected={onConnected}
      onReceived={handleReceived}
      onDisconnected={onDisconnected}
      onRejected={onRejected}
    >
      <>
        <img onClick={ResetCount} className="user_img  dropdown-toggle small_screen_resp  cursor_pointer_class header_icons" data-toggle="dropdown"
             src={require("../images/notification.png")} 
             width="28px" height="28px"/>
        {
          notificationCount > 0 && 
          <sup data-toggle="dropdown" className="cart mt-1"><span
          className="badge badge-danger badge-pill">{notificationCount}</span></sup>
        }
      </>
    </ActionCableConsumer>
  )

}

export function mapDispatchToProps(dispatch) {
  return {
    getNotifications: (userId, accessToken, clientId, uid) => dispatch(getUserNotifications(userId, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(NotificationImage);
