import PropTypes from "prop-types";
import React from "react";
import { FormattedMessage } from "react-intl";
import "../assets/css/navbar.css";
import messages from "./messages";
import Search from "./Search";

function Header(props) {
  let localLang = localStorage.getItem("language");
  function onNavClick(path) {
    props.history.push(path);
    props.postRemoved();
  }

  function handleLogoClick() {
    if (
      props.location.pathname === "/" ||
      props.location.pathname.toLowerCase() === "/homepage"
    ) {
      window.location.reload();
      // props.getLatestPost();
      // props.setSearchFinished(true);
      // props.showSearchResult()
    } else {
      props.history.push("/homePage");
    }
  }

  return (
    <>
      <div className="row nav_style pb-1 pt-2">
        <div className="container">
          <div className="d-flex">
            <div onClick={handleLogoClick}>
              <img
                className="screen_w cursor_pointer_class"
                src={require("../images/logowhite.png")}
                height="65px"
              />
              <img
                className="screen2 ml-2 cursor_pointer_class"
                src={require("../images/home.png")}
                height="25px"
              />
            </div>
            <Search
              postRemoved={props.postRemoved}
              searchCommunities={props.searchCommunities}
              isSearch={props.isSearch}
              showSearchResult={props.showSearchResult}
              {...props}
            />
          </div>
          {(props.location.pathname === "/" ||
            props.location.pathname.toLowerCase() === "/homepage") && (
            <div className="searchbar input-group bg-white rounded show-below-768">
              <input
                type="text"
                className="form-control border-0 shadow-none"
                placeholder="Search"
              />
              {/* <select className="sel1 bg-whtie cursor_pointer_class" name="sellist1">
                <option className="text-center cursor_pointer_class" >All</option>
                <option className="cursor_pointer_class" >Post</option>
                <option className="cursor_pointer_class">Communities</option>
                <option >User name</option>
              </select> */}
              <button type="button" className="btn bg-white shadow-none">
                <i className="fa fa-search" />
              </button>
            </div>
          )}
        </div>
      </div>
      <div className="row bg-white">
        <div className="col">
          <div className="d-flex justify-content-around ">
            <div className="lang row custom_margin">
              {props.nav &&
                props.nav.map(nav => {
                  if (nav.path) {
                    return props.location.pathname.toLowerCase() ===
                      nav.path.toLowerCase() ? (
                      <div key={nav.id} className="ml-5">
                        <a
                          className="alpha cursor_pointer_class"
                          onClick={() => onNavClick(nav.path.toLowerCase())}
                        >
                          <h5 className="capitalize">{nav.name}</h5>
                        </a>
                        <div className="m-md-n1 underline_nav" />
                      </div>
                    ) : (
                      <div key={nav.id} className="ml-5">
                        <a
                          className="alpha cursor_pointer_class"
                          onClick={() => onNavClick(nav.path.toLowerCase())}
                        >
                          <h5 className="capitalize">{nav.name}</h5>
                        </a>
                      </div>
                    );
                  }
                  if (nav.action) {
                    return props.selectedTab === nav.id ? (
                      <div key={nav.id} className="ml-5">
                        <a
                          className="alpha cursor_pointer_class"
                          onClick={nav.action}
                        >
                          <h5 id={nav.id} className="capitalize">
                            {nav.name}
                          </h5>
                        </a>
                        <div className="m-md-n1 underline_nav" />
                      </div>
                    ) : (
                      <div key={nav.id} className="ml-5">
                        <a
                          className="alpha cursor_pointer_class"
                          onClick={nav.action}
                        >
                          <h5 id={nav.id} className="capitalize">
                            {nav.name}
                          </h5>
                        </a>
                        <div className="m-md-n1 underline_nav" />
                      </div>
                    );
                  }
                })}
            </div>
            {props.location.pathname.toLowerCase() !== "/chat" && (
              <div className="dropdown">
                <button
                  type="button"
                  className="custom_b1 btn dropdown-toggle"
                  data-toggle="dropdown"
                >
                  <FormattedMessage
                    {...messages.headerLanguageDropDownHeading}
                  />
                </button>
                <div className="dropdown-menu w-75">
                  <p className="custom_p m-2 pb-1 pl-2">
                    <FormattedMessage
                      {...messages.headerLanguageDropDownHeading}
                    />
                  </p>
                  <span
                    id="new"
                    className="dropdown-item mt-2 pl-3 cursor_pointer_class"
                    onClick={() => handleLanguageChange("en")}
                  >
                    <FormattedMessage {...messages.headerLanguageDropDownEn} />
                    {localLang === "en" && (
                      <img
                        className="selected_Image float-right pt-1"
                        src={require("../images/clicktick.png")}
                      />
                    )}
                  </span>
                  <span
                    id="top"
                    className="dropdown-item mt-2 pl-3 cursor_pointer_class"
                    onClick={() => handleLanguageChange("ar")}
                  >
                    <FormattedMessage {...messages.headerLanguageDropDownAr} />
                    {localLang === "ar" && (
                      <img
                        className="selected_Image float-right pt-1"
                        src={require("../images/clicktick.png")}
                      />
                    )}
                  </span>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
  function handleLanguageChange(locale) {
    localStorage.setItem("language", locale);
    window.location.reload();
  }
}

Header.defaultProps = {
  postRemoved: function() {}
};

Header.propTypes = {
  openSideBar: PropTypes.func,
  postRemoved: PropTypes.func,
  selectedTab: PropTypes.string,
  searchCommunities: PropTypes.func,
  showSearchResult: PropTypes.func,
  history: PropTypes.object,
  location: PropTypes.object,
  nav: PropTypes.array,
  isSearch: PropTypes.bool,
  onNavClick: PropTypes.func
};
export default Header;
