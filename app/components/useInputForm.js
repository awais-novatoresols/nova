import { useState } from "react";

const useInputForm = (initialValues) => {
  const [inputs, setInputs] = useState(initialValues);
  const handleSubmit = (event) => {
    if (event) event.preventDefault();
  };
  const handleInputChange = (event) => {
    // console.log("form value....", event.target.value);
    event.persist();
    setInputs(inputs => ({ ...inputs, [event.target.name]: event.target.value }));
  };
  return {
    handleSubmit,
    handleInputChange,
    inputs
  };
};
export default useInputForm;
