import produce from "immer";
import { UPDATE_POST_SUCCESS, UPDATE_POST_ERROR } from "./constants";

// The initial state of the App
export const initialState = {
  success: false,
  error: false
};

/* eslint-disable default-case, no-param-reassign */
const updatePostReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
    case UPDATE_POST_SUCCESS:
      draft.success = true;
      draft.error = false;
      break;
    case UPDATE_POST_ERROR:
      draft.error = true;
      draft.success = false;
      break;
    default:
      draft.success = false,
      draft.error = false
    }
  });

export default updatePostReducer;