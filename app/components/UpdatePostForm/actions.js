import { UPDATE_POST, UPDATE_POST_SUCCESS, UPDATE_POST_ERROR } from "./constants";


export function updateUserPost(userId, postId, postData, accessToken, clientId, uid) {
  return {
    type: UPDATE_POST,
    userId, 
    postId,
    postData,
    clientId,
    accessToken,
    uid
  };
}


export function updatePostSucess() {
  return {
    type: UPDATE_POST_SUCCESS
  };
}

export function updatePostError() {
  return {
    type: UPDATE_POST_ERROR
  };
}