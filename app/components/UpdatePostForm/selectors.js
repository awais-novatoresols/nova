import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.updatePost || initialState;

const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.error
  );

export { selectGlobal, makeSelectSuccess, makeSelectError };

