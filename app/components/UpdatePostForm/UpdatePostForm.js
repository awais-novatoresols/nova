import React from "react";
import PropTypes from "prop-types";
import { ProgressBar } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import messages from "./messages";

function UpdatePostForm(props) {
  let localLanguage = localStorage.getItem('language');
  return (
    <>
      <div className="lang modal-content">
        <div className="modal-header">
          <h6 className="pt-2 text_b text_b1"><FormattedMessage {...messages.postModalUpdateHeader} /></h6>
          <button type="button" className="close mt-2 text_b3" onClick={props.hide}>&times;</button>
        </div>
        {props.post.community && props.post.community.name && <div className="modal-body d-flex">
          <h6 className="mt-1 text_b4 ml-2"><FormattedMessage {...messages.postModalCommunity} /> {props.post.community.name}</h6>
        </div>}
        <div className="col-sm-12">
          <textarea onChange={props.onInputChange} rows="3" className="form-control text_b6" value={props.title} />
        </div>
        <div className="row m-1 mt-3">
          <div className="col-sm-3 col-6">
            <span name="link"
              className={"btn btn-default w-100 but_pad pd_1" + (props.post.post_type === "link" ? " bg-info text-white" : "")}>
              <span className="fa fa-link mr-2" />{'\u00A0'}<FormattedMessage {...messages.postModalLink} />
            </span>
          </div>
          <div className="col-sm-3 col-6">
            <div className="form-group w-100 but_pad pd_1 text-center">
              <div className="input-group">
                <span className="input-group-btn w-100">
                  <span
                    className={"btn btn-default w-100 btn-file" + (props.post.post_type === "image" ? " bg-info text-white" : "")}>
                    <span className="fa fa-picture-o" />{'\u00A0'}
                    <span className="ml-2"><FormattedMessage {...messages.postModalImage} /></span>
                  </span>
                </span>
              </div>
            </div>
          </div>
          <div className="col-sm-3 col-6">
            <div className="form-group w-100 but_pad pd_1 text-center">
              <div className="input-group">
                <span className="input-group-btn w-100">
                  <span
                    className={"btn btn-default w-100 btn-file" + (props.post.post_type === "video" ? " bg-info text-white" : "")}>
                    <span className="fa fa-picture-o" />{'\u00A0'}
                    <span className="ml-2"><FormattedMessage {...messages.postModalVideo} /></span>
                  </span>
                </span>
              </div>
            </div>
          </div>
          <div className="col-sm-3 col-6">
            <span name="text"
              className={"btn btn-default w-100 custmb pd_1" + (props.post.post_type === "text" ? " bg-info text-white" : "")}>
              <span className="fa fa-font mr-2" />{'\u00A0'}<FormattedMessage {...messages.postModalText} /> 
            </span>
          </div>
        </div>
        <div className="container-fluid">
          {props.post.post_type === "image" && props.post.link && <div className="row">
            <div className="col-sm-12">
              <h6 className="ml-1 pt-2 text_b8 ml-2"><FormattedMessage {...messages.postModalImageAttached} /></h6>
            </div>
            <div className="col-sm-3 text-left ">
              <img src={props.post.link} className="r_img" width="140px;"
                height="70px" />
            </div>
          </div>}
          {
            props.post.poll_options.length !== 0 && <div className="pl-4">
              <div className="mt-4">
                <div className="row mt-5">
                  <div className="col-sm-12">
                    <div className="d-flex">
                      <img src={require("../../images/poll.png")} width="30px" />
                    </div>
                  </div>
                </div>
                {
                  props.post.poll_options.map((data, index) => {
                    return data.title !== "" && <div key={index} className="row mt-3">
                      <div className="col-sm-5 col-md-5 col-6">
                        <div className="custom-control custom-checkbox">
                          <input
                            className="bg-light"
                            id={data.id} type="radio" name="radio" disabled />
                          <label
                            id={data.id}
                            className="pl-3">{data.title}</label>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-6 col-5 mt-1">
                        <ProgressBar variant="success" now={data.count} />
                      </div>
                      <div className="col-sm-1 col-md-1 col-1" />
                    </div>;
                  })
                }
              </div>
            </div>
          }
          <div className="row mt-3">
            <div className="col-sm-12">
              <button onClick={props.onSubmit} data-dismiss="modal" type="submit"
                className={`${localLanguage === 'ar' ? 'float-md-left' : 'float-md-right'} btnsubmit p-2 mb-3 w-25`}>
                <FormattedMessage {...messages.postModalUpdate} />
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

UpdatePostForm.propTypes = {
  onSubmit: PropTypes.func,
  onInputChange: PropTypes.func,
  hide: PropTypes.func,
  post: PropTypes.object,
  title: PropTypes.string
};

export default UpdatePostForm;