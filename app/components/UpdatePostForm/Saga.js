import { put, takeLatest } from "redux-saga/effects";
import { apiFetchPut } from "../../utils/network";
import { updatePostSucess, updatePostError } from "./actions";
import { UPDATE_POST } from "./constants";

export function* updatePost(userId) {
  let jsonData = true;
  try {
    const json = yield apiFetchPut(`users/${userId.userId}/posts/${userId.postId}`, userId.postData, userId.accessToken, userId.clientId, userId.uid, jsonData);
    yield put(updatePostSucess(json));
  } catch (error) {
    console.log(error);
    updatePostError();
  }
}

export default function* updatePostData() {
  yield takeLatest(UPDATE_POST, updatePost);
}
