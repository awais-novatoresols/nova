import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import "w3-css/4/w3pro.css";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { updateUserPost } from "./actions";
import reducer from "./reducer";
import saga from "./Saga";
import { makeSelectSuccess, makeSelectError } from "./selectors";
import UpdatePostForm from "./UpdatePostForm";
import { toast } from "react-toastify";

const key = "updatePost";

export function CreatePost(props) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [title, setTitle] = useState("");
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);
  useEffect(() => {
    setTitle(props.post.title);
  }, []);

  useEffect(() => {
    if (props.error) toast.error("Invalid error occured!!");
  }, [props.error]);

  useEffect(() => {
    if (props.success) {
      toast.success("Post Updated!!");
      props.hide();
    }
  }, [props.success]);

  function updatePost() {
    if (title) {
      let postId = props.post.id;
      let formData = {
        title: title
      }
      props.updatePost(userId, postId, formData, accessToken, clientId, uid);
      props.getLatestPost();
    } else {
      alert("Please fill all field");
    }
  }

  const onInputChange = (e) => {
    setTitle(e.target.value);
  };
  return <UpdatePostForm title={title} onSubmit={updatePost} onInputChange={onInputChange} hide={props.hide} post={props.post} />;
}

CreatePost.propTypes = {
  success: PropTypes.bool,
  error: PropTypes.bool,
  updatePost: PropTypes.func,
  getLatestPost: PropTypes.func,
  hide: PropTypes.func,
  post: PropTypes.object
};
const mapStateToProps = createStructuredSelector({
  success: makeSelectSuccess(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    updatePost: (userId, postId, postData, accessToken, clientId, uid) => dispatch(updateUserPost(userId, postId, postData, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(CreatePost);
