import { defineMessages } from 'react-intl';

export default defineMessages({
  postModalUpdateHeader: {
    id: 'post-modal-update-header',
    defaultMessage: 'Update a Post',
  },
  postModalCommunity: {
    id: 'post-modal-community',
    defaultMessage: 'Community',
  },
  postModalChoose: {
    id: 'post-modal-choose',
    defaultMessage: 'Choose',
  },
  postModalAddTitle: {
    id: 'post-modal-title',
    defaultMessage: 'Add tittle here',
  },
  postModalAddDescription: {
    id: 'post-modal-description',
    defaultMessage: 'Add Description here',
  },
  postModalLink: {
    id: 'post-modal-link',
    defaultMessage: 'Link',
  },
  postModalImage: {
    id: 'post-modal-image',
    defaultMessage: 'Image',
  },
  postModalVideo: {
    id: 'post-modal-video',
    defaultMessage: 'Video',
  },
  postModalText: {
    id: 'post-modal-text',
    defaultMessage: 'Text',
  },
  postModalImageAttached: {
    id: 'post-modal-attached-Image',
    defaultMessage: 'Attached Image'
  },
  postModalVideoUploaded: {
    id: 'post-modal-video-uploaded',
    defaultMessage: 'Video Uploaded!!'
  },
  postModalAddPoll: {
    id: 'post-modal-add-poll',
    defaultMessage: 'Add Poll'
  },
  postModalGonnaVote: {
    id: 'post-modal-gonna-vote',
    defaultMessage: "I'm gonna vote",
  },
  postModalDeletePoll: {
    id: 'post-modal-delete-poll',
    defaultMessage: "Delete Poll",
  },
  postModalPost: {
    id: 'post-modal-post',
    defaultMessage: "Post",
  },
  postModalUpdate: {
    id: 'post-modal-update',
    defaultMessage: "Update",
  },
  headerlinkNewsFeeds: {
    id: 'header-link-news-feed',
    defaultMessage: 'Communities',
  },
  headerlinkCommunities: {
    id: 'header-link-communities',
    defaultMessage: 'Communities',
  },
  headerlinkProfile: {
    id: 'header-link-profile',
    defaultMessage: 'Profile',
  },
});