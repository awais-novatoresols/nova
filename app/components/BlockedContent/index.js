import React, {memo, useEffect, useState} from "react";
import {useInjectReducer} from "../../utils/injectReducer";
import {useInjectSaga} from "../../utils/injectSaga";
import reducer from "./reducer";
import saga from "./Saga";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {compose} from "redux";
import {createStructuredSelector} from "reselect";
import {toast} from "react-toastify";
import {
  makeSelectBlockedContent,
  makeSelectLoading,
  makeSelectPendingRequestSuccess,
  makeSelectSuccess
} from "./selectors";
import {
  getData,
  getFollowingStatus,
  getFollowingUser,
  getFollowUser,
  getPendingRequest,
  removeFollowerStatus,
  setFollowAndUnFollowStatus,
  unblockContent
} from "./actions";
import Header from "../Header";
import Spinner from "../Spinner";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const key = "blockedContent";

const Menu = [
  {id: 1, name: <FormattedMessage {...messages.headerlinkNewsFeeds}/>, path: "/homepage"},
  {id: 3, name: <FormattedMessage {...messages.headerlinkGeneral}/>, path: "/general"},
  {id: 2, name: <FormattedMessage {...messages.headerlinkCommunities}/>, path: "/communities"}
];

export function BlockedContent(props) {
  useInjectReducer({key, reducer});
  useInjectSaga({key, saga});

  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  const [buttonStatus, SetButtonStatus] = useState(true);
  // const [pendingStatus, setPendingStatus] = useState(false);


  const getData = (data) => {
    switch (data) {
      case "Blocked User":
        props.getData(userId, accessToken, clientId, uid);
        break;
      case "Followers":
        props.getFollowUser(userId, accessToken, clientId, uid);
        // code block
        break;
      case "Followings":
        props.getFollowingUser(userId, accessToken, clientId, uid);
        // code block
        break;
      default:
        console.log(" no case exit")
    }
  };

  useEffect(() => {
    props.getPendingRequest(userId, accessToken, clientId, uid);
    getData(props.location.title);
    console.log("props.location.id", props.location.id);
    console.log("userId", userId);
  }, [props.success]);


  function unblock(e) {
    //console.log("Unblock", e.target.id);
    let formData = new FormData();
    formData.append("block_user_id", e.target.id);
    props.unblockContent(userId, formData, accessToken, clientId, uid);
  }

  // function unFollow(e) {
  //   console.log("unFollow", e.target.id);
  //   let formData = new FormData();
  //   formData.append("followed_id", e.target.id);
  //
  //   // props.unblockContent(userId, formData, accessToken, clientId, uid);
  //   SetButtonStatus(!buttonStatus)
  // }
  //
  // function Follow(e) {
  //   console.log("Follow", e.target.id);
  //   let formData = new FormData();
  //   formData.append("id", e.target.id);
  //   // props.unblockContent(userId, formData, accessToken, clientId, uid);
  //   SetButtonStatus(!buttonStatus)
  // }

  function followAndUnFollow(e, type) {
  //  console.log("followAndUnFollow", e.target.id, type);
    let formData = new FormData();
    formData.append("id", e.target.id);
    formData.append("follow_user", type);
    props.setFollowAndUnFollowStatus(formData, accessToken, clientId, uid);
    setTimeout(() => {
      props.getFollowingUser(userId, accessToken, clientId, uid);
    }, 1);

    if (type) {
      toast.success(" Follow Successfully");
    } else {
      toast.success("UnFollow Successfully");
    }
  }

  function removeFollower(e) {
  //  console.log("removeFollower......", userId, e.target.id);
    let formData = new FormData();
    formData.append("follewer_id", e.target.id);
    formData.append("followed_id", userId);
    props.removeFollowerStatus(userId, formData, accessToken, clientId, uid);
    setTimeout(() => {
      props.getFollowUser(userId, accessToken, clientId, uid);
    }, 1);
  }

  function setRequestStatus(e, accept) {
    let formData = new FormData();
    formData.append("request_id", e.target.id);
    formData.append("accept", accept);
    props.getFollowingStatus(userId, formData, accessToken, clientId, uid);
    setTimeout(() => {
      props.getPendingRequest(userId, accessToken, clientId, uid);
    }, 1);
  }

  return (<>
    <div className="container-fluid p-0 mh-100 bg-light">
      <Header nav={Menu} {...props} />
      <div className="container mt-3">
        <div className="row">
          <div className="col-sm-12">
            {
              props.location.title === "Blocked User" &&
              <React.Fragment>
                <h4 className="pt-4 text1"><FormattedMessage {...messages.settingsBlockedUserHeading} /></h4>
                {
                  props.data && props.data.length > 0 && props.data.map(user => {
                    return <div className="col-md-12 p-2" key={user.id}>
                      <div className="bg-white p-3">
                        <img className="rounded-circle" width="50"
                             src={user.profile.profile_image ? user.profile.profile_image : require("../../images/user.png")}
                             alt="Avatar"/>
                        <span className="ml-3">{`${user.profile.name}  (${user.user_name})`}</span>
                        <button type="button" className="btn bg-ubwab text-white float-right" id={user.id}
                                onClick={unblock}><FormattedMessage {...messages.settingsBlockedUserUnblockButtonTitlr} />
                        </button>
                      </div>
                    </div>
                  })
                }
              </React.Fragment>
            }
            {
              props.location.title === "Followers" &&
              <React.Fragment>
                <h4 className="pt-4 text1"><FormattedMessage {...messages.profileTextpendingRequest}/></h4>
                {
                  props.pendingData && props.pendingData.length > 0 ? props.pendingData.map(user => {
                    return <div className="col-md-12 p-2" key={user.id}>
                      <div className="bg-white p-3">
                        <img className="rounded-circle" width="50"
                             src={user.profile.profile_image ? user.profile.profile_image : require("../../images/user.png")}
                             alt="Avatar"/>
                        <span className="ml-3">{`${user.profile.name}  (${user.user_name})`}</span>
                        <button type="button" className="btn bg-ubwab text-white ml-3 float-right" id={user.request_id}
                                onClick={(e) => setRequestStatus(e, true)}>{buttonStatus ? "Approve" : "Approved"}
                        </button>
                        <button type="button" className="btn bg-ubwab text-white float-right" id={user.request_id}
                                onClick={(e) => setRequestStatus(e, false)}>{buttonStatus ? "Reject" : "Rejected"}
                        </button>
                      </div>
                    </div>
                  }) : <div className="d-block mt-5 lead text-center">
                    <FormattedMessage {...messages.profileTextNothingYet}/></div>
                }
                <h4 className="pt-4 text1"><FormattedMessage {...messages.profilePageText}/></h4>
                {
                  props.data && props.data.length > 0 && props.data.map(user => {
                    return <div className="col-md-12 p-2" key={user.id}>
                      <div className="bg-white p-3">
                        <img className="rounded-circle" width="50"
                             src={user.profile.profile_image ? user.profile.profile_image : require("../../images/user.png")}
                             alt="Avatar"/>
                        <span className="ml-3">{`${user.profile.name}  (${user.user_name})`}</span>
                        {userId == props.location.id &&
                        <button type="button" className="btn bg-ubwab text-white float-right" id={user.id}
                                onClick={removeFollower}>❌
                        </button>
                        }
                      </div>
                    </div>
                  })
                }
              </React.Fragment>
            }
            {
              props.location.title === "Followings" &&
              <React.Fragment>
                <h4 className="pt-4 text1"><FormattedMessage {...messages.profileTextFollowings}/></h4>
                {
                  props.data && props.data.length > 0 && props.data.map(user => {
                    return <div className="col-md-12 p-2" key={user.id}>
                      <div className="bg-white p-3">
                        <img className="rounded-circle" width="50"
                             src={user.profile.profile_image ? user.profile.profile_image : require("../../images/user.png")}
                             alt="Avatar"/>
                        <span className="ml-3">{`${user.profile.name}  (${user.user_name})`}</span>
                        {
                          userId == props.location.id && 
                          <button type="button" className="btn bg-ubwab text-white float-right" id={user.id}
                          onClick={(e) => followAndUnFollow(e, false)}>
                            ❌
                          </button>
                        }
                      </div>
                    </div>
                  })
                }
              </React.Fragment>
            }
            {props.data && props.data.length === 0 && <div className="d-block mt-5 lead text-center">
              <FormattedMessage {...messages.profileTextNothingYet}/></div>}
          </div>
        </div>
      </div>
    </div>
    {/* {props.loading && <Spinner/>} */}
  </>);
}

BlockedContent.propTypes = {
  loading: PropTypes.bool,
  data: PropTypes.array,
  getData: PropTypes.func,
  getFollowUser: PropTypes.func,
  getFollowingUser: PropTypes.func,
  setFollowAndUnFollowStatus: PropTypes.func,
  getFollowingStatus: PropTypes.func,
  unblockContent: PropTypes.func,
  success: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  data: makeSelectBlockedContent(),
  pendingData: makeSelectPendingRequestSuccess(),
  loading: makeSelectLoading(),
  success: makeSelectSuccess()
});

export function mapDispatchToProps(dispatch) {
  return {
    getData: (userId, accessToken, clientId, uid) => dispatch(getData(userId, accessToken, clientId, uid)),
    getFollowUser: (userId, accessToken, clientId, uid) => dispatch(getFollowUser(userId, accessToken, clientId, uid)),
    getFollowingUser: (userId, accessToken, clientId, uid) => dispatch(getFollowingUser(userId, accessToken, clientId, uid)),
    getPendingRequest: (userId, accessToken, clientId, uid) => dispatch(getPendingRequest(userId, accessToken, clientId, uid)),
    setFollowAndUnFollowStatus: (formData, accessToken, clientId, uid) => dispatch(setFollowAndUnFollowStatus(formData, accessToken, clientId, uid)),
    getFollowingStatus: (userId, formData, accessToken, clientId, uid) => dispatch(getFollowingStatus(userId, formData, accessToken, clientId, uid)),
    removeFollowerStatus: (userId, formData, accessToken, clientId, uid) => dispatch(removeFollowerStatus(userId, formData, accessToken, clientId, uid)),
    unblockContent: (userId, formData, accessToken, clientId, uid) => dispatch(unblockContent(userId, formData, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(BlockedContent);
