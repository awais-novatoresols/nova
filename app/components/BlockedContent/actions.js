import {
  FOLLOW_UNFOLLOW_USER,
  GET_DATA,
  GET_DATA_SUCCESS,
  GET_FOLLOW_USER,
  GET_FOLLOWING_USER,
  GET_PENDING_REQUEST,
  GET_PENDING_SUCCESSFULLY,
  SET_STATUS_REQUEST,
  UNBLOCK,
  UNBLOCK_SUCCESS,
  REMOVE_FOLLOWERS
} from "./constants";

export function getData(userId, accessToken, clientId, uid) {
  return {
    type: GET_DATA,
    userId,
    accessToken,
    clientId,
    uid
  };
}

export function getDataSuccess(data) {
  return {
    type: GET_DATA_SUCCESS,
    data
  }
}

export function getFollowUser(data) {
  return {
    type: GET_FOLLOW_USER,
    data
  }
}

export function getFollowingUser(data) {
  return {
    type: GET_FOLLOWING_USER,
    data
  }
}

export function unblockContent(userId, formData, accessToken, clientId, uid) {
  return {
    type: UNBLOCK,
    userId,
    formData,
    accessToken,
    clientId,
    uid
  };
}

export function unblockSuccess() {
  return {
    type: UNBLOCK_SUCCESS
  }
}


export function getPendingRequest(userId, accessToken, clientId, uid) {
  return {
    type: GET_PENDING_REQUEST,
    userId,
    accessToken,
    clientId,
    uid
  };
}

export function getFollowingStatus(userId, formData, accessToken, clientId, uid) {
  return {
    type: SET_STATUS_REQUEST,
    userId,
    formData,
    accessToken,
    clientId,
    uid
  };
}


export function pendingRequestSuccess(data) {
  return {
    type: GET_PENDING_SUCCESSFULLY,
    data
  }
}


export function setFollowAndUnFollowStatus(formData, accessToken, clientId, uid) {
  return {
    type: FOLLOW_UNFOLLOW_USER,
    formData,
    accessToken,
    clientId,
    uid
  };
}


export function removeFollowerStatus(userId, formData, accessToken, clientId, uid) {
  return {
    type: REMOVE_FOLLOWERS,
    userId,
    formData,
    accessToken,
    clientId,
    uid
  };
}

export function removeFollowingStatus(formData, accessToken, clientId, uid) {
  return {
    type: FOLLOW_UNFOLLOW_USER,
    formData,
    accessToken,
    clientId,
    uid
  };
}
