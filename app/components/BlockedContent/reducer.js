import produce from "immer";
import {GET_DATA, GET_DATA_SUCCESS, GET_PENDING_SUCCESSFULLY, UNBLOCK, UNBLOCK_SUCCESS} from "./constants";
// The initial state of the App
export const initialState = {
  loading: false,
  blockedContent: {},
  success: false,
  pendingRequest: [],

};
/* eslint-disable default-case, no-param-reassign */
const blockedContentReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_DATA:
        draft.loading = true;
        break;
      case GET_DATA_SUCCESS:
        draft.loading = false;
        draft.blockedContent = action.data;
        break;
      case UNBLOCK:
        draft.loading = true;
        break;
      case UNBLOCK_SUCCESS:
        draft.loading = false;
        draft.success = true;
        break;
      case GET_PENDING_SUCCESSFULLY:
   //     console.log("action from pending request ", action);
        draft.loading = false;
        draft.pendingRequest = action.data.users;
        break;
      default:
        draft.loading = false;
        draft.success = false;
    }
  });
export default blockedContentReducer;


