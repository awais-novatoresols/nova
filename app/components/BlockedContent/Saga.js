import {put, takeLatest} from "redux-saga/effects";
import {apiFetch, apiFetchPost, apiFetchDelete} from "../../utils/network";
import {GET_DATA, GET_FOLLOW_USER, GET_FOLLOWING_USER, GET_PENDING_REQUEST,REMOVE_FOLLOWERS, UNBLOCK, SET_STATUS_REQUEST,FOLLOW_UNFOLLOW_USER} from "./constants";
import {getDataSuccess, pendingRequestSuccess, unblockSuccess} from "./actions";

export function* getBlockedData(user) {
  // console.log("createVote from json ...", userId);
  try {
    const json = yield apiFetch(`users/${user.userId}/blocked_users`, user.accessToken, user.clientId, user.uid);
    yield put(getDataSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export function* getFollowersData(user) {
  try {
    const json = yield apiFetch(`users/${user.data}/user_followers`, user.accessToken, user.clientId, user.uid);
    yield put(getDataSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export function* getFollowingData(user) {
  try {
    const json = yield apiFetch(`users/${user.data}/followed_users`, user.accessToken, user.clientId, user.uid);
    yield put(getDataSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export function* unblockContent(user) {
  try {
    const json = yield apiFetchPost(`users/${user.userId}/unblock_user`, user.formData, user.accessToken, user.clientId, user.uid);
    yield put(unblockSuccess(json));
  } catch (error) {
    console.log(error);
  }
}

export function* getPendingRequestData(user) {

  try {
    const json = yield apiFetch(`users/${user.userId}/pending_requests`, user.accessToken, user.clientId, user.uid);
    // console.log("getPendingRequestData from json ...", json);
    yield put(pendingRequestSuccess(json));
  } catch (error) {
    console.log(error);
  }
}


export function* setPendingRequestData(user) {

  try {
    const json = yield apiFetchPost(`users/${user.userId}/accept_or_reject_request`,user.formData, user.accessToken, user.clientId, user.uid);
    // yield put(pendingRequestSuccess(json));
  } catch (error) {
    console.log(error);
  }
}


export function* setFollowAndUnFollowUser(user) {

  try {
    const json = yield apiFetchPost(`users/follow_unfollow_user`,user.formData, user.accessToken, user.clientId, user.uid);
    // console.log("setFollowAndUnFollowUser from json ...", json);
  } catch (error) {
    console.log(error);
  }
}

export function* removeFollowUser(user) {

  try {
    const json = yield apiFetchDelete(`users/${user.userId}/remove_follower`,user.formData, user.accessToken, user.clientId, user.uid);
  } catch (error) {
    console.log(error);
  }
}

export default function* blockedContentSaga() {
  yield takeLatest(GET_DATA, getBlockedData);
  yield takeLatest(GET_FOLLOW_USER, getFollowersData);
  yield takeLatest(GET_FOLLOWING_USER, getFollowingData);
  yield takeLatest(UNBLOCK, unblockContent);
  yield takeLatest(GET_PENDING_REQUEST, getPendingRequestData);
  yield takeLatest(SET_STATUS_REQUEST, setPendingRequestData);
  yield takeLatest(FOLLOW_UNFOLLOW_USER, setFollowAndUnFollowUser);
  yield takeLatest(REMOVE_FOLLOWERS, removeFollowUser);

}
