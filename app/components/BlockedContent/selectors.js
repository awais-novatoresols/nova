import {createSelector} from "reselect";
import {initialState} from "./reducer";

const selectGlobal = state => state.blockedContent || initialState;
const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );
const makeSelectBlockedContent = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.blockedContent.users
  );
const makeSelectSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectPendingRequestSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.pendingRequest
  );


export {
  selectGlobal,
  makeSelectLoading,
  makeSelectBlockedContent,
  makeSelectSuccess,
  makeSelectPendingRequestSuccess
};
