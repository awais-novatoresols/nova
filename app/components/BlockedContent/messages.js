import { defineMessages } from "react-intl";

export default defineMessages({
  profileTextpendingRequest: {
    id: "profile-text-pending-request",
    defaultMessage: "Pending Request"
  },
  profileTextNothingYet: {
    id: "nothing-yet",
    defaultMessage: "Nothing Yet!"
  },
  profileTextFollowers: {
    id: "profile-text-followers",
    defaultMessage: "Followers"
  },
  profilePageText: {
    id: "profile-page-text",
    defaultMessage: "Followers"
  },
  profileTextFollowings: {
    id: "profile-text-followings",
    defaultMessage: "Followings"
  },
  settingsBlockedUserHeading: {
    id: "settings-container-link-blocked-user-headeing",
    defaultMessage: "Blocked User's"
  },
  settingsBlockedUserNothingYet: {
    id: "nothing-yet",
    defaultMessage: "Nothing Yet!"
  },
  settingsBlockedUserUnblockButtonTitlr: {
    id: "settings-blocked-user-unblock-button-title",
    defaultMessage: "Unblock"
  },
  headerlinkNewsFeeds: {
    id: "header-link-news-feed",
    defaultMessage: "Home"
  },
  headerlinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Categories"
  },
  headerlinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  },
  headerlinkGeneral: {
    id: "header-link-general",
    defaultMessage: "General"
  }
});
