import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import messages from "./messages";
import { FormattedMessage } from "react-intl";

const UserBar = props => {
  let userId = localStorage.getItem("userId");
  const [isAdmin, SetIsAdmin] = useState([]);
  let localLanguage = localStorage.getItem('language');

  useEffect(() => {
    let localStorageLanguage = localStorage.getItem("language");
    if (localStorageLanguage === "ar") {
      let totalElements = document.getElementsByClassName('lang');
      for(let i = 0; i < totalElements.length; i++){
        totalElements[i].classList.add("directRtl");
        totalElements[i].classList.add("right-align");
      }
    }
  }, []);

  function updateRole(role) {
    props.updateRole(props.id, role);
  }

  function deleteCommunityUser() {
    props.deleteUser(props.id);
  }

  function blackListUser() {
    //  console.log("blackListUser this user");
    props.blockThisUser(props.id);
  }

  function unBLockUser() {
    props.unBLockUser(props.id);
  }

  useEffect(() => {
    let isAdmin =
      props.admins !== undefined &&
      props.admins.length > 0 &&
      props.admins.filter(item => {
        // console.log("item.....", item.id);
        return item.id == userId;
      });
    SetIsAdmin(isAdmin);
  }, []);

  return (
    <div className="lang col-md-4 p-2">
      <div className=" bg-white p-3">
        <Link to={props.link}>
          <img
            className="rounded-circle"
            width="50"
            src={props.image ? props.image : require("../images/user.png")}
            alt="Avatar"
          />
        </Link>{'\u00A0'}
        <span className="ml-3">{props.name}</span>
        {!props.admin && !props.isBLock && isAdmin.length > 0 && (
          <div className={localLanguage === 'ar' ? "dropdown float-left" : "dropdown float-right"}>
            <button className="btn alphab1" id="menu1" data-toggle="dropdown">
              <img className="alphab2" src={require("../images/list.png")} />
            </button>
            <div className={localLanguage === 'ar' ? 'dropdown-menu text-right' : "dropdown-menu"}>
              <Link to={props.link}>
                <h6 className="font-weight-light pl-2 small border-bottom-class"><FormattedMessage {...messages.userBarViewProfile} /></h6>
              </Link>
              <h6
                onClick={() => updateRole("moderator")}
                className="font-weight-light pl-2 small cursor_pointer_class border-bottom-class"
              >
                <FormattedMessage {...messages.userBarMakeModerator} />
              </h6>
              <h6
                onClick={deleteCommunityUser}
                className="font-weight-light pl-2 small cursor_pointer_class border-bottom-class"
              >
                <FormattedMessage {...messages.userBarRemoveFromCommunity} />
              </h6>
              <h6
                onClick={blackListUser}
                className="font-weight-light pl-2 small cursor_pointer_class"
              >
                <FormattedMessage {...messages.userBarBlockFromCommunity} />
              </h6>
            </div>
          </div>
        )}
        {props.role === "moderator" && isAdmin.length > 0 && (
          <div className="dropdown float-right">
            <button className="btn alphab1" id="menu1" data-toggle="dropdown">
              <img className="alphab2" src={require("../images/list.png")} />
            </button>
            <div className="dropdown-menu">
              <h6
                onClick={() => updateRole("user")}
                className="font-weight-light pl-2 cursor_pointer_class border-bottom-class"
              >
                <FormattedMessage {...messages.userBarRemoveModerator} />
              </h6>
            </div>
          </div>
        )}
        {props.isBLock && (
          <div className="dropdown float-right">
            <button className="btn alphab1" id="menu1" data-toggle="dropdown">
              <img className="alphab2" src={require("../images/list.png")} />
            </button>
            <div className="dropdown-menu">
              <h6
                onClick={unBLockUser}
                className="font-weight-light pl-2 cursor_pointer_class border-bottom-class"
              >
                <FormattedMessage {...messages.userBarUnblockUser} />
              </h6>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

UserBar.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  link: PropTypes.string,
  blockThisUser: PropTypes.func
};

export default UserBar;
