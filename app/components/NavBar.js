import PropTypes from "prop-types";
import React, {useEffect} from "react";
import "../containers/HomePage/style.css";
import "../containers/Profile/style.css";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

let NavbarItems = [
  {id: 1, name: <FormattedMessage {...messages.profileNavBarprofile}/>, image:require("../images/myprofile1.png")},
  {id: 2, name: <FormattedMessage {...messages.profileNavBarSaved}/>, image:require("../images/savedicon.png")},
  {id: 3, name: <FormattedMessage {...messages.profileNavBarHistory}/>, image:require("../images/history1.png")},
  // {id: 4, name: <FormattedMessage {...messages.profileNavBarInbox}/>, image:require("../images/inboxicon.png")},    
  {id: 5, name: <FormattedMessage {...messages.profileNavBarJoinedList}/>, image:require("../images/joinedHistory.png")},  
  {id: 6, name: <FormattedMessage {...messages.profileNavBarModeration}/>, image:require("../images/moderation.png")},
  {id: 7, name: <FormattedMessage {...messages.profileNavBarSetting}/>, image:require("../images/settings1.png")}    
    
        
];

export default function NavBar(props) {
  let localLanguage = localStorage.getItem('language');
  useEffect(() => {
    props.navBarOption(1);
  }, []);

  useEffect(() => {
    // console.log("Use Effect .........", props.moderation);
    NavbarItems = NavbarItems.filter(item => item.name !== "Inbox");
  }, [props.moderation]);

  return (
    <React.Fragment>
      <div id="sidebar-toggle"
           className="col-lg-2 col-md-4 col-sm-4 col-8 d-none d-sm-none d-md-none d-lg-block sidenav_p p-0">
        <nav id="sidebar">
          <div className="sidebar_mid">
            <ul className="list-unstyled components mt-5">
              {NavbarItems.map(item => {
                return <li className="align-right" key={item.id}
                           onClick={() => props.navBarOption(item.id)}
                           className={props.activeView && props.activeView == item.name.props.defaultMessage ? 'mt-2 active' : 'mt-2'}>
                  <a>
                    <div className={localLanguage === 'ar' ? "directRtl row mr-4" : "row ml-5"}><img className="mr-3 mt-3" height="18px" src={item.image}/>{'\u00A0'}{'\u00A0'}
                      <h5 className="cursor_p remove_a1 pt-1">{item.name}</h5>
                    </div>
                  </a>
                </li>;
              })}
              <li className={localLanguage === 'ar' ? "directRtl float-right" : ""}>
                <button onClick={props.logoutUser} type="submit" className={localLanguage === 'ar' ? "mr-5 cursor_p btn btn-link mt-3" : "ml-5 cursor_p btn btn-link mt-3"}><i
                  className="fa fa-sign-out ml-auto sign_out1"/> <FormattedMessage {...messages.profileNavBarLogout}/>
                </button>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </React.Fragment>
  );
}
NavBar.propTypes = {
  logoutUser: PropTypes.func,
  navBarOption: PropTypes.func,
  activeView: PropTypes.string,
  moderation: PropTypes.array
};
