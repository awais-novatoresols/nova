import { defineMessages } from "react-intl";

export default defineMessages({
  joinedListTextSubcriber: {
    id: "nav-bar-joined-list-community-item-text-subscriber",
    defaultMessage: "Subscriber"
  },
  joinedListVisitPageButtonTitle: {
    id: "nav-bar-joined-list-community-row-button-visit-page",
    defaultMessage: "Visit Page"
  },
  joinedListDiscription: {
    id: "nav-bar-joined-list-community-row-drop-down-discription",
    defaultMessage: "Description"
  },
  joinedListPending: {
    id: "nav-bar-joined-list-community-pending",
    defaultMessage: "Pending"
  },
  headerlinkNewsFeeds: {
    id: "header-link-news-feed",
    defaultMessage: "News Feed"
  },
  headerlinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Communities"
  },
  headerlinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  }
});
