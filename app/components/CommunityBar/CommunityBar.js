import React from "react";
import PropTypes from "prop-types";
import "./communities.css";
import {Link} from "react-router-dom";
import FollowButton from "../FollowButton";
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const CommunityBar = props => {
  let localLanguage = localStorage.getItem('language');
  return (
    <>
      <div className={`${localLanguage === 'ar' ? 'directRtl' : ''} row mt-3 alpharow`}>
        <div className="col-sm-4 p-3 col-4">
          <a className="d-flex alphaa" href={`#details_${props.community.id}`} data-toggle="collapse">
            <img src={props.community.profile_image ? props.community.profile_image : require("../../images/ic_profile.png")}
                 className="rounded-circle" width="28px" height="28px"/>{'\u00A0'}{'\u00A0'}
            <h4 className="text">{props.community.name}</h4>
          </a>
        </div>
        <div className={props.isModeration ? "col-sm-4 col-6" : "col-sm-2 col-4"}>
          <h4 className="pt-3 text2">{`${props.community.followers} `}
          <FormattedMessage {...messages.joinedListTextSubcriber} /></h4>
        </div>
        <div className="col-sm-2 col-4">
          <h4 className="pt-3 text2">{props.community.chatroom_name}</h4>
        </div>
        {props.isModeration ? '' : <div className="col-sm-2 col-6">
          {props.community.role !== "admin" && <FollowButton community={props.community} onFollow={props.onFollow}/>}
        </div>}
        <div className="col-sm-2">
          {props.community.status === "pending" ? <p className="custom_ps text-center mt-3">
            <FormattedMessage {...messages.joinedListPending} /></p> :
            <Link to={`/communities/view/${props.community.id}`}>
              <button type="button" className="btnsubmit1 w-100 p-1 mt-3 mb-2">
                <FormattedMessage {...messages.joinedListVisitPageButtonTitle} /></button>
            </Link>}
        </div>
      </div>
      <div className="row coll_s">
        <div className="col-sm-12">
          <div id={`details_${props.community.id}`} className="collapse p-4">
            <div className="row">
              <div className="col-sm-8 p-0 col-4">
                <h3 className="c_h">
                  <FormattedMessage {...messages.joinedListDiscription} /></h3>
              </div>
              <div className="col-sm-2 col-4"/>
            </div>
            <p>{props.community.description}</p>
          </div>
        </div>
      </div>
    </>
  );
};

CommunityBar.propTypes = {
  community: PropTypes.object,
  onFollow: PropTypes.func,
  isModeration: PropTypes.bool
}
export default CommunityBar;
