import { defineMessages } from 'react-intl';

export default defineMessages({
    profileMessageModleFormHeadingTO: {
        id: 'profile-inbox-item-new-message-form-heading-to',
        defaultMessage: 'To:',
    },
    profileMessageModleFormInputPlaceHolder: {
        id: 'profile-inbox-item-new-message-form-input-placeholder',
        defaultMessage: 'Type text here',
    },
    profileMessageModelSendButtonTitle: {
        id: 'profile-inbox-item-new-message-form-button-title',
        defaultMessage: 'Send Message',
    },
    headerlinkNewsFeeds: {
        id: 'header-link-news-feed',
        defaultMessage: 'News Feed',
      },
      headerlinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Communities',
      },
      headerlinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
      },

    

});