import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { getUserChatRooms, getUserMessage } from "../../containers/Chat/actions";
import reducer from "../../containers/Chat/reducer";
import saga from "../../containers/Chat/saga";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { signUpApi } from "../../utils/network";
import messages from "./messages";
import "./style.css";
const key = "chat";
function Message(props) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [messageText, setMessageText] = useState("");
  let userName = localStorage.getItem("userName");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");

  function createMessage() {
    if (messageText !== "") {
      handleSubmitData();
      handleAfterMessage();
    }
  }
  function handleMessage(e) {
    if (e.key === "Enter") {
      handleSubmitData();
      handleAfterMessage();
    }
    setMessageText(e.target.value);
  }
  function handleAfterMessage() {
    props.getUserMessage(props.id, accessToken, clientId, uid);
    props.getUserChatRooms("", accessToken, clientId, uid);
  }
  function handleSubmitData() {
    let formData = new FormData();
    formData.append("message[recipient_id]", props.id);
    formData.append("message[sender_name]", userName);
    formData.append("message[content]", messageText);
    formData.append("message[message_type]", "message");
    formData.append("message[url]", "");
    formData.append("message[community_chat]", false);
    let json = signUpApi(`messages`, formData, accessToken, clientId, uid);
    setTimeout(() => {
      props.handleClose();
    }, [1000]);
  }
  return (
    <Modal
      show={props.show}
      onHide={props.handleClose}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <p className="modal-heading-text">
            <FormattedMessage {...messages.profileMessageModleFormHeadingTO} />
            &nbsp;&nbsp; {props.name}
          </p>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="card custom_c m-0">
          <form>
            <label className="w-100">
              <FormattedMessage
                {...messages.profileMessageModleFormInputPlaceHolder}
              >
                {placeholder => (
                  <textarea
                    value={messageText}
                    onChange={e => handleMessage(e)}
                    onKeyDown={e => handleMessage(e)}
                    rows="6"
                    className="form-control t_a"
                    placeholder={placeholder}
                  />
                )}
              </FormattedMessage>
            </label>
          </form>
        </div>
        <button
          onClick={createMessage}
          type="submit"
          className="btnsubmit p-2 float-md-right mt-2"
        >
          <FormattedMessage {...messages.profileMessageModelSendButtonTitle} />
        </button>
      </Modal.Body>
    </Modal>
  );
}
export function mapDispatchToProps(dispatch) {
  return {
    getUserChatRooms: (roomType, accessToken, clientId, uid) =>
      dispatch(getUserChatRooms(roomType, accessToken, clientId, uid)),
    getUserMessage: (recipient_id, accessToken, clientId, uid) =>
      dispatch(getUserMessage(recipient_id, accessToken, clientId, uid))
  };
}

export default connect(
  null,
  mapDispatchToProps
)(Message);
