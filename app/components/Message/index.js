import React from 'react';
import Message from './Message';

function MessageButtonContainer(props) {
  return (
    <>
    <Message data={props.data} name={props.name} id={props.id} createMessage={props.createMessage} show={props.show} handleClose={props.handleClose}
             handleShow={props.handleShow}/>

             </>
  )
}

export default MessageButtonContainer;
