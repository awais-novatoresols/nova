import React from "react";
import "../../assets/js/categorySlider";
import "./slider.css";
//import CommunityCard from "../CommunityCard";

const CommunitiesSlider = () => {
  return (
    <div className="col-sm-12 p-0 pt-4">
      <div id="carouselExample" className="carousel slide" data-ride="carousel" data-interval="9000">
        <div className="carousel-inner row" role="listbox">
          <div className="carousel-item col-sm-3 active">
            <img src="assets/football2.jpg"  height="90px" />
            <div className="carousel-caption">
              <h4 className="custom_slider">Dev</h4>
            </div>
          </div>
          <div className="carousel-item col-sm-3">
          <img src={require("../../images/food.jpg")}  height="90px" />
              <div className="carousel-caption">
                <h4 className="custom_slider">Food</h4>
              </div>
          </div>
          <div className="carousel-item col-sm-3">
          <img src={require("../../images/sports.jpg")}  height="90px" />
              <div className="carousel-caption">
                <h4 className="custom_slider">Sports</h4>
              </div>
          </div>
          <div className="carousel-item col-sm-3">
          <img src={require("../../images/health.jpg")} height="90px" />
              <div className="carousel-caption">
                <h4 className="custom_slider">Health</h4>
              </div>
          </div>
          <div className="carousel-item col-sm-3">
          <img src={require("../../images/sports.jpg")}  height="90px" />
              <div className="carousel-caption">
                <h4 className="custom_slider">Anonymus</h4>
              </div>
          </div>
        </div>
        <a className="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="sr-only">Next</span>
        </a>
      </div>
    </div>
  );
}

export default CommunitiesSlider;
