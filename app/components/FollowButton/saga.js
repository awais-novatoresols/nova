import { put, takeLatest } from "redux-saga/effects";
import { followSuccess } from "./actions";
import { FOLLOW } from "./constants";
import { apiFetchPost } from "../../utils/network";

function* follow(user) {
  try {
    yield apiFetchPost(user.url, user.formData, user.accessToken, user.clientId, user.uid);
    yield put(followSuccess());
  } catch (error) {
    console.log(error);
  }
}

export default function* followBtn() {
  yield takeLatest(FOLLOW, follow);
}
