import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { follow, deleteSuccess, followLoading } from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import { makeSelectFollow, makeSelectLoading } from "./selectors";
import Spinner from "../Spinner";
import Message from "../Message";
import messages from "./messages";
import { apiFetchDelete } from "../../utils/network";
import { FormattedMessage } from "react-intl";

const key = "followBtn";
const FollowButtonContainer = props => {
  let userLocalId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  let { userId } = useParams();
  const [show, setShow] = useState(false);
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    //props.getCommunity(userId, props.match.params.id, clientId, accessToken, uid);
  }, [props.followSuccess]);

  function handleFollow(e) {
    // console.log("handleFollow", e.target.id);
    if (props.community) {
      if (e.target.id === "Follow")
        props.follow(
          `users/${userLocalId}/communities/${props.community.id}/subscribe`,
          "",
          accessToken,
          clientId,
          uid
        );
      if (e.target.id === "Following")
        props.follow(
          `users/${userLocalId}/communities/${props.community.id}/unsubscribe`,
          "",
          accessToken,
          clientId,
          uid
        );
    }
    if (props.profile) {
      let formData = new FormData();
      if (e.target.id === "Follow") {
        formData.append("id", userId);
        props.follow(
          `users/${userId}/follow`,
          formData,
          accessToken,
          clientId,
          uid
        );
      }
      if (e.target.id === "Following" && props.profile.follow === 2) {
        props.followLoading();
        formData.append("user_id", userId);
        formData.append("follower_id", userLocalId);
        let result = apiFetchDelete(
          `users/${userId}/cancel_pending_request`,
          formData,
          accessToken,
          clientId,
          uid
        );
        setTimeout(() => {
          props.deleteSuccess();
        }, [500]);
      }
      if (e.target.id === "Following" && props.profile.follow === 1) {
        formData.append("followed_id", userId);
        props.follow(
          `users/${userId}/unfollow`,
          formData,
          accessToken,
          clientId,
          uid
        );
      }
    }
  }

  useEffect(() => {
    if (props.followSuccess) props.onFollow();
  }, [props.followSuccess]);
  return (
    <>
      {props.community && (
        <>
          {props.showButton && (
            <button
              type="button"
              className="btnsubmit1 pt-1 pb-1 pl-3 pr-3 mt-3"
              onClick={() => setShow(!show)}
            >
              <FormattedMessage {...messages.profileMessegeButtonTitle} />
            </button>
          )}
          {props.community.role !== "admin" && (
            <a onClick={handleFollow}>
              <button
                type="button"
                className={
                  props.showButton
                    ? "btnsubmit1 w-25 ml-2 p-1 mt-3"
                    : "btnsubmit1 w-100 p-1 mt-3"
                }
                id={props.community.follow ? "Following" : "Follow"}
                onClick={handleFollow}
              >
                {props.community.follow
                  ? props.community.role !== "admin" && (
                      <span onClick={handleFollow}>
                        <FormattedMessage
                          {...messages.communitiesCommunityRowButtonleave}
                        />
                      </span>
                    )
                  : props.community.role !== "admin" && (
                      <span onClick={handleFollow}>
                        <FormattedMessage
                          {...messages.communitiesCommunityRowButtonJoin}
                        />
                      </span>
                    )}
              </button>
            </a>
          )}
          <Message
            name={props.community && props.community.chatroom_name}
            id={props.community && props.community.id}
            data={props.data}
            createMessage={props.createMessage}
            show={show}
            handleClose={handleClose}
            handleShow={handleShow}
          />
        </>
      )}
      {props.profile && (
        <>
          {props.buttonId != userLocalId && (
            <button
              type="button"
              className="btnsubmit1 pt-1 pb-1 pl-3 pr-3 mt-3"
              onClick={() => setShow(!show)}
            >
              <FormattedMessage {...messages.profileMessegeButtonTitle} />
            </button>
          )}
          {"\u00A0"}
          {props.buttonId != userLocalId && (
            <a onClick={handleFollow}>
              <button
                type="button"
                className="btnsubmit1 pt-1 pb-1 pl-3 pr-3 mt-3"
                id={props.profile.follow ? "Following" : "Follow"}
                onClick={handleFollow}
              >
                {props.profile.follow === 1 ? (
                  <FormattedMessage {...messages.profileFollowingButtonTitle} />
                ) : props.profile.follow === 2 ? (
                  <FormattedMessage {...messages.profilePendingButtonTitle} />
                ) : (
                  <FormattedMessage {...messages.profileFollowButtonTitle} />
                )}
              </button>
            </a>
          )}
          <Message
            name={props.data && props.data.user_name}
            id={props.data && props.data.id}
            data={props.data}
            createMessage={props.createMessage}
            show={show}
            handleClose={handleClose}
            handleShow={handleShow}
          />
        </>
      )}
      {/* {props.loading && <Spinner />} */}
    </>
  );
};

FollowButtonContainer.propTypes = {
  community: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  followSuccess: PropTypes.bool,
  onFollow: PropTypes.func,
  createMessage: PropTypes.func,
  follow: PropTypes.func,
  profile: PropTypes.object,
  loading: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  followSuccess: makeSelectFollow(),
  loading: makeSelectLoading()
});

export function mapDispatchToProps(dispatch) {
  return {
    follow: (url, formData, accessToken, clientId, uid) =>
      dispatch(follow(url, formData, accessToken, clientId, uid)),
    followLoading: () => dispatch(followLoading()),
    deleteSuccess: () => dispatch(deleteSuccess())
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(FollowButtonContainer);
