import { defineMessages } from "react-intl";

export default defineMessages({
  profileMessegeButtonTitle: {
    id: "follow-button-profile-message-button-title",
    defaultMessage: "Message"
  },
  profileFollowButtonTitle: {
    id: "follow-button-profile-follow-button-title",
    defaultMessage: "Follow"
  },
  profileFollowingButtonTitle: {
    id: "follow-button-profile-following-button-title",
    defaultMessage: "Following"
  },
  profilePendingButtonTitle: {
    id: "follow-button-profile-pending-button-title",
    defaultMessage: "Pending"
  },
  communitiesCommunityRowButtonleave: {
    id: "communities-community-row-button-leave",
    defaultMessage: "Leave"
  },
  communitiesCommunityRowButtonJoin: {
    id: "communities-community-row-button-join",
    defaultMessage: "Join"
  },
  headerlinkNewsFeeds: {
    id: "header-link-news-feed",
    defaultMessage: "News Feed"
  },
  headerlinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Communities"
  },
  headerlinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  }
});
