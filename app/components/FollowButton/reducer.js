import produce from "immer";
import { FOLLOW_SUCCESS, FOLLOW, FOLLOW_LOADING } from "./constants";

// The initial state of the App
export const initialState = {
  follow: false,
  loading: false
};

/* eslint-disable default-case, no-param-reassign */
const followButtonReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
    case FOLLOW:
      draft.loading = true;
      break;
    case FOLLOW_SUCCESS:
      draft.follow = true;
      draft.loading = false;
      break;
    case FOLLOW_LOADING:
      draft.loading = true;
      break;
    default:
      draft.loading = false;
      draft.follow = false;
    }
  });

export default followButtonReducer;