import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.followBtn || initialState;

const makeSelectFollow = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.follow
  );

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

export { selectGlobal, makeSelectFollow, makeSelectLoading };

