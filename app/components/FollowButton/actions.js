import { FOLLOW, FOLLOW_SUCCESS, FOLLOW_LOADING } from "./constants";

export function follow(url, formData, accessToken, clientId, uid) {
  return {
    type: FOLLOW,
    url,
    formData,
    accessToken,
    clientId,
    uid
  };
}

export function followSuccess() {
  return {
    type: FOLLOW_SUCCESS
  }
}

export function deleteSuccess() {
  return {
    type: FOLLOW_SUCCESS
  }
}
export function followLoading() {
  return {
    type: FOLLOW_LOADING
  }
}