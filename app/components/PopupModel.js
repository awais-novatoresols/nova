import React from "react";
import {Modal} from "react-bootstrap";
import PropTypes from "prop-types";

const PopupModel = props => {
  return <Modal
    size={props.size ? props.size : 'md'}
    aria-labelledby="contained-modal-title-vcenter"
    centered
    show={props.display} onHide={props.hideModel}>
    <Modal.Body>
      {props.showCloseBtn && <div className="w3-center">
        <span onClick={props.hideModel}
              className="w3-button w3-xlarge w3-hover-red w3-display-topright"
              title="Close Modal">&times;</span>
      </div>}
      {props.children}
    </Modal.Body>
  </Modal>
}

PopupModel.propTypes = {
  display: PropTypes.bool,
  showCloseBtn: PropTypes.bool,
  hideModel: PropTypes.func,
  size: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.string])
}

export default PopupModel;
