import React, { useState } from "react";
import PropTypes from "prop-types";
import "../containers/HomePage/style.css";
import { FormattedMessage } from "react-intl";
import messages from "./messages";

const SortBy = props => {
  const [filterType, setFilterType] = useState("new");
  let localLanguage = localStorage.getItem("language");
  function sortByFilter(e) {
    //  console.log("id ", e.target.id);
    props.filterData(e);
    setFilterType(e.target.id);
  }

  return (
    <>
      <div className="lang row mt-4">
        <div className="col-sm-6">
          <div className="dropdown">
            <button
              type="button"
              className="custom_b1 btn dropdown-toggle"
              data-toggle="dropdown"
            >
              <FormattedMessage {...messages.sortByFilterByMainHeading} />
            </button>
            <div className="dropdown-menu w-75">
              <p className="lang custom_p m-2 pb-1 pl-2">
                <FormattedMessage {...messages.sortByFilterByMainHeading} />
              </p>
              <a
                id="new"
                onClick={sortByFilter}
                className="dropdown-item mt-2 pl-3 cursor_pointer_class"
              >
                <div className="row">
                  <div className={localLanguage === 'ar' ? "mr-n2 col-8 text-right" : "col-6"}>
                    <img src={require("../images/badge.png")} width="20px" />
                    &nbsp; <FormattedMessage {...messages.sortByFilterByNew} />
                  </div>
                  <div className={localLanguage === 'ar' ? "col-4 text-left" : "col-6 text-right"}>
                    {filterType === "new" && (
                      <img
                        className="selected_Image pt-1"
                        src={require("../images/clicktick.png")}
                      />
                    )}
                  </div>
                </div>
              </a>
              <a
                id="top"
                onClick={sortByFilter}
                className="dropdown-item mt-2 pl-3 cursor_pointer_class"
              >
                <div className="row">
                  <div className={localLanguage === 'ar' ? "mr-n2 col-8 text-right" : "col-6"}>
                    <img src={require("../images/top.png")} width="18px" />
                    &nbsp; <FormattedMessage {...messages.sortByFilterByTop} />
                  </div>
                  <div className={localLanguage === 'ar' ? "col-4 text-left" : "col-6 text-right"}>
                    {filterType === "top" && (
                      <img
                        className="selected_Image pt-1"
                        src={require("../images/clicktick.png")}
                      />
                    )}
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div className="col-sm-8" />
      </div>
    </>
  );
};
SortBy.propTypes = {
  sortByFilter: PropTypes.func,
  filterData: PropTypes.func
};
export default SortBy;
