import produce from "immer";
import {
  LIKE_SUCCESS,
  LOAD_ERROR,
  PIN_POST_SUCCESS,
  UNPIN_POST_SUCCESS,
  VOTE_FOR_POLL,
  VOTE_SUCCESSFULLY
} from "./constants";
// The initial state of the App
export const initialState = {
  loading: false,
  error: false,
  userPost: {
    post: false
  },
  likeSuccess: false,
  pinSuccess: false,
  unpinSuccess: false
};
/* eslint-disable default-case, no-param-reassign */
const createVoteReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case VOTE_FOR_POLL:
        state.userPost.post = true;
        draft.loading = true;
        draft.error = false;
        draft.likeSuccess = false;
        break;
      case VOTE_SUCCESSFULLY:
        state.userPost.post = action.post;
        draft.loading = false;
        draft.error = false;
        draft.likeSuccess = false;
        break;
      case LOAD_ERROR:
        draft.error = action.error;
        draft.loading = false;
        draft.likeSuccess = false;
        break;
      case LIKE_SUCCESS:
        draft.likeSuccess = true;
      case PIN_POST_SUCCESS:
        draft.pinSuccess = action.data;
        break;
      case UNPIN_POST_SUCCESS:
        draft.unpinSuccess = true;
        break;
      default:
        draft.uploadStatus = false;
        draft.likeSuccess = false;
    }
    // console.log(' state.userPost.post.......', JSON.stringify( state.userPost.post));
  });
export default createVoteReducer;


