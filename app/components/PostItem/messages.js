import { defineMessages } from 'react-intl';

export default defineMessages({
    postItemDropDownSave: {
        id: 'post-item-drop-down-save',
        defaultMessage: 'Save',
    },
    postItemDropDownHide: {
        id: 'post-item-drop-down-hide',
        defaultMessage: 'Hide',
    },
    postItemDropDownReport: {
        id: 'post-item-drop-down-report',
        defaultMessage: 'Report',
    },
    postItemDropDownBlock: {
        id: 'post-item-drop-down-block',
        defaultMessage: 'Block',
    },
    postItemPostTimeAgo:{
        id:"post-item-time-ago",
        defaultMessage:"ago"
    },
    postItemPostTimeAgo:{
        id:"post-item-time-hours",
        defaultMessage:"hours"
    },
    postItemComment:{
        id:"post-item-comment",
        defaultMessage:"Comment"
    },
    postItemShare:{
        id:"post-item-share",
        defaultMessage:"Share"
    },
    postItemShareDropDownCopyLink:{
        id:"post-item-share-drop-down-copy-link",
        defaultMessage:"Copy Link"
    },
    commentMainInputPlaceHolder: {
        id: 'comment-main-input-place-holder',
        defaultMessage: 'what are you thought on it type your comment here',
    },
    commentMainButtonTitle: {
        id: 'comment-main-button-title-send',
        defaultMessage: 'Send',
    },
    headerlinkNewsFeeds: {
        id: 'header-link-news-feed',
        defaultMessage: 'News Feed',
      },
      headerlinkCommunities: {
        id: 'header-link-communities',
        defaultMessage: 'Communities',
      },
      headerlinkProfile: {
        id: 'header-link-profile',
        defaultMessage: 'Profile',
      },

});