import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import moment from "moment";
import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { Collapse, ProgressBar } from "react-bootstrap";
import ReactPlayer from "react-player";
import { connect } from "react-redux";
import { Link } from "react-router-dom/cjs/react-router-dom";
import { toast } from "react-toastify";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { getCommentLoaded } from "../../containers/ViewPost/actions";
import { homeUrl } from "../../utils/constants";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import PostUserAction from "../PostUserAction";
import ShowMoreText from "react-show-more-text";
import { makeSelectDeleteSuccess } from "../PostUserAction/selector";
import Spinner from "../Spinner";
import { givePollVote, likePost, pinPost } from "./actions";
import "./post.css";
import reducer from "./reducer";
import saga from "./Saga";
import messages from "./messages";
import { FormattedMessage } from "react-intl";
import {
  makeSelectLikeSuccess,
  makeSelectLoading,
  makeSelectPoll
} from "./selectors";
import { makeSelectCommunityPinedPosts } from "../../containers/ViewCommunity/selectors";
// import { useBottomScrollListener } from "react-bottom-scroll-listener";
const key = "postItem";

export function PostItem(props) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [open, setOpen] = useState(false);
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  const [commentText, setText] = useState("");
  const [posts, setPosts] = useState([]);
  let localLanguage = localStorage.getItem('language');
  useEffect(() => {
    if (props.posts.post.posts) {
      setPosts(props.posts.post.posts);
      // console.log("Post", props.posts.post.posts);
    }
  }, [props.posts.post, posts]);

  useEffect(() => {
    if (props.likeSuccess) {
      props.getLatestPost();
    }
  }, [props.likeSuccess]);

  let userId = localStorage.getItem("userId");

  function getDuration(publishDate) {
    let startDate = new Date();
    let a = moment(startDate);
    let b = moment(publishDate);
    let difference = a.diff(b, "minutes");
    return moment.duration(difference, "minutes").humanize();
  }

  function getVoteId(e, item) {
    let formData = new FormData();
    formData.append("poll_vote_history[post_id]", item.id);
    formData.append("poll_vote_history[poll_option_id]", e);
    if (item.poll_option_index === 0) {
      props.givePollVote(formData, accessToken, clientId, uid);
      toast.success("Voted Successfully");
      props.getLatestPost();
    } else {
      toast.success("you already vote this post");
      props.getLatestPost();
    }
  }

  function getPostId(id) {
    if (
      props.location.pathname !== undefined &&
      !props.location.pathname.includes("/post")
    ) {
      let json = [];
      props.getCommentLoaded(json);
      props.history.push(`/post/${id.target.id}`);
    }
  }

  function postLike(postId, currentStatus, previousStatus, e) {
    e.preventDefault();
    let ourStatus = currentStatus;
    let apiStatus = previousStatus === undefined ? 0 : previousStatus;
    if (previousStatus === currentStatus) ourStatus = 0;
    let formData = new FormData();
    formData.append("current_status", ourStatus);
    formData.append("previous_status", apiStatus);
    props.likePost(userId, postId, formData, accessToken, clientId, uid);
    posts.forEach(post => {
      let newPost = post;
      if (newPost.id === postId) {
        newPost.vote_status = ourStatus;
        if (ourStatus === 1 && apiStatus === 0) {
          newPost.count = newPost.count + 1;
        } else if (ourStatus === 0 && apiStatus === 1) {
          newPost.count = newPost.count - 1;
        } else if (ourStatus === 2 && apiStatus === 0) {
          newPost.count = newPost.count - 1;
        } else if (ourStatus === 0 && apiStatus === 2) {
          newPost.count = newPost.count + 1;
        } else if (ourStatus === 2 && apiStatus === 1) {
          newPost.count = newPost.count - 2;
        } else if (ourStatus === 1 && apiStatus === 2) {
          newPost.count = newPost.count + 2;
        }
      }
      return newPost;
    });
  }

  function getCommentValue(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      props.createComment("", commentText, posts[0]);
    }
    setText(e.target.value);
  }

  function commentOpen(postId) {
    setOpen(!open);
    if (
      props.location.pathname !== undefined &&
      !props.location.pathname.includes("/post")
    ) {
      let json = [];
      props.getCommentLoaded(json);
      props.history.push(`/post/${postId}`);
    }
  }

  function changePinStatus(community_id, postId, unpin) {
    let formData = new FormData();
    formData.append("community_id", community_id);
    if (unpin) formData.append("unpin", unpin);
    props.pinPost(userId, postId, formData, accessToken, clientId, uid);
    if (unpin) props.getLatestPost();
  }

  function findLinkText(link) {
    let isExist = link.includes("you");
    if (isExist) {
      return true;
    } else {
      return false;
    }
  }

  return (
    <>
      {posts.map((item, index) => {
        return (
          <div className="container" key={index}>
            <div className="row">
              <div className="card mt-3 post_c w-100">
                <div className="card-header d-flex post_ch">
                  <Link to={`/profile/${item.user.id}/${item.user.profile.id}`}>
                    <img
                      src={
                        item.user.profile &&
                        item.user.profile.profile_image !== null
                          ? item.user.profile.profile_image
                          : require("../../images/user.png")
                      }
                      className="rounded-circle"
                    />
                  </Link>
                  <div className="col p-0">
                    <Link
                      to={`/profile/${item.user.id}/${item.user.profile.id}`}
                    >
                      <span className="post_p1 d-block">
                        {item.user && item.user.profile.name}
                      </span>
                    </Link>
                    {item.community && (
                      <Link to={`/communities/view/${item.community.id}`}>
                        <span className="post_p1 d-block ">
                          {item.community && item.community.name}
                        </span>
                      </Link>
                    )}{" "}
                  </div>
                  {!props.hideTime && (
                    <h6 className="ml-auto mr-2 mt-2 ">
                      {getDuration(item.created_at)} ago
                    </h6>
                  )}
                  {props.pinImage && item.pinned && (
                    <img
                      onClick={() =>
                        changePinStatus(item.community.id, item.id, true)
                      }
                      src={require("../../images/redIcon.png")}
                      className="rounded-circle cursor_pointer_class mt-1"
                      style={{ width: "30px", height: "30px" }}
                    />
                  )}
                  {!props.hidePostOptions && (
                    <PostUserAction
                      getLatestPost={props.getLatestPost}
                      item={item}
                      isSavedTab={props.isSavedTab ? true : false}
                      community={props.community}
                      changePinStatus={changePinStatus}
                    />
                  )}
                </div>
                <div className="pl-4 mt-5 cursor_pointer_class">
                  <h5 className={checkEngOrArb(item.title) ? 'directRtl text-right pr-4' : ''} value={item.id} id={item.id} onClick={getPostId}>
                    {item.title}
                  </h5>
                  {item.post_type === "text" && (
                    <>
                      {item.link.length < 300 ? (
                        <p className={checkEngOrArb(item.title) ? 'directRtl text-right pr-4' : ''}>{item.link}</p>
                      ) : (
                        <span className={checkEngOrArb(item.title) ? 'directRtl text-right pr-4' : ''}>
                          <ShowMoreText
                            lines={4}
                            more="Show more"
                            less="Show less"
                            anchorClass=""
                            expanded={false}
                          >
                            {item.link}
                          </ShowMoreText>
                        </span>
                      )}
                    </>
                  )}
                </div>
                <div>
                  {item.post_type === "image" && (
                    <img
                      value={item.id}
                      id={item.id}
                      onClick={getPostId}
                      className="custom_i2"
                      style={{ objectFit: "contain" }}
                      src={
                        item.post_type === "image" && item.link
                          ? item.link
                          : require("../../images/placeholder2.jpg")
                      }
                    />
                  )}
                  {item.post_type === "video" && (
                    <ReactPlayer
                      url={item.link}
                      config={{
                        youtube: {
                          playerVars: { showinfo: 0, rel: 0 }
                        },
                        facebook: {
                          appId: "549267839141720"
                        }
                      }}
                      controls
                      width="100%"
                    />
                  )}
                  {item.post_type === "link" && (
                    <>
                      {findLinkText(item.link) === true && (
                        <ReactPlayer
                          url={item.link}
                          config={{
                            youtube: {
                              playerVars: { showinfo: 1 }
                            },
                            facebook: {
                              appId: "549267839141720"
                            }
                          }}
                          controls={true}
                          playing
                          loop={false}
                          muted
                          controls
                          width="100%"
                        />
                      )}
                    </>
                  )}
                </div>
                {item.poll_options.length !== 0 && (
                  <div className="pl-4">
                    <div className="mt-4">
                      <div className="row mt-5">
                        <div className="col-sm-12">
                          <div className="d-flex">
                            <img
                              src={require("../../images/poll.png")}
                              width="30px"
                            />
                          </div>
                        </div>
                      </div>
                      {item.poll_options.map((data, index) => {
                        return (
                          data.title !== "" && (
                            <div key={index} className="row mt-3">
                              <div className="col-sm-5 col-md-5 col-6">
                                <div className="custom-control custom-checkbox">
                                  {item.poll_option_index === data.id ? (
                                    <div className="row">
                                      <img
                                        className="cursor_pointer_class"
                                        src={require("../../images/success.png")}
                                        height="20px"
                                      />
                                      <label className="pl-3">
                                        {data.title}
                                      </label>
                                    </div>
                                  ) : (
                                    <div
                                      className="row"
                                      id={data.id}
                                      onClick={e => getVoteId(data.id, item)}
                                    >
                                      <img
                                        className="cursor_pointer_class"
                                        src={require("../../images/circleoutline.png")}
                                        height="20px"
                                      />
                                      <label
                                        className="pl-3"
                                      >
                                        {data.title}
                                      </label>
                                    </div>
                                  )}
                                </div>
                              </div>
                              <div className="col-sm-6 col-md-6 col-5 mt-1">
                                <ProgressBar
                                  variant="success"
                                  now={data.count}
                                />
                              </div>
                              <div className="col-sm-1 col-md-1 col-1" />
                            </div>
                          )
                        );
                      })}
                    </div>
                  </div>
                )}
                <div className="card-footer post_f1 d-flex justify-content-around">
                  <div className="d-flex justify-content-around">
                    <a className="remove_a1">
                      <span
                        id="like"
                        onClick={e => postLike(item.id, 1, item.vote_status, e)}
                        className={
                          item.vote_status === 1
                            ? "fa fa-arrow-up text-success"
                            : "fa fa-arrow-up cursor_p"
                        }
                      />
                    </a>
                    &nbsp;&nbsp;{item.count > 0 ? item.count : 0}&nbsp;&nbsp;
                    <a className="remove_a1">
                      <span
                        id="dislike"
                        onClick={e => postLike(item.id, 2, item.vote_status, e)}
                        className={
                          item.vote_status === 2
                            ? "fa fa-arrow-down text-danger"
                            : "fa fa-arrow-down cursor_p"
                        }
                      />
                    </a>
                  </div>
                  <a
                    onClick={() => commentOpen(item.id)}
                    className="cursor_p remove_a1 custom_a2"
                  >
                    <img
                      src={require("../../images/comment.png")}
                      width="20px"
                    />{" "}
                    {" " + item.comment_count}{" "}
                    <FormattedMessage {...messages.postItemComment} />
                  </a>
                  <div className="dropdown">
                    <button
                      className="btn alphab1"
                      id="menu1"
                      data-toggle="dropdown"
                    >
                      <img
                        src={require("../../images/share.png")}
                        width="18px"
                        className="mr-3"
                      />
                      <FormattedMessage {...messages.postItemShare} />
                    </button>
                    <div className="dropdown-menu dropdown-menu-right">
                      <a
                        onClick={() => {
                          navigator.clipboard.writeText(
                            `${homeUrl}post/${item.id}`
                          );
                          toast.success("Link copied to clipboard!");
                        }}
                        className="remove_a1"
                      >
                        <li
                          role="presentation"
                          className="mt-2 cursor_p"
                          id="Post 1"
                        >
                          <img
                            src={require("../../images/copy.png")}
                            className="ml-2 p-1"
                          />
                          <span>
                            <FormattedMessage
                              {...messages.postItemShareDropDownCopyLink}
                            />
                          </span>
                        </li>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-5" />
          </div>
        );
      })}
      <Collapse in={open}>
        <div className="row ">
          <div className="col-sm-12">
            <div className="card t-4 mt-3">
              <div>
                <FormattedMessage {...messages.commentMainInputPlaceHolder}>
                  {placeholder => (
                    <>
                      <input
                        rows="6"
                        maxLength={100}
                        size={100}
                        className="form-control t_a mt-1 mb-5 p-3"
                        type="text"
                        onChange={getCommentValue}
                        onKeyDown={getCommentValue}
                        placeholder={placeholder}
                      />
                    </>
                  )}
                </FormattedMessage>
                <button
                  type="submit"
                  onClick={() => props.createComment("", commentText, posts[0])}
                  className="btnSubmit1 w-25 mt-2"
                >
                  <FormattedMessage {...messages.commentMainButtonTitle} />
                </button>
              </div>
            </div>
          </div>
        </div>
      </Collapse>
    </>
  );
  function checkEngOrArb(input){
    var letters = /^[0-9a-zA-Z]+$/;
    let result = input[0].match(letters);
    return !result;
  }
}

PostItem.propTypes = {
  posts: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  takeAction: PropTypes.func,
  getLatestPost: PropTypes.func,
  givePollVote: PropTypes.func,
  getDuration: PropTypes.func,
  getCommentLoaded: PropTypes.func,
  getPostId: PropTypes.func,
  history: PropTypes.object,
  isSavedTab: PropTypes.bool,
  hideTime: PropTypes.bool,
  pinImage: PropTypes.bool,
  hidePostOptions: PropTypes.bool,
  likePost: PropTypes.func,
  deletePostSuccess: PropTypes.bool,
  createComment: PropTypes.func,
  likeSuccess: PropTypes.bool,
  loading: PropTypes.bool,
  location: PropTypes.object
};
const mapStateToProps = createStructuredSelector({
  data: makeSelectPoll(),
  deletePostSuccess: makeSelectDeleteSuccess(),
  likeSuccess: makeSelectLikeSuccess(),
  loading: makeSelectLoading(),
  pinePost: makeSelectCommunityPinedPosts()
});

export function mapDispatchToProps(dispatch) {
  return {
    givePollVote: (postData, accessToken, clientId, uid) =>
      dispatch(givePollVote(postData, accessToken, clientId, uid)),
    pinPost: (userId, postId, postData, accessToken, clientId, uid) =>
      dispatch(pinPost(userId, postId, postData, accessToken, clientId, uid)),
    getCommentLoaded: json => dispatch(getCommentLoaded(json)),
    likePost: (userId, postId, formData, accessToken, clientId, uid) =>
      dispatch(likePost(userId, postId, formData, accessToken, clientId, uid))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(PostItem);
