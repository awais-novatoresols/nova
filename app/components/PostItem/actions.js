import {LIKE_POST, LIKE_SUCCESS, PIN_POST, PIN_POST_SUCCESS, VOTE_FOR_POLL, VOTE_SUCCESSFULLY,UNPIN_POST_SUCCESS} from "./constants";

export function givePollVote(postData, accessToken, clientId, uid) {
  return {
    type: VOTE_FOR_POLL,
    postData,
    accessToken,
    clientId,
    uid
  };
}

export function pollVoted(data) {
  return {
    type: VOTE_SUCCESSFULLY,
    data
  };
}

export function pinedSuccessfully(data) {
  return {
    type: PIN_POST_SUCCESS,
    data
  };
}

export function unPinedSuccessfully(data) {
  return {
    type: UNPIN_POST_SUCCESS,
    data
  };
}

export function likeSuccess() {
  return {
    type: LIKE_SUCCESS
  }
}

export function likePost(userId, postId, postData, accessToken, clientId, uid) {
  // for (let pair of postData.entries()) {
  //   console.log(userId, postId, accessToken, clientId, uid, pair[0] + ", " + pair[1]);
  // }
  return {
    type: LIKE_POST,
    userId,
    postId,
    postData,
    accessToken,
    clientId,
    uid
  };
}


export function pinPost(userId, postId, postData, accessToken, clientId, uid) {
  // for (let pair of postData.entries()) {
  //   console.log(userId, postId, accessToken, clientId, uid, pair[0] + ", " + pair[1]);
  // }
  return {
    type: PIN_POST,
    userId,
    postId,
    postData,
    accessToken,
    clientId,
    uid
  };
}
