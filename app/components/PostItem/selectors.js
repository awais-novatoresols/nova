import {createSelector} from "reselect";
import {initialState} from "./reducer";

const selectGlobal = state => state.postItem || initialState;
const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );
const makeSelectPoll = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userData
  );
const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );
const makeSelectLikeSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.likeSuccess
  );

const makeSelectPinedPostSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.pinSuccess
  );

const makeSelectUnPinedPostSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.unpinSuccess
  );


export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectPoll,
  makeSelectLikeSuccess,
  makeSelectPinedPostSuccess,
  makeSelectUnPinedPostSuccess
};
