import {put, takeLatest} from "redux-saga/effects";
import {apiFetchPost, signUpApi} from "../../utils/network";
import {LIKE_POST, PIN_POST, VOTE_FOR_POLL} from "./constants";
import {likeSuccess, pinedSuccessfully, pollVoted, unPinedSuccessfully} from "./actions";

export function* createVote(userId) {
  // console.log("createVote from json ...", userId);
  try {
    const json = yield signUpApi(`poll_voting_histories`, userId.postData, userId.accessToken, userId.clientId, userId.uid);
    // console.log("data from json ...", json);
    yield put(pollVoted(json));
  } catch (error) {
    console.log(error);
  }
}

export function* voteForPost(data) {
//  console.log("voteForPost..........", data);
  try {
    const json = yield signUpApi(`users/${data.userId}/posts/${data.postId}/vote_for_post`, data.postData, data.accessToken, data.clientId, data.uid);
    // console.log("json..........", json);
    // yield put(createCommentSuccessfully(json));
    yield put(likeSuccess());
  } catch (error) {
    console.log(error);
  }
}

export function* pinPost(data) {
  // console.log("pinPost..........", data);
  try {
    const json = yield apiFetchPost(`users/${data.userId}/posts/${data.postId}/pinned_post`, data.postData, data.accessToken, data.clientId, data.uid);
    console.group("pinPost from saga..........", json);
    yield put(pinedSuccessfully(json));
    // yield put(unPinedSuccessfully(json));
  } catch (error) {
    console.log(error);
  }
}

export default function* userVoteData() {
  yield takeLatest(VOTE_FOR_POLL, createVote);
  yield takeLatest(LIKE_POST, voteForPost);
  yield takeLatest(PIN_POST, pinPost);
}
