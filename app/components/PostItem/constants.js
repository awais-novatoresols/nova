export const VOTE_FOR_POLL = "ubwab/postitem/VOTE_FOR_POLL";
export const VOTE_SUCCESSFULLY = "ubwab/postitem/VOTE_SUCCESSFULLY";
export const LIKE_POST = "ubwab/postitem/LIKE_POST";
export const LOAD_ERROR = "ubwab/postitem/LOAD_ERROR";
export const LIKE_SUCCESS = "ubwab/postitem/LIKE_SUCCESS";
export const PIN_POST = "ubwab/postitem/PIN_POST";
export const PIN_POST_SUCCESS = "ubwab/postitem/PIN_POST_SUCCESS";
export const UNPIN_POST_SUCCESS = "ubwab/postitem/UNPIN_POST_SUCCESS";
