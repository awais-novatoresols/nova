import PropTypes from "prop-types";
import React from "react";
import FollowButton from "./FollowButton";

const Cover = props => {
  //console.log("props.showButton", props.showButton);
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-12">
          <div className="card mt-4 card_img">
            {props.editMode && (
              <div className="img_hov">
                <img
                  className="card_img"
                  src={
                    props.cover
                      ? props.cover
                      : require("../images/placeholder2.jpg")
                  }
                  width="100%"
                  height="350px"
                />
                <div className="centered">
                  <div className="image-upload">
                    <label htmlFor="file-input">
                      <img
                        className="cursor_p"
                        src={require("../images/plus.png")}
                        width="50px"
                        height="50px"
                      />
                    </label>
                    <input
                      id="file-input"
                      type="file"
                      onChange={props.onCoverImageChange}
                    />
                  </div>
                </div>
              </div>
            )}
            {!props.editMode && (
              <img
                className="card_img"
                src={
                  props.cover
                    ? props.cover
                    : require("../images/placeholder2.jpg")
                }
                width="100%"
                height="300px"
              />
            )}
            <div className="row">
              <div className="col-sm-8 col-4 bottom-left">
                {props.editMode && (
                  <div className="img_over">
                    <img
                      className="rounded-circle thumbnail_1"
                      src={
                        props.profilePic
                          ? props.profilePic
                          : require("../images/user.png")
                      }
                      width="100px"
                      height="100px"
                    />
                    <div className="bottom-left2">
                      <div className="image-upload">
                        <label htmlFor="profile-input">
                          <span className="cursor_p fa fa-plus fa-3x text-secondary" />
                        </label>
                        <input
                          id="profile-input"
                          type="file"
                          onChange={props.onProfileImageChange}
                        />
                      </div>
                    </div>
                  </div>
                )}
                {!props.editMode && (
                  <img
                    className="rounded-circle thumbnail_1"
                    src={
                      props.profilePic
                        ? props.profilePic
                        : require("../images/user.png")
                    }
                    width="100px"
                    height="100px"
                  />
                )}
              </div>
              {props.buttons && (
                <div className="col-sm-4 col-8 bottom-right_1 text-right">
                  <FollowButton
                    createMessage={props.createMessage}
                    showButton={props.showButton}
                    data={props.data}
                    buttonId={props.buttonId && props.buttonId}
                    community={props.community}
                    profile={props.profile}
                    onFollow={props.onFollow}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Cover.propTypes = {
  cover: PropTypes.string,
  profilePic: PropTypes.string,
  buttons: PropTypes.bool,
  showButton: PropTypes.bool,
  editMode: PropTypes.bool,
  createMessage: PropTypes.func,
  onCoverImageChange: PropTypes.func,
  onProfileImageChange: PropTypes.func,
  community: PropTypes.object,
  onFollow: PropTypes.func,
  profile: PropTypes.object
  // buttonId: PropTypes.Number
};
export default Cover;
