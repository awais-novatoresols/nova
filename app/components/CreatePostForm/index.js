import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.min.css";
import moment from "moment";
import PropTypes from "prop-types";
import React, { memo, useEffect, useState } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import "w3-css/4/w3pro.css";
import { useInjectReducer } from "../../utils/injectReducer";
import { useInjectSaga } from "../../utils/injectSaga";
import { createUserPost, uploadContent, postCreatedRead } from "./actions";
import CreatePostForm from "./CreatePostForm";
import reducer from "./reducer";
import saga from "./Saga";
import {
  makeSelectLoading,
  makeSelectPostCreated,
  makeSelectUploadItemURL,
  makeSelectUploadStatus,
  makeSelectUserData,
  makeSelectUserPost
} from "./selectors";

const key = "createPost";

export function CreatePost(props) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const history = useHistory();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [community, setCommunity] = useState("");
  const [type, setType] = useState("");
  const [pollOptions, setPollOptions] = useState([]);
  const [url, setURL] = useState("");
  const [isUploading, setIsUploading] = useState(false);
  const [showDescription, setShowDescription] = useState(false);
  let userId = localStorage.getItem("userId");
  let clientId = localStorage.getItem("clientId");
  let accessToken = localStorage.getItem("accessToken");
  let uid = localStorage.getItem("uid");
  // console.log("CreatePostForm CreatePost...", props.communities);
  const endTime = moment()
    .add(30, "d")
    .toISOString();
  useEffect(() => {
    if (props.uploadStatus){
      setURL(props.uploadItemURL);
      setIsUploading(false);
    }
  }, [props.uploadStatus, props.uploadItemURL]);

  useEffect(() => {
    if (props.postCreated){
      props.fetchPost();
      history.push(`/post/${props.createdPost.post.post.id}`)
    }
  }, [props.postCreated]);

  function onTypeClick(e) {
    setType(e.target.name);
    e.target.name === "link" || e.target.name === "text"
      ? setShowDescription(true)
      : setShowDescription(false);
  }

  function onImageFileSelect(e) {
    setURL();
    setIsUploading(true);
    if (e.target.files.length > 0) {
      let file = e.target.files[0];
      let data = new FormData();
      data.append("upload_content[avatar]", file);
      props.uploadContent(data, accessToken, clientId, uid);
    }
  }

  function onPollInput(e, id) {
    let newPollOption = pollOptions;
    newPollOption[id - 1] = { title: e.target.value, end_time: endTime };
    setPollOptions(newPollOption);
  }

  function createPost() {
    let communityId = props.isCommunity
      ? props.isCommunity.id
        ? props.isCommunity.id
        : false
      : community;
    let body = communityId
      ? {
          title: title,
          community_id: communityId,
          post_type: type,
          location: "Qatar",
          post_content_attributes: {
            link: url ? url : description
          },
          poll_options_attributes: pollOptions
        }
      : {
          title: title,
          post_type: type,
          location: "Qatar",
          post_content_attributes: {
            link: url ? url : description
          },
          poll_options_attributes: pollOptions
        };
    if (
      title &&
      type &&
      community !== "" &&
      (pollOptions.length === 0 || pollOptions.length >= 2)
    ) {
      // if (props.isCommunity && !community) return toast.error(`Please fill all field`);

      props.createUserPost(userId, body, clientId, accessToken, uid);
      props.hideModel();
      setTitle("");
      setDescription("");
      setCommunity("");
      setType("");
      setPollOptions([]);
      setURL("");
      setShowDescription(false);
    } else if (title === "") {
      toast.error("Title could not be empty");
    } else if (community === "") {
      toast.error("Please Select Community");
    } else if (description === "") {
      toast.error("Description could not be empty");
    }
  }

  // console.log("props.fetchPost......", props.fetchPost);
  // console.log(title, community, ".....");
  const onInputChange = (name, e) => {
    if (name === "title") setTitle(e.target.value);
    if (name === "description") {
      setDescription(e.target.value);
      setURL("");
    }
    if (name === "community") setCommunity(e.target.value);
  };
  return (
    <>
      <CreatePostForm
        onChange={onInputChange}
        data={props.communities}
        isCommunity={props.isCommunity}
        createPost={createPost}
        onTypeClick={onTypeClick}
        onImageFileSelect={onImageFileSelect}
        url={url}
        isUploading={isUploading}
        onPollInput={onPollInput}
        type={type}
        showModel={props.showModel}
        hideModel={props.hideModel}
        showDescription={showDescription}
        setCommunity={setCommunity}
      />
    </>
  );
}

CreatePost.propTypes = {
  isCommunity: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  communities: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  history: PropTypes.object,
  createUserPost: PropTypes.func,
  uploadStatus: PropTypes.bool,
  uploadContent: PropTypes.func,
  uploadItemURL: PropTypes.string,
  showModel: PropTypes.bool,
  displayModel: PropTypes.func,
  hideModel: PropTypes.func,
  fetchPost: PropTypes.func,
  postCreated: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  data: makeSelectUserData(),
  loading: makeSelectLoading(),
  uploadStatus: makeSelectUploadStatus(),
  uploadItemURL: makeSelectUploadItemURL(),
  postCreated: makeSelectPostCreated(),
  createdPost: makeSelectUserPost()
});

export function mapDispatchToProps(dispatch) {
  return {
    createUserPost: (userId, postData, clientId, accessToken, uid) =>
      dispatch(createUserPost(userId, postData, clientId, accessToken, uid)),
    uploadContent: (data, accessToken, clientId, uid) =>
      dispatch(uploadContent(data, accessToken, clientId, uid)),
    postCreatedRead: (post) => dispatch(uploadContent(post))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
export default compose(
  withConnect,
  memo
)(CreatePost);
