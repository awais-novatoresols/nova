export const CREATE_USER_POSTS = "ubwab/createPost/GET_USER_POSTS";
export const POST_CREATED_SUCCESS = "ubwab/createPost/POST_LOAD_SUCCESS";
export const LOAD_ERROR = "ubwab/createPost/LOAD_ERROR";
export const UPLOAD_CONTENT = "ubwab/createPost/UPLOAD_CONTENT";
export const UPLOAD_CONTENT_SUCCESS = "ubwab/createPost/UPLOAD_CONTENT_SUCCESS";
export const POST_CREATED_READ = "ubwab/POST_CREATED_READ";