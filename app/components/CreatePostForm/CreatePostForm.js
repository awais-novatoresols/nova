import React, { useState, useEffect } from "react";
import "./CreatePost.css";
import "font-awesome/css/font-awesome.min.css";
import "w3-css/4/w3pro.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.min";
import PropTypes from "prop-types";
import { Modal } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import messages from "./messages";
import Spinner from "../Spinner";

export default function CreatePostForm(props) {
  const [numberOfPollOptions, setNumberOfPollOptions] = useState([]);
  let localLanguage = localStorage.getItem('language');
  useEffect(() => {
    if (props.isCommunity) {
      props.setCommunity(props.isCommunity.id);
    }
  }, []);
  function addPoll() {
    let num = numberOfPollOptions.length + 1;
    let newObj = [...numberOfPollOptions, num];
    setNumberOfPollOptions(newObj);
  }

  function deletePoll() {
    let newObj = numberOfPollOptions.slice(1);
    setNumberOfPollOptions(newObj);
  }

  return (
    <>
      <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={props.showModel}
        onHide={props.hideModel}
        dialogClassName={localLanguage === 'ar' ? 'directRtl right-align' : ''}
      >
        <Modal.Body>
          <div className="modal-content">
            <div className="modal-header">
              <h6 className="pt-2 text_b text_b1">
                <FormattedMessage {...messages.postModalHeader} />
              </h6>
              <button
                type="button"
                className="close mt-2 text_b3"
                onClick={props.hideModel}
              >
                &times;
              </button>
            </div>
            <div className="row modal-body">
              <div className="col-md-9">
                <h6 className="mt-1 text_b4">
                  <FormattedMessage {...messages.postModalSelectCommunity} />
                </h6>
              </div>
              <div className="col-md-3 text-right">
                {!props.isCommunity && (
                  <select
                    onChange={value => props.onChange("community", value)}
                    className={`w-auto p-0 ml-auto text_b5 ${ localLanguage === 'ar' ? 'text-align-last-left' : 'text-align-last-right'}`}
                    id="sel1"
                    name="sellist1"
                  >
                    <FormattedMessage {...messages.postModalChoose}>
                      {optionValue => (
                        <>
                          <option key="choose" value="">
                            {optionValue}
                          </option>
                        </>
                      )}
                    </FormattedMessage>
                    {props.data &&
                      props.data.communities &&
                      props.data.communities.map(item => {
                        return (
                          <option key={item.id} value={item.id}>
                            {item.name}
                          </option>
                        );
                      })}
                  </select>
                )}
                {props.isCommunity && (
                  <span className={`text-info ${localLanguage === 'ar'? '' : 'ml-auto'}`}>
                    {props.isCommunity.name}
                  </span>
                )}
              </div>
            </div>
            <div className="col-sm-12">
              <FormattedMessage {...messages.postModalAddTitle}>
                {placeholder => (
                  <>
                    <textarea
                      maxLength={200}
                      onChange={e => props.onChange("title", e)}
                      rows="3"
                      className="form-control text_b6"
                      placeholder={placeholder}
                    />
                  </>
                )}
              </FormattedMessage>
            </div>
            {props.showDescription && (
              <div className="col-sm-12">
                <FormattedMessage {...messages.postModalAddDescription}>
                  {placeholder => (
                    <>
                      <textarea
                        maxLength={5000}
                        onChange={e => props.onChange("description", e)}
                        rows="3"
                        className="form-control text_b6"
                        placeholder={placeholder}
                      />
                    </>
                  )}
                </FormattedMessage>
              </div>
            )}
            <div className="row m-1 mt-3">
              <div className="col-sm-3 col-6">
                <button
                  type="button"
                  name="link"
                  className={
                    "btn btn-default w-100 but_pad pd_1" +
                    (props.type === "link" ? " bg-info text-white" : "")
                  }
                  onClick={props.onTypeClick}
                >
                  <span className="fa fa-link mr-2" />{'\u00A0'}
                  <FormattedMessage {...messages.postModalLink} />
                </button>
              </div>
              <div className="col-sm-3 col-6 cursor_pointer_class">
                <div className="form-group w-100 but_pad pd_1 text-center">
                  <div className="input-group">
                    <span className="input-group-btn w-100">
                      <span
                        className={
                          "btn btn-default w-100 btn-file" +
                          (props.type === "image" ? " bg-info text-white" : "")
                        }
                      >
                        <span className="fa fa-picture-o" />{'\u00A0'}
                        <input
                          name="image"
                          type="file"
                          accept="image/png, image/jpg, image/gif"
                          onClick={props.onTypeClick}
                          onChange={props.onImageFileSelect}
                        />
                        <span className="ml-2">
                          <FormattedMessage {...messages.postModalImage} />
                        </span>
                      </span>
                    </span>
                  </div>
                </div>
              </div>
              <div className="col-sm-3 col-6 cursor_pointer_class">
                <div className="form-group w-100 but_pad pd_1 text-center">
                  <div className="input-group">
                    <span className="input-group-btn w-100">
                      <span
                        className={
                          "btn btn-default w-100 btn-file" +
                          (props.type === "video" ? " bg-info text-white" : "")
                        }
                      >
                        <span className="fa fa-play-circle-o" />{'\u00A0'}
                        <input
                          name="video"
                          type="file"
                          accept="video/*"
                          onClick={props.onTypeClick}
                          onChange={props.onImageFileSelect}
                        />
                        <span className="ml-2">
                          <FormattedMessage {...messages.postModalVideo} />
                        </span>
                      </span>
                    </span>
                  </div>
                </div>
              </div>
              <div className="col-sm-3 col-6">
                <button
                  type="button"
                  name="text"
                  className={
                    "btn btn-default w-100 custmb pd_1" +
                    (props.type === "text" ? " bg-info text-white" : "")
                  }
                  onClick={props.onTypeClick}
                >
                  <span className="fa fa-font mr-2" />{'\u00A0'}
                  <FormattedMessage {...messages.postModalText} />
                </button>
              </div>
            </div>
            <div className="container-fluid">
              {props.type === "image" && props.url && (
                <div className="row">
                  <div className="col-sm-12">
                    <h6 className="ml-1 pt-2 text_b8 ml-2">
                      <FormattedMessage {...messages.postModalImageAttached} />
                    </h6>
                  </div>
                  <div className="col-sm-3 text-left">
                    <img
                      src={props.url}
                      className="r_img"
                      width="140px;"
                      height="70px"
                    />
                  </div>
                </div>
              )}
              {props.type === "video" && props.url && (
                <p>
                  <FormattedMessage {...messages.postModalVideoUploaded} />
                </p>
              )}
              <div className="row mt-2 mb-3">
                <div className="col-sm-3 pt-2">
                  <button className="btn btn-light " onClick={addPoll}>
                    <i className="fa fa-plus-square mr-1" />{'\u00A0'}
                    <FormattedMessage {...messages.postModalAddPoll} />
                  </button>
                </div>
                <div className="col-sm-9 pt-2">
                  {numberOfPollOptions.length > 0 &&
                    numberOfPollOptions.map((poll, index) => {
                      return (
                        <div key={index} className="mb-1 row">
                          <div className="col-sm-8 ">
                            <FormattedMessage {...messages.postModalGonnaVote}>
                              {placeholder => (
                                <>
                                  <input
                                    maxLength={100}
                                    type="text"
                                    className="input"
                                    onChange={e => props.onPollInput(e, poll)}
                                    placeholder={placeholder}
                                    name="email"
                                  />
                                </>
                              )}
                            </FormattedMessage>
                            <div className="line-box">
                              <div className="line" />
                            </div>
                          </div>
                          {numberOfPollOptions.length - 1 === index &&
                            numberOfPollOptions.length > 2 && (
                              <div className="col-sm-4 mt-4">
                                <button className="btn  " onClick={deletePoll}>
                                  <i className="fa fa-minus-square mr-1" />
                                  <FormattedMessage
                                    {...messages.postModalDeletePoll}
                                  />
                                </button>
                              </div>
                            )}
                        </div>
                      );
                    })}
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <button
                    onClick={props.createPost}
                    data-dismiss="modal"
                    type="submit"
                    className={`${localLanguage === 'ar' ? 'float-md-left' : 'float-md-right'} btnsubmit p-2 mb-3 w-25`}
                  >
                    <FormattedMessage {...messages.postModalPost} />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      {props.type === "video" && props.isUploading && <Spinner uploading />}
    </>
  );
}
CreatePostForm.propTypes = {
  onChange: PropTypes.func,
  createPost: PropTypes.func,
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  isCommunity: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  type: PropTypes.string,
  onTypeClick: PropTypes.func,
  showModel: PropTypes.bool,
  hideModel: PropTypes.func,
  onImageFileSelect: PropTypes.func,
  url: PropTypes.string,
  onPollInput: PropTypes.func
};
