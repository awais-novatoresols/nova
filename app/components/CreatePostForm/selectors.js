import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectGlobal = state => state.createPost || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.success
  );

const makeSelectUserData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userData
  );
const makeSelectUserPost = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userPost
  );

const makeSelectUploadStatus = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.uploadStatus
  );

const makeSelectUploadItemURL = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.uploadItemURL
  );

const makeSelectPostCreated = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.postCreated
  );

export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectUserData,
  makeSelectUserPost,
  makeSelectUploadStatus,
  makeSelectUploadItemURL,
  makeSelectPostCreated
};
