import produce from "immer";
import {CREATE_USER_POSTS, LOAD_ERROR, POST_CREATED_SUCCESS, UPLOAD_CONTENT_SUCCESS, POST_CREATED_READ} from "./constants";

// The initial state of the App
export const initialState = {
  loading: true,
  error: false,
  userPost: {
    post: false
  },
  uploadStatus: false,
  uploadItemURL: "",
  postCreated: false
};

/* eslint-disable default-case, no-param-reassign */

const createPostReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CREATE_USER_POSTS:
        draft.userPost.post = true;
        draft.loading = true;
        draft.error = false;
        break;
      case POST_CREATED_SUCCESS:
        draft.userPost.post = action.post;
        draft.loading = false;
        draft.error = false;
        draft.postCreated = true;
        break;
      case LOAD_ERROR:
        draft.error = action.error;
        draft.loading = false;
        break;
      case UPLOAD_CONTENT_SUCCESS:
        draft.uploadStatus = true;
        draft.uploadItemURL = action.url;
        break;
      case POST_CREATED_READ:
        draft.userPost = action.post;
        break;
      default:
        draft.uploadStatus = false;
        draft.postCreated = false;
    }
    // console.log(' state.userPost.post.......', JSON.stringify( state.userPost.post));
  });

export default createPostReducer;


