import {put, takeLatest} from "redux-saga/effects";
import {apiFetchPost, signUpApi} from "../../utils/network";
import {postCreated, uploadContentSuccess} from "./actions";
import {CREATE_USER_POSTS, UPLOAD_CONTENT} from "./constants";

export function* createPost(userId) {
  let jsonData = true;
  try {
    const json = yield apiFetchPost(`users/${userId.userId}/posts`, userId.postData, userId.accessToken, userId.clientId, userId.uid, jsonData);
    // console.log("data from json ...", json);
    yield put(postCreated(json));
  } catch (error) {
    // console.log(error);
  }
}

// async function uploadFile(data){
//   let upload = await apiFetchPost(`upload_contents`, data);
//   return upload;
// }

export function* uploadContent(body) {
 // console.log("Upload Content ...", body);
  try {
    //  console.log("Body", body);
    const json = yield signUpApi(`upload_contents`, body.data, body.accessToken, body.clientId, body.uid);
    //const json = yield uploadFile(data);
    //  console.log("Upload Content ...", json);
      // console.log("URL", json.content.url);
    yield put(uploadContentSuccess(json.content.url));
  } catch (error) {
    // console.log(error);
  }
}

export default function* userPostData() {
  yield takeLatest(CREATE_USER_POSTS, createPost);
  yield takeLatest(UPLOAD_CONTENT, uploadContent);
}
