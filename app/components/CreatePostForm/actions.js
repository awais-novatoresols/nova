import { CREATE_USER_POSTS, POST_CREATED_SUCCESS, UPLOAD_CONTENT, UPLOAD_CONTENT_SUCCESS, POST_CREATED_READ } from "./constants";


export function createUserPost(userId, postData, clientId, accessToken, uid) {
  return {
    type: CREATE_USER_POSTS,
    userId,
    postData,
    clientId,
    accessToken,
    uid
  };
}


export function postCreated(post) {
  return {
    type: POST_CREATED_SUCCESS,
    post
  };
}

export function postCreatedRead(post) {
  return {
    type: POST_CREATED_READ,
    post
  };
}

export function uploadContent(data, accessToken, clientId, uid){
  return {
    type: UPLOAD_CONTENT,
    data,
    clientId,
    accessToken,
    uid
  }
}

export function uploadContentSuccess(url) {
  return {
    type: UPLOAD_CONTENT_SUCCESS,
    url
  };
}