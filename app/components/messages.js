import { defineMessages } from "react-intl";

export default defineMessages({
  footerLinkAbout: {
    id: "footer-link-about",
    defaultMessage: "About"
  },
  footerLinkPrivacyPolicy: {
    id: "footer-link-privacy-policy",
    defaultMessage: "Privacy policy"
  },
  footerLinkFAQs: {
    id: "footer-link-faqs",
    defaultMessage: "FAQs"
  },
  footerLinkContentPolicy: {
    id: "footer-link-content-policy",
    defaultMessage: "Content Policy"
  },
  footerLinkApp: {
    id: "footer-link-app",
    defaultMessage: "App"
  },
  footerUserAgreement: {
    id: "footer-user-agreement",
    defaultMessage: "User Agreement"
  },

  headerlinkNewsFeeds: {
    id: "header-link-news-feed",
    defaultMessage: "News Feed"
  },
  headerlinkCommunities: {
    id: "header-link-communities",
    defaultMessage: "Communities"
  },
  headerlinkProfile: {
    id: "header-link-profile",
    defaultMessage: "Profile"
  },
  headerLanguageDropDownHeading: {
    id: "header-language-dropdown-heading",
    defaultMessage: "Language"
  },
  headerLanguageDropDownEn: {
    id: "header-language-dropdown-en",
    defaultMessage: "EN"
  },
  headerLanguageDropDownAr: {
    id: "header-language-dropdown-ar",
    defaultMessage: "عربى"
  },

  creatPostCardHeading: {
    id: "creat-post-card-text-heading",
    defaultMessage: "ubwab"
  },
  creatPostCardDiscription: {
    id: "creat-post-card-text-discription",
    defaultMessage:
      "Ubwab is a open door to all social life style, you can get connected with your best line communities people."
  },
  creatPostCardButtonTitle: {
    id: "creat-post-card-button-title",
    defaultMessage: "Create new post"
  },
  historyPostDropDownSortBy: {
    id: "profile-history-drop-down-sort-by",
    defaultMessage: "Filter by"
  },
  historyPostDropDownFilter: {
    id: "profile-history-drop-down-filter",
    defaultMessage: "Filter"
  },
  historyPostDropDownUpVote: {
    id: "profile-history-drop-down-upvote",
    defaultMessage: "Upvote"
  },
  historyPostDropDownDownVote: {
    id: "profile-history-drop-down-downvote",
    defaultMessage: "Downvote"
  },
  historyPostDropDownHide: {
    id: "profile-history-drop-down-hide",
    defaultMessage: "Hide"
  },
  sortByFilterByMainHeading: {
    id: "sort-by-filter-by-main-heading",
    defaultMessage: "Filter by"
  },
  sortByFilterByNew: {
    id: "sort-by-filter-by-new",
    defaultMessage: "New"
  },
  sortByFilterByTop: {
    id: "sort-by-filter-by-top",
    defaultMessage: "Top"
  },
  postItemDropDownSave: {
    id: "post-item-drop-down-save",
    defaultMessage: "Save"
  },
  profileNavBarprofile: {
    id: "profile-nav-bar-profile",
    defaultMessage: "My Profile"
  },
  profileNavBarSaved: {
    id: "profile-nav-bar-saved",
    defaultMessage: "Saved"
  },
  profileNavBarHistory: {
    id: "profile-nav-bar-history",
    defaultMessage: "History"
  },
  profileNavBarInbox: {
    id: "profile-nav-bar-inbox",
    defaultMessage: "Inbox"
  },
  profileNavBarJoinedList: {
    id: "profile-nav-bar-joined-list",
    defaultMessage: "Joined List"
  },
  profileNavBarModeration: {
    id: "profile-nav-bar-moderation",
    defaultMessage: "Moderation"
  },
  profileNavBarSetting: {
    id: "profile-nav-bar-setting",
    defaultMessage: "Settings"
  },
  profileNavBarLogout: {
    id: "profile-nav-bar-logout",
    defaultMessage: "Logout"
  },
  profileSavedNoPost: {
    id: "profile-saved-no-post",
    defaultMessage: "No Posts"
  },
  userBarViewProfile: {
    id: "user-bar-view-profile",
    defaultMessage: "View Profile"
  },
  userBarMakeModerator: {
    id: "user-bar-make-moderator",
    defaultMessage: "Make Moderator"
  },
  userBarRemoveFromCommunity: {
    id: "user-bar-remove-from-community",
    defaultMessage: "Remove from Community"
  },
  userBarBlockFromCommunity: {
    id: "user-bar-block-from-community",
    defaultMessage: "Block from community"
  },
  userBarRemoveModerator: {
    id: "user-bar-remove-moderator",
    defaultMessage: "Remove Moderator"
  },
  userBarUnblockUser: {
    id: "user-bar-unblock-user",
    defaultMessage: "UnBlock User"
  }
});
