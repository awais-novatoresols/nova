import React from 'react';
import styled from "styled-components";
import PropTypes from "prop-types";

const MyInput = styled.input`
    width: 100%;
    padding: 5px 20px;
    margin: 0 0 5px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
`


const InputField = props => {
  return <div className="form-group mt-2">
    <label>{props.title}</label>
    <MyInput {...props}></MyInput>
  </div>
}

InputField.propTypes = {
  title: PropTypes.string
}

export default InputField;