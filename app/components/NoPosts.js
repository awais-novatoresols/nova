import React from "react";
import PropTypes from "prop-types";
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const NoPosts = props => {
  return (<div className="d-block bg-white p-5 text-center mt-3 lead">{props.text ? props.text : 
  <FormattedMessage {...messages.profileSavedNoPost}/>}</div>);
}

NoPosts.propTypes = {
  text: PropTypes.string
}

export default NoPosts;